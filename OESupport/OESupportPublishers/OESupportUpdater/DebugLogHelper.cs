﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OESupportUpdater
{
    public class DebugLogHelper
    {
        public static string OutputFileName;

        public DebugLogHelper()
        {
            SetupDebugFile();
            WriteToDebugLog("SETUP DEBUG LOG FOR THE OEUPDATER OK!");
        }

        private void SetupDebugFile()
        {
            string BaseDIR = @"C:\Program Files (x86)\OESupport\OEDebugging\OESupportUpdater";
            string TimeOfUpdates = DateTime.Now.ToString("s").Replace("T", "_").Replace("-", "").Split('_')[0];
            OutputFileName = BaseDIR + Path.DirectorySeparatorChar.ToString() + "OESupportUpdater_" + TimeOfUpdates + ".txt";

            if (!Directory.Exists(BaseDIR)) { Directory.CreateDirectory(BaseDIR); }
            if (File.Exists(OutputFileName))
            {
                File.AppendAllText(OutputFileName, "\n\n--------------------------------------------------------------------------------------------\n");
                File.AppendAllText(OutputFileName, "OEAUTOMATION INVOKER UPDATING IN PROGRESS!\n");
                File.AppendAllText(OutputFileName, "THIS PROCESS BEGAN AT: " + DateTime.Now.ToString("T") + "\n");
            }
        }

        private string NameOfCallingClass()
        {
            string fullName;
            Type declaringType;
            int skipFrames = 2;
            do
            {
                MethodBase method = new StackFrame(skipFrames, false).GetMethod();
                declaringType = method.DeclaringType;
                if (declaringType == null)
                {
                    return method.Name;
                }
                skipFrames++;
                fullName = declaringType.FullName;
            }
            while (declaringType.Module.Name.Equals("mscorlib.dll", StringComparison.OrdinalIgnoreCase));

            var fullNameSplit = fullName.Split('.');
            fullName = fullNameSplit[fullNameSplit.Length - 1];

            return fullName;
        }

        public void WriteToDebugLog(string DebugInfo)
        {
            string timeNow = DateTime.Now.ToString("T");
            string CallingName = NameOfCallingClass();
            string writeThis = "[" + timeNow + "] ::: [" + CallingName + "] ::: " + DebugInfo + "\n";

            File.AppendAllText(OutputFileName, writeThis);
        }
    }
}
