﻿using Amazon.Runtime.Internal.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OESupportUpdater
{
    /*
     *      // Get the installer objects from my S3 bucket.
     *      // Download them and place them all onto the F: drive or C: drive depending on space.
     *
     *      // Install WinAutomation and register it.
     *      // Unzip the Waj files and launcher bat files and throw it into C:\Progfiles x86\OESupport
     *
     *      // Once done terminate.
    */

    public class MainUpdater
    {
        public static DebugLogHelper Logger;
        public static string OEType;

        public static void Main(string[] args)
        {
            // Debug log helper.
            Logger = new DebugLogHelper();
            if (Debugger.IsAttached) { Logger.WriteToDebugLog("DEBUGGING IS ON! NOT SENDING OUT COMMANDS THRU THE API!"); }

            string[] CommandLineArgs = Environment.GetCommandLineArgs();
            foreach (var arg in CommandLineArgs)
            {
                if (arg.Length > 10) { continue; }
                OEType = arg;

                break;
            }

            if (OEType == null || OEType.Length == 0)
            {
                try { OEType = new WebClient().DownloadString("http://localhost:57570/OEAutomationTasker/WinAuto/ReturnOEType/"); }
                catch { OEType = "UNKNOWN"; }
            }

            // Make a timer to keep track of how long this takes.
            Stopwatch UpdateTimer = new Stopwatch();
            UpdateTimer.Start();

            // Resize for easier reading on any size display.
            // Console.SetWindowSize(Console.WindowWidth, 40);
            ConsolePrinters.Maximize();
            ConsolePrinters.ShowTitleWindow();
            ConsolePrinters.PrintSeperator();

            // 12-07-20  ZW changed this to run off a webclient instead of run proc min.
            // Easier on CPU and makes things faster overall.
            // var InvokerClient = new WebClient();

            // 12-11-20  ZW Changed this to run off a method on the main class. Needed debugging info here.
            MakeAPICall("http://localhost:15000/DrewTechDataSvc/PleaseWait/off");
            MakeAPICall("http://localhost:57570/OEAutomationTasker/Timers/StartTimer/7");
            MakeAPICall("http://localhost:57570/OEAutomationTasker/ProgressBar/StartProgressBar//0/7");

            // 12-07-20  Changed to webcall.
            // RunProcMinimized("INVOKER, -b ProgressBar, -c TickProgressBar, -i 0, -t 1");
            MakeAPICall("http://localhost:57570/OEAutomationTasker/ProgressBar/TickProgressBar/0/1");

            // 12-09-20  Why was this here?
            // System.Threading.Thread.Sleep(5000);

            // This works ok.
            Process UserAgent = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = @"C:\Program Files\WinAutomation\WinAutomation.UserAgent.exe",
                    WindowStyle = ProcessWindowStyle.Hidden
                }
            };
            UserAgent.Start();
            Console.WriteLine("STARTED WINAUTOMATION USER AGENT PROCESS");

            string TimeSoFar = UpdateTimer.Elapsed.ToString(@"mm\:ss\.fff");
            Logger.WriteToDebugLog("TIME: " + TimeSoFar + " | STARTED WINAUTOMATION USER AGENT PROCESS.");

            // 12-07-20  Changed to webcall.
            // RunProcMinimized("INVOKER, -b ProgressBar, -c TickProgressBar, -i 0, -t 2");
            MakeAPICall("http://localhost:57570/OEAutomationTasker/ProgressBar/TickProgressBar/0/2");

            ConsolePrinters.PrintSeperator();

            // 12-07-20  Changed to webcall.
            // RunProcMinimized("INVOKER, -b ProgressBar, -c TickProgressBar, -i 0, -t 3");
            MakeAPICall("http://localhost:57570/OEAutomationTasker/ProgressBar/TickProgressBar/0/3");

            // Check for WinAutomation and the Wajs.
            var InstallCheck = new WinAutomationInstallChecker();
            if (!InstallCheck.InstalledAndFilesFound) { InstallCheck.PrintFailures(); }

            // 12-07-20  Changed to webcall.
            // RunProcMinimized("INVOKER, -b ProgressBar, -c TickProgressBar, -i 0, -t 4");
            MakeAPICall("http://localhost:57570/OEAutomationTasker/ProgressBar/TickProgressBar/0/4");

            // Contact the S3 Bucket now.
            var S3Updater = new AWSFileRetreiver();

            // 12-07-20  Changed to webcall.
            // RunProcMinimized("INVOKER, -b ProgressBar, -c TickProgressBar, -i 0, -t 5");
            MakeAPICall("http://localhost:57570/OEAutomationTasker/ProgressBar/TickProgressBar/0/5");

            // Make the new Launcher Files Here and Run if needed.
            var BatMaker = new CreateNewBatFile(S3Updater.OutputWajFiles.Distinct().ToList());

            // 12-07-20  Changed to webcall.
            // RunProcMinimized("INVOKER, -b ProgressBar, -c TickProgressBar, -i 0, -t 6");
            MakeAPICall("http://localhost:57570/OEAutomationTasker/ProgressBar/TickProgressBar/0/6");

            // If we need to run something, run the RUN_THIS bat file.
            if (BatMaker.RUN_THIS != null) { RunProcMinimized(BatMaker.RUN_THIS, true); }

            // Stop the update timer here.
            UpdateTimer.Stop();
            string TimeToUpdate = UpdateTimer.Elapsed.ToString(@"mm\:ss\.fff");
            string TimeFormatted = TimeToUpdate.Replace(":", "_").Replace("-", ".");

            // Print out info for finished updates. 
            Console.WriteLine("UPDATING TOOK A TOTAL OF: " + TimeToUpdate + " (" + TimeFormatted + ") IN TOTAL.");
            Console.WriteLine("STARTING OEAUTOMATION INVOKER NOW!");

            // 12-07-20  Changed to webcall.
            // RunProcMinimized("INVOKER, -b ProgressBar, -c TickProgressBar, -i 0, -t 7");
            // RunProcMinimized("INVOKER, -t Timers, -c StopTimer, -i 0");
            // RunProcMinimized("INVOKER, -b UpdateStatus, -c IncreaseProgressCounter, -i 0, -t 00_00-00");
            // RunProcMinimized("Starting OE Scan Now");

            MakeAPICall("http://localhost:57570/OEAutomationTasker/ProgressBar/TickProgressBar/0/7");
            MakeAPICall("http://localhost:57570/OEAutomationTasker/Timers/StopTimer/0");
            MakeAPICall("http://localhost:57570/OEAutomationTasker/UpdateStatus/IncreaseProgressCounter/0/00_00-00");
            MakeAPICall("http://localhost:15000/DrewTechDataSvc/PleaseWait/Starting OE Scan Now");

            Console.WriteLine("\nWAITING 5 SECONDS THEN CLOSING OUT");

            bool OnlyUpdating = false;
            foreach (var ArgItem in args) { OnlyUpdating = ArgItem.ToUpper() == "UPDATE"; }

            if (BatMaker.RUN_THIS == null || OnlyUpdating) 
            {
                if (OnlyUpdating) { Console.WriteLine("FOUND A REQUEST TO FORCE UPDATE WAJ FILES"); }
                if (!OnlyUpdating) { Console.WriteLine("IMPORTING ALL FILES SINCE NO OE TYPE WAS SPECIFIED"); }

                foreach (var wajFile in S3Updater.OutputWajFiles)
                {
                    S3Updater.ImportToWinAutomation(wajFile);
                    Console.WriteLine("\\__ FILE: " + wajFile);
                }
                Console.WriteLine("IMPORTED ALL FILES OK!\n");

                Console.WriteLine("RESETING ALL TASK ITEMS SINCE THERE IS NO BAT TO RUN");
                Console.WriteLine("MORE THAN LIKLEY THIS MEANS WERE IN A DEBUG ENVIRONMENT");

                MakeAPICall("http://localhost:57570/OEAutomationTasker/ViewInvoker/ResetAllItems/");
                // MakeAPICall("http://localhost:15000/DrewTechDataSvc/PleaseWait/OFF");
            }

            ConsolePrinters.PrintSeperator();
            Thread.Sleep(5000);

        }

        public static void RunProcMinimized(string Args, bool RUN_THIS = false)
        {
            try
            {
                var startInfo = new ProcessStartInfo();
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.FileName = @"C:\Program Files (x86)\OESupport\OEAppHelpers\PleaseWaitHelper.exe";
                startInfo.Arguments = Args;

                if (RUN_THIS)
                {
                    startInfo.FileName = Args;
                    startInfo.Arguments = "";
                    startInfo.WindowStyle = ProcessWindowStyle.Normal;
                } 

                var ProcToRun = new Process();
                ProcToRun.StartInfo = startInfo;

                ProcToRun.Start();
            }
            catch { }
        }

        public static void MakeAPICall(string URL)
        {
            if (Debugger.IsAttached) { return; }

            Logger.WriteToDebugLog("SENDING OUT: " + URL);
            try 
            {
                string Result = new WebClient().DownloadString(URL);
                Logger.WriteToDebugLog(Result);
            }
            catch (Exception e) { Logger.WriteToDebugLog("FAILED TO GET RESPONSE FROM API CALL!!\nERR: " + e.Message); }
        }
    }
}
