﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OESupportUpdater
{
    public class WinAutomationInstallChecker
    {
        public bool InstalledAndFilesFound = false;

        private string WinAutoName = "WinAutomation";
        private string WajDir = @"C:\Program Files (x86)\OESupport\Launchers\WajFiles";
        private string Launchers = @"C:\Program Files (x86)\OESupport\Launchers";

        private bool isInstalled;
        private bool hasWajs;
        private bool hasLaunchers;

        public WinAutomationInstallChecker(bool AutoCheck = true)
        {
            MainUpdater.Logger.WriteToDebugLog("WINAUTO INSTALL CHECKER INIT");

            if (!AutoCheck) 
            {
                MainUpdater.Logger.WriteToDebugLog("AUTO CHECKER WAS SET TO FALSE. RETURNING.");
                return;
            }

            isInstalled = CheckForInstall(WinAutoName);
            hasWajs = CheckForWajs(out int WajCoujnt);
            hasLaunchers = CheckForLaunchers(out int BatCount);

            ConsolePrinters.DividerPrint("CHECKING FOR WINAUTOMATION AND LAUNCHER FILES", ConsoleColor.DarkGreen);
            Console.WriteLine("WinAutomation Installed: " + isInstalled.ToString().ToUpper());
            Console.WriteLine("WAJ Files Exist:         " + hasWajs.ToString().ToUpper() + " - (" + WajCoujnt + " Wajs)");
            Console.WriteLine("Launcher Programs Found: " + hasLaunchers.ToString().ToUpper() + " - (" + BatCount + " BATs)");
            ConsolePrinters.PrintSeperator();

            MainUpdater.Logger.WriteToDebugLog("WinAutomation Installed: " + isInstalled.ToString().ToUpper());
            MainUpdater.Logger.WriteToDebugLog("WAJ Files Exist:         " + hasWajs.ToString().ToUpper() + " - (" + WajCoujnt + " Wajs)");
            MainUpdater.Logger.WriteToDebugLog("Launcher Programs Found: " + hasLaunchers.ToString().ToUpper() + " - (" + BatCount + " BATs)");

            InstalledAndFilesFound = isInstalled && hasLaunchers && hasWajs;
        }

        public void PrintFailures()
        {
            Console.WriteLine();
            Console.BackgroundColor = ConsoleColor.DarkRed;

            if (!isInstalled) { Console.WriteLine("WINAUTOMATION IS NOT INSTALLED!"); }
            if (!hasWajs) { Console.WriteLine("NO WAJ FILES WERE FOUND"); }
            if (!hasLaunchers) { Console.WriteLine("NO LAUNCHER BAT FILES WERE FOUND"); }

            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine();
        }

        public bool CheckForInstall(string c_name)
        {
            string displayName;

            MainUpdater.Logger.WriteToDebugLog("CHECKING FOR UNINSTALL KEY IN 32 BIT SW");
            string registryKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            RegistryKey key = Registry.LocalMachine.OpenSubKey(registryKey);
            if (key != null)
            {
                foreach (RegistryKey subkey in key.GetSubKeyNames().Select(keyName => key.OpenSubKey(keyName)))
                {
                    displayName = subkey.GetValue("DisplayName") as string;
                    if (displayName != null && displayName.Contains(c_name))
                    {
                        MainUpdater.Logger.WriteToDebugLog("WINAUTO WAS FOUND! -> " + displayName);
                        return true;
                    }
                }
                key.Close();
            }

            MainUpdater.Logger.WriteToDebugLog("CHECKING FOR UNINSTALL KEY IN 64 BIT SW");
            registryKey = @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall";
            key = Registry.LocalMachine.OpenSubKey(registryKey);
            if (key != null)
            {
                foreach (RegistryKey subkey in key.GetSubKeyNames().Select(keyName => key.OpenSubKey(keyName)))
                {
                    displayName = subkey.GetValue("DisplayName") as string;
                    if (displayName != null && displayName.Contains(c_name))
                    {
                        MainUpdater.Logger.WriteToDebugLog("WINAUTO WAS FOUND! -> " + displayName);
                        return true;
                    }
                }
                key.Close();
            }
            return false;
        }

        public bool CheckForWajs(out int CountOfWajs)
        {
            CountOfWajs = 0;
            if (!Directory.Exists(WajDir)) 
            {
                MainUpdater.Logger.WriteToDebugLog("COULD NOT FIND THE DIR FOR WAJS");
                return false;
            }

            else 
            {
                CountOfWajs = Directory.GetFiles(WajDir).Length;
                MainUpdater.Logger.WriteToDebugLog("FOUND A TOTAL OF: " + CountOfWajs + " WAJ FILES ON THIS DEVICE");
                return CountOfWajs > 0;
            }
        }

        public bool CheckForLaunchers(out int CountOfLaunchers)
        {
            CountOfLaunchers = 0;
            if (!Directory.Exists(Launchers))
            {
                MainUpdater.Logger.WriteToDebugLog("COULD NOT FIND THE DIR FOR LAUNCHER BAT FILES");
                return false;
            }
            else
            {
                CountOfLaunchers = Directory.GetFiles(Launchers).Length;
                MainUpdater.Logger.WriteToDebugLog("FOUND A TOTAL OF: " + CountOfLaunchers + " LAUNCHER FILES");
                return CountOfLaunchers > 0;
            }
        }
    }
}
