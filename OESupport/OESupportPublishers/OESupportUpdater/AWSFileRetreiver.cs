﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.Win32;

namespace OESupportUpdater
{
    public class AWSFileRetreiver
    {
        public string CONFIG_ENVIRONMENT; 

        public bool DownloadedAllOk = false;
        private bool WajsForAll;
        private bool AllAppsFound;

        public long CurrentUpdaterVersion = 637357628480569363;
        public long NewestUpdaterVersion = 0;

        public List<string> NewestS3WajFiles = new List<string>();
        public List<string> OutputWajFiles = new List<string>();
        public List<string> AllCombinedWajFiles = new List<string>();
        public List<List<string>> MakeBasedWajFiles = new List<List<string>>();

        public List<string> NewestSupportAppFiles = new List<string>();
        public List<string> OutputSupportApps = new List<string>();

        private AmazonS3Client S3Client;

        public string S3BucketName = "drivepro.logs.upload";
        private string S3AccessKey = "AKIASQ7DDKVHCJMHJEXU";
        private string S3SecretKey = "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";

        private List<string> ENV_TPYES = new List<string> { "PRODUCTION", "STAGING", "DEVELOPMENT" };
        private string SupportAppFolder = @"Deploy Directories\OEAutomation\SupportApps";
        private string FoundationFolder = @"Deploy Directories\OEAutomation\WajFiles\SCAN_FOUNDATION";
        private List<string> WajMakeFolders = new List<string>
        {
            @"Deploy Directories\OEAutomation\WajFiles\ENV_TYPE\FJDS_Ford",           
            @"Deploy Directories\OEAutomation\WajFiles\ENV_TYPE\FDRS_Ford",
            @"Deploy Directories\OEAutomation\WajFiles\ENV_TYPE\GDS2_GM",
            @"Deploy Directories\OEAutomation\WajFiles\ENV_TYPE\IHDS_Honda",
            @"Deploy Directories\OEAutomation\WajFiles\ENV_TYPE\TIS_Toyota",
            @"Deploy Directories\OEAutomation\WajFiles\ENV_TYPE\CP3_Nissan",
        };

        public AWSFileRetreiver(bool ForceUpdate = true)
        {
            S3Client = new AmazonS3Client(S3AccessKey, S3SecretKey, RegionEndpoint.EUWest1);

            // Check for WAJ dir.
            if (!Directory.Exists(@"C:\Program Files (x86)\OESupport\Launchers\WajFiles"))
            {
                Directory.CreateDirectory(@"C:\Program Files (x86)\OESupport\Launchers\WajFiles");
            }

            // Make Sure Reg Exists
            RegistrySetupHelper.SetupAllRegKeys();

            // Checks to see what the current targeted release type is. 
            CheckCONFIG_ENVIRONMENT();
            CheckLAST_OPENED();
            ConsolePrinters.DividerPrint("GETTING NEWEST WAJ AND LAUNCHER FILES (CONFIG: " + CONFIG_ENVIRONMENT + ")", ConsoleColor.DarkGreen);

            // App Logging.
            /* var allApps = GetAllSupportApps(out AllAppsFound);
            Console.WriteLine("Found A Total Of Support Apps: - " + allApps + " files");
            MainUpdater.Logger.WriteToDebugLog("Found A Total Of Support Apps: - " + allApps + " files");
            */

            // Waj logging.
            SetWajRegLists();
            Console.WriteLine("Found A Total Of Waj Files:    - " + AllCombinedWajFiles.Count + " files");
            MainUpdater.Logger.WriteToDebugLog("Found A Total Of Waj Files:     - " + AllCombinedWajFiles.Count + " files");

            DownloadedAllOk = WajsForAll && AllAppsFound;

            if (!ForceUpdate) { ConsolePrinters.PrintSeperator(); return; }

            #region Waj Files
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("\nPulling Down the Newest WAJ Files...");

            foreach (var FileItem in Directory.GetFiles(@"C:\Program Files (x86)\OESupport\Launchers\WajFiles\"))
            {
                try { File.Delete(FileItem); }
                catch (Exception e) { Console.Write("ERR: " + e.Message); }
            }

            foreach (var FileItem in Directory.GetFiles(@"C:\Program Files (x86)\OESupport\Launchers\"))
            {
                try { File.Delete(FileItem); }
                catch (Exception e) { Console.WriteLine("ERR: " + e.Message); }
            }

            var allFiles = PullDownNewestWajs();
            Console.WriteLine("Files Located: ");
            Console.BackgroundColor = ConsoleColor.Black;
            foreach (var file in allFiles) { Console.WriteLine("\\__ " + file); }
            #endregion

            // Removed on 10-15-2020 by moving this into the DriveCrashUpdater
            // This function still runs but will NOT overwrite anything.
            #region Support Apps
            /*
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("\nPulling Down the Newest Support Apps...");
            
            var allSupportApps = PullDownNewestApps();
            Console.WriteLine("Apps Downloaded: ");
            Console.BackgroundColor = ConsoleColor.Black;
            foreach (var app in allSupportApps) { Console.WriteLine("\\__ " + app); }
            */
            Console.WriteLine("\nRemoving Import Files...");

            #endregion
            
            ConsolePrinters.PrintSeperator();
        }

        public void CheckCONFIG_ENVIRONMENT()
        {
            string ConfigKey = @"HKEY_CURRENT_USER\SOFTWARE\Drew Technologies, Inc\OESupportApps\";
            string CurrentConfig = (string)Registry.GetValue(ConfigKey, "CONFIG_ENVIRONMENT", "NOT_SET");
            string LastOpened = (string)Registry.GetValue(ConfigKey, "LAST_OPENED", "NOT_SET");

            MainUpdater.Logger.WriteToDebugLog("FOUND THE VALUE FOR THE CONFIG_ENVIRONMENT REG TO BE: " + CurrentConfig);
            if (CurrentConfig == "NOT_SET" || CurrentConfig == null)
            {
                MainUpdater.Logger.WriteToDebugLog("REGISTRY FOR THE ENVIRONMENT TO TARGET WAS NOT FOUND! DEFAULTING TO A PRODUCTION RELEASE");
                CONFIG_ENVIRONMENT = "STAGING";

                Registry.SetValue(ConfigKey, "CONFIG_ENVIRONMENT", CONFIG_ENVIRONMENT);
                MainUpdater.Logger.WriteToDebugLog("SET THE REG KEY FOR CURRENT ENVIRONMENT TO STAGING.");
            }

            // DUH I FORGOT TO SAVE THE VALUE. TF IS I DOING
            CONFIG_ENVIRONMENT = CurrentConfig;            
        }

        public void CheckLAST_OPENED()
        {
            string ConfigKey = @"HKEY_CURRENT_USER\SOFTWARE\Drew Technologies, Inc\OESupportApps\";
            string LastOpened = (string)Registry.GetValue(ConfigKey, "LAST_OPENED", "NOT_SET");

            if (LastOpened == "NOT_SET" || LastOpened == null)
            {
                string DateTimeNow = DateTime.Now.ToString();
                MainUpdater.Logger.WriteToDebugLog("REGISTRY FOR LAST OPENED WINAUTOMATION CONSOLE IS BEING SETUP NOW");
                MainUpdater.Logger.WriteToDebugLog("DATETIME TO SET: " + DateTimeNow);

                Registry.SetValue(ConfigKey, "LAST_OPENED", DateTimeNow);
                MainUpdater.Logger.WriteToDebugLog("SET THE REG KEY FOR LAST TIME OPENED OK!");

                // CALL START USERCONSOLE HERE!
                return;
            }

            // So we need to check the date and time stored.
            // Then check the date time now. 
            // Subtract now from logged. If the result is > 20 dats, run the caller to start winautomations console.
            // This does however leave us with the issue of when a customer doesn't use OEScan for 30 days. then what?
        }

        public void SetWajRegLists()
        {
            S3DirectoryInfo InitFoundationWajs = new S3DirectoryInfo(S3Client, S3BucketName, FoundationFolder + @"\SCAN_INIT");
            var OrderedInitFoundation = InitFoundationWajs.GetFileSystemInfos().OrderBy(wajFile => wajFile.Name).ToArray();
            foreach (var WajFile in OrderedInitFoundation) { AllCombinedWajFiles.Add(WajFile.Name); }

            S3DirectoryInfo DoneFoundationWajs = new S3DirectoryInfo(S3Client, S3BucketName, FoundationFolder + @"\SCAN_DONE");
            var OrderedDoneFoundation = DoneFoundationWajs.GetFileSystemInfos().OrderBy(wajFile => wajFile.Name).ToArray();
            foreach (var WajFile in OrderedDoneFoundation) { AllCombinedWajFiles.Add(WajFile.Name); }

            foreach (string ENV_TYPE in ENV_TPYES)
            {
                if (ENV_TYPE != CONFIG_ENVIRONMENT) { continue; }

                List<string> NextKeySet = new List<string>();
                for (int x = 0; x < WajMakeFolders.Count; x++)
                {
                    string NewDir = WajMakeFolders[x].Replace("ENV_TYPE", CONFIG_ENVIRONMENT);
                    S3DirectoryInfo WajFileDir = new S3DirectoryInfo(S3Client, S3BucketName, NewDir);

                    var OrderedList = WajFileDir.GetFileSystemInfos().OrderBy(wajFile => wajFile.LastWriteTime).ToArray();
                    if (OrderedList.Length == 0) { continue; }

                    foreach (var WajFile in OrderedList) { AllCombinedWajFiles.Add(WajFile.Name); }
                    NextKeySet.Add(OrderedList[OrderedList.Length - 1].Name);
                }

                NextKeySet.Add(OrderedInitFoundation[OrderedInitFoundation.Length - 1].Name);
                NextKeySet.Add(OrderedDoneFoundation[OrderedDoneFoundation.Length - 1].Name);

                NewestS3WajFiles = NextKeySet.ToList();
                MainUpdater.Logger.WriteToDebugLog("LIST OF FILE MADE FOR THE CURRENT CONFIG TYPE");

                string ConfigKey_ENV = @"HKEY_CURRENT_USER\SOFTWARE\Drew Technologies, Inc\OESupportApps\";
                Registry.SetValue(ConfigKey_ENV, ENV_TYPE + "_WAJS", string.Join(",", NextKeySet));
            }

            MainUpdater.Logger.WriteToDebugLog("SETTING UP WAJ DIRS NOW BASED ON THE ENV TYPE");
            for (int x = 0; x < WajMakeFolders.Count; x++)
            {
                string NewDir = WajMakeFolders[x].Replace("ENV_TYPE", CONFIG_ENVIRONMENT);
                WajMakeFolders[x] = NewDir;
            }
        }
        public int GetAllSupportApps(out bool FoundAllApps)
        {
            S3DirectoryInfo SupportAppDir = new S3DirectoryInfo(S3Client, S3BucketName, SupportAppFolder);
            int TotalApps = SupportAppDir.GetFileSystemInfos().Length;

            FoundAllApps = TotalApps >= 4;
            return TotalApps;
        }


        public List<string> PullDownNewestWajs()
        {
            int Counter = 1;
            int CurrentLine = 0;

            foreach (var NewestFile in NewestS3WajFiles)
            {
                var Downloader = new TransferUtility(S3Client);
                if (NewestFile.Contains("LaunchVehicleScan"))
                { 
                    string BucketKeyName = FoundationFolder.Replace("\\", "/") + "/SCAN_INIT/" + NewestFile;
                    string OutputFileName = @"C:\Program Files (x86)\OESupport\Launchers\WajFiles\" + NewestFile;

                    Downloader.Download(OutputFileName, S3BucketName, BucketKeyName);

                    Counter++;
                    CurrentLine = Console.CursorTop;
                    Console.WriteLine("Downloaded: " + Counter + " Waj File of " + NewestS3WajFiles.Count());
                    Console.SetCursorPosition(0, CurrentLine);

                    MainUpdater.Logger.WriteToDebugLog("FOUND OUR INIT SCAN PROCESS OK!");
                    MainUpdater.Logger.WriteToDebugLog("NAME: " + NewestFile);
                    ImportToWinAutomation(OutputFileName, true);

                    OutputWajFiles.Add(OutputFileName);
                    continue;
                }
                if (NewestFile.Contains("ReportServiceAndCleanup"))
                {
                    string BucketKeyName = FoundationFolder.Replace("\\", "/") + "/SCAN_DONE/" + NewestFile;
                    string OutputFileName = @"C:\Program Files (x86)\OESupport\Launchers\WajFiles\" + NewestFile;

                    Downloader.Download(OutputFileName, S3BucketName, BucketKeyName);

                    Counter++;
                    CurrentLine = Console.CursorTop;
                    Console.WriteLine("Downloaded: " + Counter + " Waj File of " + NewestS3WajFiles.Count());
                    Console.SetCursorPosition(0, CurrentLine);

                    MainUpdater.Logger.WriteToDebugLog("FOUND OUR SCAN DONE PROCESS OK!");
                    MainUpdater.Logger.WriteToDebugLog("NAME: " + NewestFile);
                    ImportToWinAutomation(OutputFileName, false, true);

                    OutputWajFiles.Add(OutputFileName);
                    continue;
                }
            
                string OEType = NewestFile.Split('_')[0];
                string BaseDir = WajMakeFolders[WajMakeFolders.FindIndex(x => x.Contains(OEType))];

                var nextFile = new Amazon.S3.Model.ListObjectsRequest
                {
                    BucketName = S3BucketName,
                    Prefix = BaseDir.Replace("\\", "/")
                };

                var ListResp = S3Client.ListObjects(nextFile).S3Objects;
                var OrderedList = ListResp.OrderBy(wajFile => wajFile.LastModified).Reverse().ToArray();
                var Response = OrderedList[0];

                string OutputFile = @"C:\Program Files (x86)\OESupport\Launchers\WajFiles\" + NewestFile;
                OutputWajFiles.Add(OutputFile);

                MainUpdater.Logger.WriteToDebugLog("NEXT FILE: " + OutputFile);
                if (!File.Exists(OutputFile) && OutputFile.Contains(MainUpdater.OEType))
                {
                    MainUpdater.Logger.WriteToDebugLog("FILE DOES NOT EXIST. DOWNLOADING IT NOW");
                    Downloader.Download(OutputFile, S3BucketName, Response.Key);

                    // Import if needed here.
                    string[] CommandLineArgs = Environment.GetCommandLineArgs();
                    if (OutputFile.Contains(MainUpdater.OEType))
                    {
                        MainUpdater.Logger.WriteToDebugLog("IMPORTING INTO WINAUTOMATION");
                        ImportToWinAutomation(OutputFile);

                        MainUpdater.Logger.WriteToDebugLog("DELETING OLD WAJS FOR THIS MAKE");
                        foreach (var FileItem in Directory.GetFiles(@"C:\Program Files (x86)\OESupport\Launchers\WajFiles"))
                        {

                            if (FileItem == OutputFile) { continue; }
                            if (FileItem.Contains(MainUpdater.OEType))
                            {
                                try { File.Delete(FileItem); }
                                catch (Exception e) { Console.WriteLine("ERR: " + e.Message); }
                            }
                        }
                    }

                    Counter++;
                    CurrentLine = Console.CursorTop;
                    Console.WriteLine("Downloaded: " + Counter + " Waj File of " + NewestS3WajFiles.Count());
                    Console.SetCursorPosition(0, CurrentLine);
                }
            }

            Console.SetCursorPosition(0, CurrentLine);
            Console.BackgroundColor = ConsoleColor.Black;
            for (int pos = 0; pos < Console.WindowWidth; pos++) { Console.Write(" "); }
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.SetCursorPosition(0, CurrentLine);

            var FinalList = new List<string>();
            for (int counter = 0; counter < NewestS3WajFiles.Count; counter++)
            {
                string BucketFile = NewestS3WajFiles[counter];
                string WrittenFile = OutputWajFiles[counter];
                FinalList.Add(BucketFile + "\n   \\__ " + WrittenFile);
            }

            return FinalList;
        }
        public List<string> PullDownNewestApps()
        {
            var nextApp = new Amazon.S3.Model.ListObjectsRequest
            {
                BucketName = S3BucketName,
                Prefix = SupportAppFolder.Replace("\\", "/")
            };

            int Counter = 1;
            int CurrentLine = 0;
            var Downloader = new TransferUtility(S3Client);
            var ListResp = S3Client.ListObjects(nextApp).S3Objects;
            ListResp.Reverse();

            foreach (var Response in ListResp)    
            {
                string FileName = Response.Key.Substring(Response.Key.LastIndexOf("/")).Replace("/", "");
                string OutputFile = @"C:\Program Files (x86)\OESupport\OEAppHelpers\" + FileName;

                if (FileName.Contains("DriveSafeReportingTool"))
                    OutputFile = @"C:\Program Files (x86)\Drew Technologies, Inc\DriveCrashService\" + FileName;

                OutputSupportApps.Add(OutputFile);
                NewestSupportAppFiles.Add(Response.Key);

                try 
                {
                    CurrentLine = Console.CursorTop;
                    if (!File.Exists(OutputFile)) Downloader.Download(OutputFile, S3BucketName, Response.Key);
                    Console.WriteLine("Downloaded: " + Counter + " / " + ListResp.Count);
                    Console.SetCursorPosition(0, CurrentLine);
                    Counter++;                    
                }
                catch (Exception e)
                {
                    NewestSupportAppFiles.Remove(OutputFile);
                    OutputSupportApps.Remove(Response.Key);
                }
            }

            Console.SetCursorPosition(0, CurrentLine);
            Console.BackgroundColor = ConsoleColor.Black;
            for (int pos = 0; pos < Console.WindowWidth; pos++) { Console.Write(" "); }
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.SetCursorPosition(0, CurrentLine);

            try
            {
                File.Delete(@"C:\Program Files (x86)\Drew Technologies, Inc\DriveCrashService\DriveSafeReportingTool.exe");
                File.Copy(
                    @"C:\Program Files (x86)\OESupport\OEAppHelpers\DriveSafeReportingTool.exe",
                    @"C:\Program Files (x86)\Drew Technologies, Inc\DriveCrashService\DriveSafeReportingTool.exe");
            }
            catch { }

            var FinalList = new List<string>();
            for (int counter = 0; counter < NewestSupportAppFiles.Count; counter++)
            {
                try
                {
                    string BucketFile = NewestSupportAppFiles[counter];
                    string WrittenFile = OutputSupportApps[counter];

                    long SizeOfExe = File.ReadAllBytes(WrittenFile).Length;
                    string FileSize = ConsolePrinters.BytesToString(SizeOfExe) + " (" + SizeOfExe + " bytes)";
                    FinalList.Add(WrittenFile + "\n   \\__ " + FileSize);
                }
                catch { }
            }

            return FinalList;
        }


        public void ImportToWinAutomation(string WajFile, bool IsStartupWaj = false, bool IsCleanupWaj = false)
        {
            string WajOnly = WajFile.Substring(WajFile.LastIndexOf("\\") + 1).Split('.')[0];

            string OEName = MainUpdater.OEType;
            if (IsStartupWaj) { OEName = "StartupProcess"; }
            else if (IsCleanupWaj) { OEName = "CleanupProcess"; }

            string BatFileLoc = @"C:\Program Files (x86)\OESupport\Launchers\Import" + OEName + ".bat";

            string ProcToRun =
                "cd \"C:\\Program Files\\WinAutomation\"" +
                "\nWinAutomationController.exe /import \"NEW_WAJ_FILE_NAME\" \"/My Processes/OEAutomation/WAJ_NAME_ONLY\"";

            string NewFile = ProcToRun
                .Replace("NEW_WAJ_FILE_NAME", WajFile)
                .Replace("WAJ_NAME_ONLY", WajOnly);

            File.WriteAllText(BatFileLoc, NewFile);

            var ImportProcess = new Process();
            var StartOptions = new ProcessStartInfo();

            StartOptions.FileName = BatFileLoc;
            StartOptions.WindowStyle = ProcessWindowStyle.Hidden;
            StartOptions.Arguments = "/min";

            ImportProcess.StartInfo = StartOptions;
            ImportProcess.Start();

            ImportProcess.WaitForExit();
            try { File.Delete(BatFileLoc); }
            catch { }
        }
    }
}
