﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Win32;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Net;

namespace OESupportUpdater
{
    public class CreateNewBatFile
    {
        public string RUN_THIS;     // String value for the bat file to run.

        // Dir of the Laucnehr file and the base info for the bat file which we generate.
        private string LauncherFileHome = @"C:\Program Files (x86)\OESupport\Launchers\";
        private string BaseBatFile =
            "\nif not DEFINED IS_MINIMIZED set IS_MINIMIZED=1 && start \"\" /min \"%~dpnx0\" %* && exit" +
            "\nTITLE WINDOW_TITLE" +
            "\n\ncd \"C:\\Program Files\\WinAutomation\"" +
            "\nNET START \"WinAutomation Server\"" +
            "\nNET START \"WinAutomation Machine Agent\"" +
            "\nWinAutomation.UserAgent.exe" + 
            "\nWinAutomationController.exe /start \"My Processes/OEAutomation/WAJ_NAME_ONLY\""; 
            // + "\n\nset /p DUMMY=Hit ENTER to continue...\"";

        // Base OE key location inside the registry.
        private string BaseRegLocation = @"HKEY_CURRENT_USER\Software\Drew Technologies, Inc\LoaderApplication";
        private List<string> OE_SW_Names = new List<string> { "TIS", "GDS2", "FCA", "FJDS", "IHDS" };

        /// <summary>
        /// Constructor for the CreateNewBatFile object. Takes the string of the newest Waj Files in the machine to
        /// generate importing files.
        /// </summary>
        /// <param name="AllWajs">string of paths to the newest Waj Files grabbed from my S3 bucket.</param>
        public CreateNewBatFile(List<string> AllWajs)
        {
            ConsolePrinters.DividerPrint("GENERATING NEWEST BAT FILES", ConsoleColor.DarkGreen);
            foreach (var FileName in AllWajs)
            {
                if (FileName.Contains("LaunchVehicleScan")) { CreateLauncherBat(FileName, true); }
                if (FileName.Contains("ReportServiceAndCleanup")) { CreateLauncherBat(FileName, true); }
                else 
                {
                    if (!FileName.Contains(MainUpdater.OEType)) { continue; }
                    CreateLauncherBat(FileName, false); 
                }
            }

            ConsolePrinters.PrintSeperator();
            if (RUN_THIS != null)
            {
                ConsolePrinters.DividerPrint("RUNNING NEW BAT FILE NOW.", ConsoleColor.DarkGreen);
                Console.WriteLine(RUN_THIS);

                // Wait for user to read the screen if were in debug mode.
                if (Debugger.IsAttached)
                {
                    Console.WriteLine("\nPRESS ANY KEY TO START THE AUTOMATION SCRIPT. FILE NAME:\n\\__ " + RUN_THIS);
                    ConsolePrinters.PrintSeperator();
                    Console.ReadLine();
                }
                else { ConsolePrinters.PrintSeperator(); }
            }                   
        }

        public void CreateLauncherBat(string WajFile, bool IsStartOrStopFile = false)
        {
            string WajOnly = WajFile.Substring(WajFile.LastIndexOf("\\") + 1).Split('.')[0];
            string WindowName = MainUpdater.OEType + "Window";
            string FileType = MainUpdater.OEType + " OE Scan";
            string BatName = "Launch" + MainUpdater.OEType + ".bat";

            if (IsStartOrStopFile) 
            {
                WindowName = WajOnly.Split('_')[0] + "Window";
                FileType = WajOnly.Split('_')[0];

                if (WajFile.Contains("LaunchVehicleScan")) { BatName = "LaunchVehicleScan_" + MainUpdater.OEType + ".bat"; }
                if (WajFile.Contains("ReportServiceAndCleanup")) { BatName = "LaunchVehicleScan_" + MainUpdater.OEType + ".bat"; }
            }

            string NewFile = BaseBatFile
                .Replace("WINDOW_TITLE", WindowName)
                .Replace("BAT_FILE_TYPE", FileType)
                .Replace("NEW_WAJ_FILE_NAME", WajFile)
                .Replace("WAJ_NAME_ONLY", WajOnly);

            string OutputFile = LauncherFileHome + BatName;
            if (File.Exists(OutputFile)) { File.Delete(OutputFile); }

            File.WriteAllText(OutputFile, NewFile);

            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;

            if (IsStartOrStopFile)
            {
                Console.WriteLine("Made a new Launcher for Scan Sesisons" + "\n\\__" + OutputFile);
                Console.WriteLine("   \\__ Waj File: " + WajFile);

                // When this is used in live builds, we can remove this return and change to use the right file.
                return;
            }

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\nMade a new BAT file for " + MainUpdater.OEType + "\n\\__" + OutputFile);
            Console.WriteLine("   \\__ Waj File: " + WajFile);
            Console.ResetColor();

            string[] CommandLineArgs = Environment.GetCommandLineArgs();
            foreach (var arg in CommandLineArgs)
            {
                if (arg.Contains(MainUpdater.OEType))
                {
                    RUN_THIS = OutputFile;
                    SetAutomationRegistry(MainUpdater.OEType);

                    string WajDir = LauncherFileHome + "WajFiles\\CURRENT_WAJ_FILE.txt";
                    string ContentsOfFile = "My Processes/OEAutomation/" + WajOnly;
                    File.WriteAllText(WajDir, ContentsOfFile);

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("SETUP KILLER FILE FOR THE PROCESS: " + WajOnly);
                    Console.WriteLine("");
                    Console.ForegroundColor = ConsoleColor.White;

                    break;
                }
            }            
        }

        public void SetAutomationRegistry(string OEName)
        {
            string DebugKey = "Debug_UseMAKEAutomation";
            // foreach (var OE in OE_SW_Names)
            // {
            //     DebugKey = DebugKey.Replace("MAKE", OE);
            //     Registry.SetValue(BaseRegLocation, DebugKey, "false");
            //     DebugKey = "Debug_UseMAKEAutomation";
            // }

            DebugKey = DebugKey.Replace("MAKE", OEName);
            Registry.SetValue(BaseRegLocation, DebugKey, "true");
        }
    }
}
