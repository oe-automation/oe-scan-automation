﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OESupportUpdater
{
    public class VehicleMakeSelections
	{
		public static string Make = "";

		public static bool IsToyotaMake
		{
			get
			{
				if (string.IsNullOrEmpty(Make)) { return false; }
				else
				{
					return (Make.ToUpper() == "TOYOTA") ||
						   (Make.ToUpper() == "LEXUS") ||
						   (Make.ToUpper() == "SCION");
				}
			}
		}

		public static bool IsGMMake
		{
			get
			{
				if (string.IsNullOrEmpty(Make)) { return false; }
				else
				{
					return (Make.ToUpper() == "BUICK") ||
						   (Make.ToUpper() == "CHEVROLET") ||
						   (Make.ToUpper() == "CADILLAC") ||
						   (Make.ToUpper() == "DODGE") ||
						   (Make.ToUpper() == "GMC");
				}
			}
		}

		public static bool IsFordMake
		{
			get
			{
				if (string.IsNullOrEmpty(Make)) { return false; }
				else
				{
					return (Make.ToUpper() == "FORD") ||
						   (Make.ToUpper() == "LINCOLN") ||
						   (Make.ToUpper() == "MERCURY");
				}
			}
		}

		public static bool IsHondaMake
		{
			get
			{
				if (string.IsNullOrEmpty(Make)) { return false; }
				else
				{
					return (Make.ToUpper() == "HONDA") ||
						   (Make.ToUpper() == "ACURA");
				}
			}
		}
	}
}
