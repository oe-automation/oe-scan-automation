﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OESupportUpdater
{
    public class ConsolePrinters
    {
        private static string TitleSting =
            @"                                                                                                             " + "\n" +
            @"  +=======================================================================================================+  " + "\n" +
            @"  |     ____    ______                        _                                 _     _                   |  " + "\n" +
            @"  |    / __ \  |  ____|       /\             | |                               | |   (_)                  |  " + "\n" +
            @"  |   | |  | | | |__         /  \     _   _  | |_    ___    _ __ ___     __ _  | |_   _    ___    _ __    |  " + "\n" +
            @"  |   | |  | | |  __|       / /\ \   | | | | | __|  / _ \  | '_ ` _ \   / _` | | __| | |  / _ \  | '_ \   |  " + "\n" +
            @"  |   | |__| | | |____     / ____ \  | |_| | | |_  | (_) | | | | | | | | (_| | | |_  | | | (_) | | | | |  |  " + "\n" +
            @"  |    \____/  |______|   /_/    \_\  \__,_|  \__|  \___/  |_| |_| |_|  \__,_|  \__| |_|  \___/  |_| |_|  |  " + "\n" +
            @"  +=======================================================================================================+  " + "\n" +
            @"                                                                                                             ";



        [DllImport("user32.dll")]
        private static extern bool ShowWindow(System.IntPtr hWnd, int cmdShow);

        public static void Maximize()
        {
            if (Debugger.IsAttached) { return; }

            Process p = Process.GetCurrentProcess();
            ShowWindow(p.MainWindowHandle, 3); //SW_MAXIMIZE = 3
        }

        public static void CenterPrint(string TextToShow)
        {
            int ConsoleWidth = Console.WindowWidth;
            if (TextToShow.Contains("\n"))
            {
                string[] split = TextToShow.Split('\n');
                foreach (string s in split)
                {
                    ConsoleWidth = Console.WindowWidth;
                    Console.SetCursorPosition((ConsoleWidth - s.Length) / 2, Console.CursorTop);
                    Console.WriteLine(s);
                }
            }

            else
            {
                ConsoleWidth = Console.WindowWidth;
                Console.SetCursorPosition((ConsoleWidth - TextToShow.Length) / 2, Console.CursorTop);
                Console.WriteLine(TextToShow);
            }
        }

        public static void ShowTitleWindow()
        {
            //Maximize();
            Console.Clear();

            Console.WriteLine("\n");

            Console.BackgroundColor = ConsoleColor.DarkBlue;
            CenterPrint(TitleSting);
            Console.BackgroundColor = ConsoleColor.Black;

            Console.WriteLine("\n");

            System.Threading.Thread.Sleep(2500);
            // Console.Clear();
        }

        public static void DividerPrint(string TextToPrint, ConsoleColor bgColor = ConsoleColor.Black, ConsoleColor fgColor = ConsoleColor.White)
        {
            Console.BackgroundColor = bgColor;
            Console.ForegroundColor = fgColor;

            Console.Write("+");
            for (int counter = 0; counter < TextToPrint.Length; counter++) { Console.Write("="); }
            Console.WriteLine("+\n|" + TextToPrint + "|");
            Console.Write("+");
            for (int counter = 0; counter < TextToPrint.Length; counter++) { Console.Write("="); }
            Console.WriteLine("+");

            Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
        }

        public static void PrintSeperator()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;

            Console.WriteLine();
            for (int count = 0; count < Console.WindowWidth; count++) { Console.Write("-"); }
            Console.WriteLine();
        }

        public static string BytesToString(long byteCount)
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" }; //Longs run out around EB
            if (byteCount == 0)
                return "0" + suf[0];
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString() + suf[place];
        }
    }
}
