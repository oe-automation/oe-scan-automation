﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OESupportUpdater
{
    public class RegistrySetupHelper
    {
        private static string CurrentUser = @"HKEY_CURRENT_USER\Software\Drew Technologies, Inc\";
        private static string LoaderApp = CurrentUser + "LoaderApplication";
        private static string OESupApps = CurrentUser + "OESupportApps";

        public static List<Tuple<string, bool>> MakeKeys = new List<Tuple<string, bool>>()
        {
            new Tuple<string, bool>("Debug_UseTISAutomation", true),
            new Tuple<string, bool>("Debug_UseGDS2Automation", true),
            new Tuple<string, bool>("Debug_UseFJDSAutomation", true),
            new Tuple<string, bool>("Debug_UseIHDSAutomation", true),
            new Tuple<string, bool>("Debug_UseFDRSAutomation", true),
            new Tuple<string, bool>("Debug_UseCP3Automation", true),
            new Tuple<string, bool>("UseEXE", false)
        };
        public static List<Tuple<string, string>> WajAndEnvironment = new List<Tuple<string, string>>()
        {
            new Tuple<string,string>("CONFIG_ENVIRONMENT", "STAGING"),
            new Tuple<string,string>("PRODUCTION_WAJS", ""),
            new Tuple<string,string>("DEVELOPMENT_WAJS", ""),
            new Tuple<string,string>("STAGING_WAJS", "")
        };


        private static bool CheckForExisting(string KeyToCheck)
        {
            string BaseKey = OESupApps;
            if (KeyToCheck.Contains("Debug") || KeyToCheck.Contains("EXE")) { BaseKey = LoaderApp; }

            object CurrentKey = Registry.GetValue(BaseKey, KeyToCheck, null);
            return (CurrentKey != null);
        }
        private static bool SetNewKey(Tuple<string, string> NextValue)
        {
            object CurrentKey = Registry.GetValue(OESupApps, NextValue.Item1, null);
            if (CurrentKey != null)
            {
                MainUpdater.Logger.WriteToDebugLog("KEY TO SET WAS NOT NULL! VALUE ALREADY EXISTED");
                return true;
            }

            Registry.SetValue(OESupApps, NextValue.Item1, NextValue.Item2);
            object CheckSetValue = Registry.GetValue(OESupApps, NextValue.Item1, null);
            return (CheckSetValue != null);
        }
        private static bool SetNewKey(Tuple<string, bool> NextValue)
        {
            object CurrentKey = Registry.GetValue(LoaderApp, NextValue.Item1, null);
            if (CurrentKey != null)
            {
                MainUpdater.Logger.WriteToDebugLog("KEY TO SET WAS NOT NULL! VALUE ALREADY EXISTED");
                return true;
            }

            Registry.SetValue(LoaderApp, NextValue.Item1, NextValue.Item2.ToString().ToLower());
            object CheckSetValue = Registry.GetValue(LoaderApp, NextValue.Item1, null);
            return (CheckSetValue != null);
        }


        public static void SetupAllRegKeys()
        {
            MainUpdater.Logger.WriteToDebugLog("SETTING UP KEYS FOR BOTH THE MAKES AND WAJS NOW");
            SetupLoaderApp();
            SetupWajAndEnv();
        }

        public static void SetupLoaderApp()
        {
            MainUpdater.Logger.WriteToDebugLog("SETTING UP LOADER APP REG KEYS NOW IF THEY DONT EXIST");
            foreach (var KeyObj in MakeKeys)
            {
                if (CheckForExisting(KeyObj.Item1)) { MainUpdater.Logger.WriteToDebugLog("KEY: " + KeyObj.Item1 + " ALREADY EXISTS, MOVING ON"); }
                else
                {
                    MainUpdater.Logger.WriteToDebugLog("SETTING NEW VALUE FOR KEY: " + KeyObj.Item1);
                    try { SetNewKey(KeyObj); }
                    catch (Exception regEx)
                    {
                        MainUpdater.Logger.WriteToDebugLog("CAN NOT SET THIS REG KEY!!");
                        MainUpdater.Logger.WriteToDebugLog(regEx.Message);
                    }
                }
            }
        }
        public static void SetupWajAndEnv()
        {
            MainUpdater.Logger.WriteToDebugLog("SETTING UP OE SUPPORT APP REG KEYS NOW IF THEY DONT EXIST");
            foreach (var KeyObj in WajAndEnvironment)
            {
                if (CheckForExisting(KeyObj.Item1)) { MainUpdater.Logger.WriteToDebugLog("KEY: " + KeyObj.Item1 + " ALREADY EXISTS, MOVING ON"); }
                else
                {
                    MainUpdater.Logger.WriteToDebugLog("SETTING NEW VALUE FOR KEY: " + KeyObj.Item1);
                    try { SetNewKey(KeyObj); }
                    catch (Exception regEx)
                    {
                        MainUpdater.Logger.WriteToDebugLog("CAN NOT SET THIS REG KEY!!");
                        MainUpdater.Logger.WriteToDebugLog(regEx.Message);
                    }
                }
            }
        }
    }
}
