﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEPublisher
{
    public class PublisherMain
    {
        public static void Main(string[] args)
        {
            AWS_Helper Uploader = new AWS_Helper();

            if (args.Length == 0) { Console.WriteLine("NO ARGS GIVEN TRY AGAIN"); return; }
            if (args.Length == 1) { Console.WriteLine("NOT ENOUGH ARGS GIVEN TRY AGAIN"); return; }

            else
            {
                if (args[0].ToUpper() == "WAJ") { Uploader.UploadWajFile(args[1]); }
                if (args[0].ToUpper() == "MULTI_WAJ")
                {
                    List<string> allFiles = new List<string>();
                    foreach (var arg in args) 
                    {
                        if (!arg.Contains(".waj")) { continue; }
                        allFiles.Add(arg); 
                    }

                    Uploader.UploadListOfFiles(allFiles);
                }
                if (args[0].ToUpper().Contains("CFG"))
                {
                    string CfgFile = "";
                    foreach (var arg in args) { if (!arg.Contains("CFG")) { CfgFile += arg + " "; } }

                    if (args[0].ToUpper() == "CFG") { Uploader.PullDownConfigFile(CfgFile); }
                    if (args[0].ToUpper() == "CFG_UP") { Uploader.PutConfigFile(CfgFile); }
                }
            }

            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
