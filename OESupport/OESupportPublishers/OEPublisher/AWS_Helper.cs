﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace OEPublisher
{
    public class AWS_Helper
    {
        private AmazonS3Client S3Client;

        private string S3BucketName = "drivepro.logs.upload";
        private string S3AccessKey = "AKIASQ7DDKVHCJMHJEXU";
        private string S3SecretKey = "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";

        public static string ConfigFolder = @"Deploy Directories/OEAutomation/OEConfigFiles";
        public static string SupportAppFolder = @"Deploy Directories/OEAutomation/SupportApps";

        public static string FJDS_Wajs = @"Deploy Directories/OEAutomation/WajFiles/FJDS_Ford";
        public static string GDS2_Wajs = @"Deploy Directories/OEAutomation/WajFiles/GDS2_GM";
        public static string IHDS_Wajs = @"Deploy Directories/OEAutomation/WajFiles/IHDS_Honda";
        public static string TIS_Wajs = @"Deploy Directories/OEAutomation/WajFiles/TIS_Toyota";

        public AWS_Helper()
        {
            S3Client = new AmazonS3Client(S3AccessKey, S3SecretKey, RegionEndpoint.EUWest1);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("S3 Client Created OK!\n");
        }

        public bool UploadWajFile(string PathToFile)
        {
            if (!File.Exists(PathToFile)) { return false; }
            else
            {
                var bindingFlags = BindingFlags.Static | BindingFlags.Public;
                var fieldValues = this.GetType().GetFields(bindingFlags).Select(field => field.GetValue(this)).ToList();

                string OEType = (PathToFile.Substring(PathToFile.LastIndexOf(Path.DirectorySeparatorChar) + 1)).Split('_')[0];
                foreach (var field in fieldValues)
                {
                    if (field.ToString().Contains(OEType))
                    {
                        try
                        {
                            string FileName = PathToFile.Substring(PathToFile.LastIndexOf(Path.DirectorySeparatorChar) + 1);
                            string KeyName = (field.ToString() + "\\" + FileName).Replace("\\", "/");

                            var Downloader = new TransferUtility(S3Client);
                            Downloader.Upload(PathToFile, S3BucketName, KeyName);

                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("File: " + FileName + " was uploaded OK!");
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine("\\__ " + KeyName);
                            Console.ResetColor();

                            return true;
                        }

                        catch (Exception e)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("File could not be uploaded.");
                            Console.Write("\\__ " + e.Message);
                            Console.ResetColor();
                            return false;
                        }
                    }
                }
                return false;
            }
        }

        public void UploadListOfFiles(List<string> FilePaths)
        {
            bool allOk = true;
            int OkUploads = 0;
            foreach (var File in FilePaths) 
            {
                if (!File.Contains(".waj")) { continue; }

                allOk = UploadWajFile(File);
                if (allOk) { OkUploads += 1; }
            }

            Console.WriteLine();
            if (allOk) 
            { 
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Uploaded " + FilePaths.Count + " Files OK!");
            }

            else 
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Uploaded " + OkUploads + " of " + FilePaths.Count + " files ok. Please try manually.");
            }
        }

        public void PullDownConfigFile(string ConfigFileName)
        {
            string Make = ConfigFileName.Split('_')[0];
            string Model = ConfigFileName.Split('_')[1];
            string Year = ConfigFileName.Split('_')[2];
            if (Year.Contains(".txt")) { Year = Year.Replace(".txt", String.Empty); }

            string OutputFile = @"C:\Program Files (x86)\OESupport\VehicleOptions\" + Make + "\\" + ConfigFileName;
            string OutputDir = @"C:\Program Files (x86)\OESupport\VehicleOptions\" + Make;
            string CurrentVehFile = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\" + ConfigFileName;

            if (!Directory.Exists(OutputDir)) { Directory.CreateDirectory(OutputDir); } 

            var nextFile = new Amazon.S3.Model.ListObjectsRequest
            {
                BucketName = S3BucketName,
                Prefix = ConfigFolder
            };

            var timer = new System.Diagnostics.Stopwatch();
            timer.Start();

            var Downloader = new TransferUtility(S3Client);
            var ListResp = S3Client.ListObjects(nextFile).S3Objects;
            ListResp.Reverse();

            // Get DateTimeNow.
            var strTime = DateTime.Now.ToString("s");
            strTime = strTime.Replace("T", "_");
            strTime = strTime.Replace("-", "");

            // Replace the actual time with nothing since we need to call this file again.
            strTime = strTime.Split('_')[0];
            string DebugFile = @"C:\Program Files (x86)\OESupport\OEDebugging\OptionsDebug\CFGDownloader_" + strTime + ".txt";

            File.AppendAllText(DebugFile, "----------------------------------\n");
            File.AppendAllText(DebugFile, "PULLING DOWN CFG FILE FOR OPTIONS CONFIG.\nDESIRED FILE: " + ConfigFileName + "\n");
            File.AppendAllText(DebugFile, "FOUND A TOTAL OF: " + ListResp.Count + " CONFIG FILES\n\n");

            foreach (var objectBack in ListResp)
            {
                if (!objectBack.Key.Contains(Make)) { continue; }
                if (!objectBack.Key.Contains(Model)) { continue; }
                if (!objectBack.Key.Contains(Year)) { continue; }

                Downloader.Download(OutputFile, S3BucketName, objectBack.Key);
                File.AppendAllText(DebugFile, "DOWNLOADED CFG: " + objectBack.Key + "\n");
                File.AppendAllText(DebugFile, "SAVED LOCATION: " + OutputFile + "\n\n");

                if (File.Exists(CurrentVehFile)) { File.Delete(CurrentVehFile); }
                File.Copy(OutputFile, CurrentVehFile);

                timer.Stop();

                var TimeTaken = new TimeSpan(timer.ElapsedTicks);
                File.AppendAllText(DebugFile, "OPERATION TOOK: " + TimeTaken.TotalSeconds.ToString("F3") + " SECONDS\n\n");

                // System.Threading.Thread.Sleep(5000);
                return;
            }

            File.AppendAllText(DebugFile, "COULD NOT FIND A CONFIG FILE. TERMINATING.\n\n");
        }

        public void PutConfigFile(string ConfigFile)
        {
            string Make = ConfigFile.Split('_')[0];
            string ConfigFilePath = @"C:\Program Files (x86)\OESupport\VehicleOptions\" + Make + @"\" + ConfigFile.ToUpper();

            var timer = new System.Diagnostics.Stopwatch();
            timer.Start();

            // Get DateTimeNow.
            var strTime = DateTime.Now.ToString("s");
            strTime = strTime.Replace("T", "_");
            strTime = strTime.Replace("-", "");

            // Replace the actual time with nothing since we need to call this file again.
            strTime = strTime.Split('_')[0];
            string DebugFile = @"C:\Program Files (x86)\OESupport\OEDebugging\OptionsDebug\CFGDownloader_" + strTime + ".txt";

            File.AppendAllText(DebugFile, "----------------------------------\n");
            File.AppendAllText(DebugFile, "WRITING CFG FILE TO S3 BUCKET NOW.\nDESIRED FILE: " + ConfigFilePath + "\n");

            var Downloader = new TransferUtility(S3Client);
            string FileKey = ConfigFolder + "/" + ConfigFile.ToUpper();
            Downloader.Upload(ConfigFilePath, S3BucketName, FileKey);

            timer.Stop();

            var TimeTaken = new TimeSpan(timer.ElapsedTicks);
            File.AppendAllText(DebugFile, "OPERATION TOOK: " + TimeTaken.TotalSeconds.ToString("F3") + " SECONDS\n\n");
        }
    }
}
