﻿using OEAutomationLauncher.AutomationLauncher.ObjectClasses;
using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.DriveCrashInterface
{
    public class DriveCrashServiceInvoker
    {
        public DriveCrashServiceParams ServiceHelper;

        public bool SOAPUploadResult;
        private string ArgString;
        private string SOAPString =
                "<?xml version='1.0' encoding='utf-8'?>" +
                    "<S:Envelope xmlns:S='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns=\"http://tempuri.org/\">" +
                        "<S:Header>" +
                        "</S:Header>" +
                            "<S:Body>" +
                                "<TriggerScan  xmlns=\"http://tempuri.org/\">" +
                                    "<sessionID>_SESSIONID_</sessionID>" +
                                    "<make>_MAKE_</make>" +
                                    "<VIN>_VIN_</VIN>" +
                                    "<PreOrPost>_PRE-POST_</PreOrPost>" +
                                 "</TriggerScan>" +
                            "</S:Body>" +
                    "</S:Envelope>";

        public DriveCrashServiceInvoker()
        {
            GlobalObjects.DebugHelper.WriteDebugLog("SETTING UP A NEW CRASH SERVICE INTERFACE OBJECT");

            ServiceHelper = new DriveCrashServiceParams();
            GlobalObjects.DebugHelper.WriteDebugLog("CONFIG INFO FOR THE SERVICE IS AS FOLLOWS");
            GlobalObjects.DebugHelper.WriteDebugLog("VIN:  " + ServiceHelper.VIN_NUMBER.Trim());
            GlobalObjects.DebugHelper.WriteDebugLog("OEM:  " + ServiceHelper.OEM_NAME.Trim());
            GlobalObjects.DebugHelper.WriteDebugLog("GUID: " + ServiceHelper.GUID_OR_SESSION.Trim());
            GlobalObjects.DebugHelper.WriteDebugLog("TYPE: " + ServiceHelper.PRE_OR_POST.Trim());

            ArgString = ServiceHelper.VIN_NUMBER + " " +
                   ServiceHelper.OEM_NAME + " " +
                   ServiceHelper.GUID_OR_SESSION + " " +
                   ServiceHelper.PRE_OR_POST;

            // NO LONGER USING THIS STRING ITEM
            /* APIString += ServiceHelper.GUID_OR_SESSION + "/"
                    + ServiceHelper.OEM_NAME + "/"
                    + ServiceHelper.VIN_NUMBER + "/"
                    + ServiceHelper.PRE_OR_POST; */

            SOAPString = SOAPString.Replace("_SESSIONID_", ServiceHelper.GUID_OR_SESSION)
                                   .Replace("_MAKE_", ServiceHelper.OEM_NAME)
                                   .Replace("_VIN_", ServiceHelper.VIN_NUMBER)
                                   .Replace("_PRE-POST_", ServiceHelper.PRE_OR_POST);

            // Log out the info for both call types.
            GlobalObjects.DebugHelper.WriteDebugLog("SERVICE INVOKER ARG STRING: " + ArgString);
            GlobalObjects.DebugHelper.WriteDebugLog("SERVICE INVOKER SOAP XML:   " + SOAPString);
        }

        /// <summary>
        /// Calls the Reporting service using an EXE.
        /// </summary>
        /// <returns></returns>
        public bool SendReportUsingEXE()
        {
            Process ServiceCaller = new Process();
            ProcessStartInfo StartupInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Normal,
                Arguments = ArgString,
                FileName = "DriveSafeReportingTool.exe",
                WorkingDirectory = @"C:\Program Files (x86)\Drew Technologies, Inc\DriveCrashService\"
            };

            ServiceCaller.StartInfo = StartupInfo;
            ServiceCaller.Start();
            GlobalObjects.DebugHelper.WriteDebugLog("SERVICE INVOKER STARTED CHECK DREWTECH LOG FOLDER FOR RESULT OF UPLOAD");

            return true;
        }

        /* REMOVED ON 12-15-20. NOW USING THE SOAP CALL METHOD INSTEAD
        /// <summary>
        /// Calls the reporting service using a WebClient to the BL
        /// </summary>
        /// <returns></returns>
        public bool SendReportUsingBL()
        {
            // BL WebClient.
            var BLWebClient = new WebClient();

            // Show string we're invoking.
            GlobalObjects.DebugHelper.WriteDebugLog("CALLING BL SERVICE NOW...");
            GlobalObjects.DebugHelper.WriteDebugLog("API STRING: " + APIString);

            try
            {
                // SEND IT BITCH. 
                string RespCode = BLWebClient.DownloadString(APIString);
                GlobalObjects.DebugHelper.WriteDebugLog("RESPONSE: " + RespCode);
                GlobalObjects.DebugHelper.WriteDebugLog("REPORT WAS SENT UP OK!");

                return true;
            }

            catch (Exception blEx)
            {
                // When shit hits the fan cuz thats how we do here. Show the user what went sideways.
                GlobalObjects.DebugHelper.WriteDebugLog("ERROR ON REPORT UPLOAD: " + blEx.Message);
                GlobalObjects.DebugHelper.WriteDebugLog("THIS IS A CRITICAL FAILURE AND WILL RESULT IN A FAILED SCAN");

                return false;
            }
        }
        */ 

        public async void SendReportAsSOAP()
        {
            // HEADERS 
            // Content-Type | text/xml
            // SOAPAction   | http://tempuri.org/IDriveSafeReportingTool/TriggerScan

            // XML Params as a NVC
            /* ParamsNameValueCollection = new NameValueCollection
            {
                { "sessionID", ServiceHelper.GUID_OR_SESSION },
                { "make", ServiceHelper.OEM_NAME },
                { "VIN", ServiceHelper.VIN_NUMBER },
                { "PreOrPost", ServiceHelper.PRE_OR_POST }
            };*/

            // Timer to track upload time.
            var uploadTimer = new Stopwatch();
            uploadTimer.Start();

            GlobalObjects.DebugHelper.WriteDebugLog("SETUP A NEW TIMER TO TRACK UPLOAD TIME AND INIT'D A NVC FOR JSON PARAMS OK!"); ;
            try
            {
                GlobalObjects.DebugHelper.WriteDebugLog("STARTING UPLOAD PROCESS NOW");

                HttpClient SOAPHttpClient = new HttpClient();
                SOAPHttpClient.DefaultRequestHeaders.Add("SOAPAction", "http://tempuri.org/IDriveSafeReportingTool/TriggerScan");
                StringContent xmlString = new StringContent(SOAPString, Encoding.UTF8, "text/xml");
                GlobalObjects.DebugHelper.WriteDebugLog("WEBCLIENT MADE AND HEADERS WERE SET OK");

                var HttpResp = await SOAPHttpClient.PostAsync("http://localhost:61233/DriveSafeReportingToolApi/", xmlString);
                string ResponseBody = await HttpResp.Content.ReadAsStringAsync();

                uploadTimer.Stop();
                string TimeToUpload = uploadTimer.Elapsed.ToString(@"mm\:ss\.fff");
                GlobalObjects.DebugHelper.WriteDebugLog("DONE UPLOADING REPORT REQUEST. TOOK: " + TimeToUpload);
                GlobalObjects.DebugHelper.WriteDebugLog("FOUND RESPONSE VALUE OF: " + ResponseBody);

                if (ResponseBody.Contains("<TriggerScanResult>0</TriggerScanResult>"))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("RESPONSE CODE 0 FROM THE SERVICE PROCESSED! SCAN WENT OUT OK!");
                    SOAPUploadResult = true;

                    return;
                }

                GlobalObjects.DebugHelper.WriteDebugLog("DID NOT RECEIVE AN EXIT CODE OF 0!! THIS MEANS THE SCAN FAILED TO BE PARSED");
                GlobalObjects.DebugHelper.WriteDebugLog("CHECK THE LOGS UNDER C:\\DREWTECH\\LOGS FOR MORE INFO ON WHAT FAILED");

                SOAPUploadResult = false;
                return;
            }
            catch (Exception upEx)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("ERROR: COULD NOT UPLOAD SCAN TO THE DSRT! THIS IS FATAL!");
                GlobalObjects.DebugHelper.WriteDebugLog("EX: " + upEx.Message);
                GlobalObjects.DebugHelper.WriteDebugLog(upEx);

                uploadTimer.Stop();
                string TimeToUpload = uploadTimer.Elapsed.ToString(@"mm\:ss\.fff");
                GlobalObjects.DebugHelper.WriteDebugLog("FAILED UPLOADING AFTER: " + TimeToUpload);

                SOAPUploadResult = false;
                return;
            }
        }
    }
}