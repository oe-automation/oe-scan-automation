﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.DriveCrashInterface
{
    public class DriveCrashServiceController
    {
        public static bool RestartDSRS()
        {
            // EXE: DriveSafeReportingToolService.exe
            // NAME: Drive Safe Reporting Tool

            GlobalObjects.DebugHelper.WriteDebugLog("REBOOTING THE DRIVE SAFE OEM SERVICE TO AVOID PDF PERM ISSUES!");
            if (Debugger.IsAttached){ GlobalObjects.DebugHelper.WriteDebugLog("FOUND A DEBUGGER ON THIS SYSTEM!"); }

            if (!Debugger.IsAttached && !Environment.UserName.StartsWith("GDP"))
            {
                GlobalObjects.DebugHelper.WriteDebugLog("THIS ISNT A RELEASE DEVICE! SKIPPING THIS PROCESS");
                return true;
            }

            // 1/4 Changed to Drive Safe OEM Service.
            // 1/4 This still doesnt work? Trying a list of services now.
            ServiceController DSRTService = null;

            ServiceController[] ServicesInstalled = ServiceController.GetServices();
            foreach (var ServCtl in ServicesInstalled)
            {
                // Check each one here.
                // DriveSafeReportingToolService.exe

                if (ServCtl.ServiceName == "DriveSafe Reporting Tool" ||
                    ServCtl.DisplayName == "DriveSafe OEM Service")
                {
                    DSRTService = ServCtl;
                    GlobalObjects.DebugHelper.WriteDebugLog("FOUND A DSRT SERVICE INSTANCE OK!");
                    GlobalObjects.DebugHelper.WriteDebugLog("FOUND STATUS OF IT TO BE: " + ServCtl.Status);

                    break;
                }

                string ServName = ServCtl.ServiceName.ToUpper();
                string DispName = ServCtl.DisplayName.ToUpper();
                if (ServName.Contains("DRIVE") || DispName.Contains("DRIVE"))
                {
                    if (ServName.Contains("CRASH") || ServName.Contains("OEM") || ServName.Contains("SAFE")) { DSRTService = ServCtl; }
                    if (DispName.Contains("CRASH") || DispName.Contains("OEM") || DispName.Contains("SAFE")) { DSRTService = ServCtl; }

                    if (ServCtl != null) { break; }
                }
            } 

            if (DSRTService == null) 
            {
                try
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("CAN NOT FIND AN EXISTING DSRT SERVICE!");
                    GlobalObjects.DebugHelper.WriteDebugLog("TRYING TO MAKE AN UNVERRIFIED SERVICE INSTANCE");

                    DSRTService = new ServiceController("DriveSafe OEM Service");
                    GlobalObjects.DebugHelper.WriteDebugLog("SERVICE GENERATED: " + DSRTService.DisplayName);
                    GlobalObjects.DebugHelper.WriteDebugLog("SERVICE STATUS: " + DSRTService.Status);
                }
                catch (Exception ServEx)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("DSRS WAS NOT FOUND ON THIS DEVICE. CAN NOT STOP IT!");
                    GlobalObjects.DebugHelper.WriteDebugLog("THIS MEANS THE TOOL CAN NOT RELEASE ANY OLD PDF FILES!");
                    GlobalObjects.DebugHelper.WriteDebugLog(ServEx);
                    
                    return false;
                }
            }

            try
            {
                // Start tick timer and log how much time is to be elapsed.
                GlobalObjects.DebugHelper.WriteDebugLog("LOGGING TIME OF THE SERVICE STOP REQUEST");
                int StartMsCount = Environment.TickCount;
                TimeSpan TimeoutSpan = TimeSpan.FromMilliseconds(10000);
                GlobalObjects.DebugHelper.WriteDebugLog("TIME: " + TimeoutSpan.ToString("c"));

                // Stop service here.
                GlobalObjects.DebugHelper.WriteDebugLog("STOPPING INSTANCE OF THE DRIVE SAFE OEM TOOL");
                DSRTService.Stop();
                DSRTService.WaitForStatus(ServiceControllerStatus.Stopped, TimeoutSpan);

                // Count the rest of the timeout value here.
                GlobalObjects.DebugHelper.WriteDebugLog("LOGGING TIME AT STOP");
                int StopMsCount = Environment.TickCount;
                TimeoutSpan = TimeSpan.FromMilliseconds(10000 - (StopMsCount - StartMsCount));
                GlobalObjects.DebugHelper.WriteDebugLog("TIME: " + TimeoutSpan.ToString("c"));

                // Boot the service and wait to see its response. 
                GlobalObjects.DebugHelper.WriteDebugLog("STARING THE SERVICE NOW!");
                DSRTService.Start();
                DSRTService.WaitForStatus(ServiceControllerStatus.Running, TimeoutSpan);

                // Log it worked ok.
                GlobalObjects.DebugHelper.WriteDebugLog("RESTARTED THE OEM SERVICE OK!");

                return true;
            }
            catch (Exception servEx)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT REBOOT THE DRIVE SAFE REPORTING TOOL!", "ERROR");
                GlobalObjects.DebugHelper.WriteDebugLog("IF THIS IS BEING RUN ON A NON RLEASE DEVICE IGNORE THIS ERROR", "ERROR");
                GlobalObjects.DebugHelper.WriteDebugLog("ERROR: " + servEx.Message);
                GlobalObjects.DebugHelper.WriteDebugLog(servEx);

                return false;
            }
        }

    }
}
