﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using OEAutomationLauncher.OEOptionPicker;
using OEAutomationLauncher.AutomationLauncher.ObjectClasses;
using OEAutomationLauncher.AutomationLauncher.ConfirmPrompts;
using OEAutomationLauncher.DriveCrashInterface;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }

        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();

            GlobalObjects.AppStart = new StartupActions(this);
            GlobalObjects.AppStart.SetupGlobalObjects();

            if (!Debugger.IsAttached)
            {
                _ = new GlobalObjects(TaskGrid);
                GlobalObjects.SetupWebServerAPI();

                // Check arg and logic init.
                if (!GlobalObjects.AppStart.CheckValidOEArg()) { throw new Exception("OE TYPE COULD NOT BE PARSED KILLING NOW."); }
                GlobalObjects.AppStart.SetConfigTextBoxValues();
                GlobalObjects.AppStart.MiscLogicInit();

                // Remove the OPUS Holder
                HolderOpusGlobe.Visibility = Visibility.Hidden;

                // Run RPA Updater and start process.
                MainWindowViewHelpers.KickOffRPA(GlobalObjects.OEType);
            }
        }

        /// <summary>
        /// Shows the Confirm close/cancel OE Scan buttons and deals with the logic required to make
        /// those buttons show up.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void CancelAutomatedScanButton(object sender, RoutedEventArgs e)
        {
            // Setup Tier Autoclose.
            GlobalObjects.SetupAutoCloseTimer(typeof(ConfirmCancelScan));

            // Resize for CDP4
            if (!IsCDP4)
            {
                // Width = "450" Height = "165"
                ConfirmCancelButtonView.MainGrid.Width = 500;
                ConfirmCancelButtonView.MainGrid.Height = 175;
            }

            // Double Check with the user that we 100% want to terminate the scan process.
            string SenderContent = CancelOrStopOEScanButton.Content.ToString();
            if (SenderContent.Contains("Return To Home Screen"))
            {
                ConfirmCancelButtonView.ConfirmCloseTextBlock.Text =
                    "Are You Sure You Would Like To Return To The Main Menu?";
            }

            ConfirmCancelButtonView.ConfirmButtonsGrid.Visibility = Visibility.Visible;
            ConfirmCancelButtonView.ConfirmCloseTextBlock.Visibility = Visibility.Visible;

            ConfirmCancelButtonView.ClosingNowGrid.Visibility = Visibility.Hidden;
            ConfirmCancelButtonView.ClosingPleaseWaitBlock.Visibility = Visibility.Hidden;

            ConfirmCancelButtonView.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Makes a support ticket to the IVS360 support chat stuff. 
        /// Makes a ticket using a BL API call (DOES NOT WORK WITH THE SPOOFER)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContactOPUSIVS360Help(object sender, RoutedEventArgs e)
        {
            // NEED TO WORK IN API CALL HERE!!!!
            GlobalObjects.DebugHelper.WriteDebugLog("SHOWING HELP GRID VIEW NOW.");

            // Setup the default view type here
            GlobalObjects.TypeOfLastView = typeof(ContactIVS360Support);

            // Resize if on Drive
            if (!IsCDP4)
            {
                Contact360SupportView.MainGridLayout.Width = 900;
                Contact360SupportView.MainGridLayout.Height = 500;
            }

            // Show the window.
            Contact360SupportView.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Reads the options from the currently visible OE option view and saves them. 
        /// Kills the app when done.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ConfirmOptionsButton_Click(object sender, RoutedEventArgs e)
        {
            // Save the Options and then close out with code 0 (Exit OK)
            GlobalObjects.OEBase.SaveOptions();
            OptionsTriggered.OptionsComplete();
        }

        /// <summary>
        /// Forces this invoker to be always on top. Useful for keeping OEs hidden. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Deactivated(object sender, EventArgs e)
        {
            if (!Debugger.IsAttached && GlobalObjects.ForceOnTop)
            {
                Window window = (Window)sender;
                window.Topmost = true;
            }
        }

        /// <summary>
        /// Forces this invoker to be always on top. Useful for keeping OEs hidden. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (!Debugger.IsAttached && GlobalObjects.ForceOnTop)
            {
                Window window = (Window)sender;
                window.Topmost = true;
            }
        }

        /// <summary>
        /// Toggle out the window border or not by double clicking the VIN
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VIN_Block_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (GlobalObjects.CanResizeRelease || Debugger.IsAttached)
            {
                if (this.WindowStyle == WindowStyle.SingleBorderWindow)
                {
                    this.WindowStyle = WindowStyle.None;
                    this.WindowState = WindowState.Maximized;

                    return;
                }

                if (this.WindowStyle == WindowStyle.None)
                {
                    this.WindowStyle = WindowStyle.SingleBorderWindow;
                    this.WindowState = WindowState.Normal;
                }
            }
        }
    }
}
