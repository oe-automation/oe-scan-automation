﻿using OEAutomationLauncher.OEOptionPicker.OEApplicationObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OEAutomationLauncher.OEOptionPicker.MiscLogicHelpers
{ 
    // Contains basic vehicle config information. This class also parses out the basic info from the 
    // VIN_ENG_CONFIG file.
    public class VehicleConfigInfo_OptionsConfig
    {
        public string VIN { get; set; }
        public string Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }

        public string OptionsFileName { get; set; }
        public string OptionsFilePath { get; set; }

        public VehicleConfigInfo_OptionsConfig(string ConfigFilePath)
        {
            string[] FileContents = File.ReadAllLines(ConfigFilePath);

            var VIN_Results = OERegularExpressions.VINFinder.Matches(FileContents[0]);
            if (VIN_Results.Count == 0) { VIN = "UNKNWON VIN"; }
            else { VIN = VIN_Results[0].Groups[1].Value.ToUpper(); }

            var YMMResults = OERegularExpressions.YMMFinder.Matches(FileContents[1]);
            Make = YMMResults[0].Groups[1].Value.Trim().ToUpper();
            Model = YMMResults[0].Groups[2].Value.Trim().ToUpper();
            Year = YMMResults[0].Groups[3].Value.Trim().ToUpper();

            OptionsFileName = Make + "_" + Model + "_" + Year + ".txt";
            OptionsFilePath = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\" + OptionsFileName;
        }
    }
}
