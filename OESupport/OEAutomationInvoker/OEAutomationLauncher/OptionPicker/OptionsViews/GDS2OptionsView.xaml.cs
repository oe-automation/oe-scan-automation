﻿using OEAutomationLauncher.OEOptionPicker.OEApplicationObjects.OETargeted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OEAutomationLauncher.OEOptionPicker.OEOptionPickerViews
{
    /// <summary>
    /// Interaction logic for GDS2OptionsView.xaml
    /// </summary>
    public partial class GDS2OptionsView : UserControl
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }

        public GDS2OptionsView()
        {
            DataContext = this;
            InitializeComponent();
        }

        /// <summary>
        /// Setup the comboboxes for all GDS2 items here. 
        /// This function will work along with the grid generator function to make a new set of rows for all the option choices.
        /// </summary>
        /// <param name="AllOptions">List of all GDS2 options for the current vehicle.</param>
        public void SetOptionItems(List<GDS2ComboBoxItems> AllOptions)
        {
            // Setup autoclose timer object here for GDS2.
            GlobalObjects.SetupAutoCloseTimer(typeof(GDS2OptionsView));
            GlobalObjects.DebugHelper.WriteDebugLog("SETUP CLOSE TIMER AS A GDS2OPTIONSVIEW");

            if (!GlobalObjects.IsCDP4)
            {
                // DO RESIZE HERE Width="1000" Height="450"
                MainOptionsGridLayout.Width = 1000;
                MainOptionsGridLayout.Height = 815;

                OptionsGrid.ColumnDefinitions[1].Width = new GridLength(500, GridUnitType.Pixel);
            }

            // Resize grid to fit all options inside of it.
            OptionsGrid.Children.Clear();
            ResizeGridAndRows(AllOptions.Count);

            int counter = 0;
            foreach (var OptionSet in AllOptions)
            {
                // Store the options name and options values in temp variables for now.
                string OptionName = OptionSet.NameOfOption;
                var Options = OptionSet.Options;

                // Make a TextBlock item for the title.
                TextBlock TitleOfOption = new TextBlock();
                TitleOfOption.Text = OptionName;
                TitleOfOption.Style = this.FindResource("OptionNameTitle_GDS2") as Style;

                // Make a combobox object of the current options.
                ComboBox OptionsBox = new ComboBox();
                OptionsBox.ItemsSource = Options;
                OptionsBox.Style = this.FindResource("OptionComboBox_GDS2") as Style;

                // Set the grid posisitions here for the combobox and the textblocks.
                TitleOfOption.SetValue(Grid.RowProperty, counter);
                TitleOfOption.SetValue(Grid.ColumnProperty, 0);

                OptionsBox.SelectedIndex = 0;
                OptionsBox.SetValue(Grid.RowProperty, counter);
                OptionsBox.SetValue(Grid.ColumnProperty, 1);

                // Add the textblock and combobox to the grid now.
                OptionsGrid.Children.Add(TitleOfOption);
                OptionsGrid.Children.Add(OptionsBox);
                    
                // Increase the row counter here.
                counter++;
            }
        }

        /// <summary>
        /// Sets up the OptionsGrid to be the correct layout for the number of options the user can pick from.
        /// Scales by 75px per option. So 1 - 75, 2 - 150 etc.
        /// <paramref name="OptionsCount">Count of options to be sent into the grid</param>
        /// </summary>
        public void ResizeGridAndRows(int OptionsCount)
        {   
            // Clear out the old options grid layout and add new grid row items.
            // Dont resize under 300 if weve got less than 4 options here. 
            OptionsGrid.RowDefinitions.Clear();
            if (OptionsCount > 4) OptionsGrid.Height = OptionsCount * 75;
            else { OptionsGrid.Height = 270; OptionsCount = 3; }

            // Count off options and make grid row items here for it.
            for (int counter = OptionsCount; counter >= 0; counter--)
            {
                // Make a row definition here and set it to 1*
                RowDefinition nextRow = new RowDefinition();
                nextRow.Height = new GridLength(1.0, GridUnitType.Star);

                // Set the option row into the grid row definitions.
                OptionsGrid.RowDefinitions.Add(nextRow);
            }
        }

        /// <summary>
        /// Gets and returns the vlaues of the selected options. 
        /// Sends it back as a tring type.
        /// </summary>
        /// <returns>String value indiciating indicies of each one of the options picked.</returns>
        public string SaveOptions()
        {
            // Two strings to return connected by a "_"
            string IndexString = "";    // index of options picked (used by RPA)
            string ValueString = "";    // values of options picked (debug use only)

            // Loop all the child objects and find anything that is a combobox. 
            // On each combobox, get the selected index and the value of the box currently.
            foreach (var ChildObj in OptionsGrid.Children)
            {
                if (ChildObj is ComboBox)
                {
                    // Wrap object as a combobox for data extraction.
                    var ChildCombo = (ComboBox)ChildObj;

                    // Set the string and index values into the return strings.
                    IndexString += ChildCombo.SelectedIndex + "|";
                    ValueString += ChildCombo.Text + "|";
                }
            }

            // Remove the trailing posts (|)
            IndexString = IndexString.Substring(0, IndexString.Length - 1);
            ValueString = ValueString.Substring(0, ValueString.Length - 1);

            return IndexString + "_" + ValueString;
        }
    }
}
