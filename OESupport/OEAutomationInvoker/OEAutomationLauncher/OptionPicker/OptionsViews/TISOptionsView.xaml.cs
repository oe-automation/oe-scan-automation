﻿using OEAutomationLauncher.OEOptionPicker.OEApplicationObjects.OETargeted;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OEAutomationLauncher.OEOptionPicker.OEOptionPickerViews
{
    /// <summary>
    /// Interaction logic for TISOptionsView.xaml
    /// </summary>
    public partial class TISOptionsView : UserControl
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }

        // List of all config items for the TIS Comboboxes.
        public static List<TISComboBoxItem> ALL_CONFIGS;

        public TISOptionsView()
        {
            DataContext = this;
            InitializeComponent();
        }

        /// <summary>
        /// Sets the engine item type on the Engine Combobox.
        /// </summary>
        /// <param name="ALL_ITEMS">List of all engine config and option params for the vehicle</param>
        public void SetEngineItems(List<TISComboBoxItem> ALL_ITEMS)
        {
            if (!GlobalObjects.IsCDP4)
            {
                // DO RESIZE HERE
                MainOptionsGridTIS.Height = 400;
                MainOptionsGridTIS.Width = 900;

                MainOptionsGridTIS.ColumnDefinitions[1].Width = new GridLength(600, GridUnitType.Pixel);
            }

            ALL_CONFIGS = ALL_ITEMS;

            // Make a list of items for the engine combobox.
            var EngineNames = new List<string>();
            foreach (var set in ALL_CONFIGS) { EngineNames.Add(set.EngineName); }

            // Set the itemsource and update the index to trigger a seleciton changed event.
            ENGINE_COMBOBOX.ItemsSource = EngineNames;
            ENGINE_COMBOBOX.SelectedIndex = 0;

            //Setup the TIS Autoclose trigger
            GlobalObjects.SetupAutoCloseTimer(typeof(TISOptionsView));
            GlobalObjects.DebugHelper.WriteDebugLog("SETUP CLOSE TIMER AS A TISOPTIONSVIEW");
        }

        /// <summary>
        /// Sets the new engine type options on the view. Shows/hides needed comboboxes and titles.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ENGINE_COMBOBOX_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Grab the new engine config item.
            var currentOptions = ALL_CONFIGS[ENGINE_COMBOBOX.SelectedIndex];

            // Configure option 1 visibility, name, and combobox contents.
            OPT_1_COMBOBOX.Visibility = Visibility.Visible;
            OPT_1_COMBOBOX.ItemsSource = currentOptions.Options1;
            OPT_1_TITLE.Text = currentOptions.Options1Name;
            OPT_1_COMBOBOX.SelectedIndex = 0;
            if (OPT_1_COMBOBOX.Items.Count == 0) { OPT_1_COMBOBOX.Visibility = Visibility.Hidden; }

            // Configure option 2 visibility, name, and combobox contents.
            OPT_2_COMBOBOX.Visibility = Visibility.Visible;
            OPT_2_COMBOBOX.ItemsSource = currentOptions.Options2; 
            OPT_2_TITLE.Text = currentOptions.Options2Name;
            OPT_2_COMBOBOX.SelectedIndex = 0;
            if (OPT_2_COMBOBOX.Items.Count == 0) { OPT_2_COMBOBOX.Visibility = Visibility.Hidden; }

            // Configure option 3 visibility, name, and combobox contents.
            OPT_3_COMBOBOX.Visibility = Visibility.Visible;
            OPT_3_COMBOBOX.ItemsSource = currentOptions.Options3;
            OPT_3_TITLE.Text = currentOptions.Options3Name;
            OPT_3_COMBOBOX.SelectedIndex = 0;
            if (OPT_3_COMBOBOX.Items.Count == 0) { OPT_3_COMBOBOX.Visibility = Visibility.Hidden; }
        }

        /// <summary>
        /// Gets and returns the vlaues of the selected options. 
        /// Sends it back as a tring type.
        /// </summary>
        /// <returns>String value indiciating indicies of each one of the options picked.</returns>
        public string SaveOptions()
        {
            // Make combobox index string.
            string ComboBoxIndicies =
                ENGINE_COMBOBOX.SelectedIndex + "|" +
                OPT_1_COMBOBOX.SelectedIndex + "|" +
                OPT_2_COMBOBOX.SelectedIndex + "|" +
                OPT_3_COMBOBOX.SelectedIndex;

            // Make value selected string.
            string ValuesOfAll = 
                ENGINE_COMBOBOX.Text + "|" + 
                OPT_1_COMBOBOX.Text + "|" + 
                OPT_2_COMBOBOX.Text + "|" + 
                OPT_3_COMBOBOX.Text;

            // Combine and return them.
            return ComboBoxIndicies + "_" + ValuesOfAll;
        }
    }
}
