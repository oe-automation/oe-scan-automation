﻿using OEAutomationLauncher.OEOptionPicker.OEApplicationObjects.OETargeted;
using OEAutomationLauncher.OEOptionPicker.OEOptionPickerViews;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace OEAutomationLauncher.OEOptionPicker
{
    public class OptionsTriggered
    {
        public static void OptionsStart()
        {
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                GlobalObjects.MainWindowView.OptionsPickerButtons.Visibility = Visibility.Visible;
                GlobalObjects.MainWindowView.CancelScanGridButtons.Visibility = Visibility.Hidden;

                GlobalObjects.MainWindowView.TaskGrid.Visibility = Visibility.Hidden;
                GlobalObjects.MainWindowView.TaskGridScrollViewer.Visibility = Visibility.Hidden;
                GlobalObjects.MainWindowView.VINRequestGrid.Visibility = Visibility.Hidden;

                GlobalObjects.MainWindowView.OptionsSelectionGrid.Visibility = Visibility.Visible;

                GlobalObjects.DebugHelper.WriteDebugLog("CONFIRM BUTTON SHOWN. TASK GRID HIDDEN");
            });
        }

        public static void SetOEViewVisibility(string OEType)
        {
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                foreach (Control Child in GlobalObjects.MainWindowView.OptionsSelectionGrid.Children) { Child.Visibility = Visibility.Hidden; }

                GlobalObjects.IsOptionObject = true;
                if (OEType == "TIS")
                {
                    GlobalObjects.MainWindowView.TISView.Visibility = Visibility.Visible;
                    GlobalObjects.TISBase = new TISOEApplication(); 
                    GlobalObjects.DebugHelper.WriteDebugLog("TIS OE APP INSTANCE HAS BEEN CREATED");

                    GlobalObjects.SetupAutoCloseTimer(typeof(TISOptionsView));
                    GlobalObjects.DebugHelper.WriteDebugLog("SETUP CLOSE TIMER AS A TISOPTIONSVIEW");
                }

                if (OEType == "GDS2")
                {
                    GlobalObjects.MainWindowView.GDS2View.Visibility = Visibility.Visible;
                    GlobalObjects.GDS2Base = new GDS2OEApplication();
                    GlobalObjects.DebugHelper.WriteDebugLog("GDS2 OE APP INSTANCE HAS BEEN CREATED");

                    GlobalObjects.SetupAutoCloseTimer(typeof(GDS2OptionsView));
                    GlobalObjects.DebugHelper.WriteDebugLog("SETUP CLOSE TIMER AS A GDS2OPTIONSVIEW");
                }        
                
                GlobalObjects.DebugHelper.WriteDebugLog("FOR MORE DEBUG INFO: REFER TO THE OPTIONS CONFIG DEBUG FILE");
            });
        }

        public static string ReturnJSONOptions()
        {
            var ConfigLines = File.ReadAllText(GlobalObjects.OEBase.VIN_CONFIG);
            return "PARSED CONFIG INFO: " + ConfigLines;
        }

        public static void OptionsComplete()
        {
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                GlobalObjects.DebugHelper.WriteDebugLog("OPTION PICKING DONE. RESETING VIEWS TO MAIN/DEFAULTS HERE");

                GlobalObjects.MainWindowView.OptionsPickerButtons.Visibility = Visibility.Hidden;
                GlobalObjects.MainWindowView.CancelScanGridButtons.Visibility = Visibility.Visible;

                GlobalObjects.MainWindowView.OptionsSelectionGrid.Visibility = Visibility.Hidden;
                GlobalObjects.MainWindowView.VINRequestGrid.Visibility = Visibility.Hidden;

                GlobalObjects.MainWindowView.TaskGridScrollViewer.Visibility = Visibility.Visible;
                GlobalObjects.MainWindowView.TaskGrid.Visibility = Visibility.Visible;

                foreach (Control Child in GlobalObjects.MainWindowView.OptionsSelectionGrid.Children) { Child.Visibility = Visibility.Hidden; }

                GlobalObjects.DebugHelper.WriteDebugLog("RESET OK MOVING ON");
                GlobalObjects.DebugHelper.IsOptionDebug = false;
            });
        }
    }
}
