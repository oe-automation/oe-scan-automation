﻿using OEAutomationLauncher.OEOptionPicker;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.OEOptionPicker.OEApplicationObjects.OETargeted
// namespace OEOptionPicker.OEApplicationObjects.OETargeted
{
    /// <summary>
    /// Combobox items for a GDS2 instance. Used to populate the OptionsGrid.
    /// </summary>
    public class GDS2ComboBoxItems
    {
        public bool HasNotEquipped;
        public int IndexOfOption;
        public string NameOfOption;
        public List<string> Options;
    }

    /// <summary>
    /// Object that holds the config info for a requested engine type on a GDS2 instance.
    /// </summary>
    public class GDS2OEApplication : OEApplicationBase
    {
        private GDS2RegularExpressions Regexs;   // Regex object for parsing the config files out.
        public List<GDS2ComboBoxItems> AllOptions;  //List of all GDS2 Option objects.

        /// <summary>
        /// COnstructor for the GDS2 OE object. Initalizes the regex options for finding options.
        /// For all OEs, the regex class used will only contain the regex strings specific to that OE.
        /// Any functions run will run from the OERegularExpressions class and fed in an object/list of strings
        /// to use for the regexing.
        /// </summary>
        public GDS2OEApplication()
        {                        
            // Global Setup
            GlobalObjects.OEBase = this;
            GlobalObjects.GDS2Base = this;

            // Info for startup for a GDS2 OE APP.
            DebugHelper.WriteDebugLog("GDS2 OE APP INSTANCE MADE.");
            DebugHelper.WriteDebugLog("CREATING MAKE SPECIFIC INSTANCE NOW");

            // Unhide the GDS2 view object.
            GlobalObjects.MainWindowView.GDS2View.Visibility = System.Windows.Visibility.Visible;
            DebugHelper.WriteDebugLog("TOGGLED THE GDS2 OPTION VIEW TO ON");

            // Split our input options file into engine types.
            DebugHelper.WriteDebugLog("READING OPTIONS CONFIG FILE NOW");
            string AllTextInput = File.ReadAllText(OptionsFile);

            //REGEX OPERATIONS GO HERE.
            List<string> AllLines = new List<string>();
            List<int> OptionsRun = new List<int>();
            foreach (var lineSet in AllTextInput.Split('~'))
            {
                // If zero length move on from here.
                if (lineSet.Length == 0) { continue; }

                // Get option number and skip if it's already been done.
                int OptionNumber = 0;
                try { OptionNumber = int.Parse(lineSet.Split(']')[0].Split('_')[1]); }
                catch (Exception optEx)
                {
                    DebugHelper.WriteDebugLog(optEx, false, true);
                    OptionNumber = -1;
                }

                // Add to the list if we dont have entries matching that option index.
                // Also add to the list if the int parsing fails for some reason. Rare but possible.
                if (!OptionsRun.Contains(OptionNumber) || OptionNumber == -1) 
                {
                    // Add to option parse tracking list.
                    if (OptionNumber != -1) { OptionsRun.Add(OptionNumber); }

                    // Clean out the start of the options string set. Gets rid of the empty ones and the END CONFIG.
                    if (lineSet.Contains("END CONFIG")) { continue; }
                    if (lineSet.Trim().StartsWith("\r\n")) { AllLines.Add(lineSet.Substring(2)); continue; }
                    if (lineSet.Length >= 10) { AllLines.Add(lineSet); continue; }
                }
            }

            // Info on splitting text file input here.
            DebugHelper.WriteDebugLog("READ IN THE OPTIONS FILE AND SPLIT IT INTO: " + AllLines.Count + " OPTION CONFIGS");
            DebugHelper.WriteDebugLog("", "---");

            // Init a list of GDS2 Option boxes.
            AllOptions = new List<GDS2ComboBoxItems>();
            foreach (var Options in AllLines)
            {
                // Write info about loop to debug output.
                DebugHelper.WriteDebugLog("LOOP: " + (AllLines.IndexOf(Options) + 1) + " OF " + AllLines.Count + " RUNNING NOW.");

                // Call the option generator action. Prints out new instance to debug file.
                CleanUpAndAddOption(Options);
            }

            // Set the GDS2 view options to our new config files. Then load in a new instance of it.
            GlobalObjects.MainWindowView.GDS2View.SetOptionItems(AllOptions);
            DebugHelper.WriteDebugLog("ADDED CONFIGS TO THE GDS2 VIEW AND SET IT TO VISIBLE. MAIN WINDOW SHOULD NOW SHOW TIS CONFIG INFO");

            // Write out our cleaned up debug file here (options debug info for our help json uploads)
            GenerateCleanDebugFile(AllOptions);
            DebugHelper.WriteDebugLog("WROTE OUT A CLEANED DEBUG INFO FILE FOR ONLY THE OPTIONS PICKED FOR THIS VEHICLE");

            // Set the global GDS2 object here
            GlobalObjects.GDS2Base = this;
            DebugHelper.WriteDebugLog("SET GLOBAL GDS2 OBJECT TO THIS CURRENT INSTANCE. SHOULD BE A GLOBALLY USABLE OBJECT NOW.");
        }

        /// <summary>
        /// Cleans out the a list of combobox raw strings and formatts it into an options format.
        /// </summary>
        /// <param name="Options">String of options file lines</param>
        /// <param name="AddToList">Bool to indicate if we want to add this to the list of all options or not.</param>
        /// <returns>List of string values that has the options in their final format.</returns>
        public List<string> CleanUpAndAddOption(string Options, bool AddToList = true)
        {
            // Init a regex object here to pull out strings.
            Regexs = new GDS2RegularExpressions();

            DebugHelper.WriteDebugLog("GDS2 REGEX OBJECT INITALIZED");
            DebugHelper.WriteDebugLog("ORIGINAL LINE LENGTH --> " + Options.Length);

            // Save the cleaned out versions result and set the title of the optiosn to a string value.
            List<string> Result = Regexs.CleanUpOptionsLines(Options, out string NextTitle);
            bool NotEquipped = false;
            if (AddToList) 
            {
                // Adding to list process. Check all existing option objects and compare the items in each
                // option list. If we have something in common, remove it. Otherwise just move on to the next item
                DebugHelper.WriteDebugLog("REMOVING DUPLICATE OBJECT ENTRIES FROM THE NEW LIST");
                foreach (var OptionItem in AllOptions)
                {
                    DebugHelper.WriteDebugLog("CHECKING LIST: " + OptionItem.NameOfOption);

                    // Get the current item in  the current options object list.
                    var ExistingOptions = OptionItem.Options;
                    foreach (var stringItem in ExistingOptions)
                    {
                        // Remove if found in the list to modify out.
                        if (Result.Contains(stringItem))
                        {                            
                            // Make this work so that it keeps items which have Not Equiped in the name in the lists.
                            if (stringItem != "NOT EQUIPPED") { Result.Remove(stringItem); } 
                            if (stringItem == "NOT EQUIPPED") { NotEquipped = true; }
                        }
                    }
                }

                // Add to the global list of options here and debug out.
                var NextItem = new GDS2ComboBoxItems 
                { 
                    HasNotEquipped = NotEquipped, 
                    NameOfOption = NextTitle, 
                    Options = Result, 
                    IndexOfOption = AllOptions.Count 
                };

                if (NextItem.Options.Count > 1) { AllOptions.Add(NextItem); }

                // Print out the config of the item here.
                DebugHelper.WriteDebugLog("ADDED NEW OPTION OBJECT TO LIST OF ALL OBJECTS. CONFIG INFO IS BELOW");
                ReadOutAll(NextItem);
            }

            // Inform of ok options extraction and write to debug file.
            DebugHelper.WriteDebugLog("OPTION NAME: " + NextTitle + "(IF THIS IS PLEASE SELECT AN OPTION TITLE WAS NOT FOUND)");
            DebugHelper.WriteDebugLog("EXTRACTED A TOTAL OF " + Result.Count + " OPTIONS FROM THE INPUT LINES");
            DebugHelper.WriteDebugLog("", "---");

            return Result;
        }

        /// <summary>
        /// Saves the values of the OE selected for the current vehicle and writes them to a file. 
        /// </summary>
        public void SaveOEOptions()
        {
            // Get the current options from the GDS2 view object we made in the constructoir.
            DebugHelper.WriteDebugLog("SAVING OPTIONS FOR THIS GDS2 INSTANCE. GETTING VALUES FROM UI VIEW NOW.");
            var OptionsSelected = GlobalObjects.MainWindowView.GDS2View.SaveOptions();

            // Split the returned result into two parts. index string and the value string.
            string IndexString = OptionsSelected.Split('_')[0];
            string ValueString = OptionsSelected.Split('_')[1];
            DebugHelper.WriteDebugLog("GOT ALL OPTIONS BACK. THE INDEX VALUES ARE: " + IndexString);
            DebugHelper.WriteDebugLog("FOR THIS INSTANCE, STRING VALUES SAVED ARE: " + ValueString);

            // Write out the debug config info here. Save to file and notify of good run
            File.WriteAllText(OptionsChosen, IndexString);
            DebugHelper.WriteDebugLog("WROTE OPTIONS OUT TO THE CONFIG FILE OK!.", "DONE");
            DebugHelper.WriteDebugLog("---------------------------------", "---");
        }

        /// <summary>
        /// Prints out the config info and setup of a GDS2 Options object to the debug file once created.
        /// </summary>
        /// <param name="CurrentItem">GDS2ComboBoxItem that has options, a name for the option, and the index  from the original file</param>
        public void ReadOutAll(GDS2ComboBoxItems CurrentItem)
        {
            // Write the Options info title and combobox out to the debug file.
            // Skip it if the config list is empty somehow.
            if (CurrentItem.Options.Count > 0)
            {
                string OptLogHead = "OPT_" + CurrentItem.IndexOfOption;

                DebugHelper.WriteDebugLog("OPTION: " + CurrentItem.NameOfOption + " (INDEX: " + CurrentItem.IndexOfOption + ")", OptLogHead);
                foreach (var Item in CurrentItem.Options) { DebugHelper.WriteDebugLog("\\__ " + Item, OptLogHead); }
            }
        }

        /// <summary>
        /// Used to print out the debug info for optiosn to a cleaned text file for when we do json debug uploads.
        /// </summary>
        /// <param name="CurrentItems">List of all GDS2ComboBoxItems that are being passed around.</param>
        public void GenerateCleanDebugFile(List<GDS2ComboBoxItems> CurrentItems)
        {
            // Set the cleaned output file and delete any old versions of it.
            string OutputFile = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\CLEAN_" + ConfigInfo.OptionsFileName;
            if (File.Exists(OutputFile)) { File.Delete(OutputFile); }

            // Write File headding.
            string ConfigValues = "[" + ConfigInfo.Make + " - " + ConfigInfo.Model + " - " + ConfigInfo.Year + "]\n\n";
            File.AppendAllText(OutputFile, ConfigValues);

            // Loop the options items and print them to the file for debugging.
            foreach (var CurrentItem in CurrentItems)
            {
                // Write the Options info title and combobox out to the debug file.
                // Skip it if the config list is empty somehow.
                if (CurrentItem.Options.Count > 0)
                {
                    File.AppendAllText(OutputFile, "OPTION: " + CurrentItem.NameOfOption + " (INDEX: " + CurrentItem.IndexOfOption + ")\n");
                    foreach (var Item in CurrentItem.Options) { File.AppendAllText(OutputFile, "\t\\__ " + Item + "\n"); }

                    File.AppendAllText(OutputFile, "\n");
                }
            }

            string BaseGMFiles = @"C:\Program Files (x86)\OESupport\VehicleOptions\" + ConfigInfo.Make.ToUpper();
            File.Copy(OutputFile, BaseGMFiles);
        }
    }
}
