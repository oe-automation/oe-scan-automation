﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OEAutomationLauncher.OEOptionPicker.OEApplicationObjects.OETargeted
// namespace OEOptionPicker.OEApplicationObjects.OETargeted
{
    /// <summary>
    /// Object that holds the config info for a requested engine type on a TIS instance.
    /// </summary>
    public class TISComboBoxItem
    {
        public string EngineName;   // Name of the engine type.

        public List<string> Options1, Options2, Options3;   // Options for the current engine.
        public string Options1Name, Options2Name, Options3Name; // Names of each of the option types.

        public bool NoOptions = false;

        /// <summary>
        /// Constructor for the TIS combobox item.
        /// </summary>
        /// <param name="engine">Engine type</param>
        /// <param name="opt1">Option one config list</param>
        /// <param name="opt2">Option two config list</param>
        /// <param name="opt3">Option three config list</param>
        public TISComboBoxItem(string engine, List<string> opt1, List<string> opt2, List<string> opt3)
        {
            EngineName = engine;
            Options1 = opt1;
            Options2 = opt2;
            Options3 = opt3;

            // Set the names of the option comboboxes.
            Options1Name = SetComboBoxName(Options1);
            Options2Name = SetComboBoxName(Options2);
            Options3Name = SetComboBoxName(Options3);

            if (Options1Name == null && Options2Name == null && Options3Name == null) { NoOptions = true; }
        }

        /// <summary>
        /// Function to set name values of the combobox objects based on the box contents.
        /// </summary>
        /// <param name="OptionsList">List of options for the combobox.</param>
        /// <returns></returns>
        public string SetComboBoxName(List<string> OptionsList)
        {
            if (OptionsList.Count != 2) { return ""; }
            else
            {
                // Check the two list values and look for w/ w/o or OTHER.
                string BaseOpt = OptionsList[0].ToUpper().Trim();
                string OtherOpt = OptionsList[1].ToUpper().Trim();

                // Set the name to the base option.
                if (BaseOpt.StartsWith("W/") || OtherOpt.StartsWith("OTHER"))
                {
                    string Name = BaseOpt.ToUpper();
                    Name = Name.Replace("W/", String.Empty).Trim();

                    return Name;
                }

                // If its not in the pattern above return back A OR B
                else { return BaseOpt + " OR " + OtherOpt; }
            }
        }
    }

    public class TISOEApplication : OEApplicationBase
    {
        private TISRegularExpressions Regexs;   // Regex object for parsing the config files out.
        public List<TISComboBoxItem> AllConfigurations;    // Object that holds a list of Engine/Option matches.

        /// <summary>
        /// COnstructor for the TIS OE object. Initalizes the regex options for finding options.
        /// For all OEs, the regex class used will only contain the regex strings specific to that OE.
        /// Any functions run will run from the OERegularExpressions class and fed in an object/list of strings
        /// to use for the regexing.
        /// </summary>
        public TISOEApplication()
        {
            // Global Setup
            GlobalObjects.OEBase = this;
            GlobalObjects.TISBase = this;

            // Info for startup for a TIS OE APP.
            DebugHelper.WriteDebugLog("TIS OE APP INSTANCE MADE.");
            DebugHelper.WriteDebugLog("CREATING MAKE SPECIFIC INSTANCE NOW");

            // Unhide the TIS view object.
            GlobalObjects.MainWindowView.TISView.Visibility = System.Windows.Visibility.Visible;
            DebugHelper.WriteDebugLog("TOGGLED THE TIS OPTION VIEW TO ON");

            // Split our input options file into engine types.
            DebugHelper.WriteDebugLog("READING OPTIONS CONFIG FILE NOW");
            string AllTextInput = File.ReadAllText(OptionsFile);

            List<string> AllLines = new List<string>();
            foreach (var lineSet in AllTextInput.Split('~')) 
            {
                if (lineSet.Length == 0) { continue; }
                if (lineSet.Trim().Length == 0) { continue; }
                AllLines.Add(lineSet);
            }

            // Info on splitting input text file.
            DebugHelper.WriteDebugLog("READ IN THE OPTIONS FILE AND SPLIT IT INTO: " + AllLines.Count + " MOTOR OPTIONS FOR THIS TIS INSTANCE");
            DebugHelper.WriteDebugLog("", "---");

            // Init our matching object and run the matching functions. Loop this for all sets of options.
            AllConfigurations = new List<TISComboBoxItem>();
            foreach (var Options in AllLines)
            {
                // Write info about loop to debug output.
                DebugHelper.WriteDebugLog("LOOP: " + (AllLines.IndexOf(Options) + 1) + " OF " + AllLines.Count + " RUNNING NOW.");

                // Call the option generator action. Prints out new instance to debug file.
                ReadOutTISCreartion(Options);
            }

            // Set the TIS view options to our new config files. Then load in a new instance of it.
            GlobalObjects.MainWindowView.TISView.SetEngineItems(AllConfigurations);
            DebugHelper.WriteDebugLog("ADDED CONFIGS TO THE TIS VIEW AND SET IT TO VISIBLE. MAIN WINDOW SHOULD NOW SHOW TIS CONFIG INFO");

            // Set the global TIS object here
            GlobalObjects.TISBase = this;
            DebugHelper.WriteDebugLog("SET GLOBAL TIS OBJECT TO THIS CURRENT INSTANCE. SHOULD BE A GLOBALLY USABLE OBJECT NOW.");

            if (AllConfigurations.Count == 1)
            {
                var OptSet = AllConfigurations[0];
                if (OptSet.NoOptions) 
                {
                    DebugHelper.WriteDebugLog("ONLY ONE ENGINE CONFIG ITEM FOUND AND IT DOES NOT HAVE ANY OPTIONS. SHOWING NO OPTIONS PROMPT.");

                }
            }
        }

        /// <summary>
        /// Saves the values of the OE selected for the current vehicle and writes them to a file. 
        /// </summary>
        public void SaveOEOptions()
        {
            // Get the current options from the TIS view object we made in the constructoir.
            DebugHelper.WriteDebugLog("SAVING OPTIONS FOR THIS TIS INSTANCE. GETTING VALUES FROM UI VIEW NOW.");
            var OptionsSelected = GlobalObjects.MainWindowView.TISView.SaveOptions();

            // Split the returned result into two parts. index string and the value string.
            string IndexString = OptionsSelected.Split('_')[0];
            string ValueString = OptionsSelected.Split('_')[1];
            DebugHelper.WriteDebugLog("GOT ALL OPTIONS BACK. THE INDEX VALUES ARE: " + IndexString);
            DebugHelper.WriteDebugLog("FOR THIS INSTANCE, STRING VALUES SAVED ARE: " + ValueString);

            // Write out the debug config info here. Save to file and notify of good run
            File.WriteAllText(OptionsChosen, IndexString);
            DebugHelper.WriteDebugLog("WROTE OPTIONS OUT TO THE CONFIG FILE OK!.", "DONE");
            DebugHelper.WriteDebugLog("---------------------------------", "---");
        }

        /// <summary>
        /// Generates and adds/ignoires a new NextEngineSet.
        /// </summary>
        /// <param name="Options">Options file to parse out of</param>
        /// <param name="AddToList">Add new options object to the static list of them.</param>
        public void ReadOutTISCreartion(string Options, bool AddToList = true)
        {
            // Make regex object and get matches here.
            Regexs = new TISRegularExpressions(Options);
            DebugHelper.WriteDebugLog("MADE REGEX OBJECT FOR TIS INSTANCE AND READ IN OUR CONFIG FILE AND OPTIONS.");
            DebugHelper.WriteDebugLog("CREATED MATCH SETS FOR ALL REGEX STRINGS OK");

            // Check for successful matching in place.
            DebugHelper.WriteDebugLog("IF ONE OF THE FOLLOWING IS FALSE, THAT MEANS THE VEHICLE DOES NOT HAVE OPTIONS FOR THAT INDEX.");
            DebugHelper.WriteDebugLog("IF ENGINE IS NULL, THERE IS A MASSIVE ISSUE!!");

            DebugHelper.WriteDebugLog("ENGINE MATCHING: " + (Regexs.EngineMatches.Count > 0).ToString());
            DebugHelper.WriteDebugLog("FOUND OPTIONS 1: " + (Regexs.Option1Matches.Count > 0).ToString());
            DebugHelper.WriteDebugLog("FOUND OPTIONS 2: " + (Regexs.Option2Matches.Count > 0).ToString());
            DebugHelper.WriteDebugLog("FOUND OPTIONS 3: " + (Regexs.Option3Matches.Count > 0).ToString());

            // Make the combobox item here and add it to the list of all engine option combos.
            var NextEngineOptionSet = new TISComboBoxItem(
                Regexs.EngineMatches[0].Groups[1].Value.ToUpper(),
                Regexs.ReturnOption(Regexs.Option1Matches),
                Regexs.ReturnOption(Regexs.Option2Matches),
                Regexs.ReturnOption(Regexs.Option3Matches)
            );

            if (AddToList)
            {
                DebugHelper.WriteDebugLog("ADDED A NEW TISCOMBOBOXITEM TO THE LIST OF ALL CONFIGURATIONS. CONFIG IS AS SHOWN BELOW: ");
                AllConfigurations.Add(NextEngineOptionSet);
            }

            ReadOutAll(NextEngineOptionSet);
            DebugHelper.WriteDebugLog("", "---");
        }

        /// <summary>
        /// Prints out all config items to the debug file for a specified combobox.
        /// </summary>
        /// <param name="NextItem">TIS Config item that has the three option lists, names, and engine type.</param>
        public void ReadOutAll(TISComboBoxItem NextItem)
        {
            // Write engine name.
            DebugHelper.WriteDebugLog("ENGINE: " + NextItem.EngineName, "ENG");
            
            // Write the Options 1 combobox options and name.
            if (NextItem.Options1.Count > 0)
            {
                DebugHelper.WriteDebugLog("OPTIONS 1 - " + NextItem.Options1Name, "OPT");
                string opt1String = "";
                foreach (var Item in NextItem.Options1) { opt1String += Item + " | "; }
                DebugHelper.WriteDebugLog("\\__ " + opt1String, "OP1");
            }

            // Write the Options 2 combobox options and name.
            if (NextItem.Options2.Count > 0)
            {
                DebugHelper.WriteDebugLog("OPTIONS 2 - " + NextItem.Options2Name, "OPT");
                string opt2String = "";
                foreach (var Item in NextItem.Options2) { opt2String += Item + " | "; }
                DebugHelper.WriteDebugLog("\\__ " + opt2String, "OP2");
            }

            // Write the Options 3 combobox options and name.
            if (NextItem.Options3.Count > 0)
            {
                DebugHelper.WriteDebugLog("OPTIONS 3 - " + NextItem.Options3Name, "OPT");
                string opt3String = "";
                foreach (var Item in NextItem.Options3) { opt3String += Item + " | "; }
                DebugHelper.WriteDebugLog("\\__ " + opt3String, "OP3");
            }
        }
    }
}
