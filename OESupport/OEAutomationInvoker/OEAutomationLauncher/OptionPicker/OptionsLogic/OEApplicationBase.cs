﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using OEAutomationLauncher.OEOptionPicker.MiscLogicHelpers;
using OEAutomationLauncher.OEOptionPicker;
using System.Net;
using OEAutomationLauncher.AutomationLauncher.DebugHelpers;

namespace OEAutomationLauncher.OEOptionPicker.OEApplicationObjects
// namespace OEOptionPicker.OEApplicationObjects
{
    public class OEApplicationBase
    {
        public DebuggingHelper_Invoker DebugHelper;             // Debug file helper.
        public VehicleConfigInfo_OptionsConfig ConfigInfo;      // Basic vehicle config information. VIN YEAR MAKE MODEL etc.

        // Options base DIR and the DIR for the current vehicle in question.
        public string OptionsDir = @"C:\Program Files (x86)\OESupport\VehicleOptions";
        public string CurrentCar = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle";

        // VIN_ENG_CONFIG file used to help kick off this objects creation along with the 
        // Output file for indicies of the options we picked.
        // The options file is the base file with all the raw unformatted data from the OE application's option windows/prompts.
        public string VIN_CONFIG = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\VIN_CONFIG.txt";
        public string OptionsFile = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\";
        public string OptionsChosen = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\SelectedOptions.txt";

        /// <summary>
        /// Constructor for an OEApplication Base file.
        /// </summary>
        public OEApplicationBase()
        {
            SetupLogic();           // Sets up the logical helpers used in  this program (Device Constants, Debug File Help, ConfigInfo etc)
            SetupConfigFile();      // If the VIN Eng Config file isnt found this needs to terminate. We cant move on without it.
            SetupOptionsFile();     // Set the path for our OptionsChosen file now using the YMM info from above.
        }

        /// <summary>
        /// Initalize class helpers (DeviceConstants, GUI helpers, Debug Logging etc)
        /// </summary>
        private void SetupLogic()
        {
            // If our debugger on the invoker is null make a new one here.
            if (GlobalObjects.DebugHelper == null) { DebugHelper = new DebuggingHelper_Invoker(); }

            // Setup debug logger to log into invoker file and standalone file.
            DebugHelper = GlobalObjects.DebugHelper;

            // Info debug for GUI HELPER and Device Constants initalized.
            DebugHelper.WriteDebugLog("DEBUG HELPER INITALIZED.", "INFO");
            DebugHelper.WriteDebugLog("DEVICE CONSTANTS GUI HELPER INITALIZED.", "INFO");
        }

        /// <summary>
        /// Setup config file found in the VIN_CONFIG file. Populates a new VehicleConfigInfo object. 
        /// </summary>
        /// <returns>A boolean to show if our config file was found or not.</returns>
        private bool SetupConfigFile()
        {
            if (!File.Exists(VIN_CONFIG))
            {
                DebugHelper.WriteDebugLog("VIN CONFIG FILE WAS NOT FOUND. TERMINATING.", "ERROR");
                DebugHelper.WriteDebugLog("VIN_CONFIG FILE NOT FOUND. BASE CLASS CAN NOT BE INITALIZED WITHOUT THIS.", "TRACE");

                // Throws a custom exception. This is going to be modified later on to work into it's own class (maybe...)
                // throw new Exception("VIN_CONFIG FILE NOT FOUND. BASE CLASS CAN NOT BE INITALIZED WITHOUT THIS.");

                // This now is fucking horid to do without catching anywhere. Log and fail out. somehow.....
                // OOOOOOOOOOOOO I CAN CALL SCAN FAILED OK NOW!!!
                // TODO: ZW - 11/6 - MAKE THAT HAPPEN DICKFACE^
                // TODO: ZW - 11/12 - JUST USE THE FUCKING API CALL STOP MAKING COMMENTS ITS LEGIT A ONE LINER
                // TODO: ZW - 11/18 - OK THIS IS JUST NOT OK ANYMORE. THREE FUCKING APPENDS? REALLY? THREE?
                // DONE: ZW - 11/20 - HAAAAA I FINALLY DID IT! ONLY TOOK 4 COMPLAINTS!

                // This calls the scan failed ok API Endpoint to trigger a failed scan.
                _ = new WebClient().DownloadString("http://localhost:57570/OEAutomationTasker/ScanComplete/ScanToFailureOK/");
            }

            // Generate the new ConfigInfo object and log out the information needed on the vehicle we have here.
            ConfigInfo = new VehicleConfigInfo_OptionsConfig(VIN_CONFIG);

            DebugHelper.WriteDebugLog("VIN ENGINE FILE LOADED AND PARSED OUT.", "INFO");
            DebugHelper.WriteDebugLog("\\__ VIN:   " + ConfigInfo.VIN, "INFO");
            DebugHelper.WriteDebugLog("\\__ YEAR:  " + ConfigInfo.Year, "INFO");
            DebugHelper.WriteDebugLog("\\__ MAKE:  " + ConfigInfo.Make, "INFO");
            DebugHelper.WriteDebugLog("\\__ MODEL: " + ConfigInfo.Model, "INFO");

            return true;
        }

        /// <summary>
        /// Setup the options input file.
        /// </summary>
        /// <returns>A bool which indicates if the options setup file was found or not.</returns>
        private bool SetupOptionsFile()
        {
            OptionsFile = ConfigInfo.OptionsFilePath;
            if (!File.Exists(OptionsFile))
            {
                try { File.Copy(OptionsDir + "\\" + ConfigInfo.Make + "\\" + ConfigInfo.OptionsFileName, OptionsFile); }
                catch (Exception copyEx)
                {

                    DebugHelper.WriteDebugLog("OPTIONS CONFIG FILE WAS NOT FOUND. TERMINATING.", "ERROR");
                    DebugHelper.WriteDebugLog("OPTIONS CONFIG FILE NOT FOUND. BASE CLASS CAN NOT BE INITALIZED WITHOUT THIS.", "TRACE");
                    DebugHelper.WriteDebugLog(copyEx, false, true);

                    // Throws a custom exception. This is going to be modified later on to work into it's own class (maybe...)
                    throw new Exception("OPTIONS CONFIG FILE NOT FOUND. BASE CLASS CAN NOT BE INITALIZED WITHOUT THIS.");
                }
            }

            // Copy the options file over to current vehicle.
            try { File.Copy(OptionsFile, OptionsDir + "\\" + ConfigInfo.Make + "\\" + ConfigInfo.OptionsFileName); }
            catch (Exception copyEx)
            { 
                DebugHelper.WriteDebugLog("CONFIG FILE ALREADY PRESENT. DELETING AND RECOPYING.");
                DebugHelper.WriteDebugLog(copyEx, false, true);

                File.Delete(OptionsDir + "\\" + ConfigInfo.Make + "\\" + ConfigInfo.OptionsFileName);
                File.Copy(OptionsFile, OptionsDir + "\\" + ConfigInfo.Make + "\\" + ConfigInfo.OptionsFileName); 
            }

            DebugHelper.WriteDebugLog("OPTIONS CONFIG FILE WAS FOUND OK! LOADING AND BEGINING REGEX OPERATIONS NOW.", "INFO");
            return true;
        }

        /// <summary>
        /// Base Save operation. This gets the type of the Derived OE class and calls the save method for the derivation of the 
        /// OE base object. Not sure if theres a better way to do this or not but thats how im doin it so eh.
        /// </summary>
        public void SaveOptions()
        {
            DebugHelper.WriteDebugLog("PREPARING TO SAVE OPTIONS CONFIG NOW");

            var typeOfOE = GlobalObjects.OEBase.GetType();
            string OEName = typeOfOE.Name;
            DebugHelper.WriteDebugLog("GOT TYPE OF OE BASE OBJECT FROM THE GLOBAL VARIABLES. TYPE IS: " + OEName);

            if (OEName.Contains("TIS")) 
            { 
                if (GlobalObjects.TISBase == null) { GlobalObjects.TISBase = new OETargeted.TISOEApplication(); }
                GlobalObjects.TISBase.SaveOEOptions(); 
            }

            if (OEName.Contains("GDS2")) 
            { 
                if (GlobalObjects.GDS2Base == null) { GlobalObjects.GDS2Base = new OETargeted.GDS2OEApplication(); }
                GlobalObjects.GDS2Base.SaveOEOptions(); 
            }

            else { GlobalObjects.DebugHelper.WriteDebugLog("OE TYPE WAS WRONG. CANT SAVE OPTIONS SINCE NO OVERLOAD HAS BEEN GENRATED YET!");  }
        }
    }
}

