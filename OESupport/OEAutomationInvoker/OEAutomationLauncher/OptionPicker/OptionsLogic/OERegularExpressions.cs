﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OEAutomationLauncher.OEOptionPicker.OEApplicationObjects
{
    /// <summary>
    /// Base class for all regex operations. The VIN and YMM are static since they are used more
    /// than once in non instance sitiuations. Just easier to make it static than type it all out.
    /// </summary>
    public class OERegularExpressions
    {
        // Important File Locations
        // Old VIN_CONFIG was the VIN_ENG_CONFIG file which has since been rewritten to only be VIN_CONFIG since ENG was TIS specific.
        public static string VIN_CONFIG = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\VIN_CONFIG.txt";
        public string ConfigFileLines;

        // Regex strings used for the YMM and VIN parsing actions.
        public static Regex VINFinder = new Regex(@"\[((?>[A-Za-z0-9]){17})");
        public static Regex YMMFinder = new Regex(@"\[([A-Za-z]+) - ([A-Za-z0-9 ]+) - ([0-9]+)]");

        public OERegularExpressions()
        {
            // Reads in the Config file information and saves it.
            ConfigFileLines = File.ReadAllText(VIN_CONFIG);
        }
    }

    /// <summary>
    /// This class is used for the Toyota options picker.
    /// It contains the regexs made for toyota only and includes Engine option regexs.
    /// </summary>
    public class TISRegularExpressions : OERegularExpressions
    {
        // PUT REGEX SHIT HERE!!!
        // VIN and config info are already loaded in so no need to include those old regex values here anymore.
        private Regex Engine = new Regex(@"\[ENGINE] {([A-Za-z0-9-]+)}");   // Engine Regex for TIS.

        private Regex Option1 = new Regex(@"\[OPTN_1] {([0-9A-Za-z \/]+)\|([0-9A-Za-z \/]+)}");     // Options config regex for TIS
        private Regex Option2 = new Regex(@"\[OPTN_2] {([0-9A-Za-z \/]+)\|([0-9A-Za-z \/]+)}");     // Options config regex for TIS
        private Regex Option3 = new Regex(@"\[OPTN_3] {([0-9A-Za-z \/]+)\|([0-9A-Za-z \/]+)}");     // Options config regex for TIS

        // Hold the match collection objects here.
        public MatchCollection EngineMatches;
        public MatchCollection Option1Matches;
        public MatchCollection Option2Matches;
        public MatchCollection Option3Matches;

        public TISRegularExpressions(string OptionsFileLines)
        {
            // Get our matches here.
            EngineMatches = Engine.Matches(OptionsFileLines);    // Match engine type 

            Option1Matches = Option1.Matches(OptionsFileLines);  // Match Option 1
            Option2Matches = Option2.Matches(OptionsFileLines);  // Match Option 2
            Option3Matches = Option3.Matches(OptionsFileLines);  // Match Option 3
        }

        /// <summary>
        /// Gets the values of the matchcolleciton and sends it out as a list object.
        /// </summary>
        /// <param name="optionPicked">MatchCollection for the object picked that was sent in.</param>
        /// <returns>List of string values containing objects to pick from.</returns>
        public List<string> ReturnOption(MatchCollection optionPicked)
        {
            // If there were no matches return null.
            if (optionPicked.Count == 0 ) { return new List<string>(); }

            string Option1 = optionPicked[0].Groups[1].Value.ToUpper();
            string Option2 = optionPicked[0].Groups[2].Value.ToUpper();

            // Form a new list and return it out with the two options to pick from.
            return new List<string> { Option1, Option2 };
        }
    }

    /// <summary>
    /// This class is used for the GDS2 options picker.
    /// It contains the regexs made for GDS2 only and also includes methods needed to remove 
    /// unwanted shit from the options lists.
    /// </summary>
    public class GDS2RegularExpressions : OERegularExpressions
    {
        public Regex ExtractAllText = new Regex(@"([0-9a-zA-Z()_, -.]+)");
        public List<string> IgnoreThese = new List<string>
        {
            "sTagRPOSelectedIconTitle",
            "decrement",
            "increment", 
        };

        // PUT REGEX SHIT HERE!!!

        public List<string> CleanUpOptionsLines(string NextLines, out string OptionTitle)
        {
            OptionTitle = "";
            string SpacesRemoved = NextLines.Split('{')[1].Split('}')[0];

            foreach (var ignoreThis in IgnoreThese) { SpacesRemoved = SpacesRemoved.Replace(ignoreThis, String.Empty); }

            var ReturnList = new List<string>();
            bool NotEquipped = false;

            foreach (Match stringMatch in ExtractAllText.Matches(SpacesRemoved))
            {
                string AddThis = stringMatch.Value.Trim().ToUpper();
                if (AddThis.Length == 0) { continue; }

                if (AddThis.StartsWith("NOT_EQUIPPED"))
                {
                    if (AddThis.Contains("TRUE")) { ReturnList.Add("NOT EQUIPPED"); NotEquipped = true; }
                    if (AddThis.Contains("FALSE")) { NotEquipped = false; }

                    continue;
                }

                if (AddThis.Contains("..")) { continue; }

                if (AddThis.Contains("PLEASE"))
                {
                    OptionTitle = AddThis.Split('-')[0];
                    continue;
                }

                if (AddThis.Contains("NOT EQUIPPED"))
                {
                    if (NotEquipped) { ReturnList.Add(AddThis); }
                    continue;
                }

                if (AddThis.Contains("NCODE"))
                {
                    AddThis = AddThis.Substring(1);
                }

                ReturnList.Add(AddThis);
            }

            if (OptionTitle.Length == 0) { OptionTitle = "Please Select An Option".ToUpper(); }
            if (OptionTitle.ToUpper().Contains("SEAT MEMORY"))
            {
                var AllSeatCodes = GetAllSeatCodes(ReturnList);
                ReturnList.RemoveAll(ContainsCodeMarker);
                ReturnList.RemoveAll(ContainsSeatCode);

                ReturnList = ReturnList.Concat(AllSeatCodes).ToList();
            }

            return ReturnList.Distinct().ToList();
        }

        public List<string> GetAllSeatCodes(List<string> CurrentReturnList)
        {
            #region Sample Option input
            /*  [OPTN_2] {Seat Memory Control Module Version - Please select Diagnostic Data Identifier 
                NOT_EQUIPPED_TRUE  
                sTagRPOSelectedIconTitle
                sTagRPOSelectedIconTitle

                decrement
                increment

                increment
                decrement

                 Not Equipped
                 Not Equipped

                 Distance Sensing Cruise Control (K59)
                 Distance Sensing Cruise Control (K59) 
                sTagRPOSelectedIconTitle
                sTagRPOSelectedIconTitle

                decrement
                increment

                increment
                decrement

                 10 (Vehicle Communications Platform, 5 Connectors)
                 10 (Vehicle Communications Platform, 5 Connectors)

                 Emergency Response Assistance
                 Emergency Response Assistance  050A
                 050A |  051F
                 051F}~
            */
            #endregion

            var AllSeatMems = new List<string>();

            // So for testing purposes, im gonna feed 050A and 051F.
            // Those values are found by searching the list passed in and finding where we see CODE_FIRST and CODE_LAST
            // We convert those two strings into HEX value numbers. Like actual hex numbers but remove the leading 0s.
            // Find the difference in the two and store the difference.  In this case the diff is 15 HEX (21 dec)
            // Then do a for loop while a byte 'x' is less than the max value we wanna have (05F1). 
            // Each one is then added into a list of strings which we return in the end. I think....

            string FirstCode = CurrentReturnList.Where(x => x.StartsWith("CODE_FIRST")).First();
            string LastCode = CurrentReturnList.Where(x => x.StartsWith("CODE_LAST")).First();

            FirstCode = FirstCode.Substring(FirstCode.IndexOf("__") + 2);
            LastCode = LastCode.Substring(LastCode.IndexOf("__") + 2);
            AllSeatMems.Add(FirstCode.Trim());

            int FirstCodeInt = Int32.Parse(FirstCode, System.Globalization.NumberStyles.HexNumber);
            int LastCodeInt = Int32.Parse(LastCode, System.Globalization.NumberStyles.HexNumber);

            int NextCodeInt = FirstCodeInt;
            while (NextCodeInt < LastCodeInt - 1)
            {
                NextCodeInt += 1;
                byte[] NextCodeBytes = BitConverter.GetBytes(NextCodeInt).Reverse().ToArray();
                string NextCodeString = BitConverter.ToString(NextCodeBytes).Replace("-", "");
                NextCodeString = NextCodeString.TrimStart(new char[] { '0' });
                NextCodeString = '0' + NextCodeString;

                AllSeatMems.Add(NextCodeString);
            }

            AllSeatMems.Add(LastCode.Trim());
            return AllSeatMems;
        }

        private bool ContainsCodeMarker(string StringToCheck) { return StringToCheck.ToLower().StartsWith("code_"); }
        private bool ContainsSeatCode(string StringToCheck) { return StringToCheck.ToLower().StartsWith("0"); }
    }
}
