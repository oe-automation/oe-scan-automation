﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.HTTPClientAPI
{
    public class ProgressAPIEndpoints
    {
        public static string[] AllEndPoints = new string[]
        {
            //Base Page - Lists all possible API Calls
            "http://localhost:57570/OEAutomationTasker/",

                #region New API Endpoints

                // Debug Actions
                    "http://localhost:57570/OEAutomationTasker/Debug/DebugRPAStart/",                       // OEType | Name of the OE SW in use (TIS, GDS2, etc)"
                    "http://localhost:57570/OEAutomationTasker/Debug/ToggleDebugLogging/",                  // ON or OFF | Logging on/off
                    "http://localhost:57570/OEAutomationTasker/Debug/ReturnDebugFileName/",                 // No args/params
                    "http://localhost:57570/OEAutomationTasker/Debug/PreScanLogCheck/",                     // No args/params
                    "http://localhost:57570/OEAutomationTasker/Debug/PostScanLogCheck/",                    // No args/params    
                    "http://localhost:57570/OEAutomationTasker/Debug/SetEnvironmentConfig/",                // Set the ENV Var to a new type
                    "http://localhost:57570/OEAutomationTasker/Debug/UpdateWajFiles/",                      // Calls the OESUpportUpdater to pull newest Waj Files.
                    "http://localhost:57570/OEAutomationTasker/Debug/AutoCloseTimeout/",                    // TIME_MS | Time to close prompts auto in MILISECONDS!
               
                // WinAutoHelpers
                    "http://localhost:57570/OEAutomationTasker/WinAuto/ReturnOEType/",                      // No args/params. Feeds back the value of the OEType on scan init.
                    "http://localhost:57570/OEAutomationTasker/WinAuto/WinAutoDebugFile/",                  // Optional Arg - Name of Debug file (FILE NAME ONLY). If no file name given, return stored one.
                    "http://localhost:57570/OEAutomationTasker/WinAuto/TimeScanStarted/",                   // Optional Arg - Sting of time started. If no string given, return whats on file.
                    "http://localhost:57570/OEAutomationTasker/WinAuto/NeedsHelpValue/",                    // Optional Arg - Value of TRUE or FALSE Passed. If nothing given return whats stored.
                    "http://localhost:57570/OEAutomationTasker/WinAuto/CommandIndexValue/",                 // Optional Arg - Index OF Command To begin Cleanup. If nothing given return stored value.
                    "http://localhost:57570/OEAutomationTasker/WinAuto/ReturnOESoftwareName/",              // No args/params. Returns the name of the OE SW in use by the OE Type.
                    "http://localhost:57570/OEAutomationTasker/WinAuto/LastErrors/",                        // EX__CURSUB1_CURSUB2 - Exception, the current 1 sub and current 2 sub regions. Stores error codes.
                    
                // ViewInvoker                     
                    "http://localhost:57570/OEAutomationTasker/ViewInvoker/ResetAllItems/",                 // No args/params
                    "http://localhost:57570/OEAutomationTasker/ViewInvoker/RequestVIN/",                    // No args/params
                    "http://localhost:57570/OEAutomationTasker/ViewInvoker/OptionPicker/",                  // OEType - Name of OE SW in use

                // Custom Views for Targeted OEs
                    "http://localhost:57570/OEAutomationTasker/CustomViews/NoCommsFJDS/",                   // No Args/Params
                    "http://localhost:57570/OEAutomationTasker/CustomViews/SetFJDSROandODO/",               // No args/Params
                    "http://localhost:57570/OEAutomationTasker/CustomViews/TISRecall/",                     // Name of the OE Recall File

                // Timers
                    "http://localhost:57570/OEAutomationTasker/Timers/StartFullProcessTimer/",              // No Args/Params
                    "http://localhost:57570/OEAutomationTasker/Timers/StopFullProcessTimer/",               // No Args/Params
                    "http://localhost:57570/OEAutomationTasker/Timers/StartTimer/",                         // StartTimer/Index   | Index Of The Command Being Timed 
                    "http://localhost:57570/OEAutomationTasker/Timers/StopTimer/",                          // StopTimer/Index    | Index Of The Command Being Timed 
                    "http://localhost:57570/OEAutomationTasker/Timers/StartAllTimers/",                     // No Args/Params     | DEBUG MODE ONLY!

                // Progress Bars
                    "http://localhost:57570/OEAutomationTasker/ProgressBar/StartProgressBar/",              // StartProgressBar/Index/MaxTicks   | Defaults to 100 Ticks.
                    "http://localhost:57570/OEAutomationTasker/ProgressBar/TickProgressBar/",               // TickProgressBar/Index/TickCount   | MAX of 5 ticks each call.
                    "http://localhost:57570/OEAutomationTasker/ProgressBar/SetCustomMessage/",              // SetCustomMessage/Index/Msg/Indet  | Index, Message to show on progbar, set is indetereminate
                    "http://localhost:57570/OEAutomationTasker/ProgressBar/HideProgressBar/",               // HideProgressBar/Index             | Hides ProgBar at index.
                    "http://localhost:57570/OEAutomationTasker/ProgressBar/LoopingProgress/",               // LoopingProgress/Index/Loop_Total  | Index, Current Loop_Total Loops

                // Code Clearing
                    "http://localhost:57570/OEAutomationTasker/CodeClearing/ClearCodesOption/",             // No args/params
                    "http://localhost:57570/OEAutomationTasker/CodeClearing/ClearCodesPass/",               // No args/params
                    "http://localhost:57570/OEAutomationTasker/CodeClearing/ClearCodesFail/",               // No args/params

                // Set Task Status
                    "http://localhost:57570/OEAutomationTasker/UpdateStatus/IncreaseProgressCounter/",      // IncreaseProgressCounter/Index/Time   | Index of task, time taken.
                    "http://localhost:57570/OEAutomationTasker/UpdateStatus/DecreaseProgressCounter/",      // DecreaseProgressCounter/Index        | Index of the task to undo.
    
                // Scan Complete
                     "http://localhost:57570/OEAutomationTasker/ScanComplete/ScanToCompleteOK/",            // No Args/Params
                     "http://localhost:57570/OEAutomationTasker/ScanComplete/ScanToFailureOK/",             // No Args/Params
                     "http://localhost:57570/OEAutomationTasker/ScanComplete/CancelScanRequest/",           // No Args/Params

                // SET Vehicle Options
                    "http://localhost:57570/OEAutomationTasker/SetVehicleInfo/VIN/",                        // SetVIN/VINNumber  | VIN Number To Set
                    "http://localhost:57570/OEAutomationTasker/SetVehicleInfo/Year/",                       // SetYear/Year      | Year to Set
                    "http://localhost:57570/OEAutomationTasker/SetVehicleInfo/Make/",                       // SetMake/Make      | Make to Set
                    "http://localhost:57570/OEAutomationTasker/SetVehicleInfo/Model/",                      // SetModel/Model    | Model to Set
                                                                                                            
                // GET Vehicle Options                                                                      
                    "http://localhost:57570/OEAutomationTasker/GetVehicleInfo/VIN/",                        // GetVIN/VINNumber  | VIN Number To Get
                    "http://localhost:57570/OEAutomationTasker/GetVehicleInfo/Year/",                       // GetYear/Year      | Year to Get
                    "http://localhost:57570/OEAutomationTasker/GetVehicleInfo/Make/",                       // GetMake/Make      | Make to Get
                    "http://localhost:57570/OEAutomationTasker/GetVehicleInfo/Model/",                      // GetModel/Model    | Model to Get
                 #endregion
        };
    }
}

