﻿using OEAutomationLauncher.AutomationLauncher.ConfirmPrompts;
using OEAutomationLauncher.AutomationLauncher.CustomOEViews;
using OEAutomationLauncher.AutomationLauncher.ObjectClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.HTTPClientAPI
{
    public class APIActionTriggered
    {
        public List<string> ResultsList = new List<string>();        // Results of run string value.

        internal string CommandType;                                 // Type of Action being invoked.
        internal string BaseCommandString;                           // Raw API Call string
        internal string ExCommand;                                   // Command To Run
        internal string[] CommandSplit;                              // Command Split input string.
        
        public APIActionTriggered(string CommandString)
        {
            BaseCommandString = CommandString;
            CommandSplit = BaseCommandString.Split('/');
            ExCommand = CommandSplit[3];

            GlobalObjects.DebugHelper.WriteDebugLog(CommandType + " CALL PROCESSED. COMMAND STRING: " + BaseCommandString, "API", true);
            GlobalObjects.DebugHelper.WriteDebugLog("BASE COMMAND TO RUN: " + ExCommand, "API", true);
            GlobalObjects.DebugHelper.WriteDebugLog("BASE COMMAND STRING HAS: " + CommandSplit.Length + " PARTS TO IT", "API", true);
        }

        /// <summary>
        /// Loggs out to the debug file when a requested command does not exist.
        /// </summary>
        public void FailedToFindCommand()
        {
            ResultsList.Add(CommandType + "COMMAND UNKNOWN!");
            ResultsList.Add(BaseCommandString);
            GlobalObjects.DebugHelper.WriteDebugLog(CommandType + " COMMAND UNKNOWN! " + BaseCommandString, "ERR_" + CommandType, true);
        }
    }
}

