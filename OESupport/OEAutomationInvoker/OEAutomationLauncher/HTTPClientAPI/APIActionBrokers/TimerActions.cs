﻿using OEAutomationLauncher.HTTPClientAPI;
using System;
using System.Diagnostics;

namespace OEAutomationLauncher.AutomationLauncher.HTTPClientAPI.ActionBrokers
{
    /// <summary>
    /// Actions to process timer calls.
    /// </summary>
    public class TimerActions : APIActionTriggered
    {
        /// <summary>
        /// Constructor for timer objects.
        /// </summary>
        /// <param name="CommandString">Base full API Call string object.</param>
        public TimerActions(string CommandString) : base(CommandString)
        {
            CommandType = "TIMER_ACTIONS";
            if (ExCommand.Contains("StartFullProcessTimer"))
            {
                if (StartFullProcessTimer()) { GlobalObjects.DebugHelper.WriteDebugLog("START FULL PROCESS TIMER", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO START FULL PROCESS TIMER", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("StopFullProcessTimer"))
            {
                if (StopFullProcessTimer()) { GlobalObjects.DebugHelper.WriteDebugLog("STOP FULL PROCESS TIMER", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO STOP FULL PROCESS TIMER", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("StartTimer"))
            {
                if (StartTimer()) { GlobalObjects.DebugHelper.WriteDebugLog("STARTED TIMER - SINGLE", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT START TIMER - SINGLE", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("StopTimer"))
            {
                if (StopTimer()) { GlobalObjects.DebugHelper.WriteDebugLog("STOPPED TIMER - SINGLE", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT STOP TIMER - SINGLE", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("StartAllTimers"))
            {
                if (StartAllTimers()) { GlobalObjects.DebugHelper.WriteDebugLog("STARTED ALL TIMERS OK!", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT START ALL TIMERS!", "ERR_" + CommandType, true); }
            }

            else { FailedToFindCommand(); }
        }

        /// <summary>
        /// Starts a full process timer
        /// </summary>
        /// <returns></returns>
        private bool StartFullProcessTimer()
        {
            GlobalObjects.FullProcTimer.Reset();
            GlobalObjects.FullProcTimer.Start();
            ResultsList.Add("Timer Started");

            GlobalObjects.DebugHelper.WriteDebugLog("FULL PROCESS TIMER HAS BEEN STARTED.", "API", true);
            GlobalObjects.DebugHelper.WriteDebugLog("FULL PROCESS TIMER HAS BEEN STARTED.");

            return true;
        }

        /// <summary>
        /// Stops a full process timer.
        /// </summary>
        /// <returns></returns>
        private bool StopFullProcessTimer()
        {
            GlobalObjects.FullProcTimer.Stop();
            var TimePassed = GlobalObjects.FullProcTimer.Elapsed;
            string TimeString = TimePassed.ToString(@"mm\:ss\.fff");
            ResultsList.Add(TimeString.Replace(":", "_").Replace("-", "."));

            GlobalObjects.DebugHelper.WriteDebugLog("FULL PROCESS TIMER HAS BEEN STOPPED. TIME ELAPSED: " + TimeString, "API", true);
            GlobalObjects.DebugHelper.WriteDebugLog("FULL PROCESS TIMER HAS BEEN STOPPED. TIME ELAPSED: " + TimeString);

            return true;
        }

        /// <summary>
        /// Starts a single task timer.
        /// </summary>
        /// <returns></returns>
        private bool StartTimer()
        {
            int TaskIndex = int.Parse(CommandSplit[4]);

            var ProcessTimer = GlobalObjects.AllProcTimers[TaskIndex];
            ProcessTimer.Reset();
            ProcessTimer.Start();
            ResultsList.Add("Timer Started");

            string TaskDes = GlobalObjects.BaseTasks.AllTaskItems[TaskIndex].CommandDescription;
            GlobalObjects.DebugHelper.WriteDebugLog("SINGLE TASK PROCESS TIMER HAS BEEN STARTED. TASK NAME: " + TaskDes, "API", true);
            GlobalObjects.DebugHelper.WriteDebugLog("SINGLE TASK PROCESS TIMER HAS BEEN STARTED. TASK NAME: " + TaskDes);

            return true;
        }

        /// <summary>
        /// Stops a single task timer.
        /// </summary>
        /// <returns></returns>
        private bool StopTimer()
        {
            int TaskIndex = int.Parse(CommandSplit[3]);

            var ProcessTimer = GlobalObjects.AllProcTimers[TaskIndex];
            ProcessTimer.Stop();

            var TimePassed = ProcessTimer.Elapsed;
            string TimeString = TimePassed.ToString("c");

            GlobalObjects.AllTimes.Add(TimeString);
            ResultsList.Add(TimeString.Replace(":", "_").Replace("-", "."));

            string TaskDes = GlobalObjects.BaseTasks.AllTaskItems[TaskIndex].CommandDescription;
            GlobalObjects.DebugHelper.WriteDebugLog("SINGLE TASK PROCESS TIMER HAS BEEN STOPPED. TIME ELAPSED: " + TimeString + " FOR PROCESS: " + TaskDes, "API", true);
            GlobalObjects.DebugHelper.WriteDebugLog("SINGLE TASK PROCESS TIMER HAS BEEN STOPPED. TIME ELAPSED: " + TimeString + " FOR PROCESS: " + TaskDes);

            return true;
        }

        private bool StartAllTimers()
        {
            if (!Debugger.IsAttached)
            {
                ResultsList.Add("CAN NOT USE THIS FUNCTION WITHOUT A DEBUGGER HOOKED UP!");
                return false;
            }

            try
            {
                GlobalObjects.DebugHelper.WriteDebugLog("STARTING ALL TIMERS NOW");
                foreach (var TimerObj in GlobalObjects.AllProcTimers) { TimerObj.Start(); }

                GlobalObjects.DebugHelper.WriteDebugLog("STARTED ALL TIMERS OK!");
                ResultsList.Add("STARTED ALL TIMERS OK!");

                return true;
            }
            catch (Exception timeEx)
            {
                ResultsList.Add("COULD NOT START ALL TIMERS");
                ResultsList.Add(timeEx.Message);

                GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT START ALL TIMERS.");
                GlobalObjects.DebugHelper.WriteDebugLog("ERR: " + timeEx.Message);
                GlobalObjects.DebugHelper.WriteDebugLog(timeEx);

                return false;
            }
        }
    }
}

