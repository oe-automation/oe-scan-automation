﻿using Microsoft.Win32;
using OEAutomationLauncher.AutomationLauncher.AutomationTaskLists;
using OEAutomationLauncher.AutomationLauncher.DebugHelpers;
using OEAutomationLauncher.AutomationLauncher.ObjectClasses;
using OEAutomationLauncher.HTTPClientAPI;
using OEAutomationLauncher.OEOptionPicker;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.HTTPClientAPI.ActionBrokers
{
    /// <summary>
    /// API Debug and related calls.
    /// </summary>
    public class DebugActions : APIActionTriggered
    {
        /// <summary>
        /// Constructor for DebugActions
        /// </summary>
        /// <param name="CommandString">Command String from the API Call.</param>
        public DebugActions(string CommandString) : base(CommandString)
        {
            CommandType = "DEBUG";
            if (ExCommand.Contains("ReturnDebugFileName"))
            {
                if (ReturnDebugFileName()) { } //GlobalObjects.DebugHelper.WriteDebugLog("RAN DEBUG FILE RETURN OK", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO RETURN DEBUG FILE", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("ToggleDebugLogging"))
            {
                if (ToggleDebugLog()) { GlobalObjects.DebugHelper.WriteDebugLog("TOGGLED LOGGING OK!", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO TOGGLE LOGGING", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("DebugRPAStart"))
            {
                if (DebugRPA()) { GlobalObjects.DebugHelper.WriteDebugLog("DEBUG RPA STARTED!", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("DEBUG RPA COULD NOT LAUNCH", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("PreScanLogCheck"))
            {
                if (PreScanLogCheck()) { GlobalObjects.DebugHelper.WriteDebugLog("DEBUG RPA STARTED!", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("DEBUG RPA COULD NOT LAUNCH", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("PostScanLogCheck"))
            {
                if (PostScanLogCheck()) { GlobalObjects.DebugHelper.WriteDebugLog("DEBUG RPA STARTED!", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("DEBUG RPA COULD NOT LAUNCH", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("SetEnvironmentConfig"))
            {
                if (SetEnvironmentConfig()) { GlobalObjects.DebugHelper.WriteDebugLog("CHANGING THE ENVIRONMENT VARIABLE", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT CHANGE THE ENVIRONMENT VARIABLE", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("UpdateWajFiles"))
            {
                if (GetNewestWajs()) { GlobalObjects.DebugHelper.WriteDebugLog("GETTING THE NEWESET WAJ FILES NOW", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT GET THE NEWEST WAJ FILES", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("AutoCloseTimeout"))
            {
                if (SetAutoCloseTimeout()) { GlobalObjects.DebugHelper.WriteDebugLog("SETTING THE TIMEOUT VALUE FOR CLOSING PROMPTS NOW", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SET THE TIMEOUT VALUE!", "ERR_" + CommandType, true); }
            }

            else FailedToFindCommand();
        }

        /// <summary>
        /// Gets the debug file name for the invoker.
        /// </summary>
        /// <returns></returns>
        private bool ReturnDebugFileName()
        {
            string DebugFileName = GlobalObjects.DebugHelper.MainDebugFile;
            DebugFileName = DebugFileName.Replace("\\", "__");
            ResultsList.Add(DebugFileName);

            // GlobalObjects.DebugHelper.WriteDebugLog("DEBUG FILE NAME REQUESTED. FILE IS: " + DebugFileName, "API", true);

            return true;
        }

        /// <summary>
        /// Kicks off RPA if in debug mode.
        /// </summary>
        /// <returns></returns>
        private bool DebugRPA()
        {
            if (!Debugger.IsAttached && GlobalObjects.CONFIG_ENVIRONMENT != "PRODUCTION")
            {
                GlobalObjects.DebugHelper.WriteDebugLog("DEBUGGER NOT FOUND THIS ISNT ALLOWED WITHOUT ONE");
                ResultsList.Add("RPA NOT STARTED. NOT DEBUGGING");
            }
            else
            {
                MainWindowViewHelpers.KickOffRPA(CommandSplit[4]);
                GlobalObjects.DebugHelper.WriteDebugLog("DEBUGGER FOUND. STARTING RPA", "API", true);
                ResultsList.Add(CommandSplit[4]);
                ResultsList.Add("RPA KICKED OFF");

                return false;
            }

            return true;
        }

        /// <summary>
        /// Toggles Debug Logging On or OFF
        /// </summary>
        /// <returns></returns>
        private bool ToggleDebugLog()
        {
            bool LogOnOrOff = true;
            if (CommandSplit[4] == "OFF") { LogOnOrOff = false; }

            ResultsList.Add("DEBUBG: " + CommandSplit[4]);
            GlobalObjects.DebugHelper.WriteDebugLog("REQUEST FOR TOGGLING LOGGING TO: " + CommandSplit[4], "API", true);

            var AllDeviceList = ToggleDebugLogging_Invoker.ToggleLogging(LogOnOrOff);

            if (AllDeviceList.Count == 0) { return false; }
            foreach (var device in AllDeviceList) { ResultsList.Add(device); }

            return true;
        }

        /// <summary>
        /// Checks for logs before scanning.
        /// </summary>
        /// <returns></returns>
        private bool PreScanLogCheck()
        {
            GlobalObjects.DebugHelper.WriteDebugLog("REQUEST FOR STARTING FILE CHECK FOR J2534 DEBUG LOGS", "API", true);
            GlobalObjects.FileCollector = new PrepareFileGathering_Invoker();

            ResultsList.Add(GlobalObjects.FileCollector.ScanStartDateTime.ToShortTimeString());
            ResultsList.Add("FILES FOUND: " + GlobalObjects.FileCollector.PreScanJ2534Logs.Count);

            return true;
        }

        /// <summary>
        /// Checks for logs after doing a scan
        /// </summary>
        /// <returns></returns>
        private bool PostScanLogCheck()
        {
            bool FoundNewFiles = false;

            GlobalObjects.DebugHelper.WriteDebugLog("REQUEST TO STOP LOG FILE SCANNING PROCESSED. CHECKING FOR DIFS/GETTING FILE LIST", "API", true);
            if (GlobalObjects.FileCollector == null)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("FILE LOGGER WAS NULL CREATING AND PERFORMING A START/STOP", "API", true);
                GlobalObjects.FileCollector = new PrepareFileGathering_Invoker();

                ResultsList.Add("FILE SCANNER WAS NULL");
            }

            GlobalObjects.DebugHelper.WriteDebugLog("GETTING NEW FILES NOW", "API", true);

            FoundNewFiles = GlobalObjects.FileCollector.GetNewScanFiles();
            if (FoundNewFiles)
            {
                ResultsList.Add("START: " + GlobalObjects.FileCollector.ScanStartDateTime.ToShortTimeString());
                ResultsList.Add("STOP: " + GlobalObjects.FileCollector.ScanStopDateTime.ToShortTimeString());
                ResultsList.Add("NEW FILES FOUND: " + FoundNewFiles.ToString());
                ResultsList.Add("COUND DIFF: " + (GlobalObjects.FileCollector.PostScanJ2534Logs.Count - GlobalObjects.FileCollector.PreScanJ2534Logs.Count).ToString());

                Task.Run(() => GlobalObjects.FileCollector.CollectAndPackNewFiles());
            }

            return true;
        }

        /// <summary>
        /// Sets the value of the RegKey used to modify the environment var to set
        /// up what kind of runtime env we have. DEV, STAGE, PROD are the three valid options.
        /// </summary>
        /// <returns></returns>
        private bool SetEnvironmentConfig()
        {
            string ConfigKey = @"HKEY_CURRENT_USER\SOFTWARE\Drew Technologies, Inc\OESupportApps\";
            string CurrentConfig = (string)Registry.GetValue(ConfigKey, "CONFIG_ENVIRONMENT", "NOT_SET");

            GlobalObjects.DebugHelper.WriteDebugLog("FOUND THE VALUE FOR THE CONFIG_ENVIRONMENT REG TO BE: " + CurrentConfig);
            if (CurrentConfig == "NOT_SET" || CurrentConfig == null)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("REGISTRY FOR THE ENVIRONMENT TO TARGET WAS NOT FOUND! DEFAULTING TO A PRODUCTION RELEASE");
                Registry.SetValue(ConfigKey, "CONFIG_ENVIRONMENT", "PRODUCTION");

                GlobalObjects.DebugHelper.WriteDebugLog("SET THE REG KEY FOR CURRENT ENVIRONMENT TO PRODUCTION.");
                ResultsList.Add("NO INITIAL REG KEY FOR THE ENVIRONMENT WAS FOUND! DEFAULTED TO PRODUCTION");
            }

            try
            {
                string ReqdNewValue = CommandSplit[4];
                GlobalObjects.DebugHelper.WriteDebugLog("GOT REQUEST TO SET THE ENVIRONMENT VARIABLE");
                GlobalObjects.DebugHelper.WriteDebugLog("REQUESTED CHANGE VALUE: " + ReqdNewValue);

                var ValidTypes = new List<string> { "PRODUCTION", "DEVELOPMENT", "STAGING" };
                if (!ValidTypes.Contains(ReqdNewValue))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("CAN NOT SET ENV VALUE. NOT VALID TYPE");
                    ResultsList.Add("CAN NOT SET ENV VALUE. NOT VALID TYPE --> " + ReqdNewValue + " FROM ORG OF " + CurrentConfig);

                    return false;
                }

                GlobalObjects.DebugHelper.WriteDebugLog("SETTING NEW ENV VALUE NOW...");
                Registry.SetValue(ConfigKey, "CONFIG_ENVIRONMENT", ReqdNewValue);

                GlobalObjects.DebugHelper.WriteDebugLog("DONE! NEW ENV VALUE WAS SAVED OK!");
                ResultsList.Add("SET NEW ENV VALUE OK!");
                ResultsList.Add(ReqdNewValue + " --> " + CurrentConfig);

                return true;
            }

            catch (Exception setEx)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO SET VALUE FOR THE REG KEY.");
                GlobalObjects.DebugHelper.WriteDebugLog("EX: " + setEx);
                GlobalObjects.DebugHelper.WriteDebugLog(setEx);

                ResultsList.Add("FAILED TO SET. CAUGHT AN EXECEPTION.");
                ResultsList.Add(setEx.Message);

                return false;
            }
        }

        /// <summary>
        /// Gets the newest Waj Files on the S3 server and imports them.
        /// </summary>
        /// <returns></returns>
        private bool GetNewestWajs()
        {
            GlobalObjects.DebugHelper.WriteDebugLog("PULLING NEWEST WAJ FILES NOW USING THE OESUPP UPDATER");
            GlobalObjects.DebugHelper.WriteDebugLog("THIS MAY TAKE A SECOND");

            try
            {
                MainWindowViewHelpers.KickOffRPA("UPDATE");
                GlobalObjects.DebugHelper.WriteDebugLog("STARTED OE SUPP UPDATER WITH ARG \"UPDATE\"");

                GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("FULL PROCESS RESET HAS BEEN REQUESTED. CLEARNING EVERYTING OUT NOW.", "API", true);
                    GlobalObjects.BaseTasks = new BaseTaskList(GlobalObjects.OEType);

                    OptionsTriggered.OptionsComplete();
                });

                ResultsList.Add("UPDATE");
                ResultsList.Add("STARTED UPDATER OK");
                ResultsList.Add("RESET ALL ITEMS OK");

                return true;
            }
            catch (Exception wajEx)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO PULL NEWEST WAJ FILES!");
                GlobalObjects.DebugHelper.WriteDebugLog("ERR: " + wajEx.Message);
                GlobalObjects.DebugHelper.WriteDebugLog(wajEx);

                ResultsList.Add("FAILED TO START UPDATER");
                ResultsList.Add(wajEx.Message);

                return false;
            }
        }

        /// <summary>
        /// Sets the timeout value to auto continue a scan if a prompt isnt entered.
        /// </summary>
        /// <returns></returns>
        private bool SetAutoCloseTimeout()
        {
            bool ParsedOK = int.TryParse(CommandSplit[4], out int TimeOut);
            if (ParsedOK) 
            {
                GlobalObjects.TimeForTimeout = TimeOut; 
                GlobalObjects.TimeoutApiSet = true; 
            }
            else
            {
                GlobalObjects.DebugHelper.WriteDebugLog("CAN NOT CHANGE TIMEOUT TO SET VALUE!");
                GlobalObjects.DebugHelper.WriteDebugLog("WANTED: " + CommandSplit[4]);
                GlobalObjects.DebugHelper.WriteDebugLog("REMAINING AT: " + GlobalObjects.TimeForTimeout);

                return false;  
            }

            GlobalObjects.DebugHelper.WriteDebugLog("SET NEW TIMEOUT VALUE OK!");
            GlobalObjects.DebugHelper.WriteDebugLog("TIMEOUT NOW: " + GlobalObjects.TimeForTimeout);

            ResultsList.Add("SET OK!");
            ResultsList.Add(GlobalObjects.TimeForTimeout.ToString());
            return true;
        }
    }
}
