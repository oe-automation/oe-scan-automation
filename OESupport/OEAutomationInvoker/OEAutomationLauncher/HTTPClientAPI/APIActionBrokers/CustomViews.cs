﻿using OEAutomationLauncher.AutomationLauncher.CustomOEViews.FJDS;
using OEAutomationLauncher.AutomationLauncher.CustomOEViews.TIS;
using OEAutomationLauncher.HTTPClientAPI;

namespace OEAutomationLauncher.AutomationLauncher.HTTPClientAPI.ActionBrokers
{
    /// <summary>
    /// API Calls for custom OE view objects
    /// </summary>
    public class CustomViews : APIActionTriggered
    {
        private CustomTISViewTrigger TISTrigger = new CustomTISViewTrigger();
        private CustomFJDSViewTrigger FJDSTrigger = new CustomFJDSViewTrigger();

        /// <summary>
        /// Constructor for CustomViews
        /// </summary>
        /// <param name="CommandString">Base input string from the API Call</param>
        public CustomViews(string CommandString) : base(CommandString)
        {
            CommandType = "CUSTOM_VIEW";
            if (ExCommand.Contains("TISRecall"))
            {
                if (ShowTISRecalls()) { GlobalObjects.DebugHelper.WriteDebugLog("TIS REALL WINDOW IS OPEN", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO SHOW TIS RECALL WINDOW", "ERR_" + CommandType, true); }
            }
            if (ExCommand.Contains("NoCommsFJDS"))
            {
                if (ShowFJDSNoComms()) { GlobalObjects.DebugHelper.WriteDebugLog("FJDS NO COMMS WINDOW IS OPEN", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO SHOW FJDS NO COMMS WINDOW", "ERR_" + CommandType, true); }
            }
            if (ExCommand.Contains("SetFJDSROandODO"))
            {
                if (ShowJFDSOdoAndRO()) { GlobalObjects.DebugHelper.WriteDebugLog("FJDS ODO AND RO IS OPEN", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO FJDS ODO AND RO WINDOW", "ERR_" + CommandType, true); }
            }
            else { FailedToFindCommand(); }
        }

        /// <summary>
        /// Shows the custom TIS Recall view.
        /// </summary>
        /// <returns>True if a TIS window is made, False if not.</returns>
        private bool ShowTISRecalls()
        {
            // Setup Close Timer
            GlobalObjects.SetupAutoCloseTimer(typeof(TISRecallView));

            GlobalObjects.DebugHelper.WriteDebugLog("CREATING NEW TIS RECALL VIEW OBJECT NOW.");
            if (TISTrigger.ScanIsTIS)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("GETTING LIST OF ALL RECALLS NOW");
                string CurrentRecalls = TISTrigger.ReturnAllTISRecalls();

                string[] RecallsSplit = CurrentRecalls.Split('_');
                ResultsList.Add(RecallsSplit.Length.ToString());
                foreach (var recallItem in RecallsSplit) { ResultsList.Add(recallItem); }

                GlobalObjects.DebugHelper.WriteDebugLog("RECALL COUNT: " + RecallsSplit.Length);
                GlobalObjects.DebugHelper.WriteDebugLog("RECALLS: " + CurrentRecalls);

                return true;
            }

            else
            {
                GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT MAKE TIS RECALL WINDOW DUE TO AN ERROR.");
                ResultsList.Add("FAILED - NON TIS SCAN");

                return false;
            }
        }

        /// <summary>
        /// Shows the custom FJDS No comms view.
        /// </summary>
        /// <returns>True if a window is made, False if not.</returns>
        private bool ShowFJDSNoComms()
        {
            // Setup Auto close.
            GlobalObjects.SetupAutoCloseTimer(typeof(FJDSNoComms));

            if (FJDSTrigger.ScanIsFJDS)
            {
                bool OpenOK = FJDSTrigger.ShowFJDSNoCommsWindow();
                ResultsList.Add("STATUS: " + OpenOK);

                return OpenOK;
            }

            else
            {
                GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT MAKE FJDS COMMS WINDOW DUE TO AN ERROR.");
                ResultsList.Add("FAILED - NON FJDS SCAN");

                return false;
            }
        }

        /// <summary>
        /// Shows the FJDS window to put in an RO and an odomoter view.
        /// </summary>
        /// <returns></returns>
        private bool ShowJFDSOdoAndRO()
        {
            // Setup Auto close.
            GlobalObjects.SetupAutoCloseTimer(typeof(FJDSEnterOdoAndRo));

            if (FJDSTrigger.ScanIsFJDS)
            {
                bool OpenOK = FJDSTrigger.ShowFJDSRoAndOdoWindow();
                ResultsList.Add("STATUS: " + OpenOK);

                return OpenOK;
            }

            else
            {
                GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT MAKE FJDS RO AND ODO WINDOW DUE TO AN ERROR.");
                ResultsList.Add("FAILED - NON FJDS SCAN");

                return false;
            }
        }
    }
}

