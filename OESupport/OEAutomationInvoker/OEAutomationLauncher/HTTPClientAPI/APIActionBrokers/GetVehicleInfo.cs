﻿using OEAutomationLauncher.HTTPClientAPI;

namespace OEAutomationLauncher.AutomationLauncher.HTTPClientAPI.ActionBrokers
{
    /// <summary>
    /// Sets or Gets current Vehicle information
    /// </summary>
    public class GetVehicleInfo : APIActionTriggered
    {
        /// <summary>
        /// Constructor for the get vehicle info API requests. 
        /// </summary>
        /// <param name="CommandString">Input URL from command string</param>
        public GetVehicleInfo(string CommandString) : base(CommandString)
        {
            CommandType = "GET_VEHICLE_INFO";
            if (ExCommand.Contains("VIN"))
            {
                if (GetVehicleVIN()) { GlobalObjects.DebugHelper.WriteDebugLog("RETURNING VEHICLE VIN NUMBER", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO RETURN VEHICLE VIN NUMBER", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("Year"))
            {
                if (GetVehicleYear()) { GlobalObjects.DebugHelper.WriteDebugLog("RETURNING VEHICLE YEAR", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO RETURN VEHICLE YEAR", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("Make"))
            {
                if (GetVehicleMake()) { GlobalObjects.DebugHelper.WriteDebugLog("RETURNING VEHICLE MAKE", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO RETURN VEHICLE MAKE", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("Model"))
            {
                if (GetVehicleModel()) { GlobalObjects.DebugHelper.WriteDebugLog("RETURNING VEHICLE MODEL", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO RETURN VEHICLE MODEL", "ERR_" + CommandType, true); }
            }

            else { FailedToFindCommand(); }
        }

        /// <summary>
        /// Gets the VIN From global objects and returns it to the result list.
        /// </summary>
        /// <returns>Good or bad VIN pull attempt.</returns>
        private bool GetVehicleVIN()
        {
            // GlobalObjects.DebugHelper.WriteDebugLog("TRYING TO GRAB VEHICLE CONFIG INFO FROM GLOBAL CONFIG OBJECT.");
            // GlobalObjects.DebugHelper.WriteDebugLog("VALUE RETURNED FROM VIN CALL: " + GlobalObjects.ConfigInfo.VIN);

            GlobalObjects.DebugHelper.WriteDebugLog("TRYING TO GRAB VEHICLE CONFIG INFO FROM GLOBAL CONFIG OBJECT.", "API", true);
            GlobalObjects.DebugHelper.WriteDebugLog("VALUE RETURNED FROM VIN CALL: " + GlobalObjects.ConfigInfo.VIN, "API", true);
            ResultsList.Add(GlobalObjects.ConfigInfo.VIN);

            return true;
        }

        /// <summary>
        /// Gets the Year From global objects and returns it to the result list.
        /// </summary>
        /// <returns>Good or bad Year pull attempt.</returns>
        private bool GetVehicleYear()
        {
            // GlobalObjects.DebugHelper.WriteDebugLog("TRYING TO GRAB VEHICLE CONFIG INFO FROM GLOBAL CONFIG OBJECT.");
            // GlobalObjects.DebugHelper.WriteDebugLog("VALUE RETURNED FROM YEAR CALL: " + GlobalObjects.ConfigInfo.Year);

            GlobalObjects.DebugHelper.WriteDebugLog("TRYING TO GRAB VEHICLE CONFIG INFO FROM GLOBAL CONFIG OBJECT.", "API", true);
            GlobalObjects.DebugHelper.WriteDebugLog("VALUE RETURNED FROM YEAR CALL: " + GlobalObjects.ConfigInfo.Year, "API", true);
            ResultsList.Add(GlobalObjects.ConfigInfo.Year);

            return true;
        }

        /// <summary>
        /// Gets the Make From global objects and returns it to the result list.
        /// </summary>
        /// <returns>Good or bad Make pull attempt.</returns>
        private bool GetVehicleMake()
        {
            // GlobalObjects.DebugHelper.WriteDebugLog("TRYING TO GRAB VEHICLE CONFIG INFO FROM GLOBAL CONFIG OBJECT.");
            // GlobalObjects.DebugHelper.WriteDebugLog("VALUE RETURNED FROM MAKE CALL: " + GlobalObjects.ConfigInfo.Make);

            GlobalObjects.DebugHelper.WriteDebugLog("TRYING TO GRAB VEHICLE CONFIG INFO FROM GLOBAL CONFIG OBJECT.", "API", true);
            GlobalObjects.DebugHelper.WriteDebugLog("VALUE RETURNED FROM MAKE CALL: " + GlobalObjects.ConfigInfo.Make, "API", true);
            ResultsList.Add(GlobalObjects.ConfigInfo.Make);

            return true;
        }

        /// <summary>
        /// Gets the Model From global objects and returns it to the result list.
        /// </summary>
        /// <returns>Good or bad Model pull attempt.</returns>
        private bool GetVehicleModel()
        {
            // GlobalObjects.DebugHelper.WriteDebugLog("TRYING TO GRAB VEHICLE CONFIG INFO FROM GLOBAL CONFIG OBJECT.");
            // GlobalObjects.DebugHelper.WriteDebugLog("VALUE RETURNED FROM MODEL CALL: " + GlobalObjects.ConfigInfo.Model);

            GlobalObjects.DebugHelper.WriteDebugLog("TRYING TO GRAB VEHICLE CONFIG INFO FROM GLOBAL CONFIG OBJECT.", "API", true);
            GlobalObjects.DebugHelper.WriteDebugLog("VALUE RETURNED FROM MODEL CALL: " + GlobalObjects.ConfigInfo.Model, "API", true);
            ResultsList.Add(GlobalObjects.ConfigInfo.Model);

            return true;
        }
    }
}

