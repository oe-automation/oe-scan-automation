﻿using Microsoft.Win32;
using OEAutomationLauncher.AutomationLauncher.ConfirmPrompts;
using OEAutomationLauncher.AutomationLauncher.DebugHelpers;
using OEAutomationLauncher.AutomationLauncher.DriveCrashInterface;
using OEAutomationLauncher.AutomationLauncher.ObjectClasses;
using OEAutomationLauncher.HTTPClientAPI;
using System;
using System.IO;

namespace OEAutomationLauncher.AutomationLauncher.HTTPClientAPI.ActionBrokers
{
    /// <summary>
    /// Scan Completed Actions.
    /// </summary>
    public class ScanCompleted : APIActionTriggered
    {
        /// <summary>
        /// Constructor for the scan compelted actions.
        /// </summary>
        /// <param name="CommandString">Base full API Call string object.</param>
        public ScanCompleted(string CommandString) : base(CommandString)
        {
            CommandType = "SCAN_COMPLETE";
            if (ExCommand.Contains("ScanToCompleteOK"))
            {
                if (ScanComplete(true)) { GlobalObjects.DebugHelper.WriteDebugLog("UPDATING SCAN SCAN TO DONE OK!", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO UPDATE SCAN TATUS TO DONE OK", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("ScanToFailureOK"))
            {
                if (ScanComplete(false)) { GlobalObjects.DebugHelper.WriteDebugLog("UPDATING SCAN STATUS TO NOT DONE", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO UPDATE TASK STATUS TO NOT DONE", "ERR_" + CommandType, true); }
            }
            
            else if (ExCommand.Contains("CancelScanRequest"))
            {
                if (CancelScanRequest()) { GlobalObjects.DebugHelper.WriteDebugLog("SHOWING CANCEL SCAN NOW. REQUEST WAS PROCESSED OK!"); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SHOW THE CANCEL SCAN WINDOW!", "ERR_" + CommandType, true); }
            }

            else { FailedToFindCommand(); }
        }

        /// <summary>
        /// Marks a scan as completed OK.
        /// </summary>
        /// <returns></returns>
        private bool ScanComplete(bool PassOrFail)
        {
            // SET GLOBAL PASS OR FAIL.
            GlobalObjects.ScanCompletePassed = PassOrFail;

            // Setup Auto close
            if (!GlobalObjects.TimeoutApiSet) { GlobalObjects.TimeForTimeout = 120000; }
            GlobalObjects.SetupAutoCloseTimer(typeof(ScanCompleteCloseWindow));

            if (PassOrFail) { ResultsList.Add("SCAN MARKED COMPLETE OK"); }
            if (!PassOrFail) { ResultsList.Add("SCAN MARKED FAILED OK"); }

            // Write timer info
            LogOutTimers();

            // True = Pass False = Fail
            ToggleDebugLogging_Invoker.ToggleLogging(false);
            MainWindowViewHelpers.ScanCompleteButtonToggle(PassOrFail);

            // If we failed dont bother sending up a report item just term.
            if (!PassOrFail)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("FAILED SCAN. KILLING PROCESS NOW.");
                return true;
            }

            // Remove the old launcher Bat Files here.
            GlobalObjects.DebugHelper.WriteDebugLog("REMOVING OLD LAUNCHER FILES NOW");
            foreach (var FileItem in Directory.GetFiles(@"C:\Program Files (x86)\OESupport\Launchers\"))
            {
                try { File.Delete(FileItem); }
                catch (Exception e) 
                {
                    if (GlobalObjects.DebugHelper != null) { GlobalObjects.DebugHelper.WriteDebugLog(e); }
                    Console.WriteLine("ERR: " + e.Message); 
                }
            }

            // Make a report generator caller object here. 
            // This calls the DSRT from a CLI exe vs the webservice since it seems to not be working.
            // Based on a regkey, this item is either used or not to send up a scan item.
            var ReportGeneratorInvoker = new DriveCrashServiceInvoker();

            // 12-9-20 Using a new trigger to setup using an EXE scan or a BL called scan.
            string BaseKey = @"HKEY_CURRENT_USER\SOFTWARE\Drew Technologies, Inc\LoaderApplication";
            string UseEXE = (string)Registry.GetValue(BaseKey, "UseEXE", "false");
            bool UseEXEBool = UseEXE.ToLower() == "true";

            // Log out the value of use EXE
            GlobalObjects.DebugHelper.WriteDebugLog("USE EXE TRIGGER WAS FOUND TO BE: " + UseEXE.ToUpper());
            GlobalObjects.DebugHelper.WriteDebugLog("PREPARING TO SEND REPORT UP NOW USING PREDEFINED ROUTINES");

            // Do an EXE or BL Called scan here.
            bool ReportOK;
            if (UseEXEBool) { ReportOK = ReportGeneratorInvoker.SendReportUsingEXE(); }
            else
            { 
                GlobalObjects.DebugHelper.WriteDebugLog("RUNNING SOAP CALL TO THE DSRT NOW");
                ReportGeneratorInvoker.SendReportAsSOAP();

                GlobalObjects.DebugHelper.WriteDebugLog("SOAP UPLOAD RESULT: " + ReportGeneratorInvoker.SOAPUploadResult.ToString().ToUpper());
                ReportOK = ReportGeneratorInvoker.SOAPUploadResult;
            }

            if (ReportOK)
            {
                // Inform scan went out ok.
                GlobalObjects.DebugHelper.WriteDebugLog("SCAN UPLOADED OK!");

                // 10-13-20 This is never to be removed because it brings me joy unlike most other things - ZW
                GlobalObjects.DebugHelper.WriteDebugLog("IF YOU SEE THIS DEBUG LOG SOMEHOW, BY THE GRACE OF CTHULHU, THIS WHOLE CLUSTERFUCK WORKED OK.");
                GlobalObjects.DebugHelper.WriteDebugLog("CRANK UP THOSE SPEAKERS AND THROW ON DRAGON PIRATES BY TUT TUT CHILD AND VIBE");
                GlobalObjects.DebugHelper.WriteDebugLog("WE DID IT BOYS WE FOUND THE META");

                return true;
            }
            else
            {
                // Inform scan did not go out ok.
                GlobalObjects.DebugHelper.WriteDebugLog("REPORT COULD NOT BE SENT OUT!");
                return false;
            }
        }

        /// <summary>
        /// SHows the scan cancel window when requested.
        /// </summary>
        /// <returns>Returns bool showing worked or not.</returns>
        private bool CancelScanRequest()
        {
            // Setup Auto close
            GlobalObjects.SetupAutoCloseTimer(typeof(ConfirmCancelScan));

            try
            {
                GlobalObjects.DebugHelper.WriteDebugLog("REQUEST FOR SCAN CANCEL PROCESSED. SHOWING CANCEL BUTTONS NOW");
                GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
                {
                    GlobalObjects.ConfirmCancelButtons.Visibility = System.Windows.Visibility.Visible;
                    GlobalObjects.ScanWasTerminated = true;
                    GlobalObjects.DebugHelper.WriteDebugLog("SHOWING SCAN CANCEL VIEW NOW");

                    GlobalObjects.TypeOfLastView = typeof(ConfirmCancelScan);
                    GlobalObjects.ViewCloser = new LogicalHelpers.AutoCloseTimerCalls(GlobalObjects.TypeOfLastView);
                });

                return true;
            }

            catch (Exception uiEx)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO TOGGLE VIEW!");
                GlobalObjects.DebugHelper.WriteDebugLog("RESON: " + uiEx.Message);
                GlobalObjects.DebugHelper.WriteDebugLog(uiEx);

                return false;
            }
        }

        /// <summary>
        /// Writes timer values out to the debug file. THIS DOES NOT WRITE THEM TO THE GLOBAL TIME LIST!
        /// </summary>
        private void LogOutTimers()
        {
            GlobalObjects.FullProcTimer.Stop();
            var TimePassed = GlobalObjects.FullProcTimer.Elapsed;
            string TimeString = TimePassed.ToString("c");
            GlobalObjects.DebugHelper.WriteDebugLog("PROCESS STOPPED AT " + TimeString);

            for (int Counter = 0; Counter < GlobalObjects.AllProcTimers.Count; Counter++)
            {
                GlobalObjects.AllProcTimers[Counter].Stop();
                
                var TimerTime = GlobalObjects.AllProcTimers[Counter].Elapsed.ToString("c");
                string CmdDes = GlobalObjects.BaseTasks.AllTaskItems[Counter].CommandDescription;
                string index = Counter.ToString();

                GlobalObjects.DebugHelper.WriteDebugLog("PROCESS: [" + index + "] [" + CmdDes + "] [" + TimerTime + "]");
            }
        }

    }
}

