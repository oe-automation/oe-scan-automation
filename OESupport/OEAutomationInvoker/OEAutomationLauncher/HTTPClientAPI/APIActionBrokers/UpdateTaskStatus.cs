﻿using OEAutomationLauncher.HTTPClientAPI;
using System;
using System.Threading.Tasks;
using System.Windows;

namespace OEAutomationLauncher.AutomationLauncher.HTTPClientAPI.ActionBrokers
{
    /// <summary>
    /// Task Updating action functions.
    /// </summary>
    public class UpdateTaskStatus : APIActionTriggered
    {
        /// <summary>
        /// Constructor for the task item updater
        /// </summary>
        /// <param name="CommandString">Base full API Call string object.</param>
        public UpdateTaskStatus(string CommandString) : base(CommandString)
        {
            CommandType = "UPDATE_TASK";
            if (ExCommand.Contains("IncreaseProgressCounter"))
            {
                if (IncreaseTaskStatus()) { GlobalObjects.DebugHelper.WriteDebugLog("UPDATING TASK STATUS TO DONE", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO UPDATE TASK STATUS TO DONE", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("DecreaseProgressCounter"))
            {
                if (DecreaseTaskStatus()) { GlobalObjects.DebugHelper.WriteDebugLog("UPDATING TASK STATUS TO NOT DONE", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO UPDATE TASK STATUS TO NOT DONE", "ERR_" + CommandType, true); }
            }

            else { FailedToFindCommand(); }
        }

        /// <summary>
        /// Increases Taks List item status to 100% and marks the obj as done.
        /// </summary>
        /// <returns></returns>
        private bool IncreaseTaskStatus()
        {
            int TaskIndex = int.Parse(CommandSplit[4]);
            string TimeTaken = "";

            var ProcessTimer = GlobalObjects.AllProcTimers[TaskIndex];

            if (ExCommand.Contains("Increase"))
            {
                if (CommandSplit.Length < 6)
                {
                    ProcessTimer.Stop();
                    var TimePassed = ProcessTimer.Elapsed;
                    TimeTaken = TimePassed.ToString(@"mm\:ss\.fff");
                }

                else if (CommandSplit[5] == "00_00-00")
                {
                    ProcessTimer.Stop();
                    var TimePassed = ProcessTimer.Elapsed;
                    TimeTaken = TimePassed.ToString(@"mm\:ss\.fff");
                }

                else
                {
                    TimeTaken = CommandSplit[5];
                    TimeTaken = TimeTaken.Replace("_", ":").Replace("-", ".");
                }

                if (GlobalObjects.AllTimes.Count == 0) { GlobalObjects.AllTimes.Add(TimeTaken); }
                else 
                {
                    if (GlobalObjects.AllTimes[TaskIndex] != null) { GlobalObjects.AllTimes[TaskIndex] = TimeTaken; }
                    else { GlobalObjects.AllTimes[TaskIndex] = TimeTaken; }
                }
            }

            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                // 1-20-21  Set this up so the Spinner for please wait remains on top.
                // if (TaskIndex > 0) { GlobalObjects.ForceOnTop = true; }

                var Cmd = GlobalObjects.BaseTasks.UpdateTaskStatus(TaskIndex, TimeTaken);

                ResultsList.Add("UPDATED OK!");
                ResultsList.Add(TaskIndex.ToString());
                ResultsList.Add(Cmd.CommandDescription);
                ResultsList.Add(Cmd.CommandTimeElapsed);

                GlobalObjects.DebugHelper.WriteDebugLog("SINGLE TASK MARKED AS COMPLETE. TASK IS DESCRIBED AS: " + Cmd.CommandDescription, "API", true);
                GlobalObjects.DebugHelper.WriteDebugLog("SINGLE TASK MARKED AS COMPLETE. TASK IS DESCRIBED AS: " + Cmd.CommandDescription);
            });

            return true;
        }

        /// <summary>
        /// NOT IMPLIMENTED
        /// </summary>
        /// <returns></returns>
        private bool DecreaseTaskStatus()
        {
            throw new NotImplementedException();
        }
    }
}

