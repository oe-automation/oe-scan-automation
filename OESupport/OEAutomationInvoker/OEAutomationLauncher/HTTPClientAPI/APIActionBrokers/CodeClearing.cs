﻿using OEAutomationLauncher.AutomationLauncher.ConfirmPrompts;
using OEAutomationLauncher.HTTPClientAPI;
using System;
using System.Net;
using System.Windows;

namespace OEAutomationLauncher.AutomationLauncher.HTTPClientAPI.ActionBrokers
{
    /// <summary>
    /// Actions to process code clearing calls.
    /// </summary>
    public class CodeClearing : APIActionTriggered
    {
        /// <summary>
        /// Constructor for ViewInvoker
        /// </summary>
        /// <param name="CommandString">Base input string from the API Call</param>
        public CodeClearing(string CommandString) : base(CommandString)
        {
            CommandType = "CODE_CLEAR";
            if (ExCommand.Contains("ClearCodesOption"))
            {
                if (ClearCodesOptionsShow()) { GlobalObjects.DebugHelper.WriteDebugLog("SHOWING CODE CLEAR BUTTONS NOW", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SHOW CODE CLEAR BUTTONS", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("ClearCodesPass"))
            {
                if (ClearCodesPass()) { GlobalObjects.DebugHelper.WriteDebugLog("SHOWING CODE CLEAR PASS OK VIEW NOW", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SHOW CODE CLEAR PASS OK VIEW", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("ClearCodesFail"))
            {
                if (ClearCodesFail()) { GlobalObjects.DebugHelper.WriteDebugLog("SHOWING CODE CLEAR FAILED VIEW NOW", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SHOW CODE CLEAR FAILED VIEW", "ERR_" + CommandType, true); }
            }

            else { FailedToFindCommand(); }
        }

        /// <summary>
        /// SHows the clear codes buttons object on the main window.
        /// </summary>
        /// <returns></returns>
        private bool ClearCodesOptionsShow()
        {
            // Setup Stop Timer
            GlobalObjects.SetupAutoCloseTimer(typeof(ClearCodesDialogue));

            GlobalObjects.DebugHelper.WriteDebugLog("SHOWING CLEAR CODES BUTTONS VIEW NOW. UNHIDING FROM MAINWINDOW", "API", true);
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                var CodeClearButtons = GlobalObjects.MainWindowView.ClearCodesButtonsView;
                CodeClearButtons.Visibility = Visibility.Visible;
                CodeClearButtons.ClearCodes = true;
                CodeClearButtons.RetryClear = false;
                CodeClearButtons.ConfirmClearTextBlock.Text = "Would You Like To Clear All Stored DTCs In This Vehicle?";
                GlobalObjects.DebugHelper.WriteDebugLog("BUTTONS SHOWN OK", "API", true);

                    /*
                    Task.Run(() =>
                    {
                        GlobalObjects.DebugHelper.WriteDebugLog("PREPARING FOR AUTO CLOSE IF USER DOES NOT RESPOND IN 10 SECONDS", "API", true);
                        
                        Thread.Sleep(10000);
                        if (CodeClearButtons.Visibility == Visibility.Visible)
                        {
                            GlobalObjects.DebugHelper.WriteDebugLog("CLOSING AND PICKING NO. USER AFK (DUMBASS...)", "API", true);

                            ClearCodesDialogue.DontScanIdleTrigger(true);
                            CodeClearButtons.Visibility = Visibility.Hidden;
                        }
                    });
                    */
            });

            ResultsList.Add("CODE CLEAR VIEW SHOWN");
            return true;
        }

        /// <summary>
        /// Shows user info about passed code clear.
        /// </summary>
        /// <returns></returns>
        private bool ClearCodesPass()
        {
            GlobalObjects.DebugHelper.WriteDebugLog("CODE CLEAR PASSED", "API", true);
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                var CodeClearButtons = GlobalObjects.MainWindowView.ClearCodesButtonsView;

                CodeClearButtons.Visibility = Visibility.Visible;
                CodeClearButtons.ClearCodes = true;
                CodeClearButtons.RetryClear = false;
                CodeClearButtons.ConfirmButtonsGrid.Visibility = Visibility.Hidden;
                CodeClearButtons.OKReturnToScanButton.Visibility = Visibility.Visible;

                CodeClearButtons.ConfirmClearTextBlock.Text = "Codes Were Cleared From The Vehicle OK!";
                ResultsList.Add("CODE CLEAR PASSED");

                GlobalObjects.DebugHelper.WriteDebugLog("BUTTONS SHOWN OK", "API", true);

                /*
                Task.Run(() =>
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("PREPARING FOR AUTO CLOSE IF USER DOES NOT RESPOND IN 10 SECONDS", "API", true);

                    Thread.Sleep(10000);
                    if (CodeClearButtons.Visibility == Visibility.Visible)
                    {
                        GlobalObjects.DebugHelper.WriteDebugLog("CLOSING AND PICKING NO. USER AFK (DUMBASS...)", "API", true);

                        ClearCodesDialogue.DontScanIdleTrigger(true);
                        CodeClearButtons.Visibility = Visibility.Hidden;
                    }
                });
                */
            });

            if (GlobalObjects.PreOrPost == "") { GlobalObjects.PreOrPost = "OTHER"; }
            GlobalObjects.DebugHelper.WriteDebugLog("SETTING SCAN TYPE IN THE BOOTLOADER NOW!");
            GlobalObjects.DebugHelper.WriteDebugLog("TYPE TO SET: " + GlobalObjects.PreOrPost);

            try
            {
                Uri ScanUri = new Uri("http://localhost:15000/DrewTechDataSvc/SetOEScanType/" + GlobalObjects.PreOrPost);
                new WebClient().DownloadStringAsync(ScanUri);
            }
            catch (Exception WebEx)
            { 
                GlobalObjects.DebugHelper.WriteDebugLog("CAN NOT SET SCAN TYPE OBJECT HERE!");
                GlobalObjects.DebugHelper.WriteDebugLog(WebEx, false, false);
            }

            ResultsList.Add("CODE CLEAR VIEW SHOWN");
            return true;
        }

        /// <summary>
        /// Shows user when the codes clear fails.
        /// </summary>
        /// <returns></returns>
        private bool ClearCodesFail()
        {
            GlobalObjects.DebugHelper.WriteDebugLog("CODE CLEAR FAILED. ASKING FOR RETRY NOW.", "API", true);
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                var CodeClearButtons = GlobalObjects.MainWindowView.ClearCodesButtonsView;

                CodeClearButtons.Visibility = Visibility.Visible;
                CodeClearButtons.ClearCodes = false;
                CodeClearButtons.RetryClear = true;

                if (CodeClearButtons.ClearAttempts >= 3)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("TOO MANY FAILED CODE CLEARS!!!", "API", true);

                    CodeClearButtons.ConfirmButtonsGrid.Visibility = Visibility.Hidden;
                    CodeClearButtons.OKReturnToScanButton.Visibility = Visibility.Visible;
                    CodeClearButtons.ConfirmClearTextBlock.Text = "Failed To Clear DTCs. For Assistance Please Reach Out To IVS 360 Support.";
                    ResultsList.Add("CODE CLEAR FAILED THREE TIMES. CANCELING REPEAT");

                    GlobalObjects.DebugHelper.WriteDebugLog("BUTTONS SHOWN OK", "API", true);

                    /*
                    Task.Run(() =>
                    {
                        GlobalObjects.DebugHelper.WriteDebugLog("PREPARING FOR AUTO CLOSE IF USER DOES NOT RESPOND IN 10 SECONDS", "API", true);

                        Thread.Sleep(10000);
                        if (CodeClearButtons.Visibility == Visibility.Visible)
                        {
                            GlobalObjects.DebugHelper.WriteDebugLog("CLOSING AND PICKING NO. USER AFK (DUMBASS...)", "API", true);

                            ClearCodesDialogue.DontScanIdleTrigger(false);
                            CodeClearButtons.Visibility = Visibility.Hidden;
                        }
                    });
                    */
                }
                else
                {
                    CodeClearButtons.ConfirmButtonsGrid.Visibility = Visibility.Visible;
                    CodeClearButtons.OKReturnToScanButton.Visibility = Visibility.Hidden;
                    CodeClearButtons.ConfirmClearTextBlock.Text = "Code Clearing Failed! Would You Like To Retry Clearing DTCs?";

                        /*
                        Task.Run(() =>
                        {
                            GlobalObjects.DebugHelper.WriteDebugLog("PREPARING FOR AUTO CLOSE IF USER DOES NOT RESPOND IN 10 SECONDS", "API", true);

                            Thread.Sleep(10000);
                            if (CodeClearButtons.Visibility == Visibility.Visible)
                            {
                                GlobalObjects.DebugHelper.WriteDebugLog("CLOSING AND PICKING NO. USER AFK (DUMBASS...)", "API", true);

                                ClearCodesDialogue.DontScanIdleTrigger(false);
                                CodeClearButtons.Visibility = Visibility.Hidden;
                            }
                        });
                        */
                }
            });

            if (GlobalObjects.PreOrPost == "") { GlobalObjects.PreOrPost = "OTHER"; }
            GlobalObjects.DebugHelper.WriteDebugLog("SETTING SCAN TYPE IN THE BOOTLOADER NOW!");
            GlobalObjects.DebugHelper.WriteDebugLog("TYPE TO SET: " + GlobalObjects.PreOrPost);

            try
            {
                Uri ScanUri = new Uri("http://localhost:15000/DrewTechDataSvc/SetOEScanType/" + GlobalObjects.PreOrPost);
                new WebClient().DownloadStringAsync(ScanUri);
            }
            catch (Exception WebEx)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("CAN NOT SET SCAN TYPE OBJECT HERE!");
                GlobalObjects.DebugHelper.WriteDebugLog(WebEx, false, false);
            }

            ResultsList.Add("CODE CLEAR VIEW SHOWN");
            return true;
        }
    }
}

