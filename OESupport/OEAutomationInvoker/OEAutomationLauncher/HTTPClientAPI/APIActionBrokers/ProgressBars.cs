﻿using OEAutomationLauncher.HTTPClientAPI;

namespace OEAutomationLauncher.AutomationLauncher.HTTPClientAPI.ActionBrokers
{
    /// <summary>
    /// Actions to process progress bar updates
    /// </summary>
    public class ProgressBars : APIActionTriggered
    {
        private int TaskIndex;
        private int TickCount;

        /// <summary>
        /// Constructor for Progress Bar actions
        /// </summary>
        /// <param name="CommandString">Base full API Call string object.</param>
        public ProgressBars(string CommandString) : base(CommandString)
        {
            if (!ExCommand.Contains("Looping"))
            {
                TaskIndex = int.Parse(CommandSplit[4]);
                TickCount = 100;
            }

            CommandType = "PROGRESS_BAR";
            if (ExCommand.Contains("StartProgressBar"))
            {
                if (StartProgressBar()) { GlobalObjects.DebugHelper.WriteDebugLog("STARTING NEW PROGRESS BAR", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO START NEW PROGRESS BAR", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("TickProgressBar"))
            {
                if (TickProgressBar()) { GlobalObjects.DebugHelper.WriteDebugLog("TICKING PROGRESS BAR", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO TICK PROGRESS BAR", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("HideProgressBar"))
            {
                if (HideProgressBar()) { GlobalObjects.DebugHelper.WriteDebugLog("HIDING PROGRESS BAR", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT HIDE PROGRESS BAR", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("LoopingProgress"))
            {
                if (LoopingProgress()) { GlobalObjects.DebugHelper.WriteDebugLog("LOOPING PROGRESS BAR. UPDATING VIEW", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT LOOP PROGRESS BAR", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("SetCustomMessage"))
            {
                if (SetCustomMessage()) { GlobalObjects.DebugHelper.WriteDebugLog("LOOPING PROGRESS BAR. UPDATING VIEW", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT LOOP PROGRESS BAR", "ERR_" + CommandType, true); }
            }

            else { FailedToFindCommand(); }
        }


        private bool StartProgressBar()
        {
            if (CommandSplit.Length > 4) { TickCount = int.Parse(CommandSplit[5]); }
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                var Cmd = GlobalObjects.BaseTasks.ProgressBarInit(TaskIndex, TickCount);
                GlobalObjects.AllProcTimers[TaskIndex].Start();

                ResultsList.Add("PROGBAR MADE OK!");
                ResultsList.Add("TIMER MADE OK!");
                ResultsList.Add(TaskIndex.ToString());
                ResultsList.Add(Cmd.CommandDescription);
                ResultsList.Add(Cmd.ProgBar.Value.ToString("F2"));
                ResultsList.Add(Cmd.ProgBar.Maximum.ToString("F2"));

                GlobalObjects.DebugHelper.WriteDebugLog("PROGRESS BAR MADE FOR TASK: " + Cmd.CommandDescription, "API", true);
                    // GlobalObjects.DebugHelper.WriteDebugLog("PROGRESS BAR MADE FOR TASK: " + Cmd.CommandDescription);
                });

            return true;
        }

        private bool TickProgressBar()
        {
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                    // MAX VALUE OF 5 TICKS PER RUN. DONT WANNA GO TOO WILD.
                    if (CommandSplit.Length > 4) { TickCount = int.Parse(CommandSplit[5]); }

                var Cmd = GlobalObjects.BaseTasks.ProgressBarTick(TaskIndex, TickCount);

                ResultsList.Add("PROGBAR TICKED OK!");
                ResultsList.Add(TaskIndex.ToString());
                ResultsList.Add(Cmd.CommandDescription);
                ResultsList.Add(Cmd.ProgBar.Value.ToString("F2"));
                ResultsList.Add(Cmd.ProgBar.Maximum.ToString("F2"));
            });

            return true;
        }

        private bool HideProgressBar()
        {
            if (CommandSplit.Length > 4) { TickCount = int.Parse(CommandSplit[5]); }
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                var Cmd = GlobalObjects.BaseTasks.HideProgressBar(TaskIndex);
                ResultsList.Add("PROGBAR HIDDEN OK!");
                ResultsList.Add(TaskIndex.ToString());
                ResultsList.Add(Cmd.CommandDescription);
                ResultsList.Add(Cmd.ProgBar.Value.ToString("F2"));
                ResultsList.Add(Cmd.ProgBar.Maximum.ToString("F2"));

                GlobalObjects.DebugHelper.WriteDebugLog("PROGRESS BAR HIDDEN FOR TASK: " + Cmd.CommandDescription, "API", true);
                    // GlobalObjects.DebugHelper.WriteDebugLog("PROGRESS BAR HIDDEN FOR TASK: " + Cmd.CommandDescription);
                });

            return true;
        }

        private bool LoopingProgress()
        {
            TaskIndex = int.Parse(CommandSplit[4]);
            string Combined = CommandSplit[5];
            int CurrentLoop = int.Parse(Combined.Split('_')[0]);
            int AllLoops = int.Parse(Combined.Split('_')[1]);

            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                var Cmd = GlobalObjects.BaseTasks.ProgressBarLooping(TaskIndex, CurrentLoop, AllLoops);
                ResultsList.Add("PROGBAR LOOPING START OK!");
                ResultsList.Add(TaskIndex.ToString());
                ResultsList.Add(Cmd.CommandDescription);
                ResultsList.Add("CURRENT LOOP: " + CurrentLoop);
                ResultsList.Add("TOTAL LOOPS: " + AllLoops);

                GlobalObjects.DebugHelper.WriteDebugLog("PROGRESS BAR NOW LOOPING FOR TASK: " + Cmd.CommandDescription, "API", true);
                    // GlobalObjects.DebugHelper.WriteDebugLog("PROGRESS BAR NOW LOOPING FOR TASK: " + Cmd.CommandDescription);
                });

            return true;
        }

        private bool SetCustomMessage()
        {
            TaskIndex = int.Parse(CommandSplit[4]);
            string MessageValue = CommandSplit[5].Replace("%20", " ");
            string Endless = CommandSplit[6].ToUpper();

            bool EndlessBool = false;
            if (Endless == "YES") { EndlessBool = true; }

            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                var Cmd = GlobalObjects.BaseTasks.SetCustomMessage(TaskIndex, MessageValue, EndlessBool);
                ResultsList.Add("PROGBAR MESSAGE SET OK!");
                ResultsList.Add(TaskIndex.ToString());
                ResultsList.Add(Cmd.CommandDescription);
                ResultsList.Add(Cmd.ProgBarTextBlock.Text);

                GlobalObjects.DebugHelper.WriteDebugLog("PROGRESS BAR MESSAGE SET FOR TASK: " + Cmd.CommandDescription + " MESSAGE IS: " + Cmd.ProgBarTextBlock.Text, "API", true);
                    // GlobalObjects.DebugHelper.WriteDebugLog("PROGRESS BAR NOW LOOPING FOR TASK: " + Cmd.CommandDescription);
                });

            return true;
        }
    }
}

