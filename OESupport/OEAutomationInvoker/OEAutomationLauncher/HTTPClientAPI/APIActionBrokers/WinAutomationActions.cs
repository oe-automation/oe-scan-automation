﻿using OEAutomationLauncher.AutomationLauncher.ObjectClasses;
using OEAutomationLauncher.HTTPClientAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.HTTPClientAPI.ActionBrokers
{
    public class WinAutomationActions : APIActionTriggered
    {
        /// <summary>
        /// Used to WinAuto helper actions from the API.
        /// </summary>
        /// <param name="CommandString">Base input string from the API Call.</param>
        public WinAutomationActions(string CommandString) : base(CommandString)
        {
            CommandType = "WINAUTO_GET_SET";
            if (ExCommand.Contains("ReturnOEType"))
            {
                if (ReturnOEType()) { GlobalObjects.DebugHelper.WriteDebugLog("PULLING OE TYPE AND RETURNING IT NOW", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO RETURN OE TYPE", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("WinAutoDebugFile"))
            {
                if (WinAutoDebugFile()) { GlobalObjects.DebugHelper.WriteDebugLog("SETTING OR GETTING WINAUTO DEBUG FILE", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SET OR GET WINAUTO DEBUG FILE", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("TimeScanStarted"))
            {
                if (TimeScanStarted()) { GlobalObjects.DebugHelper.WriteDebugLog("SETTING OR GETTING TIME SCAN BEGAN STRING", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SET OR GET TIME FOR THE SCAN INIT", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("NeedsHelpValue"))
            {
                if (NeedsHelpValue()) { GlobalObjects.DebugHelper.WriteDebugLog("SETTING OR GETTING NEEDS HELP VALUE", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT GET OR SET THE NEEDS HELP VALUE", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("CommandIndexValue"))
            {
                if (CommandIndexValue()) { GlobalObjects.DebugHelper.WriteDebugLog("SETTING OR GETTING THE COMMAND INDEX VALUE", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT GET OR SET THE COMMAND INDEX VALUE", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("ReturnOESoftwareName"))
            {
                if (ReturnOESoftwareName()) { GlobalObjects.DebugHelper.WriteDebugLog("RETURNING THE OE SOFTWARE NAME IN USE", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("ERROR RETURNING THE OE SOFTWARE NAME IN USE", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("LastErrors"))
            {
                if (LastErrors()) { GlobalObjects.DebugHelper.WriteDebugLog("SETTING OR GETTING THE LAST ERROR VALUES OK! THESE CAN BE USED IN FUTURE CALLS", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("ERROR SETTING OR GETTING THE LAST ERRORS AND EXCEPTIONS!", "ERR_" + CommandType, true); }
            }

            else { FailedToFindCommand(); }
        }

        /// <summary>
        /// Returns the OE Type from the WinAuto Global object.
        /// </summary>
        /// <returns>Bool showing this function worked or not.</returns>
        private bool ReturnOEType()
        {
            try
            {
                GlobalObjects.DebugHelper.WriteDebugLog("PULLING THE OE TYPE FROM OUR GLOBAL WINAUTO HELPER NOW");
                GlobalObjects.DebugHelper.WriteDebugLog("VALUE FOUND: " + GlobalObjects.WinAutoArgs.OEType);

                ResultsList.Add(GlobalObjects.WinAutoArgs.OEType);
                return true;
            }
            catch (Exception ex)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO PULL OE TYPE!");
                GlobalObjects.DebugHelper.WriteDebugLog("ERR: " + ex.Message);
                GlobalObjects.DebugHelper.WriteDebugLog(ex);

                ResultsList.Add("FAILED TO RETURN OE TYPE");
                ResultsList.Add(ex.Message);

                return false;
            }
        }

        /// <summary>
        /// Get or Set WinAutomation Debug file name based on the args involved here.
        /// </summary>
        /// <returns>Bool showing this function worked or not.</returns>
        private bool WinAutoDebugFile()
        {
            // Setting value
            if (CommandSplit.Length > 3)
            {
                try
                {
                    string SetFileName = CommandSplit[4];
                    GlobalObjects.DebugHelper.WriteDebugLog("SETTING WINAUTO DEBUG FILE NAME.");
                    GlobalObjects.DebugHelper.WriteDebugLog("NAME TO SET: " + SetFileName);

                    GlobalObjects.WinAutoArgs.WinAutoDebugFile = SetFileName;
                    GlobalObjects.DebugHelper.WriteDebugLog("COMPARING NEW FILE VS VALUE IN THE WINAUTO OBJECT");

                    if (GlobalObjects.WinAutoArgs.WinAutoDebugFile == SetFileName)
                    {
                        GlobalObjects.DebugHelper.WriteDebugLog("SET EVERYTHING OK!");

                        ResultsList.Add("FILE SET");
                        ResultsList.Add(SetFileName);

                        return true;
                    }
                    else { throw new Exception("FILE NAME WAS EITHER INVALID OR WAS SEEN TO BE A FULL PATH RATHER THAN A NAME"); }
                }

                catch (Exception setEx)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SET THE DEBUG FILE NAME");
                    GlobalObjects.DebugHelper.WriteDebugLog("ERR: " + setEx.Message);
                    GlobalObjects.DebugHelper.WriteDebugLog(setEx);

                    ResultsList.Add("FAILED TO SET FILE NAME");
                    ResultsList.Add(setEx.Message);

                    return false;
                }
            }

            // Pulling Value
            else
            {
                if (string.IsNullOrEmpty(GlobalObjects.WinAutoArgs.WinAutoDebugFile))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("WINAUTO DEBUG FILE WAS NOT SET YET!");
                    GlobalObjects.DebugHelper.WriteDebugLog("CAN NOT RETURN A FILE THAT ISNT SET");

                    ResultsList.Add("WINAUTO FILE WAS NEVER SET. CAN NOT RETURN FILE NAME");

                    return false;
                }
                else
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("RETURNING WINAUTOMATION DEBUG FILE NAME NOW");
                    GlobalObjects.DebugHelper.WriteDebugLog("FILE NAME: " + GlobalObjects.WinAutoArgs.WinAutoDebugFile);

                    ResultsList.Add(GlobalObjects.WinAutoArgs.WinAutoDebugFile);

                    return true;
                }
            }
        }

        /// <summary>
        /// Sets or Gets the time string indicating when the scan session began.
        /// </summary>
        /// <returns>Bool showing this function worked or not.</returns>
        private bool TimeScanStarted()
        {
            // Setting Value
            if (CommandSplit.Length > 3)
            {
                try
                {
                    string SetTimeString = CommandSplit[4];
                    GlobalObjects.DebugHelper.WriteDebugLog("SETTING TIME OF SCAN INIT");
                    GlobalObjects.DebugHelper.WriteDebugLog("NAME TO SET: " + SetTimeString);

                    GlobalObjects.WinAutoArgs.TimeScanStarted = SetTimeString;
                    ResultsList.Add("TIME SET");
                    ResultsList.Add(SetTimeString);

                    return true;
                }

                catch (Exception setEx)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SET THE TIME STRING VALUE");
                    GlobalObjects.DebugHelper.WriteDebugLog("ERR: " + setEx.Message);
                    GlobalObjects.DebugHelper.WriteDebugLog(setEx);

                    ResultsList.Add("FAILED TO SET TIME STRING");
                    ResultsList.Add(setEx.Message);

                    return false;
                }
            }

            // Pulling Value
            else
            {
                if (string.IsNullOrEmpty(GlobalObjects.WinAutoArgs.TimeScanStarted))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("TIME STRING WAS NOT SET YET!");
                    GlobalObjects.DebugHelper.WriteDebugLog("CAN NOT RETURN A TIME STRING THAT ISNT SET");

                    ResultsList.Add("WINAUTO FILE WAS NEVER SET. CAN NOT RETURN TIME STRING VALUE");

                    return false;
                }
                else
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("RETURNING TIME STRING FOR SCAN INIT NOW");
                    GlobalObjects.DebugHelper.WriteDebugLog("TIME STRING: " + GlobalObjects.WinAutoArgs.TimeScanStarted);

                    ResultsList.Add(GlobalObjects.WinAutoArgs.TimeScanStarted);

                    return true;
                }
            }
        }

        /// <summary>
        /// Sets or Gets the NEEDS_HELP value for WinAutomation path choices.
        /// </summary>
        /// <returns>Bool showing this function worked or not.</returns>
        private bool NeedsHelpValue()
        {
            // Setting Value
            if (CommandSplit.Length > 3)
            {
                try
                {
                    string NewHelpValue = CommandSplit[4];
                    GlobalObjects.DebugHelper.WriteDebugLog("SETTING NEEDS HELP VALUE");
                    GlobalObjects.DebugHelper.WriteDebugLog("VALUE TO SET: " + NewHelpValue);

                    var UpperValue = NewHelpValue.ToUpper();
                    GlobalObjects.WinAutoArgs.NeedsHelpValue = UpperValue;

                    if (UpperValue == GlobalObjects.WinAutoArgs.NeedsHelpValue)
                    {
                        GlobalObjects.WinAutoArgs.TimeScanStarted = NewHelpValue;
                        ResultsList.Add("NEEDS HELP VALUE");
                        ResultsList.Add(NewHelpValue);

                        return true;
                    }
                    else { throw new Exception("CAN NOT SET THE NEW HELP VALUE TO ANYTHING BUT TRUE/FALSE"); }
                }

                catch (Exception setEx)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SET NEW VALUE FOR NEEDS HELP");
                    GlobalObjects.DebugHelper.WriteDebugLog("ERR: " + setEx.Message);
                    GlobalObjects.DebugHelper.WriteDebugLog(setEx);

                    ResultsList.Add("FAILED TO SET NEEDS HELP");
                    ResultsList.Add(setEx.Message);

                    return false;
                }
            }

            // Pulling Value
            else
            {
                if (string.IsNullOrEmpty(GlobalObjects.WinAutoArgs.NeedsHelpValue))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("NEEDS HELP STRING WAS NOT SET YET!");
                    GlobalObjects.DebugHelper.WriteDebugLog("CAN NOT RETURN A NEEDS HELP VALUE STRING THAT ISNT SET");

                    ResultsList.Add("NEEDS HELP WAS NOT SET");

                    return false;
                }
                else
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("RETURNING NEEDS HELP VALUE NOW");
                    GlobalObjects.DebugHelper.WriteDebugLog("NEEDS HELP: " + GlobalObjects.WinAutoArgs.NeedsHelpValue);

                    ResultsList.Add(GlobalObjects.WinAutoArgs.NeedsHelpValue);

                    return true;
                }
            }
        }

        /// <summary>
        /// Gets or Sets the Command Index to begin the GUID and Report Helper funcs.
        /// </summary>
        /// <returns>Bool showing this function worked or not.</returns>
        private bool CommandIndexValue()
        {
            // Setting Value
            if (CommandSplit.Length > 3)
            {
                try
                {
                    string IndexToSet = CommandSplit[4];
                    GlobalObjects.DebugHelper.WriteDebugLog("SETTING COMMAND INDEX NOW");
                    GlobalObjects.DebugHelper.WriteDebugLog("INDEX TO SET: " + IndexToSet);

                    if (int.TryParse(IndexToSet, out int CmdIndex))
                    {
                        GlobalObjects.WinAutoArgs.CommandIndexValue = CmdIndex.ToString();
                        ResultsList.Add("COMMAND INDEX");
                        ResultsList.Add(IndexToSet);

                        return true;
                    }
                    else { throw new Exception("COMMAND INDEX GIVEN WAS NOT AN INT VALUE. THIS CAN NOT BE SET"); }
                }

                catch (Exception setEx)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SET A NEW COMMAND INDEX VALUE");
                    GlobalObjects.DebugHelper.WriteDebugLog("ERR: " + setEx.Message);
                    GlobalObjects.DebugHelper.WriteDebugLog(setEx);

                    ResultsList.Add("FAILED TO COMMAND INDEX");
                    ResultsList.Add(setEx.Message);

                    return false;
                }
            }

            // Pulling Value
            else
            {
                if (string.IsNullOrEmpty(GlobalObjects.WinAutoArgs.CommandIndexValue))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("COMMAND INDEX WAS NOT SET YET!");
                    GlobalObjects.DebugHelper.WriteDebugLog("CAN NOT RETURN A COMMAND INDEX THAT ISNT SET");

                    ResultsList.Add("COMMAND INDEX WAS NEVER SET");

                    return false;
                }
                else
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("RETURNING COMMAND INDEX FOR FUNCS NOW");
                    GlobalObjects.DebugHelper.WriteDebugLog("COMMAND INDEX:" + GlobalObjects.WinAutoArgs.CommandIndexValue);

                    ResultsList.Add(GlobalObjects.WinAutoArgs.CommandIndexValue);

                    return true;
                }
            }
        }

        /// <summary>
        /// Returns the value of the OE Sw Name in use based on the OE Type.
        /// </summary>
        /// <returns>Bool showing this function worked or not.</returns>
        private bool ReturnOESoftwareName()
        {
            try
            {
                GlobalObjects.DebugHelper.WriteDebugLog("PULLING BACK THE OE SOFTWARE NAME NOW FOR THIS VEHICLE");
                var OEBools = new OEMakeBools();
                string SWType = OEBools.ReturnSWName();

                if (SWType == "OE Software")
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT FIND A VALID SW TYPE?");
                    GlobalObjects.DebugHelper.WriteDebugLog("OE TYPE IN THE GLOBALS: " + GlobalObjects.OEType);

                    ResultsList.Add(SWType);
                    return false;
                }

                GlobalObjects.DebugHelper.WriteDebugLog("PULLED OUT OE SW NAME OK!");
                GlobalObjects.DebugHelper.WriteDebugLog("SOFTWARE NAME: " + SWType);

                ResultsList.Add(SWType);
                return true;
            }

            catch (Exception retEx)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO RETURN THE SW NAME FOR THIS OE");
                GlobalObjects.DebugHelper.WriteDebugLog("ERR: " + retEx.Message);
                GlobalObjects.DebugHelper.WriteDebugLog(retEx);

                return false;
            }
        }

        /// <summary>
        /// Sets the last set of erros for the current waj/automation script that is running.
        /// </summary>
        /// <returns>Bool showing this function worked or not.</returns>
        private bool LastErrors()
        {
            if (CommandSplit.Length > 3)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("SETTING LAST SET OF WINAUTO EXCEPTIONS NOW");
                try
                {
                    string NewStringOfErrors = CommandSplit[4].Replace("__", "|");
                    GlobalObjects.DebugHelper.WriteDebugLog("SPLITTING ERROR VALUES NOW");

                    string[] SplitErrors = NewStringOfErrors.Split('|');
                    GlobalObjects.WinAutoArgs.StringOfException = NewStringOfErrors;

                    if (SplitErrors.Length == 3)
                    {
                        GlobalObjects.WinAutoArgs.LastException = SplitErrors[0];
                        GlobalObjects.WinAutoArgs.RegionSub1 = SplitErrors[1];
                        GlobalObjects.WinAutoArgs.RegionSub2 = SplitErrors[2];

                        return true;
                    }

                    else
                    {
                        GlobalObjects.WinAutoArgs.StringOfException = "FAILED|NULL|NULL";
                        GlobalObjects.WinAutoArgs.LastException = "FAILED TO SPLIT ERROR STRING SEE FILE!";
                        GlobalObjects.WinAutoArgs.RegionSub1 = "NULL";
                        GlobalObjects.WinAutoArgs.RegionSub2 = "NULL";

                        GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO PARSE STRING OUT TO THREE PARTS BUT FOUND SOME VALUES OK.");
                        return false;
                    }
                }

                catch (Exception setEx)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SET NEW VALUE FOR ERRORS ON WINAUTO");
                    GlobalObjects.DebugHelper.WriteDebugLog("ERR: " + setEx.Message);
                    GlobalObjects.DebugHelper.WriteDebugLog(setEx);

                    ResultsList.Add("FAILED TO SET WINAUTO ERRORS");
                    ResultsList.Add(setEx.Message);

                    return false;
                }
            }

            // Pulling Value
            else
            {
                string ExString = GlobalObjects.WinAutoArgs.LastException + "|" + GlobalObjects.WinAutoArgs.RegionSub1 + "|" + GlobalObjects.WinAutoArgs.RegionSub2;
                if (ExString == "|||" || string.IsNullOrEmpty(GlobalObjects.WinAutoArgs.StringOfException))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("LAST EX AND REGIONS WAS NOT SET YET!");
                    GlobalObjects.DebugHelper.WriteDebugLog("CAN NOT RETURN AN ERROR STRING THAT ISNT SET");

                    ResultsList.Add("ERROR STRIGN WAS NEVER SET");

                    return false;
                }

                else
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("RETURNING ERROR STRING NOW");
                    GlobalObjects.DebugHelper.WriteDebugLog("ERROR STRING:" + GlobalObjects.WinAutoArgs.StringOfException);

                    ResultsList.Add(GlobalObjects.WinAutoArgs.StringOfException);

                    return true;
                }
            }
        }
    }
}
