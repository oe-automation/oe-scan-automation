﻿using OEAutomationLauncher.AutomationLauncher.AutomationTaskLists;
using OEAutomationLauncher.HTTPClientAPI;
using OEAutomationLauncher.OEOptionPicker;
using OEAutomationLauncher.RequestVIN;
using System;

namespace OEAutomationLauncher.AutomationLauncher.HTTPClientAPI.ActionBrokers
{
    /// <summary>
    /// API View Changing Calls
    /// </summary>
    public class ViewInvoker : APIActionTriggered
    {
        /// <summary>
        /// Constructor for ViewInvoker
        /// </summary>
        /// <param name="CommandString">Base input string from the API Call</param>
        public ViewInvoker(string CommandString) : base(CommandString)
        {
            CommandType = "VIEW_INVOKER";
            if (ExCommand.Contains("RequestVIN"))
            {
                if (RequestVINView()) { GlobalObjects.DebugHelper.WriteDebugLog("REQUEST VIN IS OPEN", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO SHOW REQUEST VIN", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("OptionPicker"))
            {
                if (OptionPickerView()) { GlobalObjects.DebugHelper.WriteDebugLog("OPTION PICKER IS OPEN", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO SHOW PICKER", "ERR_" + CommandType, true); }
            }

            else if (ExCommand.Contains("ResetAllItems"))
            {
                if (ResetAllItemsView()) { GlobalObjects.DebugHelper.WriteDebugLog("RESET ALL TASK ITEMS", "API", true); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SHOW TASK ITEMS", "ERR_" + CommandType, true); }
            }

            else { FailedToFindCommand(); }
        }

        /// <summary>
        /// SHows the VIN Entry view.
        /// </summary>
        /// <returns></returns>
        private bool RequestVINView()
        {
            // Setup Auto close
            GlobalObjects.SetupAutoCloseTimer(typeof(VINRequestUserControl));

            GlobalObjects.DebugHelper.WriteDebugLog("GOT REQUEST FOR VIN ENTRY. TRIGGERING UI CHANGE", "API", true);
            VINRequestTrigger.VINRequestStart();

            GlobalObjects.DebugHelper.WriteDebugLog("SHOWING VIN OBJECTS HERE", "API", true);

            ResultsList.Add("RAN VIN TOGGLE OK");

            return true;
        }

        /// <summary>
        /// Shows the option picker view.
        /// </summary>
        /// <returns></returns>
        private bool OptionPickerView()
        {
            string Type = CommandSplit[4];

            GlobalObjects.DebugHelper.IsOptionDebug = true;

            GlobalObjects.DebugHelper.WriteDebugLog("GOT REQUEST FOR OPTIONS CONFIG. TRIGGERING UI CHANGE", "API", true);
            OptionsTriggered.OptionsStart();

            GlobalObjects.DebugHelper.WriteDebugLog("SETTING OE PICKER VIEW VISIBILITY HERE", "API", true);
            OptionsTriggered.SetOEViewVisibility(Type);

            GlobalObjects.DebugHelper.WriteDebugLog("PICK DEM OPTIONS", "API", true);
            ResultsList.Add(OptionsTriggered.ReturnJSONOptions());

            return true;
        }

        /// <summary>
        /// Resets all items in the main task list view.
        /// </summary>
        /// <returns></returns>
        private bool ResetAllItemsView()
        {
            try { new System.Net.WebClient().DownloadStringAsync(new Uri("http://localhost:15000/DrewTechDataSvc/PleaseWait/OFF")); }
            catch (Exception WebEx)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT TURN OFF THE SPINNER ON THE BL IF IT EXISTS!");
                GlobalObjects.DebugHelper.WriteDebugLog(WebEx);
            }

            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                GlobalObjects.DebugHelper.WriteDebugLog("FULL PROCESS RESET HAS BEEN REQUESTED. CLEARNING EVERYTING OUT NOW.", "API", true);
                GlobalObjects.BaseTasks = new BaseTaskList(GlobalObjects.OEType);

                OptionsTriggered.OptionsComplete();
            });

            ResultsList.Add("RESET ALL ITEMS OK");

            return true;
        }
    }
}

