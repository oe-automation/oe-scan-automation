﻿using OEAutomationLauncher;
using OEAutomationLauncher.AutomationLauncher.HTTPClientAPI.ActionBrokers;
using OEAutomationLauncher.OEOptionPicker;
using OEAutomationLauncher.RequestVIN;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OEAutomationLauncher.AutomationLauncher.HTTPClientAPI
{
    public class ProgressAPIResponseTemplate
    {
        public string BaseCommand { get; set; }
        // public string Command { get; set; }
        public string RanOK { get; set; }
        public List<string> Results { get; set; }

        public ProgressAPIResponseTemplate(string InputCommand)
        {
            Results = new List<string>();
            string[] CommandSplit = InputCommand.Split('/');
            BaseCommand = CommandSplit[2];

            try
            {
                // For Individual regions: 
                //  Need to set them up so they call StartTimer, then run their actions.
                //  Once actions are done, call StopTimer and save the string back. 
                //  Put the string info into WinAutomations Debug file so we can log this info easier.
                //  Call the Increase function and pass in the index of the region, along with the time taken to run.

                // On 10-08-2020 ZW reworked the API Calling functions. This was a mess of a giant if statement. 
                // See APIActionTriggered for the new action calling. 
                // See ProgressAPIEndpoints for the new endpoints to call for external interfacing.
                // Doing this now only makes this cleaner, it allows internal use of all these commands as well.

                // On 10-13-2020 ZW Added Get Vehicle config API Endpoint triggers.

                if (BaseCommand.Contains("Debug"))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("PROCESSED A DEBUG COMMAND FROM THE API", "DEBUG_CMD");

                    var ActionResults = new DebugActions(InputCommand);
                    Results = ActionResults.ResultsList;
                }
                else if (BaseCommand.Contains("ViewInvoker"))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("PROCESSED A VIEW INVOKER COMMAND FROM THE API", "VIEW_INVOKE_CMD");

                    var ActionResults = new ViewInvoker(InputCommand);
                    Results = ActionResults.ResultsList;
                }
                else if (BaseCommand.Contains("CustomViews"))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("PROCESSED A TARGETED OE VIEW COMMAND FROM THE API", "CUSTOM_VIEW_CMD");

                    var ActionResults = new CustomViews(InputCommand);
                    Results = ActionResults.ResultsList;
                }
                else if (BaseCommand.Contains("Timers"))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("PROCESSED A TIMER COMMAND FROM THE API", "TIMER_CMD");

                    var ActionResults = new TimerActions(InputCommand);
                    Results = ActionResults.ResultsList;
                }
                else if (BaseCommand.Contains("ProgressBar"))
                {
                    // This was clogging up my debug logging. Turning this off for now.
                    // GlobalObjects.DebugHelper.WriteDebugLog("PROCESSED A PROGRESS BAR COMMAND FROM THE API", "PROG_BAR_CMD");

                    var ActionResults = new ProgressBars(InputCommand);
                    Results = ActionResults.ResultsList;
                }
                else if (BaseCommand.Contains("CodeClearing"))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("PROCESSED A CODE CLEARING COMMAND", "CODE_CLEAR_CMD");

                    var ActionResults = new CodeClearing(InputCommand);
                    Results = ActionResults.ResultsList;
                }
                else if (BaseCommand.Contains("UpdateStatus"))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("PROCESSED A STATUS UPDATE COMMAND FROM THE API", "CMD_STATUS_UPDATE");

                    var ActionResults = new UpdateTaskStatus(InputCommand);
                    Results = ActionResults.ResultsList;
                }
                else if (BaseCommand.Contains("ScanComplete"))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("PROCESSED COMPLETE SCAN COMMAND FROM THE API", "SCAN_COMPLETE_CMD");

                    var ActionResults = new ScanCompleted(InputCommand);
                    Results = ActionResults.ResultsList;
                }
                else if (BaseCommand.Contains("GetVehicle"))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("PROCESSED A GET VEHICLE INFO COMMAND FROM THE API", "GET_VEH_INFO_CMD");

                    var ActionResults = new GetVehicleInfo(InputCommand);
                    Results = ActionResults.ResultsList;
                }
                else if (BaseCommand.Contains("SetVehicle"))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("SETTING INFO IS NOT YET SUPPORTED", "ERR");
                    Results = new List<string> { "SETTING VEHICLE INFO IS NOT YET SUPPORTED!!" };
                }
                else if (BaseCommand.Contains("WinAuto"))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("PROCESSED A SET OR GET WINAUTOMATION INFO COMMAND FROM THE API", "GET_SET_WINAUTO_INFO");

                    var ActionResults = new WinAutomationActions(InputCommand);
                    Results = ActionResults.ResultsList;
                }
                else
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("COMMAND NOT RECGONIZED.");
                    GlobalObjects.DebugHelper.WriteDebugLog("COMMAND: " + InputCommand);

                    Results.Add("INVALID COMMAND: " + InputCommand);
                }

                RanOK = "true";
                Console.ForegroundColor = ConsoleColor.White;
            }

            catch (Exception e)
            {
                GlobalObjects.DebugHelper.WriteDebugLog(e, true);

                RanOK = "false";
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.Message);
                Results.Add(e.Message);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
