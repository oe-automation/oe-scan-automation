﻿using OEAutomationLauncher.AutomationLauncher.AutomationTaskLists;
using OEAutomationLauncher.AutomationLauncher.DebugHelpers;
using OEAutomationLauncher.OEOptionPicker;
using OEAutomationLauncher.RequestVIN;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OEAutomationLauncher.AutomationLauncher.ObjectClasses
{
    public class MainWindowViewHelpers
    {
        /// <summary>
        /// Launches the updater silently and waits for it to run out.
        /// </summary>
        /// <param name="CmdTextString"></param>
        public static void KickOffRPA(string CmdTextString)
        {
            // NEED TO UPDATE THE UPDATER PROGRAM TO UNDERSTAND IT NEEDS TO LOOK AT THE 
            // NEW LAUNCHER APP INSTEAD OF BL.

            var startInfo = new ProcessStartInfo();
            // if (!Debugger.IsAttached) { startInfo.WindowStyle = ProcessWindowStyle.Hidden; } 
            startInfo.FileName = @"C:\Program Files (x86)\OESupport\OEAppHelpers\OESupportUpdater.exe";
            startInfo.Arguments = CmdTextString;

            var ProcToRun = new Process();
            ProcToRun.StartInfo = startInfo;

            ProcToRun.Start();
        }

        /// <summary>
        /// Checks the OE type and makes the OEbase app object a new instance of the requested OE
        /// </summary>
        /// <returns>Bool which shows if we have a matching derived class of the OE requested or not.</returns>
        public static bool GetOEAppType(out string OEType)
        {
            // Stirng to hold the name of the current OE SW running. Passed in from CMD Line args.
            var allOeTypes = new List<string> { "TIS", "GDS2", "FJDS", "IHDS", "FCA", "CP3", "FDRS" };
            OEType = "";

            // Pull cmd line args and look to see what the OE SW in use here is.
            // Workaround for debug mode. This only works for TIS. Change it to another OE name for other testing.
            //if (Debugger.IsAttached) { OEType = "GDS2"; }   

            // Check args for an OE Name. IF none given return.
            if (!Debugger.IsAttached)
            {
                var AllArgs = Environment.GetCommandLineArgs();

                if (AllArgs.Length == 0) { return false; }
                else
                {
                    foreach (var Arg in AllArgs)
                    {
                        GlobalObjects.DebugHelper.WriteDebugLog("ARG: " + Arg);
                        if (allOeTypes.Contains(Arg))
                        {
                            OEType = Arg;
                            continue;
                        }

                        if (Arg.ToUpper() == "RESIZE")
                        {
                            GlobalObjects.CanResizeRelease = true;
                            continue;
                        }

                        else if (!Arg.Contains('\\') && Arg.Length != 17)
                        {
                            if (allOeTypes.Contains(Arg)) { OEType = Arg; }
                            else if (Arg.Contains("_")) { OEType = Arg.Split('_')[0]; }                 
                        }
                    }
                }
            }

            // If debugging we pick from the selected vehicle type
            if (Debugger.IsAttached) { OEType = FallBackVehicleConfigs.SelectedConfig.OEType; }

            // Make sure we have a value here then check for each type of OE instance. Make one that matches.
            if (OEType.Length == 0) { return false; }
            if (allOeTypes.Contains(OEType))
            {
                GlobalObjects.BaseTasks =
                    new BaseTaskList(OEType);
            }
            else { return false; }

            // If we have an FDJS Scan, remove the old session object.
            if (OEType.Contains("FJDS"))
            {
                // GlobalObjects.MainWindowView.DebugViewAndAPIFunctionsView.OptionsToggleDebugButton.IsEnabled = false;
                try { System.IO.Directory.Delete(@"C:\ProgramData\Ford Motor Company\FJDS\Sessions", true); }
                catch (Exception delEx)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT DELETE OLD SESSIONS!");
                    GlobalObjects.DebugHelper.WriteDebugLog(delEx);
                }
            }

            // Send back an OK if we got here.
            return true;
        }

        /// <summary>
        /// Sets the complete scan button state.
        /// </summary>
        /// <param name="ScanPassedOK">True for passed scan false for failed scan.</param>
        public static void ScanCompleteButtonToggle(bool ScanPassedOK)
        {
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                if (ScanPassedOK) GlobalObjects.MainWindowView.ScanCompleteButtonView.ConfirmCloseTextBlock.Text = "OE Scan Was Completed OK! Thanks For Choosing OPUS IVS";
                if (!ScanPassedOK) GlobalObjects.MainWindowView.ScanCompleteButtonView.ConfirmCloseTextBlock.Text = "OE Scan Failed! An OPUS IVS Support Representative Will Reach Out Shortly";

                GlobalObjects.ScanCompletePassed = ScanPassedOK;

                GlobalObjects.MainWindowView.ScanCompleteButtonView.Visibility = Visibility.Visible;
                GlobalObjects.MainWindowView.CancelOrStopOEScanButton.Content = "Return To Home Screen";
            });
        }

        public static List<string> ToggleActionsList(bool ResetTasks)
        {
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                if (ResetTasks) { GlobalObjects.BaseTasks = new BaseTaskList(GlobalObjects.OEType); }

                GlobalObjects.MainWindowView.OptionsPickerButtons.Visibility = Visibility.Hidden;
                GlobalObjects.MainWindowView.CancelScanGridButtons.Visibility = Visibility.Visible;

                GlobalObjects.MainWindowView.OptionsSelectionGrid.Visibility = Visibility.Hidden;
                GlobalObjects.MainWindowView.VINRequestGrid.Visibility = Visibility.Hidden;

                GlobalObjects.MainWindowView.TaskGrid.Visibility = Visibility.Visible;
                GlobalObjects.MainWindowView.TaskGridScrollViewer.Visibility = Visibility.Visible;
            });

            return new List<string> { "SHOWING TASK GRID." };
        }
        public static List<string> ToggleOptionsList(bool ShowView)
        {
            GetOEAppType(out string Type);

            if (ShowView)
            {
                GlobalObjects.DebugHelper.IsOptionDebug = true;

                GlobalObjects.DebugHelper.WriteDebugLog("GOT REQUEST FOR OPTIONS CONFIG. TRIGGERING UI CHANGE");
                OptionsTriggered.OptionsStart();

                GlobalObjects.DebugHelper.WriteDebugLog("SETTING OE PICKER VIEW VISIBILITY HERE");
                OptionsTriggered.SetOEViewVisibility(Type);
            }

            if (!ShowView) { OptionsTriggered.OptionsComplete(); }

            return new List<string> { "TOGGLED OPTIONS VIEW" };
        }
        public static List<string> ToggleVINRequest(bool ShowView)
        {
            if (ShowView) { VINRequestTrigger.VINRequestStart(); }
            if (!ShowView) { VINRequestTrigger.VINRequestComplete(); }

            return new List<string> { "VIN REQUEST TOGGLED" };
        }
    }
}
