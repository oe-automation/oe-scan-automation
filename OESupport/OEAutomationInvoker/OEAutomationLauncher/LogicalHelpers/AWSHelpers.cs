﻿using Amazon;
using Amazon.Auth.AccessControlPolicy;
using Amazon.S3;
using Amazon.S3.Transfer;
using OEAutomationLauncher.Properties;
using OEAutomationLauncher;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Input;
using Amazon.S3.IO;
using Microsoft.Win32;
using System.Net;
using Amazon.S3.Model;
using OEAutomationLauncher.AutomationLauncher.DebugHelpers;
using System.Text.RegularExpressions;
using OEAutomationLauncher.DebugHelpers;

namespace OEAutomationLauncher.AutomationLauncher.TaskListHelpers
{
    public class AWSHelpers
    {
        public AmazonS3Client S3Client;
        public TransferUtility Uploader;

        public static string TaskListFolder = @"Deploy Directories/OEAutomation/AutomationTaskLists";
        public static string DebugLogDirectory = @"Deploy Directories/OEAutomation/OEAutomationLogs";
        public static string TestingMatrixDirectory = @"Deploy Directories/OEAutomation/TestMatrixValues";

        public string S3BucketName = "drivepro.logs.upload";
        public string S3AccessKey = "AKIASQ7DDKVHCJMHJEXU";
        public string S3SecretKey = "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";

        public string TaskListFile;

        private List<string> FileNames = new List<string>();
        private string CombinedDateTime;

        private string CSVStringToUpload;

        public AWSHelpers()
        {
            S3Client = new AmazonS3Client(S3AccessKey, S3SecretKey, RegionEndpoint.EUWest1);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void GetOETaskList(string OEType)
        {
            string OutputDir = @"C:\Program Files (x86)\OESupport\Launchers\TaskListFiles";
            string OutputFile = OutputDir + "\\" + OEType + "_TaskList.txt";

            if (!Directory.Exists(OutputDir)) { Directory.CreateDirectory(OutputDir); }

            var nextFile = new Amazon.S3.Model.ListObjectsRequest
            {
                BucketName = S3BucketName,
                Prefix = TaskListFolder.Replace("\\", "/")
            };

            var timer = new System.Diagnostics.Stopwatch();
            timer.Start();

            var Downloader = new TransferUtility(S3Client);
            var ListResp = S3Client.ListObjects(nextFile).S3Objects;
            ListResp.Reverse();

            GlobalObjects.DebugHelper.WriteDebugLog("---- AWS PULLER STARTED UP ----");
            GlobalObjects.DebugHelper.WriteDebugLog("PULLING DOWN TASK LIST FILE FOR " + OEType);
            GlobalObjects.DebugHelper.WriteDebugLog("FOUND A TOTAL OF: " + ListResp.Count + " TASK FILES");

            foreach (var objectBack in ListResp)
            {
                if (!objectBack.Key.Contains(".txt")) { continue; }
                if (!objectBack.Key.Contains(OEType)) { continue; }

                Downloader.Download(OutputFile, S3BucketName, objectBack.Key);
                timer.Stop();

                var TimeTaken = new TimeSpan(timer.ElapsedTicks);
                GlobalObjects.DebugHelper.WriteDebugLog("OPERATION TOOK: " + TimeTaken.TotalSeconds.ToString("F3") + " SECONDS");

                TaskListFile = OutputFile;
                return;
            }

            GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT FIND A CONFIG FILE. DEFAULTING TO LOCAL RESOURCE TASK LIST");
            if (File.Exists(OutputFile)) { File.Delete(OutputFile); }
            if (OEType == "TIS") { File.WriteAllText(OutputFile, FallBackItems.TISFallBackTaskList); }
            if (OEType == "GDS2") { File.WriteAllText(OutputFile, FallBackItems.GDS2FallBackTaskList); }
            if (OEType == "FJDS") { File.WriteAllText(OutputFile, FallBackItems.FJDSFallBackTaskList); }
            if (OEType == "CP3") { File.WriteAllText(OutputFile, FallBackItems.CP3FallBackTaskList); }
            if (OEType == "FDRS") { File.WriteAllText(OutputFile, FallBackItems.FDRSFallBackTaskList); }

            else
            {
                GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT FIND A TASK LIST FOR THIS OE TYPE. THIS IS CRITICAL!");
                Environment.Exit(0);
            }
        }
        public async void PushDebugLogsToBucket(string J2534FileName, string SDFCombinedFile)
        {
            // CANCEL SCAN ClosingPleaseWaitBlock - SCANTERMINATED = TRUE 
            // SCAN COMPLETE ClosingPleaseWaitBlock - SCANTERMINATED = FALSE
            // IF SCANTERMINATED -> TextBlock ClosingBlock = Global.MainWindowView.CancelScanButtons.BLOCK_NAME
            // ELSE -> TextBlock ClosingBlock = Global.MainWindowView.ScanCompleteCloseWindow.BLOCK_NAME

            // Global.MainWindowView.Dispathcer.Invoke(() => 
            // {
            //      if (SCANTERMINATED) Global.MainWindowView.CancelScanButtons.BLOCK.Text = "TEXT_VALUE_HERE";
            //      if (!SCANTERMINATED) Global.MainWindowView.ScanCompleteCloseWindow.BLOCK.Text = "TEXT_VALUE_HERE";
            // });
            //      

            Uploader = new TransferUtility(S3Client);
            GlobalObjects.DebugHelper.WriteDebugLog("INITALIZED THE AWS CLIENT AND UPLOADING TIMER OK!");

            var UploadStopWatch = new Stopwatch();
            UploadStopWatch.Start();

            FileNames = GetAllFilesToUpload(J2534FileName, SDFCombinedFile);
            CSVStringToUpload = GenerateCSVString() + "\n";

            UpdateTextAndProgress("0%", 0, 5);

            StringsForTimes(out CombinedDateTime);
            UpdateTextAndProgress("20%", 1, 5);

            UploadFilesToAWS(FileNames);
            UpdateTextAndProgress("40%", 2, 5);

            UploadScanInfoString();
            UpdateTextAndProgress("60%", 3, 5);

            RemoveOldFiles();
            UpdateTextAndProgress("80%", 4, 5);

            UploadStopWatch.Stop();
            SendUpDebugFile(new TimeSpan(UploadStopWatch.ElapsedTicks));
            UpdateTextAndProgress("100%", 5, 5);

            System.Threading.Thread.Sleep(1000);
            UpdateTextAndProgress("", -1, 100);
        }

        public List<IS3FileSystemInfo> ReturnListOfFoldersInBucket()
        {
            GlobalObjects.TempTextToFill.Add("INITALIZED THE AWS CLIENT AND UPLOADING TIMER OK!");
            S3DirectoryInfo AllLogFiles = new S3DirectoryInfo(S3Client, S3BucketName, DebugLogDirectory);

            return AllLogFiles.GetFileSystemInfos().ToList();
        }
        public List<string> ReturnJSONForFolder(IS3FileSystemInfo FolderToScan)
        {
            var ListOfJSONs = new List<string>();
            string KeyString = DebugLogDirectory + "/" + FolderToScan.Name;

            var ListOfVinsUsed = new List<string>();
            var ListOfSubFolders = new S3DirectoryInfo(S3Client, S3BucketName, KeyString).GetFileSystemInfos();
            var FinalFolderList = new List<IS3FileSystemInfo>();

            foreach (var SubFolder in ListOfSubFolders)
            {
                string RegexVinPatt = "[0-9A-Z]{17}";
                string VinFound = Regex.Match(SubFolder.FullName, RegexVinPatt).Value;
                if (VinFound == null) { continue; }

                if (!ListOfVinsUsed.Contains(VinFound)) 
                {
                    ListOfVinsUsed.Add(VinFound);
                    FinalFolderList.Add(SubFolder);
                }
            }

            GlobalObjects.TempTextToFill.Add("SPLIT UP ALL REPEAT LOG DIRS AND REMOVED THEM FROM THE LIST OF ALL FOLDERS");
            GlobalObjects.TempTextToFill.Add("THERE SHOULD NOT BE ANY REPEAT VINS NOW");
            Parallel.ForEach(FinalFolderList, (CurrentFolder) =>
            {
                GlobalObjects.TempTextToFill.Add("FOLDER: " + CurrentFolder.Name);

                string FolderKey = KeyString + "/" + CurrentFolder.Name;
                var CurrentFolderContents = new S3DirectoryInfo(S3Client, S3BucketName, FolderKey).GetFileSystemInfos();
                foreach (var FileName in CurrentFolderContents)
                {
                    if (!FileName.Name.Contains("JSON")) { continue; }

                    GetObjectRequest fileRequest = new GetObjectRequest();
                    fileRequest.BucketName = S3BucketName;
                    fileRequest.Key = FolderKey + "/" + FileName.Name;

                    GetObjectResponse fileResponse = S3Client.GetObject(fileRequest);
                    StreamReader FileReader = new StreamReader(fileResponse.ResponseStream);

                    ListOfJSONs.Add(FileReader.ReadToEnd());
                }
            });

            return ListOfJSONs;
        }

        #region ProgAndStatus for GUI
        private void UpdateTextAndProgress(string TextToApply, int CurrentValue, int MaxValue)
        {
            UpdateTextBlockValues(TextToApply);
            UpdateProgressValues(CurrentValue, MaxValue);
        }

        private void UpdateTextBlockValues(string TextToSend)
        {
            if (GlobalObjects.ScanWasTerminated)
            {
                GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
                {
                    GlobalObjects.MainWindowView.ConfirmCancelButtonView.ClosingPleaseWaitBlock.Text = TextToSend;
                });
            }

            if (!GlobalObjects.ScanWasTerminated)
            {
                GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
                {
                    GlobalObjects.MainWindowView.ScanCompleteButtonView.ClosingPleaseWaitBlock.Text = TextToSend;
                });
            }
        }
        private void UpdateProgressValues(int CurrentValue, int MaxValue)
        {
            bool SetIndet = CurrentValue == -1;
            if (GlobalObjects.ScanWasTerminated)
            {
                GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
                {
                    GlobalObjects.MainWindowView.ConfirmCancelButtonView.ClosingNowProgBar.Maximum = MaxValue;
                    GlobalObjects.MainWindowView.ConfirmCancelButtonView.ClosingNowProgBar.Value = CurrentValue;
                    GlobalObjects.MainWindowView.ConfirmCancelButtonView.ClosingNowProgBar.IsIndeterminate = SetIndet;
                });
            }

            if (!GlobalObjects.ScanWasTerminated)
            {
                GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
                {
                    GlobalObjects.MainWindowView.ScanCompleteButtonView.ClosingNowProgBar.Maximum = MaxValue;
                    GlobalObjects.MainWindowView.ScanCompleteButtonView.ClosingNowProgBar.Value = CurrentValue;
                    GlobalObjects.MainWindowView.ScanCompleteButtonView.ClosingNowProgBar.IsIndeterminate = SetIndet;
                });
            }
        }
        #endregion

        #region SETUP FOR UPLOADING TO S3 BUCKET
        private void StringsForTimes(out string CombinedDateTime)
        {
            var DateTimeNow = DateTime.Now;
            string[] TimeNow = DateTimeNow.TimeOfDay.ToString().Split(':');
            string CombinedTimeNow = TimeNow[0] + TimeNow[1];

            string DateNow = DateTimeNow.ToShortDateString().Replace("/", "");
            CombinedDateTime = DateNow + CombinedTimeNow;
        }

        private List<string> GetAllFilesToUpload(string J2534FileName, string SDFCombinedFile)
        {
            List<string> FileNames = new List<string>
            {
                GlobalObjects.DebugHelper.APIDebugFileName,
                @"C:\Program Files (x86)\OESupport\OEDebugging\WinAutomationDebug\" + GlobalObjects.ConfigInfo.VIN + "_WinAutomationDebug.txt",
            };

            var FileSet = Directory.GetFiles(@"C:\temp");
            foreach (var FileObject in FileSet)
            {
                if (FileObject.Contains(GlobalObjects.ConfigInfo.VIN))
                {
                    try
                    {
                        GlobalObjects.DebugHelper.WriteDebugLog("FOUND REPORT FILE: " + FileObject);
                        FileNames.Add(FileObject);
                    }
                    catch (Exception fileEx)
                    {
                        GlobalObjects.DebugHelper.WriteDebugLog(fileEx);
                        continue;
                    }
                }
            }

            if (J2534FileName.Length > 0) { FileNames.Add(J2534FileName); }
            else { GlobalObjects.DebugHelper.WriteDebugLog("J2534 COMBINED LOG FILE DOES NOT EXIST!"); }

            if (SDFCombinedFile.Length > 0) { FileNames.Add(SDFCombinedFile); }
            else { GlobalObjects.DebugHelper.WriteDebugLog("SDF CONVERSION FILE WAS NOT FOUND!"); }

            if (GlobalObjects.OEBase != null) { FileNames.Add(GlobalObjects.OEBase.DebugHelper.MainDebugFile); }
            else { GlobalObjects.DebugHelper.WriteDebugLog("OE OPTION PICKER DEBUG FILE DOES NOT EXIST!"); }

            if (GlobalObjects.Colorizer.HTMLOutput != null) { FileNames.Add(GlobalObjects.Colorizer.HTMLOutput); }
            else { GlobalObjects.DebugHelper.WriteDebugLog("HTML DEBUG LOG WAS NOT FOUND!"); }

            if (GlobalObjects.OEType == "TIS")
            {
                string RecallFile = GlobalObjects.MainWindowView.TISRecallView.CleanedRecallInfo;
                if (File.Exists(RecallFile)) { FileNames.Add(RecallFile); }
                else { GlobalObjects.DebugHelper.WriteDebugLog("NO RECALL OR TSB FILE WAS FOUND!"); }
            }

            string InvokerHome = @"C:\Program Files (x86)\OESupport\OEDebugging\AutomationInvokerDebug";
            string FileToWrite = InvokerHome + "\\" + "VehicleJSON_" + GlobalObjects.ConfigInfo.VIN + ".txt";
            if (GlobalObjects.VehicleJSONInfo.Length == 0) { GlobalObjects.SetupConfigInfo(); }

            if (GlobalObjects.VehicleJSONInfo.Length != 0)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("ADDED VEHICLE JSON INFO TO THE DEBUG FOLDER");
                File.WriteAllText(FileToWrite, GlobalObjects.VehicleJSONInfo);
                FileNames.Add(FileToWrite);
            }

            return FileNames;
        }
        public void UploadFilesToAWS(List<string> FileNames)
        {
            GlobalObjects.DebugHelper.WriteDebugLog("UPLOADING THE FOLLOWING FILES TO THE S3 BUCKET:");
            GlobalObjects.DebugHelper.WriteDebugLog("---> " + new FileInfo(GlobalObjects.DebugHelper.MainDebugFile).Name);
            foreach (var FileItem in FileNames)
            {
                string FileNameOnly = new FileInfo(FileItem).Name;
                GlobalObjects.DebugHelper.WriteDebugLog("---> " + FileNameOnly);
            }

            string YMM_Combined = string.Join("", GlobalObjects.ConfigInfo.ReturnStringOfBaseInfo());
            foreach (var FileName in FileNames)
            {
                if (!File.Exists(FileName)) { GlobalObjects.DebugHelper.WriteDebugLog("FILE: " + FileName + " DOES NOT EXIST!!!"); continue; }

                string FileOnly = FileName.Substring(FileName.LastIndexOf("\\") + 1);
                string KeyBase = DebugLogDirectory + "/" + Environment.MachineName + "/" + GlobalObjects.ConfigInfo.VIN + "_" + YMM_Combined + "_" + CombinedDateTime + "_" + GlobalObjects.OEScanGUID.Replace("-", String.Empty);
                string FileKey = KeyBase + "/" + FileOnly;

                GlobalObjects.DebugHelper.WriteDebugLog("UPLOADING FILE: " + FileName);
                GlobalObjects.DebugHelper.WriteDebugLog("NEW KEY MADE: " + FileKey);

                try { Uploader.Upload(FileName, S3BucketName, FileKey); }
                catch (Exception e)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT UPLOAD FILE: " + FileOnly);
                    GlobalObjects.DebugHelper.WriteDebugLog("EXCEPTION: " + e.Message);
                    GlobalObjects.DebugHelper.WriteDebugLog(e);
                }
            }
        }

        private void UploadScanInfoString()
        {
            if (!Environment.MachineName.Contains("CDIV") &&
                !Environment.MachineName.Contains("CDP4") && 
                !Environment.MachineName.Contains("DRIVE"))
            {
                if (Environment.UserName != "GDPRemote")
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("USER NAME WAS: " + Environment.UserName);
                    GlobalObjects.DebugHelper.WriteDebugLog("THIS DEVICE APPEARS TO NOT BE A VALID RELEASE OPUS PRODUCT");
                    GlobalObjects.DebugHelper.WriteDebugLog("SKIPPING THIS PROCESS SINCE THE CSV FILES IN PLACE ARE FOR SS OR DP DEVICES");

                    return;
                }
            }

            string MatrixFile = TestingMatrixDirectory + "/" + GlobalObjects.OEType + "_MatrixInfo.txt";
            string AllScanFile = TestingMatrixDirectory + "/ALL_SCAN_RESULTS.txt";

            string DownloadedMatrix = @"C:\Program Files (x86)\OESupport\" + GlobalObjects.OEType + "_MatrixInfo.txt";
            string DownloadedAllScans = @"C:\Program Files (x86)\OESupport\ALL_SCAN_RESULTS.txt";

            try
            {
                GlobalObjects.DebugHelper.WriteDebugLog("TRYING TO DOWNLOAD MATRIX FILE: " + MatrixFile);
                GlobalObjects.DebugHelper.WriteDebugLog("TRYING TO DOWNLOAD ALL SCANS FILE: " + AllScanFile);

                Uploader.Download(DownloadedMatrix, S3BucketName, MatrixFile);
                Uploader.Download(DownloadedAllScans, S3BucketName, AllScanFile);

                if (!File.Exists(DownloadedMatrix)) { throw new Exception("COULD NOT DOWNLOAD MAKE MATRIX FILE FROM THE S3 BUCKET!"); }
                if (!File.Exists(DownloadedAllScans)) { throw new Exception("COULD NOT DOWNLOAD ALL SCANS FILE FROM THE S3 BUCKET!"); }

                GlobalObjects.DebugHelper.WriteDebugLog("PULLED DOWN MATRIX FILE AND ALL TIMES FILE OK!");
                GlobalObjects.DebugHelper.WriteDebugLog("APPENDING THIS RUN TO BOTH MATRIX FILES NOW");

                GlobalObjects.DebugHelper.WriteDebugLog("STRING TO APPEND INTO THE TEST MATRIX: " + CSVStringToUpload.Trim());

                File.AppendAllText(DownloadedMatrix, CSVStringToUpload);
                File.AppendAllText(DownloadedAllScans, CSVStringToUpload);
                GlobalObjects.DebugHelper.WriteDebugLog("APPENDED STRING TO BOTH FILES OK!");

                Uploader.Upload(DownloadedMatrix, S3BucketName, MatrixFile);
                Uploader.Upload(DownloadedAllScans, S3BucketName, AllScanFile);

                try
                {
                    File.Delete(DownloadedMatrix);
                    GlobalObjects.DebugHelper.WriteDebugLog("REMOVED OLD TEST MATRIX FROM THE DEVICE");

                    File.Delete(DownloadedAllScans);
                    GlobalObjects.DebugHelper.WriteDebugLog("REMOVED ALL SCANS TEXT MATRIX FROM THE DEVICE");
                }
                catch (Exception delEx)
                { 
                    GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT REMOVE THE OLD MATRIX DOWNLOADED FILE!");
                    GlobalObjects.DebugHelper.WriteDebugLog(delEx);
                }
            }
            catch (Exception timerEx)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("CAN NOT APPEND MATRIX TIME OR APPEND ALL SCAN RESULTS INTO THE LIST OF TEST RUNS!");
                GlobalObjects.DebugHelper.WriteDebugLog("ERR: " + timerEx.Message);
                GlobalObjects.DebugHelper.WriteDebugLog(timerEx);
            }
        }
        private void SendUpDebugFile(TimeSpan TimeTaken)
        {
            GlobalObjects.DebugHelper.WriteDebugLog("DONE UPLOADING. TOOK: " + TimeTaken.TotalSeconds.ToString("F3") + " SECONDS");

            string YMM_Combined = string.Join("", GlobalObjects.ConfigInfo.ReturnStringOfBaseInfo());

            string DebugFile_FileOnly = new FileInfo(GlobalObjects.DebugHelper.MainDebugFile).Name;
            string DebugFile_KeyBase = DebugLogDirectory + "/" + Environment.MachineName + "/" + GlobalObjects.ConfigInfo.VIN + "_" + YMM_Combined + "_" + CombinedDateTime + "_" + GlobalObjects.OEScanGUID.Replace("-", String.Empty);
            string DebugFile_FileKey = DebugFile_KeyBase + "/" + DebugFile_FileOnly;

            Uploader.Upload(GlobalObjects.DebugHelper.MainDebugFile, S3BucketName, DebugFile_FileKey);

            // Removed to check logs on tools.
            // try { File.Delete(GlobalObjects.DebugHelper.MainDebugFile); }
            // catch { }
        }

        private void RemoveOldFiles()
        {
            string FileToKeep = new FileInfo(GlobalObjects.DebugHelper.MainDebugFile).Name;
            GlobalObjects.DebugHelper.WriteDebugLog("DELETING FILES THAT ARE NOT STRICTLY DEBUG LOGS: ");
            foreach (var FileName in FileNames)
            {
                if (FileName.Contains("OEAutomationInvoker_")) { continue; }

                GlobalObjects.DebugHelper.WriteDebugLog("FILE: " + FileName);
                try { File.Delete(FileName); }
                catch (Exception e)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("FILE UPLOADED OK BUT COULD NOT BE REMOVED FROM THIS DEVICE!");
                    GlobalObjects.DebugHelper.WriteDebugLog("ERROR: " + e.Message);
                    GlobalObjects.DebugHelper.WriteDebugLog(e);

                }
            }
        }
        private string GenerateCSVString()
        {
            // Add the stirng of vehicle info.
            var CSVStringList = new List<string>();
            CSVStringList.Add(GlobalObjects.ConfigInfo.VIN);
            CSVStringList.AddRange(GlobalObjects.ConfigInfo.ReturnStringOfBaseInfo());
            CSVStringList.Add(Environment.MachineName);

            // Add times of all segments of the scan here.
            int count = 0;
            GlobalObjects.AllTimes = new List<string>();
            foreach (var TimeVal in GlobalObjects.AllProcTimers)
            {
                TimeSpan TimeElapsed = GlobalObjects.AllProcTimers[count].Elapsed;
                var TimerTime = TimeElapsed.ToString("c");

                CSVStringList.Add(TimerTime);
                GlobalObjects.AllTimes.Add(TimerTime);
                count++;
            }

            // Add scan status value here.
            string ScanStatus = "";
            if (GlobalObjects.ScanWasTerminated) { ScanStatus = "TERMINATED"; }
            else
            {
                if (GlobalObjects.ScanCompletePassed) { ScanStatus = "PASSED"; }
                if (!GlobalObjects.ScanCompletePassed) { ScanStatus = "FAILED"; }
            }

            if (ScanStatus == "") { ScanStatus = "UNKNOWN"; }
            CSVStringList.Add(ScanStatus);

            // Add the GUID and DEV Config of the scan session now.
            CSVStringList.Add(GlobalObjects.CONFIG_ENVIRONMENT);

            try
            {
                string BLGUID = new WebClient().DownloadString("http://localhost:15000//DrewTechDataSvc/SessionID");
                if (BLGUID != new Guid().ToString())
                {
                    BLGUID = BLGUID.ToUpper();

                    GlobalObjects.DebugHelper.WriteDebugLog("PULLED DOWN A GOOD GUID FROM THE BL API!");
                    GlobalObjects.DebugHelper.WriteDebugLog("BL GUID VALUE WAS SEEN TO BE: " + BLGUID);

                    GlobalObjects.DebugHelper.WriteDebugLog("CHANGED SESSION GUID TO THE BL GUID.");
                    GlobalObjects.DebugHelper.WriteDebugLog("WAS: " + GlobalObjects.OEScanGUID);
                    GlobalObjects.DebugHelper.WriteDebugLog("NOW: " + BLGUID);

                    GlobalObjects.OEScanGUID = BLGUID;

                    CSVStringList.Add(GlobalObjects.OEScanGUID);
                    CSVStringList.Add("BOOTLOADER_GUID");
                }
            }
            catch (Exception webEx)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT RETURN A GUID FROM THE BL API");
                GlobalObjects.DebugHelper.WriteDebugLog("KEEPING GUID SET AS: " + GlobalObjects.OEScanGUID);

                GlobalObjects.DebugHelper.WriteDebugLog("FAILURE: " + webEx.Message);
                GlobalObjects.DebugHelper.WriteDebugLog(webEx);

                CSVStringList.Add(GlobalObjects.OEScanGUID);
                CSVStringList.Add("INVOKER_GUID");
            }

            // Convert this to a string and return it.
            return string.Join(",", CSVStringList);
        }
        #endregion
    }
}
