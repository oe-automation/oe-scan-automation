﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OEAutomationLauncher.AutomationLauncher.ObjectClasses
{
    public class SizeAndVisibilitySettings
    {
        public List<double> MainWindowSizes { get; set; }
        public double DebugFunctionsGridHeight { get; set; }
        public double DebugTextBoxGridWidth { get; set; }

        public bool DebugFuncsVisibile { get; set; }
    }

    public class UIScalingHelpers
    {
        private MainWindow MainWindowView;
        private bool DebuggerAttached;

        // CDP4 Lists for sizes.
        private List<List<double>> CDP4_ReleaseSizes = new List<List<double>>
        {
           new List<double> { 815.0, 480.0 },
           new List<double> { 0.00, 0.00 }
        };
        private List<List<double>> CDP4_DebugModeSizes = new List<List<double>>
        {
            new List<double> { 1425.0, 605.0 },
            new List<double> { 135.0, 600.0 }
        };

        // Drive Lists for Sizes.
        private List<List<double>> Drive_ReleaseSizes = new List<List<double>>
        {
            new List<double> { 1024.0, 760.0 },
            new List<double> { 0.0, 0.0 }
        };
        private List<List<double>> Drive_DebugModeSizes = new List<List<double>>
        {
            new List<double> { 1885.0, 885.0 },
            new List<double> { 150.0, 800.0 }
        };

        /// <summary>
        /// Private function used to return the value stored in the image ID.
        /// </summary>
        /// <returns>String of image ID or ERROR if its not existant.</returns>
        private string GetImageID()
        {
            string Key = @"HKEY_LOCAL_MACHINE\SYSTEM\DrewTechSerial";
            string Type = (string)Microsoft.Win32.Registry.GetValue(Key, "Imageid", "");

            if (Type == null) { Type = "UNKNOWN IMAGE ID!!!! THIS IS A MASSIVE ISSUE"; }
            else { Type = Type.ToUpper(); }

            return Type;
        }

        /// <summary>
        /// Makes a resize object that is setup/defied above. 
        /// Includes window sizes and debug grid sizes.
        /// </summary>
        /// <returns>Resize object used in the setMainWindowSIze function to resize the UI For DrivePRO or CDP4</returns>
        private SizeAndVisibilitySettings ReturnWindowSizes()
        {
            var NewSettings = new SizeAndVisibilitySettings();
            NewSettings.DebugFuncsVisibile = DebuggerAttached;

            GlobalObjects.DebugHelper.WriteDebugLog("GENERATING RESIZE OBJECT NOW.");
            if (!DebuggerAttached)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("NO DEBUGGER FOUND");
                if (GlobalObjects.IsCDP4)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("UNIT TYPE IS SEEN TO BE A CDP4");
                    NewSettings.MainWindowSizes = CDP4_ReleaseSizes[0];
                    NewSettings.DebugFunctionsGridHeight = CDP4_ReleaseSizes[1][1];
                    NewSettings.DebugTextBoxGridWidth = CDP4_ReleaseSizes[1][0];
                }

                if (!GlobalObjects.IsCDP4)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("UNIT TYPE IS SEEN TO BE A DRIVE DEVCIE");
                    NewSettings.MainWindowSizes = Drive_ReleaseSizes[0];
                    NewSettings.DebugFunctionsGridHeight = Drive_ReleaseSizes[1][0];
                    NewSettings.DebugTextBoxGridWidth = Drive_ReleaseSizes[1][1];
                }
            }

            if (DebuggerAttached)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("DEBUGGER FOUND");
                if (GlobalObjects.IsCDP4)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("UNIT TYPE IS SEEN TO BE A CDP4");
                    NewSettings.MainWindowSizes = CDP4_DebugModeSizes[0];
                    NewSettings.DebugFunctionsGridHeight = CDP4_DebugModeSizes[1][0];
                    NewSettings.DebugTextBoxGridWidth = CDP4_DebugModeSizes[1][1];
                }
                if (!GlobalObjects.IsCDP4)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("UNIT TYPE IS SEEN TO BE A DRIVE DEVCIE");
                    NewSettings.MainWindowSizes = Drive_DebugModeSizes[0];
                    NewSettings.DebugFunctionsGridHeight = Drive_DebugModeSizes[1][0];
                    NewSettings.DebugTextBoxGridWidth = Drive_DebugModeSizes[1][1];
                }
            }

            GlobalObjects.DebugHelper.WriteDebugLog("WIDTH AND HEIGHT FOR MAIN WINDOW " + NewSettings.MainWindowSizes[0] + " - " + NewSettings.MainWindowSizes[1]);
            GlobalObjects.DebugHelper.WriteDebugLog("WIDTH AND HEIGHT FOR DEBUG FUNCS " + NewSettings.DebugTextBoxGridWidth + " - " + NewSettings.DebugFunctionsGridHeight);

            return NewSettings;
        }

        public UIScalingHelpers(MainWindow MainView)
        {
            GlobalObjects.DebugHelper.WriteDebugLog("GOT INSTANCE OF MAIN WINDOW. SETTING IT AS LOCAL VARIABLE.");
            MainWindowView = MainView;

            // DebuggerAttached = false;
            DebuggerAttached = Debugger.IsAttached;
            GlobalObjects.DebugHelper.WriteDebugLog("SETTING DEBUGGER ON STATUS TO: " + DebuggerAttached);

            GlobalObjects.DebugHelper.WriteDebugLog("UISCALERS KICKED OFF.", "UI_SCALING");
            GlobalObjects.DebugHelper.WriteDebugLog("IMAGE ID: " + GetImageID(), "UI_SCALING");

            if (GlobalObjects.IsCDP4) GlobalObjects.DebugHelper.WriteDebugLog("DEVICE TYPE: " + "CDP4");
            if (!GlobalObjects.IsCDP4)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("DEVICE TYPE: " + "DRIVE");
                // if (MainView.IsGerber) { GlobalObjects.DebugHelper.WriteDebugLog("SKINNED FOR GERBER RELEASE!"); }
            }

            GlobalObjects.DebugHelper.WriteDebugLog("SETTING MAINWINDOW SIZE NOW...");
            SetMainWindowSize();
        }

        public void SetMainWindowSize()
        {
            var NewSizeObject = ReturnWindowSizes();
            if (NewSizeObject.MainWindowSizes.Count == 0)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("CAN NOT FIND THE TYPE OF DEVICE SOMEHOW. KILLING.");
                GlobalObjects.DebugHelper.WriteDebugLog("TERMINATING DUE TO INVALID KEY COMBO FOR TYPE AND DIST CHANNEL");
                throw new Exception("INVALID KEY AND DIST CHANNEL COMBO. CAN NOT SIZE UI CORRECTLY");
            }

            Visibility FunsVisibile = Visibility.Hidden;
            if (NewSizeObject.DebugFuncsVisibile) { FunsVisibile = Visibility.Visible; }

            MainWindowView.Dispatcher.Invoke(() =>
            {
                if (GlobalObjects.IsCDP4)
                {
                    MainWindowView.MainScanGrid.RowDefinitions[1].Height = new GridLength(.65, GridUnitType.Star);
                    MainWindowView.MainScanGrid.RowDefinitions[2].Height = new GridLength(.15, GridUnitType.Star);
                    if (FunsVisibile == Visibility.Hidden) { MainWindowView.MainScanGrid.RowDefinitions[3].Height = new GridLength(0, GridUnitType.Star); }
                }

                if (!GlobalObjects.IsCDP4)
                {
                    MainWindowView.MainScanGrid.RowDefinitions[1].Height = new GridLength(.75, GridUnitType.Star);
                    MainWindowView.MainScanGrid.RowDefinitions[2].Height = new GridLength(.125, GridUnitType.Star);
                    if (FunsVisibile == Visibility.Hidden) { MainWindowView.MainScanGrid.RowDefinitions[3].Height = new GridLength(0, GridUnitType.Star); }
                }

                MainWindowView.MainScanGrid.ColumnDefinitions[0].Width = new GridLength(1, GridUnitType.Star);
                if (FunsVisibile == Visibility.Visible)
                {
                    MainWindowView.MainScanGrid.ColumnDefinitions[1].Width = new GridLength(20, GridUnitType.Pixel);
                    MainWindowView.MainScanGrid.ColumnDefinitions[2].Width = new GridLength(.75, GridUnitType.Star);
                }

                else
                {
                    MainWindowView.MainScanGrid.ColumnDefinitions[1].Width = new GridLength(0, GridUnitType.Star);
                    MainWindowView.MainScanGrid.ColumnDefinitions[2].Width = new GridLength(0, GridUnitType.Star);
                }

                MainWindowView.Width = NewSizeObject.MainWindowSizes[0];
                MainWindowView.Height = NewSizeObject.MainWindowSizes[1];

                // MainWindowView.DebugViewAndAPIFunctionsView.Height = GridLength.Auto.Value;
                MainWindowView.DebugViewAndAPIFunctionsView.Visibility = FunsVisibile;

                // MainWindowView.DebugConfigAndLoggerView.Width = GridLength.Auto.Value;
                MainWindowView.DebugConfigAndLoggerView.Visibility = FunsVisibile;

                MainWindowView.ResizerDebugMode.Visibility = FunsVisibile;
                MainWindowView.DebugHelpBgRectange.Visibility = FunsVisibile;

                GlobalObjects.DebugHelper.WriteDebugLog("MAIN WINDOW HAS BEEN RESIZED TO DESIRED SIZE BASED ON KIT INFO");
            });
        }
    }
}
