﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MColor = System.Windows.Media.Color;
using DColor = System.Drawing.Color;

using System.Windows.Media;

namespace OEAutomationLauncher.LogicalHelpers
{
    public class BrushHelpers
    {
        /// <summary>
        /// Gets brush for the name of the calling function. 
        /// </summary>
        /// <param name="NameOfCaller"></param>
        /// <returns></returns>
        public static Brush ReturnBrushForName(string NameOfCaller, bool OpaqueForBg = false)
        {
            switch (NameOfCaller)
            {
                // Purple
                case string a when a.Contains("BaseTaskList"):
                case string b when b.Contains("TaskListItem"):
                case string c when c.Contains("+<>c__"):
                    return ReturnBrushFromHex("#9641E8", OpaqueForBg);

                // Green
                case string a when a.Contains("ConfirmPromptLogic"):
                case string b when b.Contains("ClearCodesDialogue"):
                case string c when c.Contains("ConfirmCancelScan"):
                case string d when d.Contains("ContactIVS360Support"):
                case string e when e.Contains("ScanCompleteCloseWindow"):
                    return ReturnBrushFromHex("#168333", OpaqueForBg);

                // Orange
                case string a when a.Contains("CustomFDRSViewTrigger"):
                case string b when b.Contains("FDRSKeyCycleView"):
                case string c when c.Contains("CustomFJDSViewTrigger"):
                case string d when d.Contains("FJDSNoComms"):
                case string e when e.Contains("FJDSEnterOdoAndRo"):
                case string f when f.Contains("TISRecallView"):
                case string g when g.Contains("CustomTISViewTrigger"):
                    return ReturnBrushFromHex("#CD8828", OpaqueForBg);

                // Light Blue
                case string a when a.Contains("LoggingAndConfigDebugger"):
                case string b when b.Contains("ViewAndAPIDebugger"):
                case string c when c.Contains("DebuggingHelper_Invoker"):
                case string d when d.Contains("FallBackVehicleConfigs"):
                case string e when e.Contains("ToggleDebugLogging_Invoker"):
                    return ReturnBrushFromHex("#1499E6", OpaqueForBg);

                // Deep Green
                case string a when a.Contains("DriveCrashServiceController"):
                case string b when b.Contains("DriveCrashServiceInvoker"):
                    return ReturnBrushFromHex("#066235", OpaqueForBg);

                // Deep Blue
                case string m when m.Contains("APIActionTriggered"):
                case string n when n.Contains("ProgressAPIEndpoints"):
                case string o when o.Contains("ProgressAPITasker"):
                    return ReturnBrushFromHex("#292D55", OpaqueForBg);

                // Sky Blue           
                case string a when a.Contains("CodeClearing"):
                case string b when b.Contains("CustomViews"):
                case string c when c.Contains("DebugActions"):
                case string d when d.Contains("GetVehicleInfo"):
                case string e when e.Contains("ProgressBars"):
                case string f when f.Contains("ScanCompleted"):
                case string g when g.Contains("TimerActions"):
                case string h when h.Contains("UpdateTaskStatus"):
                case string i when i.Contains("ViewInvoker"):
                case string j when j.Contains("WinAutomationActions"):
                case string k when k.Contains("ProgressBars"):
                case string l when l.Contains("ScanCompleted"):
                    return ReturnBrushFromHex("#0195D6", OpaqueForBg);

                // Light Purple
                case string a when a.Contains("AutoCloseTimerCalls"):
                case string b when b.Contains("AVEditColorizer"):
                case string c when c.Contains("AWSHelpers"):
                case string d when d.Contains("BrushHelpers"):
                case string e when e.Contains("InterlopConsoleToggle_Invoker"):
                case string f when f.Contains("MainWindowViewHelpers"):
                case string g when g.Contains("OEMakeBools"):
                case string h when h.Contains("PrepareFileGathering_Invoker"):
                case string i when i.Contains("StartupActions"):
                    return ReturnBrushFromHex("#C879E9", OpaqueForBg);

                // Orange
                case string a when a.Contains("DriveCrashServiceParams"):
                case string b when b.Contains("PhoneNumberCallingTag_Invoker"):
                case string c when c.Contains("UserConfigInfo_Invoker"):
                case string d when d.Contains("VehicleConfigInfo_Invoker"):
                case string e when e.Contains("WinAutomationArgs"):
                    return ReturnBrushFromHex("#FEAC1E", OpaqueForBg);

                // Maroon
                case string a when a.Contains("VehicleConfigInfo_OptionsConfig"):
                case string b when b.Contains("GDS2OEApplication"):
                case string c when c.Contains("TISOEApplication"):
                case string d when d.Contains("OEApplicationBase"):
                case string e when e.Contains("OERegularExpressions"):
                case string f when f.Contains("OptionsTriggered"):
                case string g when g.Contains("GDS2OptionsView"):
                case string h when h.Contains("TISOptionsView"):
                    return ReturnBrushFromHex("#72210F", OpaqueForBg);

                // Navy Blue
                case string a when a.Contains("VINRequestTriggers"):
                case string b when b.Contains("VINRequestUserControl"):
                case string c when c.Contains("UserConfigInfo_Invoker"):
                case string d when d.Contains("VehicleConfigInfo_Invoker"):
                case string e when e.Contains("WinAutomationArgs"):
                    return ReturnBrushFromHex("#092231", OpaqueForBg);

                // Teal
                case string a when a.Contains("MainWindow"):
                case string b when b.Contains("GlobalObjects"):
                case string c when c.Contains("UIScalingHelpers"):
                case string d when d.Contains("RegistrySetupHelper"):
                    return ReturnBrushFromHex("#0CAD6E", OpaqueForBg);

                default:
                    return Brushes.Black;
            }
        }

        /// <summary>
        /// Gets brush for the name of the calling function. 
        /// </summary>
        /// <param name="NameOfCaller"></param>
        /// <returns></returns>
        public static string ReturnBrushString(string NameOfCaller, bool OpaqueForBg = false)
        {
            bool GetInvokedColor = NameOfCaller.Contains("+<>c__");

            switch (NameOfCaller)
            {
                // Purple
                case string a when a.Contains("BaseTaskList"):
                case string b when b.Contains("TaskListItem"):
                case string c when c.Contains("+<>c__"):
                    if (OpaqueForBg) { return ReturnOpaqueBrushString("#9641E8"); }                   
                    return "#9641E8";

                // Green
                case string a when a.Contains("ConfirmPromptLogic"):
                case string b when b.Contains("ClearCodesDialogue"):
                case string c when c.Contains("ConfirmCancelScan"):
                case string d when d.Contains("ContactIVS360Support"):
                case string e when e.Contains("ScanCompleteCloseWindow"):
                    if (OpaqueForBg) { return ReturnOpaqueBrushString("#168333"); }
                    return "#168333";

                // Orange
                case string a when a.Contains("CustomFDRSViewTrigger"):
                case string b when b.Contains("FDRSKeyCycleView"):
                case string c when c.Contains("CustomFJDSViewTrigger"):
                case string d when d.Contains("FJDSNoComms"):
                case string e when e.Contains("FJDSEnterOdoAndRo"):
                case string f when f.Contains("TISRecallView"):
                case string g when g.Contains("CustomTISViewTrigger"):
                    if (OpaqueForBg) { return ReturnOpaqueBrushString("#CD8828"); }
                    return "#CD8828";

                // Light Blue
                case string a when a.Contains("LoggingAndConfigDebugger"):
                case string b when b.Contains("ViewAndAPIDebugger"):
                case string c when c.Contains("DebuggingHelper_Invoker"):
                case string d when d.Contains("FallBackVehicleConfigs"):
                case string e when e.Contains("ToggleDebugLogging_Invoker"):
                    if (OpaqueForBg) { return ReturnOpaqueBrushString("#1499E6"); }
                    return "#1499E6";

                // Deep Green
                case string a when a.Contains("DriveCrashServiceController"):
                case string b when b.Contains("DriveCrashServiceInvoker"):
                    if (OpaqueForBg) { return ReturnOpaqueBrushString("#066235"); }
                    return "#066235";

                // Deep Blue
                case string m when m.Contains("APIActionTriggered"):
                case string n when n.Contains("ProgressAPIEndpoints"):
                case string o when o.Contains("ProgressAPITasker"):
                    if (OpaqueForBg) { return ReturnOpaqueBrushString("#292D55"); }
                    return "#292D55";

                // Sky Blue           
                case string a when a.Contains("CodeClearing"):
                case string b when b.Contains("CustomViews"):
                case string c when c.Contains("DebugActions"):
                case string d when d.Contains("GetVehicleInfo"):
                case string e when e.Contains("ProgressBars"):
                case string f when f.Contains("ScanCompleted"):
                case string g when g.Contains("TimerActions"):
                case string h when h.Contains("UpdateTaskStatus"):
                case string i when i.Contains("ViewInvoker"):
                case string j when j.Contains("WinAutomationActions"):
                case string k when k.Contains("ProgressBars"):
                case string l when l.Contains("ScanCompleted"):
                    if (OpaqueForBg) { return ReturnOpaqueBrushString("#0195D6"); }
                    return "#0195D6";

                // Light Purple
                case string a when a.Contains("AutoCloseTimerCalls"):
                case string b when b.Contains("AVEditColorizer"):
                case string c when c.Contains("AWSHelpers"):
                case string d when d.Contains("BrushHelpers"):
                case string e when e.Contains("InterlopConsoleToggle_Invoker"):
                case string f when f.Contains("MainWindowViewHelpers"):
                case string g when g.Contains("OEMakeBools"):
                case string h when h.Contains("PrepareFileGathering_Invoker"):
                case string i when i.Contains("StartupActions"):
                    if (OpaqueForBg) { return ReturnOpaqueBrushString("#C879E9"); }
                    return "#C879E9";

                // Orange
                case string a when a.Contains("DriveCrashServiceParams"):
                case string b when b.Contains("PhoneNumberCallingTag_Invoker"):
                case string c when c.Contains("UserConfigInfo_Invoker"):
                case string d when d.Contains("VehicleConfigInfo_Invoker"):
                case string e when e.Contains("WinAutomationArgs"):
                    if (OpaqueForBg) { return ReturnOpaqueBrushString("#FEAC1E"); }
                    return "#FEAC1E";

                // Maroon
                case string a when a.Contains("VehicleConfigInfo_OptionsConfig"):
                case string b when b.Contains("GDS2OEApplication"):
                case string c when c.Contains("TISOEApplication"):
                case string d when d.Contains("OEApplicationBase"):
                case string e when e.Contains("OERegularExpressions"):
                case string f when f.Contains("OptionsTriggered"):
                case string g when g.Contains("GDS2OptionsView"):
                case string h when h.Contains("TISOptionsView"):
                    if (OpaqueForBg) { return ReturnOpaqueBrushString("#72210F"); }
                    return "#72210F";

                // Navy Blue
                case string a when a.Contains("VINRequestTriggers"):
                case string b when b.Contains("VINRequestUserControl"):
                case string c when c.Contains("UserConfigInfo_Invoker"):
                case string d when d.Contains("VehicleConfigInfo_Invoker"):
                case string e when e.Contains("WinAutomationArgs"):
                    if (OpaqueForBg) { return ReturnOpaqueBrushString("#092231"); }
                    return "#092231";

                // Teal
                case string a when a.Contains("MainWindow"):
                case string b when b.Contains("GlobalObjects"):
                case string c when c.Contains("UIScalingHelpers"):
                case string d when d.Contains("RegistrySetupHelper"):
                    if (OpaqueForBg) { return ReturnOpaqueBrushString("#0CAD6E"); }
                    return "#0CAD6E";

                default:
                    if (OpaqueForBg) { return ReturnOpaqueBrushString("#000000"); }
                    return "#000000";
            }
        }

        /// <summary>
        /// Converts an HTML random color code into a SCB
        /// </summary>
        /// <param name="HexValue">HEX value to pass in for this color.</param>
        /// <returns>BRUSH object to color with.</returns>
        public static Brush ReturnBrushFromHex(string HexValue, bool OpaqueForBg = false)
        {
            if (!HexValue.StartsWith("#")) { HexValue = "#" + HexValue.ToUpper(); }

            var dColorVal = System.Drawing.ColorTranslator.FromHtml(HexValue.ToUpper());
            var mColorVal = MColor.FromArgb(dColorVal.A, dColorVal.R, dColorVal.G, dColorVal.B);

            SolidColorBrush retBrush = new SolidColorBrush(mColorVal);
            if (OpaqueForBg) { retBrush.Opacity = .35; }

            return new SolidColorBrush(mColorVal);
        }

        /// <summary>
        /// Gets hex brush and removes opacity and sends it out.
        /// </summary>
        /// <param name="HexValue">String Value to return into the function</param>
        /// <returns></returns>
        public static string ReturnOpaqueBrushString(string HexValue)
        {
            if (!HexValue.StartsWith("#")) { HexValue = "#" + HexValue.ToUpper(); }

            var dColorVal = System.Drawing.ColorTranslator.FromHtml(HexValue.ToUpper());
            var mColorVal = MColor.FromArgb(dColorVal.A, dColorVal.R, dColorVal.G, dColorVal.B);

            SolidColorBrush retBrush = new SolidColorBrush(mColorVal);
            retBrush.Opacity = .35;

            return retBrush.Color.ToString();
        }

        /// <summary>
        /// Returns if a color is light or dark.
        /// </summary>
        /// <param name="InputColor">Color To Check</param>
        /// <returns></returns>
        public static bool IsDark(Brush InputColor)
        {
            var SCBHex = (SolidColorBrush)InputColor;
            Color ColorToConvert = SCBHex.Color;

            bool IsDark = ColorToConvert.R * 0.2126 + ColorToConvert.G * 0.7152 + ColorToConvert.B * 0.0722 > 255 / 1.5;
            return IsDark;
        }
    }
}
