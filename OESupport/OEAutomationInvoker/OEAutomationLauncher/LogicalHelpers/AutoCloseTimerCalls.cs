﻿using OEAutomationLauncher.AutomationLauncher;
using OEAutomationLauncher.AutomationLauncher.ConfirmPrompts;
using OEAutomationLauncher.AutomationLauncher.CustomOEViews.FJDS;
using OEAutomationLauncher.AutomationLauncher.CustomOEViews.TIS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.LogicalHelpers
{
    public class AutoCloseTimerCalls
    {
        public string MethodToExecute;
        public string MethodParamsString;

        private bool DebuggerSetup = GlobalObjects.DebugHelper != null;
        private Type TypeOfView;

        public AutoCloseTimerCalls(Type ObjectToClose)
        {
            TypeOfView = ObjectToClose;
            if (DebuggerSetup)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("PASSED IN TYPE OF: " + TypeOfView.ToString().ToUpper());
                GlobalObjects.DebugHelper.WriteDebugLog("PARSING OUT THE TYPES OF METHODS FOUND IN THIS OBJECT");
            }
        }

        // Reinits this object with no values so things dont get messy betwneen uses as a static class.
        public AutoCloseTimerCalls()
        {
            MethodToExecute = null;
            MethodParamsString = null;

            DebuggerSetup = false;
            TypeOfView = null;
        }

        /// <summary>
        /// Sets up the current method and args to use to close the open view/prompt.
        /// </summary>
        /// <param name="AllMethods">List of all method names that can be used.</param>
        public void ClickButtonToClose()
        {
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                if (GlobalObjects.IsOptionObject) 
                {
                    GlobalObjects.MainWindowView.ConfirmOptionsButton_Click(null, null);
                    MethodToExecute = "ConfirmOptionsButton_Click";
                    MethodParamsString = "null, null";

                    GlobalObjects.IsOptionObject = false;
                }
                else if (TypeOfView == typeof(ClearCodesDialogue) || TypeOfView == typeof(ConfirmCancelScan) || TypeOfView == typeof(ScanCompleteCloseWindow))
                {
                    if (TypeOfView == typeof(ClearCodesDialogue)) { GlobalObjects.CodeClearingButtons.ClickNoConfirmButton(null, null); }
                    
                    // Changing call for these two to set a timeout of 120 seconds. This way we can get user to see the response.
                    // If they arent there in 120 seconds, well thats their loss lol.
                    if (TypeOfView == typeof(ConfirmCancelScan)) { GlobalObjects.ConfirmCancelButtons.ClickYesConfirmButton(null, null); }
                    if (TypeOfView == typeof(ScanCompleteCloseWindow)) { GlobalObjects.ScanCompleteButtons.ClickYesConfirmButton(null, null); }

                    MethodToExecute = "ClickYesConfirmButton";
                    if (TypeOfView == typeof(ClearCodesDialogue)) { MethodToExecute = "ClickNoConfirmButton"; }
                    MethodParamsString = "null, null";
                }
                else if (TypeOfView == typeof(FJDSEnterOdoAndRo)) 
                {
                    GlobalObjects.FJDS_OdoAndROView.SetOdoAndRoButtonClick(null, null);
                    MethodToExecute = "SetOdoAndRoButtonClick";
                    MethodParamsString = "null, null";
                }
                else if (TypeOfView == typeof(FJDSNoComms)) 
                {
                    GlobalObjects.FJDS_NoCommsView.ConfirmOrCancelCommsRetry(null, null);
                    MethodToExecute = "ConfirmOrCancelCommsRetry";
                    MethodParamsString = "null, null";
                }
                else if (TypeOfView == typeof(TISRecallView)) { GlobalObjects.TIS_RecallView.CloseRecallViewButton(null, null); }
                else
                {
                    MethodToExecute = "NO METHOD FOUND OR SET!";
                    MethodParamsString = "NO PARAMS COULD BE SET!";

                    if (DebuggerSetup)
                    {
                        GlobalObjects.DebugHelper.WriteDebugLog("NO TIMER WAS SETUP FOR THIS VIEW TYPE");
                        GlobalObjects.DebugHelper.WriteDebugLog("TYPE OF VIEW PASSED IN HERE: " + TypeOfView.Name.ToUpper());
                    }
                } 
            });
        }
    }
}

