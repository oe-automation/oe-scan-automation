﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.ObjectClasses
{
    public class OEMakeBools
    {
        public string MakeSet { get; set; }
        public string ModelSet { get; set; }
        public string YearSet { get; set; }

        public OEMakeBools(bool ForceUpdate = true)
        {
            if (ForceUpdate) { UpdateValues(); }
        }

        public OEMakeBools(string Make, string Model, string Year)
        {
            MakeSet = Make;
            ModelSet = Model;
            YearSet = Year;
        }

        private void UpdateValues()
        {
            if (GlobalObjects.ConfigInfo == null) { return; }

            MakeSet = GlobalObjects.ConfigInfo.Make;
            ModelSet = GlobalObjects.ConfigInfo.Model;
            YearSet = GlobalObjects.ConfigInfo.Year;

            // GlobalObjects.SetLoggerBoxContent();
            if (GlobalObjects.DebugHelper == null) { return; }

            string NewInfo = MakeSet + " - " + ModelSet + " - " + YearSet;
            GlobalObjects.DebugHelper.WriteDebugLog("UPDATED VEHICLE INFO: " + NewInfo);
        }

        public string ReturnSWName()
        {
            if (IsToyotaMake) { return "Techstream"; }
            else if (IsGMMake) { return "GDS2"; }
            else if (IsHondaMake) { return "IHDS"; }
            else if (IsNissanMake) { return "CONSULT 3+"; }
            else if (IsFordMake)
            {
                if (IsFDRSVehicle) { return "FDRS"; }
                else { return "FJDS"; }
            }

            // This just feeds back if we dont match anything here.
            else { return "OE Software"; }
        }

        public bool IsToyotaMake
        {
            get
            {
                UpdateValues();

                if (string.IsNullOrEmpty(MakeSet))
                {
                    GlobalObjects.TempTextToFill.Add("MAKE WAS NULL OR WHITESPACE!");
                    return false;
                }

                bool IsToyota =
                    MakeSet.ToUpper() == "TOYOTA" ||
                    MakeSet.ToUpper() == "LEXUS" ||
                    MakeSet.ToUpper() == "SCION";

                if (IsToyota)
                {
                    string ResultString = string.Format("CURRENT MAKE: {0} | RESULT FOR IS TOYOTA: {1}",
                        MakeSet.ToUpper(),
                        IsToyota.ToString().ToUpper());
                    GlobalObjects.TempTextToFill.Add(ResultString);

                    GlobalObjects.SetLoggerBoxContent();
                }
                return IsToyota;
            }
        }

        // Checks if the vehicle attached is a GM Variant.
        public bool IsGMMake
        {
            get
            {
                UpdateValues();

                if (string.IsNullOrEmpty(MakeSet))
                {
                    GlobalObjects.TempTextToFill.Add("MAKE WAS NULL OR WHITESPACE!");
                    return false;
                }

                bool IsGM =
                    MakeSet.ToUpper() == "BUICK" ||
                    MakeSet.ToUpper() == "CHEVROLET" ||
                    MakeSet.ToUpper() == "CADILLAC" ||
                    MakeSet.ToUpper() == "DODGE" ||
                    MakeSet.ToUpper() == "GMC";

                if (IsGM)
                {
                    string ResultString = string.Format("CURRENT MAKE: {0} | RESULT FOR IS GM: {1}",
                        MakeSet.ToUpper(),
                        IsGM.ToString().ToUpper());
                    GlobalObjects.TempTextToFill.Add(ResultString);

                    GlobalObjects.SetLoggerBoxContent();
                }
                return IsGM;
            }
        }

        // Checks if the vehicle attached is a Ford Variant.
        public bool IsFordMake
        {
            get
            {
                UpdateValues();

                if (string.IsNullOrEmpty(MakeSet))
                {
                    GlobalObjects.TempTextToFill.Add("MAKE WAS NULL OR WHITESPACE!");
                    return false;
                }

                bool IsFord =
                        MakeSet.ToUpper() == "FORD" ||
                        MakeSet.ToUpper() == "LINCOLN" ||
                        MakeSet.ToUpper() == "MERCURY";

                if (IsFord)
                {
                    string ResultString = string.Format("CURRENT MAKE: {0} | RESULT FOR IS FORD: {1}",
                        MakeSet.ToUpper(),
                        IsFord.ToString().ToUpper());
                    GlobalObjects.TempTextToFill.Add(ResultString);

                    GlobalObjects.SetLoggerBoxContent();
                }
                return IsFord;
            }
        }

        // Checks if the vehicle attached is a Honda Variant.
        public bool IsHondaMake
        {
            get
            {
                UpdateValues();

                if (string.IsNullOrEmpty(MakeSet))
                {
                    GlobalObjects.TempTextToFill.Add("MAKE WAS NULL OR WHITESPACE!");
                    return false;
                }

                bool IsHonda =
                    MakeSet.ToUpper() == "HONDA" ||
                    MakeSet.ToUpper() == "ACURA";

                if (IsHonda)
                {
                    string ResultString = string.Format("CURRENT MAKE: {0} | RESULT FOR IS HONDA: {1}",
                        MakeSet.ToUpper(),
                        IsHonda.ToString().ToUpper());
                    GlobalObjects.TempTextToFill.Add(ResultString);

                    GlobalObjects.SetLoggerBoxContent();
                }
                return IsHonda;
            }
        }

        // Checks if the vehicle attached is a Nissan Variant.
        public bool IsNissanMake
        {
            get
            {
                UpdateValues();

                if (string.IsNullOrEmpty(MakeSet))
                {
                    GlobalObjects.TempTextToFill.Add("MAKE WAS NULL OR WHITESPACE!");
                    return false;
                }

                bool IsNissan =
                    MakeSet.ToUpper() == "NISSAN" ||
                    MakeSet.ToUpper() == "INFINITI";

                if (IsNissan)
                {
                    string ResultString = string.Format("CURRENT MAKE: {0} | RESULT FOR IS NISSAN: {1}",
                        MakeSet.ToUpper(),
                        IsNissan.ToString().ToUpper());
                    GlobalObjects.TempTextToFill.Add(ResultString);

                    GlobalObjects.SetLoggerBoxContent();
                }
                return IsNissan;
            }
        }

        // Checks for an FJDS/FDRS Vehicle
        public bool IsFDRSVehicle
        {
            get
            {
                UpdateValues();

                if (string.IsNullOrEmpty(ModelSet))
                {
                    GlobalObjects.TempTextToFill.Add("MODEL WAS NULL OR WHITESPACE!");
                    return false;
                }

                // Store the year as an int for easier comparisons.
                if (!int.TryParse(YearSet, out int VehicleYear))
                {
                    GlobalObjects.TempTextToFill.Add("CAN NOT PARSE OUT YEAR OF VEHICLE! -> " + YearSet);
                    return false;
                }

                switch (ModelSet.ToUpper())
                {
                    case "BRONCO SPORT":
                    case "BRONCO":
                    case "EDGE":
                    case "F-150":
                    case "MUSTANG":
                    case "MUSTANG MACH-E":
                    case "NAUTILUS":
                    case "SUPERDUTY":
                        return VehicleYear >= 2021;

                    case "AVIATOR":
                    case "CORSAIR":
                    case "ESCAPE":
                    case "EXPLORER":
                    case "TRANSIT":
                        return VehicleYear >= 2020;

                    case "RANGER":
                    case "TRANSIT CONNECT":
                        return VehicleYear >= 2019;

                    case "EXPEDITION":
                    case "ECOSPORT":
                    case "NAVIGATOR":
                        return VehicleYear >= 2018;

                    // If we're here and have nothing to return:
                    default:
                        GlobalObjects.TempTextToFill.Add("MODEL: " + ModelSet.ToUpper() + " | WAS NOT FOUND IN THE FDRS SWITCH! THIS IS NO GOOD");
                        return false;
                }
            }
        }
    }
}
