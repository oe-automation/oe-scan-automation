﻿using OEAutomationLauncher.AutomationLauncher.DebugHelpers;
using OEAutomationLauncher.DriveCrashInterface;
using OEAutomationLauncher.LogicalHelpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OEAutomationLauncher.AutomationLauncher.ObjectClasses
{
    public class StartupActions
    {
        private bool DebuggerAttached = false;
        private MainWindow MainWindowView;

        public StartupActions(MainWindow mainWindow)
        {
            DebuggerAttached = Debugger.IsAttached;
            MainWindowView = mainWindow;

            ConsoleSetup();
            SetupWindowStates();
            SetupDebugLogViewer();
        }

        public void ConsoleSetup()
        {
            var CONSOLE_HANDLE = InterlopConsoleToggle_Invoker.GetConsoleWindow();

            if (DebuggerAttached) { InterlopConsoleToggle_Invoker.ShowWindow(CONSOLE_HANDLE, 5); }
            if (!DebuggerAttached) { InterlopConsoleToggle_Invoker.ShowWindow(CONSOLE_HANDLE, 0); }
        }

        public void SetupWindowStates()
        {
            string[] ArgsFromApp = Environment.GetCommandLineArgs();
            foreach (var Arg in ArgsFromApp)
            {
                if (!Arg.Contains("RESIZE")) { continue; }

                GlobalObjects.CanResizeRelease = true;
                break;
            }

            if (DebuggerAttached || GlobalObjects.CanResizeRelease)
            {
                MainWindowView.Dispatcher.Invoke(() =>
                {
                    MainWindowView.ResizeMode = ResizeMode.CanResize;
                    MainWindowView.WindowStyle = WindowStyle.SingleBorderWindow;
                });
            }
            else
            {
                MainWindowView.Dispatcher.Invoke(() =>
                {
                    MainWindowView.ResizeMode = ResizeMode.NoResize;
                    MainWindowView.WindowStyle = WindowStyle.None;
                });
            }
        }

        public void SetupDebugLogViewer()
        {
            string AddThisText = DateTime.Now.ToString("s") + " DEBUG LOG HELPER WINDOW INITALIZED OK!\n";
            MainWindowView.Dispatcher.Invoke(() => MainWindowView.DebugConfigAndLoggerView.DebugLogHelperBox.Text += AddThisText);
        }

        public void SetConfigTextBoxValues()
        {
            MainWindowView.Dispatcher.Invoke(() =>
            {
                MainWindowView.VIN_Block.Text = GlobalObjects.ConfigInfo.VIN;
                MainWindowView.Year_Block.Text = GlobalObjects.ConfigInfo.Year;
                MainWindowView.Make_Block.Text = GlobalObjects.ConfigInfo.Make;
                MainWindowView.Model_Block.Text = GlobalObjects.ConfigInfo.Model;
            });

            GlobalObjects.DebugHelper.WriteDebugLog("SETUP VEHICLE INFO BOXES OK!");
            GlobalObjects.DebugHelper.WriteDebugLog("VALUES: " + 
                GlobalObjects.ConfigInfo.VIN + 
                GlobalObjects.ConfigInfo.Year +
                GlobalObjects.ConfigInfo.Make + 
                GlobalObjects.ConfigInfo.Model);
        }

        public void SetupGlobalObjects()
        {
            GlobalObjects.MainWindowView = MainWindowView;
            GlobalObjects.ContactSupportGrid = MainWindowView.Contact360SupportView;
            GlobalObjects.ConfirmCancelButtons = MainWindowView.ConfirmCancelButtonView;
            GlobalObjects.ScanCompleteButtons = MainWindowView.ScanCompleteButtonView;
            GlobalObjects.DebugInfoTextBox = MainWindowView.DebugConfigAndLoggerView.DebugLogHelperBox;
            GlobalObjects.ViewAndAPIDebugView = MainWindowView.DebugViewAndAPIFunctionsView;
            GlobalObjects.LogAndConfigView = MainWindowView.DebugConfigAndLoggerView;
            GlobalObjects.WindowHelpers = new MainWindowViewHelpers();

            MainWindowView.Dispatcher.Invoke(() =>
            {
                _ = new FallBackVehicleConfigs();
                foreach (var ItemType in FallBackVehicleConfigs.AllConfigItems)
                {
                    string NameOfCar = ItemType.NameOfVehicle;
                    MainWindowView.DebugConfigAndLoggerView.VehicleConfigNameCombobox.Items.Add(NameOfCar);
                }
                MainWindowView.DebugConfigAndLoggerView.VehicleConfigNameCombobox.SelectedIndex = 0;
            });
        }

        public bool CheckValidOEArg()
        {
            if (!DebuggerAttached)
            {
                if (!MainWindowViewHelpers.GetOEAppType(out string OEType))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT DETERMINE THE OE TYPE FROM ARGS. KILLING");
                    Console.WriteLine("COULD NOT DETERMINE THE OE TYPE FROM ARGS. KILLING");

                    if (Debugger.IsAttached) { Console.ReadLine(); }
                    return false;
                }

                GlobalObjects.DebugHelper.WriteDebugLog("OE TYPE: " + OEType);
                Console.WriteLine("OE TYPE: " + OEType);

                GlobalObjects.OEType = OEType;
                return true;
            }
            return true;
        }

        public void MiscLogicInit()
        {
            // Setup Timer Objects here.
            GlobalObjects.SetupTimers();
            GlobalObjects.FullProcTimer.Start();

            // Pull user config info so it can populate easier.
            MainWindowView.Dispatcher.Invoke(() => MainWindowView.Contact360SupportView.GetUserConfigInfo());

            // Get a list of the current J2534 Logs and prepare to collect only new ones.
            GlobalObjects.FileCollector = new PrepareFileGathering_Invoker();

            // Setup UI Scaling and Bools to Go with them. -- FORCE RESIZE ON!
            GlobalObjects.UIHelpers = new UIScalingHelpers(MainWindowView);

            // Populate the logger TB if we're debugging
            GlobalObjects.SetLoggerBoxContent(true);

            // Reboot the DSRS
            DriveCrashServiceController.RestartDSRS();
        }
    }
}
