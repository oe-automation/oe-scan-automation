﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.ObjectClasses
{
    public class PrepareFileGathering_Invoker
    {
        public DateTime ScanStartDateTime;
        public DateTime ScanStopDateTime;

        private string DefaultLogLocation = @"C:\Drewtech\Logs";
        public List<string> PreScanJ2534Logs;   // Logs before the scan
        public List<string> PostScanJ2534Logs;  // Logs after the scan

        public List<string> NewFilesToCollect;
        public string CombinedLogFile;
        public string SDFCombinedFile;

        public PrepareFileGathering_Invoker()
        {
            // Get The current Date and Time
            ScanStartDateTime = DateTime.Now;
            GlobalObjects.DebugHelper.WriteDebugLog("SCAN LOG GATHERING STARTED AT: " + ScanStartDateTime.ToShortDateString(), "FILES");

            // Get a list of all files in the J2534 log dir.
            PreScanJ2534Logs = Directory.GetFiles(DefaultLogLocation).ToList();
            GlobalObjects.DebugHelper.WriteDebugLog("COUNTED UP DEFAULT LOG FILES AND FOUND: " + PreScanJ2534Logs.Count + "FILES", "FILES");

            // Filter out the non J2534 Logs now
            PreScanJ2534Logs = PreScanJ2534Logs.Where(x => x.Contains("CarDAQ") || x.Contains("GDP")).ToList();
            GlobalObjects.DebugHelper.WriteDebugLog("AFTER FILTERING LOG FILES THERE ARE: " + PreScanJ2534Logs.Count + "FILES LEFT", "FILES");
        }

        /// <summary>
        /// Gets the list of new files after running an OE Scan process.
        /// </summary>
        /// <returns>A bool showing if theres new files in the Dir or not.</returns>
        public bool GetNewScanFiles()
        {
            // Get the current Date and Time.
            ScanStopDateTime = DateTime.Now;
            GlobalObjects.DebugHelper.WriteDebugLog("SCAN LOG GATHERING STOPPED AT: " + ScanStopDateTime.ToShortDateString(), "FILES");

            // Get the list of the new J2534 Files.
            PostScanJ2534Logs = Directory.GetFiles(DefaultLogLocation).ToList();
            GlobalObjects.DebugHelper.WriteDebugLog("COUNTED UP DEFAULT LOG FILES AND FOUND: " + PostScanJ2534Logs.Count + "FILES AFTER OE SCAN WAS DONE", "FILES");

            // Filter out anything Not J2534
            PostScanJ2534Logs = PostScanJ2534Logs.Where(x => x.Contains("CarDAQ") || x.Contains("GDP")).ToList();
            GlobalObjects.DebugHelper.WriteDebugLog("AFTER FILTERING LOG FILES THERE ARE: " + PostScanJ2534Logs.Count + "FILES LEFT", "FILES");

            if (PostScanJ2534Logs.Count > PreScanJ2534Logs.Count)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("NEW FILES FOUND!");

                GlobalObjects.DebugHelper.WriteDebugLog("FOUND NEW FILES! MAKING DIFF LIST", "FILES");
                CreateDiffList();

                GlobalObjects.DebugHelper.WriteDebugLog("COMBINING NEW FILES NOW", "FILES");
                CombinedLogFile = CombineLogs(NewFilesToCollect);

                try
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("CREATING SDF FILE FROM THE COMBINED J2534 LOG FILE NOW");
                    CreateSDFOutput();
                }
                catch (Exception sdfEx)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO CONVERT TO A SDF FILE!");
                    GlobalObjects.DebugHelper.WriteDebugLog("EX: " + sdfEx.Message);
                    GlobalObjects.DebugHelper.WriteDebugLog(sdfEx);
                }

                return true;
            }

            GlobalObjects.DebugHelper.WriteDebugLog("NO NEW FILES WERE FOUND. NO REASON TO GATHER", "FILES");
            return false;
        }

        public void CollectAndPackNewFiles()
        {
            if (Debugger.IsAttached) { GlobalObjects.DebugHelper.WriteDebugLog("DEBUGGER FOUND. THIS MAY CAUSE ONE OR MORE FILES TO FAIL TO UPLOAD"); }

            GlobalObjects.DebugHelper.WriteDebugLog("UPLOADING LOG FILE AND DEBUG LOGS TO S3 BUCKET NOW");
            var Uploader = new TaskListHelpers.AWSHelpers();

            if (CombinedLogFile == null) { CombinedLogFile = ""; }
            if (SDFCombinedFile == null) { SDFCombinedFile = ""; }
            Uploader.PushDebugLogsToBucket(CombinedLogFile, SDFCombinedFile);
        }

        private void CreateDiffList()
        {
            NewFilesToCollect = PostScanJ2534Logs.Except(PreScanJ2534Logs).ToList();
            GlobalObjects.DebugHelper.WriteDebugLog("FILE DIFF: " + NewFilesToCollect.Count, "FILES");

            foreach (var FileObject in NewFilesToCollect)
            {
                string FileNameOnly = FileObject.Substring(FileObject.LastIndexOf(Path.DirectorySeparatorChar) + 1);
                GlobalObjects.DebugHelper.WriteDebugLog("FILE: " + FileNameOnly + " IS BEING COLLECTED", "FILES");
            }
        }

        private string CombineLogs(List<string> LogFiles)
        {
            GlobalObjects.DebugHelper.WriteDebugLog("COMBINING FILES INTO ONE NOW FOR UPLOADING DEBUG INFO", "FILES");

            string BasePath = @"C:\Program Files (x86)\OESupport\OEDebugging\AutomationInvokerDebug\";
            string TempJ2534 = BasePath + "Temp2534";

            List<string> CoppiedLogFiles = new List<string>();
            if (!Directory.Exists(TempJ2534)) Directory.CreateDirectory(TempJ2534);
            foreach (var FileName in LogFiles)
            {
                string FileNameOnly = FileName.Substring(FileName.LastIndexOf("\\") + 1);
                try
                {
                    File.Copy(FileName, TempJ2534 + "\\" + FileNameOnly);
                    CoppiedLogFiles.Add(TempJ2534 + "\\" + FileNameOnly);

                    GlobalObjects.DebugHelper.WriteDebugLog("FILE: " + FileNameOnly + " WAS GENERATED AND COPPIED OK", "FILE");
                }
                catch { GlobalObjects.DebugHelper.WriteDebugLog("FILE: " + FileNameOnly + " COULD NOT BE COPPIED!!!"); }
            }

            var strTime = ScanStartDateTime.ToString("s");
            strTime = strTime.Replace("T", "_");
            strTime = strTime.Replace("-", "");
            strTime = strTime.Split('_')[0];

            string OutputFile = "CombinedJ2534Logs_" + GlobalObjects.ConfigInfo.VIN + "_" + strTime + ".txt";
            CombinedLogFile = Path.Combine(BasePath, OutputFile);
            GlobalObjects.DebugHelper.WriteDebugLog("FINAL FILE: " + OutputFile, "FILES");

            // Remove old version of the file if they exist.
            if (File.Exists(OutputFile)) { File.Delete(OutputFile); }

            try
            {
                using (var FileWriterStream = File.Create(CombinedLogFile))
                {
                    foreach (var InputLogFile in CoppiedLogFiles)
                    {
                        try
                        {
                            GlobalObjects.DebugHelper.WriteDebugLog("COPYING FILE: " + InputLogFile, "FILES");
                            using (var FileReaderStream = File.OpenRead(InputLogFile))
                            {
                                // Buffer size can be passed as the second argument if we find this is slow as balls.
                                FileReaderStream.CopyTo(FileWriterStream);
                            }
                            GlobalObjects.DebugHelper.WriteDebugLog("FILE COPPIED OK!", "FILES");
                        }
                        catch { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT COPY FILE: " + InputLogFile); }
                    }
                }
            }
            catch (Exception ex) { GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO COMBINE: " + ex.Message); }

            try { Directory.Delete(TempJ2534, true); }
            catch { GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT REMOVE TEMP J2534 DIR"); }

            GlobalObjects.DebugHelper.WriteDebugLog("COMBINED LOG FILE: " + CombinedLogFile);
            return CombinedLogFile;
        }

        private void CreateSDFOutput()
        {
            string PathToConverter = @"C:\Program Files (x86)\OESupport\OEAppHelpers\JLogToSim.exe";
            if (!File.Exists(PathToConverter)) { GlobalObjects.DebugHelper.WriteDebugLog("JLOGTOSIM WAS NOT FOUND! SKIPPING SDF CREATION"); }
            else
            {
                string DirOfFile = new FileInfo(CombinedLogFile).DirectoryName;
                string FileToParse = new FileInfo(CombinedLogFile).Name;

                if (!File.Exists(CombinedLogFile))
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("COMBINED J2534 DEBUG LOG DOES NOT EXIST!");
                    GlobalObjects.DebugHelper.WriteDebugLog("FILE DESIRED: " + FileToParse);
                    GlobalObjects.DebugHelper.WriteDebugLog("THIS IS A FATAL ERROR FOR THE SDF CREATOR");

                    return;
                }

                var JLogToSimtimer = new Stopwatch();
                JLogToSimtimer.Start();
                GlobalObjects.DebugHelper.WriteDebugLog("STARTED JLOG TO SIM TIMER!");

                // Call JLogToSim here and wait for it to execute totally. 
                var StartInfo = new ProcessStartInfo()
                {
                    WindowStyle = ProcessWindowStyle.Normal,
                    FileName = PathToConverter,
                    Arguments = FileToParse,
                    WorkingDirectory = DirOfFile
                };

                GlobalObjects.DebugHelper.WriteDebugLog("JLOGTOSIM START INFO MADE OK!");
                GlobalObjects.DebugHelper.WriteDebugLog("FILE TO CONVERT: " + CombinedLogFile);

                // Start process now and wait for window to open then wait for the proc to extt out.
                var ConverterProcess = Process.Start(StartInfo);
                GlobalObjects.DebugHelper.WriteDebugLog("JLOGTOSIM CONVERTER STARTED NOW!");

                // Wait for exit.
                GlobalObjects.DebugHelper.WriteDebugLog("JLOGTOSIM WAITING FOR EXIT NOW");
                ConverterProcess.WaitForExit();

                // Inform we're done here.
                GlobalObjects.DebugHelper.WriteDebugLog("JLOGTOSIM CONVERTER DONE AND CLOSED. ADDING TO FILE LIST FOR UPLOAD");

                // Timer done, close it all up here. 
                JLogToSimtimer.Stop();
                string TimeToConvert = JLogToSimtimer.Elapsed.ToString(@"mm\:ss\.fff");
                GlobalObjects.DebugHelper.WriteDebugLog("CONVERSION DONE. PROCESS TOOK A TOTAL OF: " + TimeToConvert);

                foreach (var FileItem in Directory.GetFiles(DirOfFile))
                {
                    FileInfo FileProps = new FileInfo(FileItem);
                    if (FileProps.Extension.ToUpper().Contains("SDF"))
                    {
                        GlobalObjects.DebugHelper.WriteDebugLog("FOUND AN SDF FILE!");
                        GlobalObjects.DebugHelper.WriteDebugLog("FILE: " + FileProps.Name);
                        GlobalObjects.DebugHelper.WriteDebugLog("SIZE: " + File.ReadAllBytes(FileItem).Length);

                        SDFCombinedFile = FileItem;
                        break;
                    }
                }
            }
        }
    }
}
