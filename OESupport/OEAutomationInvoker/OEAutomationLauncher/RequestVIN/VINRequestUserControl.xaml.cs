﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OEAutomationLauncher.RequestVIN
{
    /// <summary>
    /// Interaction logic for VINRequestUserControl.xaml
    /// </summary>
    public partial class VINRequestUserControl : UserControl
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }
        public static bool IsValidVIN { get; set; }

        public VINRequestUserControl()
        {
            DataContext = this;
            InitializeComponent();

            VinTextBox.Text = "";
            IsValidVIN = false;

            GlobalObjects.VINEntryVIew = this;
        }

        private void VinTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            IsValidVIN = VinTextBox.Text.ToString().Length == 17;
            if (IsValidVIN)
            {
                ConfirmBtn.Background = Brushes.Green;
                ConfirmBtn.Content = "Confirm VIN Number";
            }

            if (!IsValidVIN)
            {
                ConfirmBtn.Background = Brushes.Red;
                ConfirmBtn.Content = "Invalid VIN Number";
            }
        }

        private void ConfirmClick(object sender, RoutedEventArgs e)
        {
            if (!IsValidVIN) { return; }
            else
            {
                var btnSend = (Button)sender;
                btnSend.Content = "Saving VIN Number...";

                string VinValue = VinTextBox.Text.ToString();
                string TotalPath = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\UserVIN.txt";

                if (File.Exists(TotalPath)) { File.Delete(TotalPath); }
                File.WriteAllText(TotalPath, VinValue);

                VINRequestTrigger.VINRequestComplete();
            }
        }
    }
}
