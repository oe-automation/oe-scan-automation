﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace OEAutomationLauncher.RequestVIN
{
    public class VINRequestTrigger
    {
        public static void VINRequestStart()
        {
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                GlobalObjects.MainWindowView.OptionsPickerButtons.Visibility = Visibility.Hidden;
                GlobalObjects.MainWindowView.CancelScanGridButtons.Visibility = Visibility.Visible;

                GlobalObjects.MainWindowView.TaskGridScrollViewer.Visibility = Visibility.Hidden;
                GlobalObjects.MainWindowView.TaskGrid.Visibility = Visibility.Hidden;
                GlobalObjects.MainWindowView.OptionsSelectionGrid.Visibility = Visibility.Hidden;

                GlobalObjects.MainWindowView.VINRequestGrid.Visibility = Visibility.Visible;

                GlobalObjects.DebugHelper.WriteDebugLog("VIN BUTTONS POPULATED!");
            });
        }

        public static void VINRequestComplete()
        {
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                GlobalObjects.DebugHelper.WriteDebugLog("VIN REQUEST DONE. RESETING VIEWS TO MAIN/DEFAULTS HERE");

                GlobalObjects.MainWindowView.OptionsPickerButtons.Visibility = Visibility.Hidden;
                GlobalObjects.MainWindowView.CancelScanGridButtons.Visibility = Visibility.Visible;

                GlobalObjects.MainWindowView.OptionsSelectionGrid.Visibility = Visibility.Hidden;
                GlobalObjects.MainWindowView.VINRequestGrid.Visibility = Visibility.Hidden;

                GlobalObjects.MainWindowView.TaskGridScrollViewer.Visibility = Visibility.Visible;
                GlobalObjects.MainWindowView.TaskGrid.Visibility = Visibility.Visible;

                foreach (Control Child in GlobalObjects.MainWindowView.OptionsSelectionGrid.Children) { Child.Visibility = Visibility.Hidden; }

                GlobalObjects.DebugHelper.WriteDebugLog("RESET OK MOVING ON");
            });
        }
    }
}
