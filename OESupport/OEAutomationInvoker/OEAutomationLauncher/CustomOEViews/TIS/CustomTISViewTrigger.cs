﻿using OEAutomationLauncher.AutomationLauncher.CustomOEViews.TIS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.CustomOEViews.TIS
{
    public class CustomTISViewTrigger
    {
        public bool ScanIsTIS
        {
            get
            {
                if (GlobalObjects.OEType != "TIS")
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("CAN NOT SHOW RECALL INFO FOR A NON TIS SCAN OBJECT!");
                    return false;
                }

                return true;
            }
        }

        public List<TISRecallObject> CreateAllRecallObjects()
        {
            // Input File Lines.
            string[] RecallLinesSplit = File.ReadAllText(GlobalObjects.MainWindowView.TISRecallView.RecallInfoFile).Split('\n');

            // List of all split recall lines here.
            List<string> SeperateRecalls = new List<string>();

            // Once we have lists/sets of lines per recall, they can be fed into the recall 
            // object constructor to feed back good recall objects.

            // Count off lines and append them into that list above.
            int StartIndex = 0;
            while (StartIndex < RecallLinesSplit.Length)
            {
                if ((StartIndex + 14) > RecallLinesSplit.Length) { break; }
                string[] NextLines = RecallLinesSplit.Skip(StartIndex).Take(13).ToArray();

                string NextLinesParsed = "";
                foreach (var linePart in NextLines) { NextLinesParsed += linePart + "\n"; }

                SeperateRecalls.Add(NextLinesParsed);
                StartIndex += 14;
            }

            // Convert the strings into RecallObjects here.
            List<TISRecallObject> ReturnRecalls = new List<TISRecallObject>();
            foreach (var stringSet in SeperateRecalls) { ReturnRecalls.Add(new TISRecallObject(stringSet)); }

            return ReturnRecalls;
        }

        public string ReturnAllTISRecalls()
        {
            // This needs to pull from a list of recall objects in the mainwindow recall view.
            GlobalObjects.DebugHelper.WriteDebugLog("READING INFO IN FROM THE RECALL CONFIG OBJECTS");

            string CombinedRecallString = "";
            var AllRecalls = GlobalObjects.MainWindowView.TISRecallView.AllRecalls;

            if (AllRecalls.Count == 0)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("FOUND ZERO RECALL OBJECTS ON THE RECALL VIEW!");
                return "NONE";
            }

            else
            {
                foreach (var RecallObj in AllRecalls) { CombinedRecallString += RecallObj.ReturnStringForm() + " || "; }
                CombinedRecallString = CombinedRecallString.Trim();

                GlobalObjects.DebugHelper.WriteDebugLog("RECALL OBJECTS: " + CombinedRecallString.Replace("\n", "---"));
                return CombinedRecallString;
            }
        }
    }
}
