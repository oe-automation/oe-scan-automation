﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OEAutomationLauncher.AutomationLauncher.CustomOEViews.TIS
{
    /// <summary>
    /// Interaction logic for TISRecalAlerts.xaml
    /// </summary>
    public partial class TISRecallView : UserControl
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }

        private CustomTISViewTrigger TISViewTrigger;

        public List<TISRecallObject> AllRecalls { get; set; }
        public string RecallInfoFile { get; set; }
        public string CleanedRecallInfo { get; set; }


        public TISRecallView()
        {
            DataContext = this;
            InitializeComponent();

            AllRecalls = new List<TISRecallObject>();
            TISViewTrigger = new CustomTISViewTrigger();

            GlobalObjects.TIS_RecallView = this;
            // GlobalObjects.TypeOfLastView = typeof(TISRecallView);
        }

        public void CheckRecallViewConditions()
        {
            string RecallBase = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\CustomViewConfigs\";
            string RecallFileName =
                GlobalObjects.ConfigInfo.Year + "_" +
                GlobalObjects.ConfigInfo.Make.ToUpper().Replace(" ", "") + "_" +
                GlobalObjects.ConfigInfo.Model.ToUpper().Replace(" ", "") + "_" +
                "RECALLS.txt";
            RecallInfoFile = RecallBase + RecallFileName;

            if (!File.Exists(RecallInfoFile)) 
            {
                GlobalObjects.DebugHelper.WriteDebugLog("RECALL CONFIG FILE NOT FOUND!!");
                GlobalObjects.DebugHelper.WriteDebugLog("TRIED TO FIND FILE: " + RecallInfoFile);
            }

            else
            {
                AddRecallItemsToView();
                this.Visibility = Visibility.Visible;
            }
        }

        public void AddRecallItemsToView()
        {
            // DUMP RECALLS INTO WINDOW HERE!!!
            AllRecalls = TISViewTrigger.CreateAllRecallObjects();
            AllRecallsDataGrid.ItemsSource = AllRecalls;

            GlobalObjects.DebugHelper.WriteDebugLog("GOT ALL RECALL OBJECTS. " + AllRecalls.Count + " TOTAL. IMPORTING THEM NOW.");
            GlobalObjects.DebugHelper.WriteDebugLog("POPULATED RECALL GRID AND SHOWED OK!");
        }

        public void CloseRecallViewButton(object sender, RoutedEventArgs e)
        {
            // Stop AutoClose Timer
            GlobalObjects.StopCloseTimer();

            string OutputString = "";
            foreach (var RecalObj in AllRecalls)
            {
                OutputString += "CODE: " + RecalObj.Code + " -- " ;
                OutputString += "DESC: " + RecalObj.Description + " -- ";
                OutputString += "STAT: " + RecalObj.Status + "\n";
            }

            string CleanedRecallBase = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\CustomViewConfigs\";
            string CleanedRecallFileName = 
                GlobalObjects.ConfigInfo.Year + "_" +
                GlobalObjects.ConfigInfo.Make.ToUpper().Replace(" ", "") + "_" +
                GlobalObjects.ConfigInfo.Model.ToUpper().Replace(" ", "") + "_" +
                "CLEANED_RECALLS.txt";

            CleanedRecallInfo = CleanedRecallBase + CleanedRecallFileName;

            try { File.WriteAllText(CleanedRecallInfo, OutputString); }
            catch (Exception writeEx)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT WRITE RECAL PROCESSING FILE OUT!");
                GlobalObjects.DebugHelper.WriteDebugLog(writeEx);
            }

            File.WriteAllText(@"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\RecallWindowClosed.txt", "CLOSED");
            this.Visibility = Visibility.Hidden;
        }
    }
}
