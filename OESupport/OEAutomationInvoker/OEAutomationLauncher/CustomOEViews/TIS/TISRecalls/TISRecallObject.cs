﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.CustomOEViews.TIS
{
    public class TISRecallObject
    {
        public string Code { get; set; } 
        public string Description { get; set; }
        public string Status { get; set; }

        public TISRecallObject(string RecallFileLines)
        {
            #region Sample Input Lines 
            /* (Yes the two blank lines count)
             1 |
             2 |
             3 |   Code
             4 |   Code
             5 |   Description
             6 |   Description
             7 |   Status
             8 |   Status
             9 |   KLG
            10 |   KLG
            11 |   LIMITED SERVICE CAMPAIGN KLG (Remedy Notice) - Multiple Models and Model Years - Safety Connect System - Vehicle Location Inaccurate
            12 |   LIMITED SERVICE CAMPAIGN KLG (Remedy Notice) - Multiple Models and Model Years - Safety Connect System - Vehicle Location Inaccurate
            13 |   Not Completed
            14 |   Not Completed
            */

            #endregion

            int IndexOfStatus = RecallFileLines.LastIndexOf("Status") + 6;
            string ValuesOnly = RecallFileLines.Substring(IndexOfStatus);

            List<string> AllDescriptionValues = new List<string>();
            foreach (string ValuePart in ValuesOnly.Split('\n'))
            { 
                if (ValuePart == "\n" || ValuePart == "\r") { continue; }
                if (ValuePart.Trim().Length < 2) { continue; }

                AllDescriptionValues.Add(ValuePart.Trim()); 
            }

            var FinalValues = AllDescriptionValues.Distinct().ToList();
            
            Code = FinalValues[0];
            Description = FinalValues[1];
            Status = FinalValues[2];
        }

        public string ReturnStringForm()
        {
            return "{CODE - " + Code + "}{DESC - " + Description + "}{STATUS - " + Status + "}";
        }
    }
}
