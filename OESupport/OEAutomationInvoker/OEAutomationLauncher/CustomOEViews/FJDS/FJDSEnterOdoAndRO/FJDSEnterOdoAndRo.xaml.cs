﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OEAutomationLauncher.AutomationLauncher.CustomOEViews.FJDS
{
    /// <summary>
    /// Interaction logic for FJDSEnterOdoAndRo.xaml
    /// </summary>
    public partial class FJDSEnterOdoAndRo : UserControl
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }

        public string OutputODO = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\CustomViewConfigs\FJDS_OdoValue.txt";
        public string OutputRO = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\CustomViewConfigs\FJDS_ROValue.txt";

        public FJDSEnterOdoAndRo()
        {
            DataContext = this;
            InitializeComponent();

            if (File.Exists(OutputODO)) { File.Delete(OutputODO); }
            if (File.Exists(OutputRO)) { File.Delete(OutputRO); }
            
            GlobalObjects.FJDS_OdoAndROView = this;
            // GlobalObjects.TypeOfLastView = typeof(FJDSEnterOdoAndRo);
        }

        public void SetOdoAndRoButtonClick(object sender, RoutedEventArgs e)
        {
            // Stop Autoclose timer
            GlobalObjects.StopCloseTimer();

            this.Visibility = Visibility.Hidden;
            FJDSOdoAndROObject NewVals = new FJDSOdoAndROObject();

            int Counter = 0;
            foreach (var ChildObj in InfoGrid.Children)
            {
                try
                {
                    if (!(ChildObj is TextBox)) { continue; }

                    var TextBoxFound = (TextBox)ChildObj;
                    if (Counter == 0)
                    {
                        if (!NewVals.SetOdoValue(TextBoxFound.Text))
                        {
                            string NewText = TextBoxFound.Text.Substring(0, TextBoxFound.Text.IndexOf("."));
                            NewVals.SetOdoValue(NewText);
                        }
                    }
                    if (Counter == 1) { NewVals.RoValue = TextBoxFound.Text; }
                }

                catch (Exception SetEx)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("FAILED SETTING NEW VALS OBJ: [" + Counter + "]: " + SetEx.Message);
                    GlobalObjects.DebugHelper.WriteDebugLog(SetEx);
                    if (Counter == 0) { NewVals.SetOdoValue("0.0"); }
                    if (Counter == 1) { NewVals.RoValue = "RO_VALUE"; }
                }

                Counter++;
            }


            try { File.WriteAllText(OutputODO, NewVals.OdoValue); }
            catch(Exception odoEx)
            {
                File.WriteAllText(OutputODO, "0000");
                GlobalObjects.DebugHelper.WriteDebugLog("CANT WRITE ODO: " + odoEx.Message);
                GlobalObjects.DebugHelper.WriteDebugLog(odoEx);
            }

            try { File.WriteAllText(OutputRO, NewVals.RoValue); }
            catch (Exception roEx)
            {
                File.WriteAllText(OutputRO, "0000");
                GlobalObjects.DebugHelper.WriteDebugLog("CANT WRITE RO:" + roEx.Message);
                GlobalObjects.DebugHelper.WriteDebugLog(roEx);
            }
        }
    }
}
