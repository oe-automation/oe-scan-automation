﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.CustomOEViews.FJDS
{
    public class FJDSOdoAndROObject
    {
        public string OdoValue { get; set; }
        public string RoValue { get; set; }

        public bool SetOdoValue(string OdoInput)
        {
            bool ConvertedOK;
            if (OdoInput.Contains("."))
            {
                ConvertedOK = double.TryParse(OdoInput, out double tempOdo);
                if (ConvertedOK)
                {
                    int NumDecs = OdoInput.Split('.')[1].Length;
                    OdoValue = tempOdo.ToString("F" + NumDecs);

                    return ConvertedOK;
                }

                return ConvertedOK;
            }

            else
            {
                ConvertedOK = int.TryParse(OdoInput, out int tempOdo);
                if (ConvertedOK)
                {
                    OdoValue = tempOdo.ToString();
                    return ConvertedOK;
                }

                return ConvertedOK;
            }
        }
    }
}
