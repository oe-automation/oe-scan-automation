﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OEAutomationLauncher.AutomationLauncher.CustomOEViews.FJDS
{
    /// <summary>
    /// Interaction logic for FJDSNoComms.xaml
    /// </summary>
    public partial class FJDSNoComms : UserControl
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }
        public FJDSNoComms()
        {
            DataContext = this;
            InitializeComponent();

            GlobalObjects.FJDS_NoCommsView = this;
            // GlobalObjects.TypeOfLastView = typeof(FJDSNoComms);
        }

        public void ConfirmOrCancelCommsRetry(object sender, RoutedEventArgs e)
        {
            // Stop Autoclose timer.
            GlobalObjects.StopCloseTimer();

            if (sender == null) { sender = CancelNoComms; }

            var SendBtn = (Button)sender;
            string SendBtnContent = SendBtn.Content.ToString();

            string RetryFile = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\RetryCommsFJDS.txt";

            if (SendBtnContent.Contains("Retry")) { File.WriteAllText(RetryFile, "YES"); }
            else { File.WriteAllText(RetryFile, "NO"); }

            this.Visibility = Visibility.Hidden;
        }
    }
}
