﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace OEAutomationLauncher.AutomationLauncher.CustomOEViews.FJDS
{
    public class CustomFJDSViewTrigger
    {
        public bool ScanIsFJDS
        {
            get
            {
                if (GlobalObjects.OEType != "FJDS")
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("CAN NOT SHOW COMMS FAILED FOR A NON FJDS SCAN OBJECT!");
                    return false;
                }

                return true;
            }
        }

        public bool ShowFJDSNoCommsWindow()
        {
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                GlobalObjects.MainWindowView.NoFJDSComms.Visibility = Visibility.Visible;
                GlobalObjects.DebugHelper.WriteDebugLog("SHOWING FJDS COMMS FAIL WINDOW");
            });

            return true;
        }

        public bool ShowFJDSRoAndOdoWindow()
        {
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                var Children = GlobalObjects.MainWindowView.FJDSEnterRoAndOdo.InfoGrid.Children;
                foreach (var ChildVar in Children)
                {
                    if (ChildVar is TextBox)
                    {
                        var TbChild = (TextBox)ChildVar;
                        TbChild.Text = "";
                    }
                }

                GlobalObjects.MainWindowView.FJDSEnterRoAndOdo.Visibility = Visibility.Visible;
                GlobalObjects.DebugHelper.WriteDebugLog("SHOWING FJDS ENTER ODO AND RO WINDOW");
            });

            return true;
        }
    }
}
