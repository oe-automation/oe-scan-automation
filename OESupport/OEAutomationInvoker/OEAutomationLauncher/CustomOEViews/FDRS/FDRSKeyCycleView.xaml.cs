﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OEAutomationLauncher.AutomationLauncher.CustomOEViews.FDRS
{
    /// <summary>
    /// Interaction logic for FDRSKeyCycleView.xaml
    /// </summary>
    public partial class FDRSKeyCycleView : UserControl
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }

        public FDRSKeyCycleView()
        {
            DataContext = this;
            InitializeComponent();

            GlobalObjects.FDRS_KeyCycleView = this;
        }
    }
}
