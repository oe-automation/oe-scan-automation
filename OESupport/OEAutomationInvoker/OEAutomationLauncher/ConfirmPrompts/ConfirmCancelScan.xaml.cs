﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OEAutomationLauncher.AutomationLauncher.ConfirmPrompts
{
    /// <summary>
    /// Interaction logic for ConfirmCancelScan.xaml
    /// </summary>
    public partial class ConfirmCancelScan : UserControl
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }

        public ConfirmCancelScan()
        {
            DataContext = this;
            InitializeComponent();

            GlobalObjects.ConfirmCancelButtons = this;
        }

        /// <summary>
        /// Event dealer for when we want to close the scan out and kill everything.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public async void ClickYesConfirmButton(object sender, RoutedEventArgs e)
        {
            // Set global var of cancled here.
            GlobalObjects.ScanWasTerminated = true;

            // Stop AutoClose
            GlobalObjects.StopCloseTimer();

            // Hide this view object here.
            ConfirmButtonsGrid.Visibility = Visibility.Hidden;
            ConfirmCloseTextBlock.Visibility = Visibility.Hidden;

            ClosingNowBlockTitle.Visibility = Visibility.Visible;
            ClosingNowGrid.Visibility = Visibility.Visible;
            ClosingPleaseWaitBlock.Visibility = Visibility.Visible;

            await Task.Run(() =>
            {
                ConfirmPromptLogic.KillScanAndWaj();
                ConfirmPromptLogic.GatherAndUpload(GlobalObjects.ScanCompletePassed);
            });

            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                // GlobalObjects.DebugHelper.WriteDebugLog("SHOWING OPUS GLOBE HOLDER");
                GlobalObjects.MainWindowView.HolderOpusGlobe.Visibility = Visibility.Visible;
            });

            await Task.Run(() =>
            {
                System.Threading.Thread.Sleep(2500);
                Dispatcher.Invoke(() => this.Visibility = Visibility.Hidden);

                GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
                {
                    // GlobalObjects.DebugHelper.WriteDebugLog("HIDING OPUS GLOBE HOLDER");
                    if (Debugger.IsAttached) { GlobalObjects.MainWindowView.HolderOpusGlobe.Visibility = Visibility.Hidden; }
                });
            });

            ClosingNowBlockTitle.Visibility = Visibility.Hidden;
            if (!Debugger.IsAttached) { Environment.Exit(0); }
        }

        /// <summary>
        /// Cancel the canceling of the OE Scan here.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickNoConfirmButton(object sender, RoutedEventArgs e)
        {
            // Hide this view object here and return to normal.
            this.Visibility = Visibility.Hidden;

            // Stop Autoclose
            GlobalObjects.StopCloseTimer();
        }
    }
}
