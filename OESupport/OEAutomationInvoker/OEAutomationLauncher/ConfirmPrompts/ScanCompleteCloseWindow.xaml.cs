﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OEAutomationLauncher.AutomationLauncher.ConfirmPrompts
{
    /// <summary>
    /// Interaction logic for ScanCompleteCloseWindow.xaml
    /// </summary>
    public partial class ScanCompleteCloseWindow : UserControl
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }

        public ScanCompleteCloseWindow()
        {
            DataContext = this;
            InitializeComponent();

            GlobalObjects.ScanCompleteButtons = this;
        }

        public async void ClickYesConfirmButton(object sender, RoutedEventArgs e)
        {
            // Stop Autoclose Timer
            GlobalObjects.StopCloseTimer();

            // Hide this view object here.
            ConfirmButtonsGrid.Visibility = Visibility.Hidden;
            ConfirmCloseTextBlock.Visibility = Visibility.Hidden;

            ClosingNowBlockTitle.Visibility = Visibility.Visible;
            ClosingNowGrid.Visibility = Visibility.Visible;
            ClosingPleaseWaitBlock.Visibility = Visibility.Visible;

            DoNotPowerOffBlock.Visibility = Visibility.Visible;

            await Task.Run(() =>
            {
                ConfirmPromptLogic.KillScanAndWaj();
                ConfirmPromptLogic.GatherAndUpload(GlobalObjects.ScanCompletePassed);
            });

            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                // GlobalObjects.DebugHelper.WriteDebugLog("SHOWING OPUS GLOBE HOLDER");
                GlobalObjects.MainWindowView.HolderOpusGlobe.Visibility = Visibility.Visible;
            });

            if (!GlobalObjects.ScanCompletePassed) { ConfirmPromptLogic.CreateHelpSessionTicket(); }

            await Task.Run(() =>
            {
                System.Threading.Thread.Sleep(2500);
                Dispatcher.Invoke(() => this.Visibility = Visibility.Hidden);

                GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
                {
                    // GlobalObjects.DebugHelper.WriteDebugLog("HIDING OPUS GLOBE HOLDER");
                    if (Debugger.IsAttached) { GlobalObjects.MainWindowView.HolderOpusGlobe.Visibility = Visibility.Hidden; }
                });
            });

            ClosingNowBlockTitle.Visibility = Visibility.Hidden;
            if (!Debugger.IsAttached) { Environment.Exit(0); }

        }

        private void ClickNoConfirmButton(object sender, RoutedEventArgs e)
        {
            // Hide this.
            this.Visibility = Visibility.Hidden;

            // Stop Autoclose timer
            GlobalObjects.StopCloseTimer();
        }
    }
}