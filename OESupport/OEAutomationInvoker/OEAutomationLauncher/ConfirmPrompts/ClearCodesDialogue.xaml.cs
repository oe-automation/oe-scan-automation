﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OEAutomationLauncher.AutomationLauncher.ConfirmPrompts
{
    /// <summary>
    /// Interaction logic for ClearCodesDialogue.xaml
    /// </summary>
    public partial class ClearCodesDialogue : UserControl
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }

        public int ClearAttempts = 0;

        public bool ClearCodes = false;
        public bool RetryClear = false;

        private static string CodeClearStatusFile = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\ClearCodesStatus.txt";
        private static string RetryCodeClearFile = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\RetryClearCodes.txt";

        public ClearCodesDialogue()
        {
            DataContext = this;
            InitializeComponent();

            if (File.Exists(CodeClearStatusFile)) { File.Delete(CodeClearStatusFile); } 
            if (File.Exists(RetryCodeClearFile)) { File.Delete(RetryCodeClearFile); }

            GlobalObjects.CodeClearingButtons = this;
            // GlobalObjects.TypeOfLastView = typeof(ClearCodesDialogue);
        }

        public static void DontScanIdleTrigger(bool ClearOrRetry)
        {
            if (ClearOrRetry) File.WriteAllText(CodeClearStatusFile, "NO");
            if (!ClearOrRetry) File.WriteAllText(RetryCodeClearFile, "NO");
        }

        public void ClickYesConfirmButton(object sender, RoutedEventArgs e)
        {
            if (RetryClear) { ClearAttempts++; }

            // Hide this view object here and return to normal.
            this.Visibility = Visibility.Hidden;

            // Write clear value to file.
            if (ClearCodes) File.WriteAllText(CodeClearStatusFile, "YES");
            if (RetryClear) File.WriteAllText(RetryCodeClearFile, "YES");

            GlobalObjects.PreOrPost = "POST";
            GlobalObjects.StopCloseTimer();
        }

        public void ClickNoConfirmButton(object sender, RoutedEventArgs e)
        {
            // Hide this view object here and return to normal.
            this.Visibility = Visibility.Hidden;

            // Write clear value to file.
            if (ClearCodes) File.WriteAllText(CodeClearStatusFile, "NO");
            if (RetryClear) File.WriteAllText(RetryCodeClearFile, "NO");

            GlobalObjects.PreOrPost = "PRE";
            GlobalObjects.StopCloseTimer();
        }

        private void OKReturnToScanButton_Click(object sender, RoutedEventArgs e)
        {          
            // Hide this view object here and return to normal.
            this.Visibility = Visibility.Hidden;

            // Tell RPA to stop retrying.
            if (RetryClear) File.WriteAllText(RetryCodeClearFile, "NO");

            // Try stop timer for autoclose.
            GlobalObjects.StopCloseTimer();

            // Make sure something is in here.
            if (GlobalObjects.PreOrPost == "") { GlobalObjects.PreOrPost = "OTHER"; }
        }
    }
}
