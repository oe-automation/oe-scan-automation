﻿using Newtonsoft.Json;
using OEAutomationLauncher.AutomationLauncher.ObjectClasses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OEAutomationLauncher.AutomationLauncher.DebugHelpers;
using OEAutomationLauncher.DebugHelpers;

namespace OEAutomationLauncher.AutomationLauncher.ConfirmPrompts
{
    #region Sample JSON Responses
    /*
     * LINK: http://localhost:15000/DrewTechDataSvc/GetUserInfo
     * {
     *  "NameFirst":"Zachary",
     *  "NameLast":"Walsh",
     *  "NameFull":"Zachary Walsh",
     *  "NameFullNoSpaces":"ZacharyWalsh",
     *  "Image":"images/UserWindow/user-shadow.png",
     *  "IsNewUser":false,
     *  "IsLoaded":true,
     *  "IsLoggedIn":true,
     *  "Email":"zachary.walsh@opusivs.com",
     *  "AuthToken":"a0528b133ef67f6ecfe2245e2dc0cafac00a8b353f4ca5fda9dc4a63161e273b",
     *  "Country":"United States"
     * }
     */

    #endregion

    /// <summary>
    /// JSON Help support ticket object.
    /// </summary>
    public class JSONHelpObject
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Reason { get; set; }
        public string AditionalInfo { get; set; }
    }


    /// <summary>
    /// Interaction logic for ContactIVS360Support.xaml
    /// </summary>
    public partial class ContactIVS360Support : UserControl
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }

        private WebClient API_Invoker = new WebClient();
        private string GetUserInfoLink = @"http://localhost:15000/DrewTechDataSvc/GetUserInfo";
        private string SendJSONHelpLink = @"http://localhost:15000/DrewTechDataSvc/RequestHelp360/";

        public string AllUserInfo = "";
        public UserConfigInfo_Invoker UserInfoObject;

        private bool ValidPhoneNumber = false;
        private string TempText =
            "Please enter a brief description of the issue you're encountering to help " +
            "our technicians assist you.";


        public ContactIVS360Support()
        {
            DataContext = this;
            InitializeComponent();

            GlobalObjects.ContactSupportGrid = this;
        }

        /// <summary>
        /// Pulls down the user config info from the BL API using the link set in GetUserInfoLink.
        /// Stored in the public AllUSerInfo string.
        /// </summary>
        /// <returns>String value of the user info in json form.</returns>
        public async void GetUserConfigInfo()
        {
            GlobalObjects.DebugHelper.WriteDebugLog("ATTEMPTING TO PULL JSON DATA FROM BL NOW");
            try
            {
                using (API_Invoker = new WebClient())
                {
                    Uri StringToPullFrom = new Uri(GetUserInfoLink);
                    AllUserInfo = await API_Invoker.DownloadStringTaskAsync(StringToPullFrom);
                };

                GlobalObjects.DebugHelper.WriteDebugLog("PULLED USER JSON DATA OK!");
                GlobalObjects.DebugHelper.WriteDebugLog("JSON DATA: " + AllUserInfo);
            }
            catch (Exception infoEx)
            {
                AllUserInfo = FallBackItems.FallBackUserInfo;
                GlobalObjects.DebugHelper.WriteDebugLog("FAILED TO PULL USER JSON FROM BOOTLOADER! (Ofc....)");
                GlobalObjects.DebugHelper.WriteDebugLog("FALLBACK DEFAULT USER JSON: " + AllUserInfo);

                GlobalObjects.DebugHelper.WriteDebugLog(infoEx);
            }

            UserInfoObject = JsonConvert.DeserializeObject<UserConfigInfo_Invoker>(AllUserInfo);

            GlobalObjects.DebugHelper.WriteDebugLog("POPULATING USER CONFIG INFO OBJECT");
            TechNameBox.Text = UserInfoObject.NameFull;
            TechEmailBox.Text = UserInfoObject.Email;
        }

        private void TechPhoneBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string TempNumber = TechPhoneBox.Text;

            var AllRegions = CultureInfo.GetCultures(CultureTypes.SpecificCultures).Select(x => new RegionInfo(x.LCID));
            var DesiredRegion = AllRegions.FirstOrDefault(region => region.EnglishName.Contains(UserInfoObject.Country)).TwoLetterISORegionName;
            string CountryCode = PhoneNumberCallingTag_Invoker.GetCountryCallCode(DesiredRegion);

            if (Regex.Match(TempNumber, @"(\d{3})(\d{3})(\d{4})").Success)
            {
                var NewNumber = Regex.Replace(TempNumber, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3");
                TechPhoneBox.Text = CountryCode + " " + NewNumber;

                GlobalObjects.DebugHelper.WriteDebugLog("GOT NEW PHONE NUMBER VALUE INPUT AND PARSED IT OUT INTO A CORRECT FORMAT");
                GlobalObjects.DebugHelper.WriteDebugLog("NEW PHONE NUMBER: " + TechPhoneBox.Text + " WAS SET");

                ValidPhoneNumber = true;
                SendRequestButton.IsEnabled = true;
            }

            else
            {
                ValidPhoneNumber = false;
                SendRequestButton.IsEnabled = false;
            }
        }

        private void CancelHelpRequestButton(object sender, RoutedEventArgs e)
        {
            GlobalObjects.DebugHelper.WriteDebugLog("CANCELING THE SUPPORT TICKET CREATION. AND SHOWING LAST OPEN VIEW");
            this.Visibility = Visibility.Hidden;
        }

        private void SubmitHelpRequestButton(object sender, RoutedEventArgs e)
        {
            /* Sample Help JSON
             * {
             *    Name: %HELP_REQUEST_DATA.Name%, 
             *    Phone: %HELP_REQUEST_DATA.Phone%, 
             *    Reason: %HELP_REQUEST_DATA.Reason%, 
             *    AdditionalInfo: %HELP_REQUEST_DATA.AdditionalInfo%
             * }
             */

            var newJsonObj = new JSONHelpObject
            {
                Name = UserInfoObject.NameFull,
                Reason = "OE Automated Scan Assistance Required",
                Phone = TechPhoneBox.Text,
                AditionalInfo = AditionalInfoTextBox.Text
            };

            string JSONConverted = JsonConvert.SerializeObject(newJsonObj);

            GlobalObjects.DebugHelper.WriteDebugLog("JSON HELP OBJECT MADE. OBJECT CONTAINS ALL NEEDED HELP INFO");
            GlobalObjects.DebugHelper.WriteDebugLog(JSONConverted);

            Task.Run(() =>
            {            
                // URL For Help Ticket: localhost:15000/DrewTechDataSvc/RequestHelp360/*JSON SHIT HERE*
                SendJSONHelpLink += JSONConverted;

                GlobalObjects.DebugHelper.WriteDebugLog("FULL LINK TO SEND UP: " + SendJSONHelpLink);
                GlobalObjects.DebugHelper.WriteDebugLog("LINK SENDING UP NOW. RESPONSE PROCESSING."); 

                try
                {
                    string RespFromSend = API_Invoker.DownloadString(SendJSONHelpLink);
                    GlobalObjects.DebugHelper.WriteDebugLog("RESPONSE: " + RespFromSend);
                } 
                catch (Exception ex)
                {
                    if (Debugger.IsAttached) { GlobalObjects.DebugHelper.WriteDebugLog("DONT WORRY THIS PROBS SHOULDN'T HAVE WORKED LOCALLY"); }
                    GlobalObjects.DebugHelper.WriteDebugLog("LINK WASNT ABLE TO BE SENT/CONNECTED.");
                    GlobalObjects.DebugHelper.WriteDebugLog(ex);
                }
            });

            Dispatcher.Invoke(() =>
            {
                GlobalObjects.DebugHelper.WriteDebugLog("RESET ALL VIEW COMPONENTS OK!");

                CloseWindowButton.Visibility = Visibility.Visible;
                UserInfoGrid.Visibility = Visibility.Hidden;
                ButtonGrid.IsEnabled = false;
                ThanksForSubmissionGrid.Visibility = Visibility.Visible;
            });

        }

        private void CloseSubmitTicketSentButtonClick(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                GlobalObjects.DebugHelper.WriteDebugLog("CLOSING DOWN THE SUBMIT HELP BUTTON VIEW NOW.");

                this.Visibility = Visibility.Hidden;

                UserInfoGrid.Visibility = Visibility.Visible;
                ThanksForSubmissionGrid.Visibility = Visibility.Hidden;
                CloseWindowButton.Visibility = Visibility.Hidden;

                ButtonGrid.IsEnabled = true;

                GlobalObjects.DebugHelper.WriteDebugLog("CLOSED OK");
            });
        }

        /// <summary>
        /// Resizes grid on input for the Text box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AditionalInfoTextBox_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            GlobalObjects.DebugHelper.WriteDebugLog("FOUND A KEYBOARD FOCUSED EVENT!");
            GlobalObjects.DebugHelper.WriteDebugLog("RESIZING GRID NOW");

            GlobalObjects.DebugHelper.WriteDebugLog("RESIZING GRID COL 0 TO 0PX");
            UserInfoGrid.ColumnDefinitions[0].Width = new GridLength(0, GridUnitType.Star);

            for (int Counter = 0; Counter < UserInfoGrid.RowDefinitions.Count; Counter++)
            {
                if (Counter >= 0 && Counter < 3)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("RESIZING GRID ROW: " + Counter.ToString() + " TO 0PX");
                    UserInfoGrid.RowDefinitions[Counter].Height = new GridLength(0, GridUnitType.Star);
                }
                if (Counter == 3)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("RESIZING GRID ROW: " + Counter.ToString() + " TO 2 STAR");
                    UserInfoGrid.RowDefinitions[Counter].Height = new GridLength(2, GridUnitType.Star);
                }
                if (Counter == 4)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("RESIZING GRID ROW: " + Counter.ToString() + " TO .5 STAR");
                    UserInfoGrid.RowDefinitions[Counter].Height = new GridLength(.5, GridUnitType.Star);
                }
            }
        }

        /// <summary>
        /// Resets grid to normal size on the screen for the user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AditionalInfoTextBox_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            GlobalObjects.DebugHelper.WriteDebugLog("FOUND A KEYBOARD LOST FOCUS EVENT!");
            GlobalObjects.DebugHelper.WriteDebugLog("RESETTING GRID SIZE TO DEFAULTNOW");

            GlobalObjects.DebugHelper.WriteDebugLog("RESIZING GRID COL 0 TO HALF WIDTH OF VIEW");
            UserInfoGrid.ColumnDefinitions[0].Width = new GridLength(.5, GridUnitType.Star);

            for (int Counter = 0; Counter < UserInfoGrid.RowDefinitions.Count; Counter++)
            {
                if (Counter >= 0 && Counter < 3)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("RESIZING GRID ROW: " + Counter.ToString() + " TO 1 STAR");
                    UserInfoGrid.RowDefinitions[Counter].Height = new GridLength(1, GridUnitType.Star);
                }
                if (Counter == 3)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("RESIZING GRID ROW: " + Counter.ToString() + " TO 2 STAR");
                    UserInfoGrid.RowDefinitions[Counter].Height = new GridLength(2, GridUnitType.Star);
                }
                if (Counter == 4)
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("RESIZING GRID ROW: " + Counter.ToString() + " TO 0 STAR");
                    UserInfoGrid.RowDefinitions[Counter].Height = new GridLength(0, GridUnitType.Star);
                }
            }
        }
    }
}
