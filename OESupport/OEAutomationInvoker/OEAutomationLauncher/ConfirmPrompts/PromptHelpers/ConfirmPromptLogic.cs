﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.ConfirmPrompts
{
    public class ConfirmPromptLogic
    {
        public static void KillScanAndWaj()
        {
            // GlobalObjects.MainWindowView.ConfirmCancelButtonView.Visibility = Visibility.Hidden;

            string OESWName = "";
            if (GlobalObjects.OEType == "TIS") { OESWName = "TechStream.exe"; }
            if (GlobalObjects.OEType == "GDS2") { OESWName = "javaw.exe"; }
            if (GlobalObjects.OEType == "FJDS") { OESWName = "TabMan*"; }
            if (GlobalObjects.OEType == "CP3") { OESWName = "CONSULT4.exe"; }
            if (GlobalObjects.OEType == "FDRS") { OESWName = "FDRS.exe"; }

            try
            {
                // NEED TO HOOK IN THE STOP RPA PROCESS FUNCTION HERE!!!
                string WajDir = @"C:\Program Files (x86)\OESupport\Launchers\WajFiles\";
                string CurrentWaj = "";

                try
                { 
                    CurrentWaj = File.ReadAllText(WajDir + "CURRENT_WAJ_FILE.txt");
                    GlobalObjects.DebugHelper.WriteDebugLog("OE TYPE WAS: " + GlobalObjects.OEType);
                    GlobalObjects.DebugHelper.WriteDebugLog("WAJ TO KILL: " + CurrentWaj);
                }
                catch 
                {
                    GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT READ IN WAJ FILE NAME TO BE KILLED");
                    GlobalObjects.DebugHelper.WriteDebugLog("STOPPING THIS PROCESS SINCE NO WAJ COULD BE FOUND TO KILL!");

                    return;
                }

                string CommandContents =
                    "\ntaskkill /F /T /IM " + OESWName +
                    "\ncd \"C:\\Program Files\\WinAutomation\"" +
                    "\nWinAutomationController.exe /stop \"" + CurrentWaj + "\"";

                if (Environment.MachineName.Contains("DRIVE")) { CommandContents += "\nTASKKILL /F /IM WinAutomation.UserAgent.exe"; }

                File.WriteAllText(@"C:\Program Files (x86)\OESupport\Launchers\KillerProc.bat", CommandContents);

                var KillerProcess = new Process();
                var StartOptions = new ProcessStartInfo();

                StartOptions.FileName = @"C:\Program Files (x86)\OESupport\Launchers\KillerProc.bat";
                StartOptions.WindowStyle = ProcessWindowStyle.Hidden;
                StartOptions.Arguments = " / min";

                KillerProcess.StartInfo = StartOptions;
                KillerProcess.Start();

                GlobalObjects.DebugHelper.WriteDebugLog("KILLING WAJ PROCESS: " + CurrentWaj);
                GlobalObjects.DebugHelper.WriteDebugLog("BAT FILE MADE AND RUNNING NOW");
            }

            catch (Exception ex)
            {
                GlobalObjects.DebugHelper.WriteDebugLog("KILLING WAJ PROCESS FAILED");
                GlobalObjects.DebugHelper.WriteDebugLog(ex);
            }

            // if (!Debugger.IsAttached) { Environment.Exit(0); }
        }

        public static void GatherAndUpload(bool RanOK)
        {
            // Get log files and cleanup.
            GlobalObjects.DebugHelper.WriteDebugLog("GETTING NEW FILES NOW", "EXIT");

            var FoundNewFiles = GlobalObjects.FileCollector.GetNewScanFiles();
            if (!FoundNewFiles) { GlobalObjects.DebugHelper.WriteDebugLog("FOUND NO DIFF IN FILES???"); }

            GlobalObjects.FileCollector.CollectAndPackNewFiles();
        }

        public static void CreateHelpSessionTicket()
        {

        }
    }
}
