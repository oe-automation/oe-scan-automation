﻿using Newtonsoft.Json;
using OEAutomationLauncher.OEOptionPicker.OEApplicationObjects.OETargeted;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Controls;
using OEAutomationLauncher.HTTPClientAPI;
using OEAutomationLauncher.OEOptionPicker.OEApplicationObjects;
using OEAutomationLauncher.AutomationLauncher.ConfirmPrompts;
using System;
using OEAutomationLauncher.AutomationLauncher.ObjectClasses;
using OEAutomationLauncher.AutomationLauncher.DebugHelpers;
using ICSharpCode.AvalonEdit;
using OEAutomationLauncher.AutomationLauncher.DebugHelpers.DebugViews;
using Microsoft.Win32;
using OEAutomationLauncher.AutomationLauncher.CustomOEViews.FJDS;
using OEAutomationLauncher.AutomationLauncher.CustomOEViews.TIS;
using OEAutomationLauncher.RequestVIN;
using System.Timers;
using OEAutomationLauncher.AutomationLauncher.AutomationTaskLists;
using OEAutomationLauncher.AutomationLauncher;
using OEAutomationLauncher.AutomationLauncher.CustomOEViews.FDRS;
using OEAutomationLauncher.AutomationLauncher.HTTPClientAPI;
using OEAutomationLauncher.LogicalHelpers;
using OEAutomationLauncher.DebugHelpers;
using OEAutomationLauncher.OEOptionPicker.OEOptionPickerViews;
using System.IO;

namespace OEAutomationLauncher
{
    public class GlobalObjects
    {
        // Is this a CDP4 or CRASH
        public static bool IsCDP4
        {
            get
            {
                string Key = @"HKEY_LOCAL_MACHINE\SYSTEM\DrewTechSerial";
                string Type = (string)Microsoft.Win32.Registry.GetValue(Key, "Imageid", "");
                if (Type == null)
                {
                    if (Debugger.IsAttached)
                    {
                        // GlobalObjects.DebugHelper.WriteDebugLog("NO REG KEY FOR A DREW ID WAS FOUND. MAKE SURE THIS EXISTS. FORCING CDP4 UI");
                        Type = "CDP4";
                    }
                    else { Type = ""; }
                }

                if (Type.ToUpper().Contains("CDP4")) { return true; }
                return false;
            }
        }

        // Type of OE SW to automate and the raw JSON of the vehicle being scanned.
        // PreOrPost is used to invoke the scan reporting service. Sets if we cleared codes or not.
        public static string OEType;
        public static string VehicleJSONInfo;
        public static string PreOrPost = "";

        public static StartupActions AppStart;
        public static UIScalingHelpers UIHelpers;
        public static bool CanResizeRelease = false;
        public static bool ForceOnTop = false;

        // Used to scan uploading and winautomation controling.
        public static string OEScanGUID;
        public static string CONFIG_ENVIRONMENT;
        public static WinAutomationArgs WinAutoArgs;

        // Used for the debugging and file collection setup. Included vehicle info since its a core function.
        public static bool ServerRunning = false;
        public static VehicleConfigInfo_Invoker ConfigInfo;
        public static DebuggingHelper_Invoker DebugHelper;
        public static LogColorizer Colorizer;
        public static PrepareFileGathering_Invoker FileCollector;
        public static FallBackVehicleConfigs FallBackConfigItems;
        public static int FallBackJSONIndex;

        // Textbox for debug logging. Also includes list for when this is undefined.
        public static bool AddedColorizer = false;
        public static TextEditor DebugInfoTextBox;
        public static List<string> TempTextToFill = new List<string>();

        // Main critical UI components and helpers. BaseTaskList makes the list of task items.
        public static MainWindow MainWindowView;
        public static MainWindowViewHelpers WindowHelpers;
        public static BaseTaskList BaseTasks;
        public static Grid TaskGrid;

        // Timer info used to close prompts auto if they are open for longer than the set time value.
        public static int TimeForTimeout = 60000;           // Timeout value for view to auto close (See API props for changing this)
        public static bool TimeoutApiSet = false;           // Sees if the API overwrote the timeout value or not.
        public static bool TimerWasStopped = false;         // Tells if the timer is stopped or not.
        public static Timer NoInputTimer;                   // Timer to track how long has passed
        public static DateTime TimerStartedTime;            // Time at which the elapsed timer was started.
        public static Type TypeOfLastView;                  // View Type for the object to close.
        public static AutoCloseTimerCalls ViewCloser;       // Closing object used to close the views out.
        public static bool IsOptionObject;                  // Sets if we're closing options or not. Types arent working for this...

        // These are the custom views/usercontrols used to show prompts.
        public static ContactIVS360Support ContactSupportGrid;              // NO AUTO CONFIRM SINCE THE USER HAS TO OPEN THIS.
        public static ConfirmCancelScan ConfirmCancelButtons;               // CLICK RETURN TO MAIN MENU.
        public static ScanCompleteCloseWindow ScanCompleteButtons;          // CLICK RETURN TO MAIN MENU.
        public static ClearCodesDialogue CodeClearingButtons;               // CLICK NO ON CLEAR OPTIONS. IF FAIL DONT RETRY 
        public static ScanCompleteCloseWindow ScanCompleteCloseButtons;     // AUTO CLICK RETRUN TO MAIN MENU
        public static FJDSEnterOdoAndRo FJDS_OdoAndROView;                  // SET ODO TO N/A AND RO TO AUTO
        public static FJDSNoComms FJDS_NoCommsView;                         // CANCEL SCAN/FAIL IT 
        public static TISRecallView TIS_RecallView;                         // PRESS CONFIRM AND CLOSE WINDOW
        public static FDRSKeyCycleView FDRS_KeyCycleView;                   // IF NO ENTRY PRESS OK AND CLOSE
        public static VINRequestUserControl VINEntryVIew;                   // IGNORING THIS. NO NEED FOR IT. BUT IT IS DEFINED.

        // Debug views for logger to TB and the API Invoker.
        public static ViewAndAPIDebugger ViewAndAPIDebugView;
        public static LoggingAndConfigDebugger LogAndConfigView;

        // Timer lists for timing process length. List of times to include and a stopwatch for full operation time.
        public static List<Stopwatch> AllProcTimers = new List<Stopwatch>();
        public static List<string> AllTimes = new List<string>();
        public static Stopwatch FullProcTimer = new Stopwatch();

        // Base OEAPP for Options config and instances for GDS2/TIS. These parse config files and make lists for 
        // the comboboxes.
        public static OEApplicationBase OEBase;
        public static TISOEApplication TISBase;
        public static GDS2OEApplication GDS2Base;

        // Info for TIS Recalls and scan status.
        public static bool ScanCompletePassed = true;
        public static bool ScanWasTerminated = false;
        
        public GlobalObjects(Grid MainGrid)
        {
            TaskGrid = MainGrid;
            SetupDebugInfo(SetupConfigInfo());
            SetupCONFIG_ENVIRONMENT();
            SetupGUIDForSession();
            FallBackConfigItems = new FallBackVehicleConfigs();
            ToggleDebugLogging_Invoker.ToggleLogging(true);
            WinAutoArgs = new WinAutomationArgs();
            RegistrySetupHelper.SetupAllRegKeys();

            DebugHelper.WriteDebugLog("GLOBALS OBJECT INITALIZED OK. READY TO LAUNCH THE WEBSERVER.", "PASS");
            DebugHelper.WriteDebugLog("SHOUTOUT TO THE DAY 1S WHO KEPT ME SANE BUILDING THIS SHIT");
            DebugHelper.WriteDebugLog(">BONNER.GIF HAS COMPILED OK!");
            DebugHelper.WriteDebugLog(">INSERT MEME USE CASE HERE: RE BEAR");
            DebugHelper.WriteDebugLog(">BUILDING DOUG.EXE...");
            DebugHelper.WriteDebugLog(">BILLYC0123 READY TO BURN");
            DebugHelper.WriteDebugLog(">JACK R - YOU KNOW WHAT YOU DID");
            DebugHelper.WriteDebugLog(">FLIPPING JAKES NAME UPSIDE DOWN NOW...");
            DebugHelper.WriteDebugLog(">LEARNING HOW TO DRIBBLE FROM AMMO");
        }

        public static void SetLoggerBoxContent(bool RefreshForce = false)
        {
            try
            {
                MainWindowView.Dispatcher.Invoke(() =>
                {
                    if (!Debugger.IsAttached && !RefreshForce) { return; }
                    if (DebugInfoTextBox.Text.Length == 0 || RefreshForce)
                    {
                        if (DebugHelper == null) { DebugHelper = new DebuggingHelper_Invoker(); }
                        foreach (var LineOfText in TempTextToFill)
                        {
                            DebugHelper.WriteDebugLog(LineOfText);
                            // DebugInfoTextBox.Text += LineOfText + "\n";
                        }

                        TempTextToFill.Clear();
                    }

                    // Setup Debug log colorizer.
                    if (!AddedColorizer && Debugger.IsAttached)
                    {
                        if (ConfigInfo != null)
                        {
                            if (Colorizer == null) { Colorizer = new LogColorizer(); }

                            DebugInfoTextBox.TextArea.TextView.LineTransformers.Add(new LogColorizerEdit(Colorizer));
                            AddedColorizer = true;
                        }
                    }
                });
            }

            catch { }
        }

        public static void SetupTimers()
        {
            if (BaseTasks == null)
            {
                OEType = FallBackVehicleConfigs.SelectedConfig.OEType;
                BaseTasks = new BaseTaskList(OEType);
            }

            // So this needs a null check for the init run. Otherwie shit bork hard.
            if (BaseTasks.OEType != OEType) { BaseTasks = new BaseTaskList(OEType); }

            if (AllProcTimers.Count != 0) { AllProcTimers.Clear(); }
            for (int count = 0; count < BaseTasks.AllTaskItems.Count; count++)
            {
                AllProcTimers.Add(new Stopwatch());
            }
        }

        public static void SetupAutoCloseTimer(Type CurrentViewType)
        {
            TypeOfLastView = CurrentViewType;

            bool isValidType = TypeOfLastView == typeof(TISOptionsView) || TypeOfLastView == typeof(GDS2OptionsView);
            if (IsOptionObject && !isValidType)
            {
                DebugHelper.WriteDebugLog("FOUND THE SPECIFIED CURRENT TYPE TO BE INCORRECT!");
                DebugHelper.WriteDebugLog("TYPE SHOULD BE OPTIONS BUT TYPE IS SEEN TO BE: " + TypeOfLastView.ToString());

                if (OEType == "TIS") { CurrentViewType = typeof(TISOptionsView); }
                if (OEType == "GDS2") { CurrentViewType = typeof(GDS2OptionsView); }

                DebugHelper.WriteDebugLog("SETUP NEW TYPE OF: " + CurrentViewType.ToString());
                TypeOfLastView = CurrentViewType;
            }

            DebugHelper.WriteDebugLog("WAITING FOR CLOSE SIGNAL NOW!");

            TimerWasStopped = false;
            ViewCloser = new AutoCloseTimerCalls(TypeOfLastView);

            // Modified to include more time for when the options trigger is set up.
            if (CONFIG_ENVIRONMENT == "DEVELOPMENT")
            {
                TimeForTimeout = 10000; 
                if (IsOptionObject) { TimeForTimeout = 30000; }
            }
           
            // REMOVED to allow use of the custom timeout time API endpoint.
            /* else 
            {
                if (IsOptionObject) { TimeForTimeout = 90000; }
                TimeForTimeout = 60000; 
            }*/

            DebugHelper.WriteDebugLog("CREATING NEW AUTO CLOSE TIMER NOW");
            DebugHelper.WriteDebugLog("NO INPUT DELAY TIME: " + TimeForTimeout + " MILISECONDS");
            DebugHelper.WriteDebugLog("TYPE OF OBJECT TO CLOSE: " + TypeOfLastView);

            TimerStartedTime = DateTime.Now;
            NoInputTimer = new Timer();

            NoInputTimer.Interval = TimeForTimeout;
            NoInputTimer.Elapsed += OnTimedEvent;

            NoInputTimer.AutoReset = true;
            NoInputTimer.Enabled = true;
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            DebugHelper.WriteDebugLog("TYPE OF LAST SEEN VIEW: " + TypeOfLastView.ToString());
            DebugHelper.WriteDebugLog("TIMER TIMED OUT AT: " + e.SignalTime);

            // RUN CUSTOM CLOSE OUT FUNC BASED ON THE TYPEOF THE CURRENT ITEM IN THE TYPEOF WINDOW. 
            // IF THERE IS NO TYPE OR ITS NULL JUST RETURN OUT

            // Get the type of the last opened View.
            if (TypeOfLastView == null)
            {
                DebugHelper.WriteDebugLog("LAST CONFIG TYPEOF WAS NULL!");
                return;
            }

            MainWindowView.Dispatcher.Invoke(() =>
            {
                DebugHelper.WriteDebugLog("EXECUTING TIMEOUT METHOD NOW!");
                DebugHelper.WriteDebugLog("METHOD: " + ViewCloser.MethodToExecute);
                DebugHelper.WriteDebugLog("PARAMS: " + ViewCloser.MethodParamsString);

                ViewCloser.ClickButtonToClose();

                ViewCloser = new AutoCloseTimerCalls();
                DebugHelper.WriteDebugLog("RESET THE VIEW CLOSER PARAMS AND STRINGS OK!");
            });
        }

        public static void StopCloseTimer()
        {
            // Make sure it was stopped once and only once.
            if (TimerWasStopped) { return; }

            NoInputTimer.Stop();
            ViewCloser = new AutoCloseTimerCalls();

            string TimePassed = (DateTime.Now - TimerStartedTime).ToString("c");
            DebugHelper.WriteDebugLog("STOPPED TIMER AT A TIME OF: " + TimePassed);

            TimerWasStopped = true;
        }

        public static void SetupDebugInfo(string RawJSON)
        {
            DebugHelper = new DebuggingHelper_Invoker();
            Colorizer = new LogColorizer();

            DebugHelper.WriteDebugLog("DEBUG FILE STARTED NOW", "INIT");

            DebugHelper.WriteDebugLog("CONFIGURED VEHICLE JSON VALUES INTO A NEW JSON CONVERTED OBJECT");
            DebugHelper.WriteDebugLog("VEHICLE CONFIG STRING VALUE: " + RawJSON);

            DebugHelper.WriteDebugLog("-------- VEHICLE CONFIG INFO --------");
            DebugHelper.WriteDebugLog("VEHICLE CONFIG VIN:   " + ConfigInfo.VIN);
            DebugHelper.WriteDebugLog("VEHICLE CONFIG YEAR:  " + ConfigInfo.Year);
            DebugHelper.WriteDebugLog("VEHICLE CONFIG MAKE:  " + ConfigInfo.Make);
            DebugHelper.WriteDebugLog("VEHICLE CONFIG MODEL: " + ConfigInfo.Model);
            DebugHelper.WriteDebugLog("-------- VEHICLE CONFIG INFO --------");

            if (!Directory.Exists(@"C:\Program Files (x86)\OESupport\VehicleOptions\" + ConfigInfo.Make.ToUpper()))
            {
                Directory.CreateDirectory(@"C:\Program Files (x86)\OESupport\VehicleOptions\" + ConfigInfo.Make.ToUpper());
            }
        }

        private void SetupCONFIG_ENVIRONMENT()
        {
            string ConfigKey = @"SOFTWARE\Drew Technologies, Inc\OESupportApps\";
            string CurrentConfig = (string)Registry.GetValue("HKEY_CURRENT_USER\\" + ConfigKey, "CONFIG_ENVIRONMENT", "NOT_SET");

            DebugHelper.WriteDebugLog("FOUND THE VALUE FOR THE CONFIG_ENVIRONMENT REG TO BE: " + CurrentConfig);
            if (CurrentConfig == "NOT_SET" || CurrentConfig == null)
            {
                DebugHelper.WriteDebugLog("REGISTRY FOR THE ENVIRONMENT TO TARGET WAS NOT FOUND! DEFAULTING TO A PRODUCTION RELEASE");
                CONFIG_ENVIRONMENT = "PRODUCTION";

                Registry.SetValue("HKEY_CURRENT_USER\\" + ConfigKey, "CONFIG_ENVIRONMENT", CONFIG_ENVIRONMENT);
                DebugHelper.WriteDebugLog("SET THE REG KEY FOR CURRENT ENVIRONMENT TO PRODUCTION.");
                return;
            }

            CONFIG_ENVIRONMENT = CurrentConfig;
            DebugHelper.WriteDebugLog("PULLED A REG VALUE FOR CURRENT CONFIG OK! THIS WILL MOSTLY ONLY AFFECT THE OESUP UPDATER");
        }

        private void SetupGUIDForSession()
        {
            // CONSIDER SWAPPING THIS TO PULL A GUID FROM THE BL AND MAKE OUR SESSIONS AT THE START OF SCANNING!
            // THIS MAY MAKE IT EASIER FOR US TO TRACK SESSIONS IN THE INTERNAL WEB AND PAIR THEM WITH DEBUG LOGS 
            // INSTEAD OF MAKING A SEPERATE GUID FOR THE SESSION AND FOR MY INTERNAL TESTING.

            // TO DO THIS, MAKE A WEBCALL TO THE BL AND PULL DOWN THE RESPONSE TO CREATESESSIONANDSCANREPORT()
            // THE RESPONSE CONTAINS OUR GUID AND THE LINK USED FOR THE REPORT SERVICE. 

            DebugHelper.WriteDebugLog("SETTING UP GUID FOR THIS OE SCAN SESSION NOW");
            DebugHelper.WriteDebugLog("GETITNG GUID NOW");

            string SessionGUID_1 = Guid.NewGuid().ToString();
            string SessionGUID_2 = Guid.NewGuid().ToString();
            DebugHelper.WriteDebugLog("TEMP GUID 1: " + SessionGUID_1);
            DebugHelper.WriteDebugLog("TEMP GUID 2: " + SessionGUID_2);

            string CombinedGUIDResult = "";
            for (int Counter = 0; Counter < SessionGUID_1.Length; Counter++)
            {
                if (Counter % 2 == 0) { CombinedGUIDResult += SessionGUID_1[Counter]; }
                else { CombinedGUIDResult += SessionGUID_2[Counter]; }
            }

            OEScanGUID = CombinedGUIDResult.ToUpper();
            DebugHelper.WriteDebugLog("GUID OUPUT VAUE IS: " + OEScanGUID);
        }

        public static string SetupConfigInfo()
        {
            try 
            { 
                VehicleJSONInfo = new WebClient().DownloadString("http://localhost:15000/DrewTechDataSvc/VehicleInfo");
                ConfigInfo = JsonConvert.DeserializeObject<VehicleConfigInfo_Invoker>(VehicleJSONInfo);
            }

            catch 
            {
                if (Debugger.IsAttached)
                {
                    var NewJSON = FallBackVehicleConfigs.SelectedConfig.JSON;
                    if (NewJSON.Length > 0) 
                    { 
                        VehicleJSONInfo = NewJSON;
                        ConfigInfo = JsonConvert.DeserializeObject<VehicleConfigInfo_Invoker>(VehicleJSONInfo);
                    }
                }
            }
            return VehicleJSONInfo;
        }

        public static void SetupWebServerAPI()
        {
            if (!ServerRunning)
            {
                DebugHelper.WriteDebugLog("STARTING API INTERFACE NOW", "START");
                Task.Run(() => ProgressAPITasker.HttpListener(ProgressAPIEndpoints.AllEndPoints));

                ServerRunning = true;
            }
        }
    }
}
