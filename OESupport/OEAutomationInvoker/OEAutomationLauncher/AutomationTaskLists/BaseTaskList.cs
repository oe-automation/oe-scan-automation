﻿using OEAutomationLauncher.AutomationLauncher.TaskListHelpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace OEAutomationLauncher.AutomationLauncher.AutomationTaskLists
{
    public class TaskTimeAndDesPair
    {
        public string Description;
        public string Time;
    }

    public class BaseTaskList
    {
        private AWSHelpers TaskFilePuller;

        public string InputTaskList;
        public string OEType;

        public List<TaskTimeAndDesPair> TaskDescriptions = new List<TaskTimeAndDesPair>();
        public List<TaskListItem> AllTaskItems = new List<TaskListItem>();

        public BaseTaskList(string Type)
        {
            OEType = Type;
            GlobalObjects.OEType = Type;

            GlobalObjects.DebugHelper.WriteDebugLog("INITALIZING AN AWS TASK FILE RETREIVER.");
            TaskFilePuller = new AWSHelpers();
            TaskFilePuller.GetOETaskList(OEType);

            GlobalObjects.DebugHelper.WriteDebugLog("EXTRACTING TASK LIST INFO ITEMS AND MAKING LISTS OF DESCRIPTIONS AND VIEW ITEMS");
            CreateLists();

            GlobalObjects.DebugHelper.WriteDebugLog("RESIZING MAIN GRID TO FIT ALL ITEMS INSIDE OF IT.");
            SetupMainWindowGrid();

            GlobalObjects.DebugHelper.WriteDebugLog("APPENDING TASK ITEMS INTO THE MAIN GRID NOW");
            SetTaskItems();
        }

        public void CreateLists()
        {
            string[] AllTaskObjects = File.ReadAllLines(TaskFilePuller.TaskListFile);
            GlobalObjects.DebugHelper.WriteDebugLog("READ IN TASK FILE LINES OK.");

            foreach (var TaskItem in AllTaskObjects)
            {
                string IndexOfTask = TaskItem.Split('|')[0].Trim();
                string TaskDescription = TaskItem.Split('|')[1].Trim();

                TaskDescriptions.Add(new TaskTimeAndDesPair() { Description = TaskDescription, Time = "" });
                GlobalObjects.DebugHelper.WriteDebugLog("ADDED TASK: " + TaskDescription + " (" + IndexOfTask + " TASKS SO FAR)");
            }

            GlobalObjects.DebugHelper.WriteDebugLog("ADDED " + TaskDescriptions.Count + " TASKS TO THE TASK VIEW AND SETUP TASK OBJECT DESCRIPTIONS");
        }
        public void SetupMainWindowGrid()
        {
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                // Clear out old Grid Size defs.
                GlobalObjects.TaskGrid.RowDefinitions.Clear();

                // Set the height based on CDP4/Drive. 
                int HeightSet = 50;
                if (!GlobalObjects.IsCDP4) { HeightSet = 65; }

                // Get the count of items here and set the grid height accordingly.
                int OptionsCount = TaskDescriptions.Count;
                GlobalObjects.TaskGrid.Height = OptionsCount * HeightSet;

                // Count off options and make grid row items here for it.
                for (int counter = TaskDescriptions.Count; counter >= 0; counter--)
                {
                    // Make a row definition here and set it to 1*
                    RowDefinition nextRow = new RowDefinition();
                    nextRow.Height = new GridLength(HeightSet, GridUnitType.Pixel);

                    // Set the option row into the grid row definitions.
                    GlobalObjects.TaskGrid.RowDefinitions.Add(nextRow);
                }
                GlobalObjects.DebugHelper.WriteDebugLog("CONFIGURED GRID HEIGHT OK AND SETUP LIST FOR OBJECT CREATION");

            });
        }
        public void SetTaskItems()
        {
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                GlobalObjects.TaskGrid.Children.Clear();

                int counter = 1;
                foreach (var TaskItem in TaskDescriptions)
                {
                    // Store the options name and options values in temp variables for now.
                    string TaskIndex = counter.ToString();
                    string TaskDescription = TaskItem.Description;
                    string ElapsedTime = TaskItem.Time;

                    // Make a new TaskListView Item and add it to the list of all tasks.
                    // Check if the item has been modified to include elapsed time or not.
                    var NextTask = new TaskListItem(TaskIndex, TaskDescription, ElapsedTime);
                    AllTaskItems.Add(NextTask);

                    // Set the Grid.Row prop of this item so they dont stack on each other.
                    NextTask.SetValue(Grid.RowProperty, counter - 1);

                    // Add the textblock and combobox to the grid now.
                    GlobalObjects.TaskGrid.Children.Add(NextTask);
                    GlobalObjects.TaskGrid.UpdateLayout();

                    // Increase the row counter here.
                    counter++;
                }
            });
            GlobalObjects.DebugHelper.WriteDebugLog("ADDED ALL CONFIG TASKS OK!. THIS SHOULD NOW SHOW US THE TASK ITEMS FOR THIS OE RUN");
        }
        public void UpdateTaskEntries()
        {
            GlobalObjects.TaskGrid.Children.Clear();

            int counter = 0;
            foreach (var NextTask in AllTaskItems)
            {
                // If Done
                // if (NextTask.CommandCompleted)
                // {
                //     NextTask.Visibility = Visibility.Collapsed;
                //     continue;
                // }

                // Set the Grid.Row prop of this item so they dont stack on each other.
                NextTask.SetValue(Grid.RowProperty, counter);

                // Add the textblock and combobox to the grid now.
                GlobalObjects.TaskGrid.Children.Add(NextTask);
                GlobalObjects.TaskGrid.UpdateLayout();

                counter++;
            }
        }


        public TaskListItem UpdateTaskStatus(int IndexOfTask, string TimeElapsed = "", bool SetRunning = false)
        {
            string TimeFormatted = TimeElapsed;
            if (TimeFormatted.Contains("_") && TimeFormatted.Contains("-"))
            {
                TimeFormatted = "Time Elapsed: " + TimeElapsed
                    .Replace("_", ":")
                    .Replace("-", ".");
            }
            else { TimeFormatted = ""; }

            if (SetRunning) { TimeFormatted = "Running..."; }

            var TaskViewItem = AllTaskItems[IndexOfTask];
            var TimeAndDescription = TaskDescriptions[IndexOfTask];

            if (SetRunning) { TaskViewItem.CommandCompleted = false; }

            TaskViewItem.CommandCompleted = true;
            TaskViewItem.CompleteCheckBox.IsChecked = true;
            TaskViewItem.CommandTimeElapsed = TimeFormatted;
            TaskViewItem.ProgBar.Value = TaskViewItem.ProgBar.Maximum;
            TaskViewItem.ProgBarTextBlock.Text = "100%";

            TimeAndDescription.Description = TaskViewItem.CommandDescription;
            TimeAndDescription.Time = TimeFormatted;

            AllTaskItems[IndexOfTask] = TaskViewItem;
            TaskDescriptions[IndexOfTask] = TimeAndDescription;

            UpdateTaskEntries();

            return TaskViewItem;
        }


        public TaskListItem ProgressBarInit(int IndexOfTask, int MaxTicks)
        {
            var TaskViewItem = AllTaskItems[IndexOfTask];
            var TimeAndDescription = TaskDescriptions[IndexOfTask];

            TaskViewItem.CommandCompleted = false;
            TaskViewItem.CompleteCheckBox.IsChecked = TaskViewItem.CommandCompleted;
            TaskViewItem.ProgBar.Value = 0;
            TaskViewItem.ProgBarGrid.Visibility = Visibility.Visible;
            TaskViewItem.ProgBar.Maximum = MaxTicks;
            TaskViewItem.ProgBar.IsIndeterminate = true;
            TaskViewItem.ProgBarTextBlock.Text = "Waiting To Execute";

            TimeAndDescription.Time = TaskViewItem.CommandTimeElapsed;
            TimeAndDescription.Description = TaskViewItem.CommandDescription;

            AllTaskItems[IndexOfTask] = TaskViewItem;
            TaskDescriptions[IndexOfTask] = TimeAndDescription;

            return TaskViewItem;
        }
        public TaskListItem ProgressBarTick(int IndexOfTask, int NumberOfTicks)
        {
            // Index >= 1 force on top. So 0 lets the spinner pop up then at one or more, dont.
            // This might also just make it so that the spinner flashes up which isnt ideal but eh.
            if (IndexOfTask >= 1) { GlobalObjects.ForceOnTop = true; }

            var TaskViewItem = AllTaskItems[IndexOfTask];
            var TimeAndDescription = TaskDescriptions[IndexOfTask];

            TaskViewItem.CommandCompleted = false;
            TaskViewItem.CompleteCheckBox.IsChecked = TaskViewItem.CommandCompleted;
            TaskViewItem.ProgBarGrid.Visibility = Visibility.Visible;
            TaskViewItem.ProgBar.Value = NumberOfTicks;
            TaskViewItem.ProgBar.IsIndeterminate = false;
            TaskViewItem.ProgBarTextBlock.Text = (TaskViewItem.ProgBar.Value / TaskViewItem.ProgBar.Maximum * 100).ToString("F0") + "%";

            if (TaskViewItem.ProgBarTextBlock.Text.Contains("100") || NumberOfTicks == (int)TaskViewItem.ProgBar.Maximum)
            {
                TaskViewItem.ProgBar.Value = TaskViewItem.ProgBar.Maximum;
                TaskViewItem.CommandCompleted = true;
                TaskViewItem.ProgBarTextBlock.Text = "100%";
                TaskViewItem.CompleteCheckBox.IsChecked = true;
            }

            TimeAndDescription.Time = TaskViewItem.CommandTimeElapsed;
            TimeAndDescription.Description = TaskViewItem.CommandDescription;

            AllTaskItems[IndexOfTask] = TaskViewItem;
            TaskDescriptions[IndexOfTask] = TimeAndDescription;

            return TaskViewItem;
        }
        public TaskListItem ProgressBarLooping(int IndexOfTask, int CurrentLoop, int TotalLoops)
        {
            var TaskViewItem = AllTaskItems[IndexOfTask];
            var TimeAndDescription = TaskDescriptions[IndexOfTask];

            TaskViewItem.CommandCompleted = false;
            TaskViewItem.CompleteCheckBox.IsChecked = TaskViewItem.CommandCompleted;
            TaskViewItem.ProgBarGrid.Visibility = Visibility.Visible;
            TaskViewItem.ProgBar.IsIndeterminate = false;
            TaskViewItem.ProgBar.Value = CurrentLoop;
            TaskViewItem.ProgBar.Maximum = TotalLoops;
            TaskViewItem.ProgBarTextBlock.Text = "Step " + CurrentLoop.ToString() + " of " + TotalLoops.ToString();

            TimeAndDescription.Time = TaskViewItem.CommandTimeElapsed;
            TimeAndDescription.Description = TaskViewItem.CommandDescription;

            AllTaskItems[IndexOfTask] = TaskViewItem;
            TaskDescriptions[IndexOfTask] = TimeAndDescription;

            return TaskViewItem;
        }
        public TaskListItem SetCustomMessage(int IndexOfTask, string MessageToSet, bool Endless)
        {
            var TaskViewItem = AllTaskItems[IndexOfTask];
            var TimeAndDescription = TaskDescriptions[IndexOfTask];

            TaskViewItem.ProgBarGrid.Visibility = Visibility.Visible;
            TaskViewItem.ProgBar.IsIndeterminate = Endless;

            // Check to see if we have a custom pct input value.
            if (MessageToSet.Contains("_PCT")) { MessageToSet = MessageToSet.Split('_')[0] + "%)"; }
            TaskViewItem.ProgBarTextBlock.Text = MessageToSet;

            // Added to show full progress bar items here.
            if (Endless == false) { TaskViewItem.ProgBar.Value = 100.00; }

            AllTaskItems[IndexOfTask] = TaskViewItem;
            TaskDescriptions[IndexOfTask] = TimeAndDescription;

            return TaskViewItem;
        }
        public TaskListItem HideProgressBar(int IndexOfTask)
        {
            var TaskViewItem = AllTaskItems[IndexOfTask];
            var TimeAndDescription = TaskDescriptions[IndexOfTask];

            TaskViewItem.ProgBarGrid.Visibility = Visibility.Hidden;
            TaskViewItem.ProgBar.Value = 0;
            TaskViewItem.ProgBar.IsIndeterminate = true;
            TaskViewItem.ProgBarTextBlock.Text = ((double)(TaskViewItem.ProgBar.Value / TaskViewItem.ProgBar.Maximum) * 100.00).ToString("F0") + "%";

            TimeAndDescription.Time = TaskViewItem.CommandTimeElapsed;
            TimeAndDescription.Description = TaskViewItem.CommandDescription;

            AllTaskItems[IndexOfTask] = TaskViewItem;
            TaskDescriptions[IndexOfTask] = TimeAndDescription;

            return TaskViewItem;
        }
    }
}
