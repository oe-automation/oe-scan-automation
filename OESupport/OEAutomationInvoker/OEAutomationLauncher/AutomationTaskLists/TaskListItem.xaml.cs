﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OEAutomationLauncher.AutomationLauncher.AutomationTaskLists
{
    /// <summary>
    /// Interaction logic for TaskListItem.xaml
    /// </summary>
    public partial class TaskListItem : UserControl
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }

        public string CommandIndex { get; set; }
        public bool CommandCompleted { get; set; }
        public string CommandDescription { get; set; }
        public string CommandTimeElapsed { get; set; }
        public string ProgBarValue { get; set; }

        public TaskListItem(string Index, string Description, string TimeElapsed = "")
        {
            InitializeComponent();
            DataContext = this;

            this.CommandIndex = Index;
            this.CommandDescription = Description;
            this.CommandCompleted = false;
            this.CommandTimeElapsed = TimeElapsed;
            this.TimeBlock.Text = TimeElapsed;

            if (ProgBarTextBlock.Text.Contains("100")) 
            {
                ProgBarTextBlock.FontWeight = FontWeights.DemiBold;
                this.CommandCompleted = true;
                this.CompleteCheckBox.IsChecked = true;
                this.ProgBar.Value = this.ProgBar.Maximum;
                this.ProgBar.Foreground = Brushes.DarkGreen;
            }

            if (TimeElapsed == "Running...") { this.CommandCompleted = false; }
            if (TimeElapsed != "") { CommandCompleted = true; }
        }
    }
}
