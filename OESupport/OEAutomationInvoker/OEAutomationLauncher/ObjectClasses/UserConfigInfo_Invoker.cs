﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.ObjectClasses
{
    public class UserConfigInfo_Invoker
    {
        public string NameFirst { get; set; }
        public string NameLast { get; set; }
        public string NameFull { get; set; }
        public string NameFullNoSpaces { get; set; }
        public string Image { get; set; }
        public string IsNewUser { get; set; }
        public string IsLoaded { get; set; }
        public string IsLoggedIn { get; set; }
        public string Email { get; set; }
        public string AuthToken { get; set; }
        public string Country { get; set; }
    }
}
