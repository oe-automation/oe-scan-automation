﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.ObjectClasses
{
    public class DriveCrashServiceParams
    {
        public string VIN_NUMBER { get; set; }
        public string OEM_NAME { get; set; }
        public string GUID_OR_SESSION { get; set; }
        public string PRE_OR_POST { get; set; }

        public DriveCrashServiceParams()
        {
            if (GlobalObjects.ConfigInfo == null) { GlobalObjects.SetupConfigInfo(); }

            VIN_NUMBER = GlobalObjects.ConfigInfo.VIN;
            OEM_NAME = GlobalObjects.ConfigInfo.Make;

            if (GlobalObjects.PreOrPost != "") { PRE_OR_POST = GlobalObjects.PreOrPost; }
            else { PRE_OR_POST = "PRE"; }

            try { GUID_OR_SESSION = new WebClient().DownloadString("http://localhost:15000/DrewTechDataSvc/SessionID").Trim(); }
            catch 
            {
                GUID_OR_SESSION = "00000000-0000-0000-0000-000000000000";
                GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT PULL DOWN A GOOD GUID");
            }
        }
    }
}
