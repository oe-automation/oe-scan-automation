﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.ObjectClasses
{
    public class WinAutomationArgs
    {
        public string OEType { get; set; }

        #region WinAuto Debug File
        private string _winAutoDebugFile;
        public string WinAutoDebugFile 
        {
            get { return _winAutoDebugFile; }
            set
            {
                if (!value.Contains(Path.DirectorySeparatorChar)) { _winAutoDebugFile = value; }
                else
                {
                    string[] newFile = value.Split(Path.DirectorySeparatorChar);
                    foreach (var part in newFile)
                    {
                        if (!part.Contains("WinAutomation")) { continue; }
                        else 
                        { 
                            _winAutoDebugFile = part;
                            break;
                        }
                    }
                }
            }
        }
        #endregion

        public string TimeScanStarted { get; set; }

        #region Needs Help Value
        private string _needsHelpValue;
        public string NeedsHelpValue 
        {
            get { return _needsHelpValue; }
            set
            {
                var upVal = value.ToUpper();
                if (upVal != "TRUE" && upVal != "FALSE")
                {
                    _needsHelpValue = "";
                    return;
                }

                else { _needsHelpValue = value.ToUpper(); }
            }
        }
        #endregion

        public string CommandIndexValue { get; set; }

        public string StringOfException = "";
        public string LastException = "";
        public string RegionSub1 = "";
        public string RegionSub2 = "";

        public WinAutomationArgs()
        {
            OEType = GlobalObjects.OEType;
            NeedsHelpValue = "FALSE";
            TimeScanStarted = "NOT_SET";
            CommandIndexValue = 0.ToString();
        }
    }
}
