﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.ObjectClasses
{
    public class VehicleConfigInfo_Invoker
    {
        public string VIN { get; set; }
        public string Year { get; set; }
        public string OEM { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Engine { get; set; }
        public string Error { get; set; }
        public string Voltage { get; set; }
        public string Trim { get; set; }
        public string[] AdasModules { get; set; }
        public string IsAdas { get; set; }
        public string IsNotAdas { get; set; }
        public string Odometer { get; set; }

        public List<string> ReturnStringOfBaseInfo()
        {
            return new List<string>()
            {
                Year, Make.ToUpper(), Model.ToUpper()
            };
        }
    }
}
