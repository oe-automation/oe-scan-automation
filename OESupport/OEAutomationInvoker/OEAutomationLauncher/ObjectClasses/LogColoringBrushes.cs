﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace OEAutomationLauncher.ObjectClasses
{
    public class LogColoringBrushes
    {
        public string InfoType;
        public string CallingType;

        public Tuple<Brush, Brush> TimeBrushes;
        public Tuple<Brush, Brush> InfoBrushes;
        public Tuple<Brush, Brush> CallingBrushes;
        public Tuple<Brush, Brush> LogStringBrushes;

        public Tuple<string, string> TimeStrings;
        public Tuple<string, string> InfoStrings;
        public Tuple<string, string> CallingStrings;
        public Tuple<string, string> LogLineStrings;

        public LogColoringBrushes(string InfoType, string CallingType)
        {
            this.InfoType = InfoType;
            this.CallingType = CallingType;
        }
    }
}
