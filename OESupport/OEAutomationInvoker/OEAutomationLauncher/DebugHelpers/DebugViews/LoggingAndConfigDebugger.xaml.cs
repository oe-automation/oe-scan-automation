﻿using Newtonsoft.Json;
using OEAutomationLauncher.AutomationLauncher.ObjectClasses;
using OEAutomationLauncher.AutomationLauncher.TaskListHelpers;
using OEAutomationLauncher.DriveCrashInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OEAutomationLauncher.AutomationLauncher.DebugHelpers.DebugViews
{
    /// <summary>
    /// Interaction logic for LoggingAndConfigDebugger.xaml
    /// </summary>
    public partial class LoggingAndConfigDebugger : UserControl
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }

        public LoggingAndConfigDebugger()
        {
            DataContext = this;
            InitializeComponent();

            if (GlobalObjects.IsCDP4) { SetCDP4OrDriveTool.Content = "Set DRVIE"; }
            if (!GlobalObjects.IsCDP4) { SetCDP4OrDriveTool.Content = "Set CDP4"; }
        }

        /// <summary>
        /// DEBUG FUNCTION Used to set a vehicle config info value.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ConfirmNewVehicleInfoClick(object sender, RoutedEventArgs e)
        {
            var CurrentItem = VehicleConfigNameCombobox.SelectedIndex;
            FallBackVehicleConfigs.SelectedConfig = FallBackVehicleConfigs.AllConfigItems[CurrentItem];

            Button SendBtn = (Button)sender;

            SendBtn.IsEnabled = false;
            SendBtn.Content = "Setting...";
            VehicleConfigNameCombobox.IsEnabled = false;
            LoadingConfigViewGrid.Visibility = Visibility.Visible;
            SetCDP4OrDriveTool.IsEnabled = false;
            RefreshVehicleJSON.IsEnabled = false;

            await Task.Run(() => { RunGlobalsSetup(); });
        }

        /// <summary>
        /// DEBUG FUNCTION Used to set global objects up.
        /// </summary>
        private async void RunGlobalsSetup()
        {
            _ = new GlobalObjects(GlobalObjects.MainWindowView.TaskGrid);
            GlobalObjects.SetupWebServerAPI();

            GlobalObjects.OEType = FallBackVehicleConfigs.SelectedConfig.OEType;
            await Task.Run(() =>
            {
                GlobalObjects.AppStart.SetConfigTextBoxValues();
                GlobalObjects.AppStart.MiscLogicInit();
            });

            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                // GlobalObjects.MainWindowView.DebugViewAndAPIFunctionsView.ResetTaskItemsButton(null, null);
                if (!GlobalObjects.IsCDP4)
                {
                    // NEEDS TO BE DISPATHER INVOKED FUCKWAD. WHEN YOU GONNA LEARN THIS SHIT YOU BEEN PROGRAMMING FOR YEARS NOW.
                    MainGridLayout.RowDefinitions[0].Height = new GridLength(.175, GridUnitType.Star);
                }

                GlobalObjects.MainWindowView.ResizeMode = ResizeMode.CanResize;
                // SetAndToggleTypeGrid.ColumnDefinitions[0].Width = new GridLength(3, GridUnitType.Star);
                // ToggleAndSetButtons.ColumnDefinitions[0].Width = new GridLength(0, GridUnitType.Star);
            });

            Dispatcher.Invoke(() =>
            {
                LoadingConfigViewGrid.Visibility = Visibility.Hidden;
                RefreshVehicleJSON.IsEnabled = true;
                SetVehicleButton.IsEnabled = true;
                SetCDP4OrDriveTool.IsEnabled = false;
                SetVehicleButton.Content = "Set Vehicle";

                VehicleConfigNameCombobox.IsEnabled = true;
                if (GlobalObjects.IsCDP4) { VehicleConfigNameCombobox.Height = 50.0; }
                if (!GlobalObjects.IsCDP4) { VehicleConfigNameCombobox.Height = 60.0; }

            });
        }

        private void ToggleCDP4orDrive(object sender, RoutedEventArgs e)
        {
            var SendBtn = (Button)sender;

            string Key = @"HKEY_LOCAL_MACHINE\SYSTEM\DrewTechSerial";
            string CurrentValue = (string)Microsoft.Win32.Registry.GetValue(Key, "Imageid", "");

            // Null check on currentvalue. 
            // Changed this stupid thing to isnullorwhitespace. 
            if (string.IsNullOrEmpty(CurrentValue))
            {
                try { GlobalObjects.DebugHelper.WriteDebugLog("NO REG KEY FOUND!! FORCING CDP4"); }
                catch { GlobalObjects.DebugInfoTextBox.Text += "NO REG KEY FOUND. DEBUGGER NULL. WHAT A FUCKIN MESS THIS IS ISNT IT?\n"; }

                SendBtn.Content = "NO KEY!";
                SendBtn.IsEnabled = false;

                GlobalObjects.SetLoggerBoxContent(true);

                return;
            }

            if (CurrentValue.Contains("CRH2"))
            {
                Microsoft.Win32.Registry.SetValue(Key, "Imageid", "CDP4");

                string NewSetValue = (string)Microsoft.Win32.Registry.GetValue(Key, "Imageid", "");
                if (NewSetValue == null) { NewSetValue = ""; }

                if (NewSetValue.Contains("CDP4")) { GlobalObjects.TempTextToFill.Add("TOGGLED DEVICE TYPE TO CDP4 OK!"); }
                else { GlobalObjects.TempTextToFill.Add("COULD NOT TOGGLE TO CDP4"); }

                GlobalObjects.TempTextToFill.Add("ISCDP4 BOOLEAN IS NOW: " + GlobalObjects.IsCDP4.ToString());
                SendBtn.Content = "Set DRIVE";

            }

            else if (CurrentValue.Contains("CDP4"))
            {
                Microsoft.Win32.Registry.SetValue(Key, "Imageid", "CRH2");

                string NewSetValue = (string)Microsoft.Win32.Registry.GetValue(Key, "Imageid", "");
                if (NewSetValue == null) { NewSetValue = ""; }

                if (NewSetValue.Contains("CRH2")) { GlobalObjects.TempTextToFill.Add("TOGGLED DEVICE TYPE TO DRIVE OK!"); }
                else { GlobalObjects.TempTextToFill.Add("COULD NOT TOGGLE TO DRIVE"); }

                GlobalObjects.TempTextToFill.Add("ISCDP4 BOOLEAN IS NOW: " + GlobalObjects.IsCDP4.ToString());
                SendBtn.Content = "Set CPD4";
            }

            GlobalObjects.SetLoggerBoxContent(true);

            // Reboot the app here.
            if (!Debugger.IsAttached)
            {
                // Set logger box info. 
                GlobalObjects.TempTextToFill.Add("WAITING 5 SECONDS AND REBOOTING THE APP");
                GlobalObjects.SetLoggerBoxContent(true);

                System.Windows.Forms.Application.Restart();
                Application.Current.Shutdown();
            }
        }

        private async void PullS3BucketJSON(object sender, RoutedEventArgs e)
        {
            // if (GlobalObjects.ConfigInfo == null) { GlobalObjects.SetupConfigInfo(); }

            PullingVehicleJSONLoading.Visibility = Visibility.Visible;
            RefreshVehicleJSON.IsEnabled = false;
            SetVehicleButton.IsEnabled = false;
            SetCDP4OrDriveTool.IsEnabled = false;
            VehicleConfigNameCombobox.IsEnabled = false;

            GlobalObjects.TempTextToFill.Add("GETTING NEW JSON INFO FOR VEHICLES IN BUCKET NOW");

            var AWSHelper = new AWSHelpers();
            var FoldersFromS3Bucket = AWSHelper.ReturnListOfFoldersInBucket();

            RefreshingTextBlock.Text = "Found A Total Of " + FoldersFromS3Bucket.Count() + " Debug Folders";
            GlobalObjects.TempTextToFill.Add("FOUND A TOTAL OF " + FoldersFromS3Bucket.Count() + " FOLDERS TO SCAN THROUGH");
            GlobalObjects.SetLoggerBoxContent(true);

            RefreshingTextBlock.Text = "Searching for JSON Information Now...";

            List<FallBackConfigItem> ConfigsToAdd = new List<FallBackConfigItem>();
            await Task.Run(() =>
            {
                Parallel.ForEach(FoldersFromS3Bucket, (FolderObj) =>
                {
                    var StringListOfJSON = AWSHelper.ReturnJSONForFolder(FolderObj);
                    GlobalObjects.TempTextToFill.Add("FOR DEVICE: " + FolderObj.Name + " | FOUND TOTAL OF: " + StringListOfJSON.Count + " JSON VALUES IN THIS FOLDER");

                    foreach (var JsonString in StringListOfJSON)
                    {
                        GlobalObjects.TempTextToFill.Add("JSON: " + JsonString);

                        var ConfigInfo = JsonConvert.DeserializeObject<VehicleConfigInfo_Invoker>(JsonString);

                        string OEType = "";
                        var MakeSetup = new OEMakeBools(ConfigInfo.Make, ConfigInfo.Model, ConfigInfo.Year);
                        if (MakeSetup.IsFordMake)
                        {
                            if (MakeSetup.IsFDRSVehicle) { OEType = "FDRS"; }
                            else { OEType = "FJDS"; }
                        }
                        if (MakeSetup.IsGMMake) { OEType = "GDS2"; }
                        if (MakeSetup.IsHondaMake) { OEType = "IHDS"; }
                        if (MakeSetup.IsNissanMake) { OEType = "CP3"; }
                        if (MakeSetup.IsToyotaMake) { OEType = "TIS"; }

                        ConfigInfo.OEM = OEType; 
                        string CleanModel = ConfigInfo.Model.Replace(" ", "-").Replace("-", "");
                        string YMMString = (ConfigInfo.Year + "_" + ConfigInfo.Make + "_" + CleanModel).ToUpper();

                        FallBackConfigItem NextConfigItem = new FallBackConfigItem
                        {
                            JSON = JsonString,
                            OEType = OEType,
                            NameOfVehicle = (ConfigInfo.Year + "_" + ConfigInfo.Make + "_" + CleanModel).ToUpper()
                        };

                        GlobalObjects.TempTextToFill.Add("MADE A NEW CONFIG ITEM FOR VEHICLE: " + YMMString);

                        bool ShouldAdd = true;
                        foreach (var FallBackItem in FallBackVehicleConfigs.AllConfigItems)
                        {
                            var FoundConfig = JsonConvert.DeserializeObject<VehicleConfigInfo_Invoker>(FallBackItem.JSON);
                            if (FoundConfig.VIN == ConfigInfo.VIN)
                            {
                                ShouldAdd = false;
                                break;
                            }
                        }

                        if (ShouldAdd) { ConfigsToAdd.Add(NextConfigItem); }                   
                    }
                });
            });

            // Add to All config items.
            foreach (var ConfigItem in ConfigsToAdd) { AddItemToFallback(ConfigItem); }
            GlobalObjects.SetLoggerBoxContent(true);

            GlobalObjects.TempTextToFill.Add("LISTING ALL VEHICLE ITEMS OUT NOW");
            GlobalObjects.TempTextToFill.Add("-------------------------------------------------");
            GlobalObjects.TempTextToFill.Add(" # |     VIN NUMBER    |       VEHICLE NAME      ");
            GlobalObjects.TempTextToFill.Add("-------------------------------------------------");

            int CountOfVehicles = 0;
            foreach (var FallBackItem in FallBackVehicleConfigs.AllConfigItems)
            {
                string RegexPatt = "{\\\"VIN\\\":\\\"([0-9A-Z]{17})\\\"";
                var VinRegex = Regex.Match(FallBackItem.JSON, RegexPatt);

                string StringToWrite = String.Format("{0,2} | {1,-10} | {2,5}", CountOfVehicles, VinRegex.Groups[1].Value, FallBackItem.NameOfVehicle);
                GlobalObjects.TempTextToFill.Add(StringToWrite);

                // GlobalObjects.TempTextToFill.Add("\\__ OESW: " + FallBackItem.OEType);
                // GlobalObjects.TempTextToFill.Add("\\__ JSON: " + FallBackItem.JSON);

                GlobalObjects.SetLoggerBoxContent(true);
                CountOfVehicles += 1;
            }
            GlobalObjects.TempTextToFill.Add("-------------------------------------------------");
            GlobalObjects.SetLoggerBoxContent(true);

            // Clear out items.
            VehicleConfigNameCombobox.Items.Clear();

            // Setup new items at index 0
            foreach (var ItemType in FallBackVehicleConfigs.AllConfigItems)
            {
                string NameOfCar = ItemType.NameOfVehicle;
                if (NameOfCar.StartsWith("ITEM")) { NameOfCar = NameOfCar.Replace("ITEM_", ""); }
                VehicleConfigNameCombobox.Items.Add(NameOfCar);
            }
            VehicleConfigNameCombobox.SelectedIndex = 0;

            // Toggle Button visis.
            PullingVehicleJSONLoading.Visibility = Visibility.Hidden;
            RefreshVehicleJSON.IsEnabled = true;
            SetVehicleButton.IsEnabled = true;
            SetCDP4OrDriveTool.IsEnabled = true;
            VehicleConfigNameCombobox.IsEnabled = true;
        }

        private void AddItemToFallback(FallBackConfigItem NextItem)
        {
            FallBackVehicleConfigs.AllConfigItems.Add(NextItem);
            using (ResXResourceWriter resxWrite = new ResXResourceWriter("FallBackItems.resx"))
            {
                resxWrite.AddResource("ITEM_" + NextItem.NameOfVehicle, NextItem.JSON);
                resxWrite.Close();
            }

            GlobalObjects.TempTextToFill.Add("ADDED NEW VEHICLE: " + NextItem.NameOfVehicle + " TO THE LIST OF RESOURCES!");
        }
    }
}
