﻿using OEAutomationLauncher.AutomationLauncher.ObjectClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OEAutomationLauncher.AutomationLauncher.DebugHelpers.DebugViews
{
    /// <summary>
    /// Interaction logic for ViewAndAPIDebugger.xaml
    /// </summary>
    public partial class ViewAndAPIDebugger : UserControl
    {
        public bool IsCDP4
        {
            get { return GlobalObjects.IsCDP4; }
        }
        public ViewAndAPIDebugger()
        {
            DataContext = this;
            InitializeComponent();
        }

        /// <summary>
        /// DEBUG FUNCTION Used to either shows the task grid or resets all items.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ResetTaskItemsButton(object sender, RoutedEventArgs e)
        {
            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
            {
                if (GlobalObjects.MainWindowView.DebugConfigAndLoggerView.LoadingConfigViewGrid.Visibility == Visibility.Visible)
                {
                    GlobalObjects.MainWindowView.DebugConfigAndLoggerView.LoadingConfigViewGrid.Visibility = Visibility.Hidden;
                    GlobalObjects.MainWindowView.DebugConfigAndLoggerView.SetVehicleButton.IsEnabled = true;
                    GlobalObjects.MainWindowView.DebugConfigAndLoggerView.VehicleConfigNameCombobox.IsEnabled = true;
                    GlobalObjects.MainWindowView.DebugConfigAndLoggerView.RefreshVehicleJSON.IsEnabled = true;
                    GlobalObjects.MainWindowView.DebugConfigAndLoggerView.SetVehicleButton.Content = "Set Vehicle";
                }
            });

            Task.Run(() => { MainWindowViewHelpers.ToggleActionsList(true); });
        }
        /// <summary>
        /// DEBUG FUNCTION Used to show/hide the option picker grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToggleOptionPickerView(object sender, RoutedEventArgs e)
        {
            Button SendBtn = (Button)sender;
            string BtnContent = SendBtn.Content.ToString();

            if (BtnContent == "Show Picker")
            {
                SendBtn.Content = "Hide Picker";
                Task.Run(() => { Dispatcher.Invoke(() => { MainWindowViewHelpers.ToggleOptionsList(true); }); });
            }

            if (BtnContent == "Hide Picker")
            {
                SendBtn.Content = "Show Picker";
                Task.Run(() => { Dispatcher.Invoke(() => { MainWindowViewHelpers.ToggleOptionsList(false); }); });
            }
        }
        /// <summary>
        /// DEBUG FUNCTION Used to show/hide the VIN Request Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToggleVinRequestView(object sender, RoutedEventArgs e)
        {
            Button SendBtn = (Button)sender;
            string BtnContent = SendBtn.Content.ToString();

            if (BtnContent == "Show User VIN")
            {
                SendBtn.Content = "Hide User VIN";
                Task.Run(() => { MainWindowViewHelpers.ToggleVINRequest(true); });
            }

            if (BtnContent == "Hide User VIN")
            {
                SendBtn.Content = "Show User VIN";
                Task.Run(() => { MainWindowViewHelpers.ToggleVINRequest(false); });
            }
        }
        /// <summary>
        /// DEBUG FUNCTION Used to test API Endpoint calls on the HTTP Client.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TestAPISendingEndpoints(object sender, RoutedEventArgs e)
        {
            Button SendButton = (Button)sender;
            string BaseURL = @"http://localhost:57570/OEAutomationTasker";
            string EndOfURL = TestAPILinkBox.Text;

            if (!EndOfURL.StartsWith("/")) { EndOfURL = "/" + EndOfURL; }
            if (!Regex.Match(EndOfURL, @"\/([a-zA-Z]+)\/([a-zA-Z]+)\/").Success && !EndOfURL.StartsWith(BaseURL))
            {
                SendButton.Content = "Wait";

                ResponseAPICallBox.Foreground = Brushes.Red;
                ResponseAPICallBox.FontWeight = FontWeights.DemiBold;
                ResponseAPICallBox.Text = "INVALID ENDPOINT. PLEASE TRY AGAIN WITH THE CORRECT FORMAT FOR THE END OF AN API CALL";

                SendButton.Content = "Send";

                GlobalObjects.DebugHelper.WriteDebugLog("DEBUG MODE API ENDPOINT WAS INVALID TRY AGAIN.", "DEBUG_MODE");

                return;
            }

            ResponseAPICallBox.Foreground = Brushes.White;
            ResponseAPICallBox.FontWeight = FontWeights.Normal;

            string SendRequestLink = BaseURL + EndOfURL;
            if (EndOfURL.StartsWith("/http")) 
            {
                SendRequestLink = EndOfURL.Substring(1);
                GlobalObjects.DebugHelper.WriteDebugLog("URL WAS FOUND TO BE A FULL URL LINK TO THE API. SETTING IT NOW");
                GlobalObjects.DebugHelper.WriteDebugLog("URL: " + SendRequestLink);
            }

            GlobalObjects.DebugHelper.WriteDebugLog("DEBUG MODE API TO CALL: " + SendRequestLink, "DEBUG_MODE");
            GlobalObjects.DebugHelper.WriteDebugLog("DEBUG MODE API TO CALL: " + SendRequestLink, "DEBUG_MODE", true);

            ResponseAPICallBox.Text = "Sending Command: " + SendRequestLink;
            SendButton.IsEnabled = false;
            SendButton.Content = "Wait";

            Dispatcher.Invoke(() =>
            {
                Task.Run(() =>
                {
                    string ResponseStringValue = new WebClient().DownloadString(SendRequestLink);
                    GlobalObjects.DebugHelper.WriteDebugLog("RESPONSE BACK FROM API CALL: " + ResponseStringValue, "DEBUG_MODE");
                    GlobalObjects.DebugHelper.WriteDebugLog("RESPONSE BACK FROM API CALL: " + ResponseStringValue, "DEBUG_MODE", true);

                    Dispatcher.Invoke(() =>
                    {
                        ResponseAPICallBox.Text = ResponseStringValue;

                        SendButton.Content = "Send";
                        SendButton.IsEnabled = true;
                    });
                });
            });
        }
    }
}
