﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OEAutomationLauncher.DebugHelpers {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class FallBackItems {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal FallBackItems() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("OEAutomationLauncher.DebugHelpers.FallBackItems", typeof(FallBackItems).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1 | Check for System Updates
        ///2 | Launch CONSULT 3+
        ///3 | Start Diagnostic Session
        ///4 | Run Health Check
        ///5 | Clear Codes
        ///6 | Save Vehicle Report
        ///7 | Prepare Report For Upload
        ///8 | Scan Completed.
        /// </summary>
        internal static string CP3FallBackTaskList {
            get {
                return ResourceManager.GetString("CP3FallBackTaskList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {&quot;NameFirst&quot;:&quot;Zachary&quot;,&quot;NameLast&quot;:&quot;Walsh&quot;,&quot;NameFull&quot;:&quot;Zachary Walsh&quot;,&quot;NameFullNoSpaces&quot;:&quot;ZacharyWalsh&quot;,&quot;Image&quot;:&quot;images/UserWindow/user-shadow.png&quot;,&quot;IsNewUser&quot;:false,&quot;IsLoaded&quot;:true,&quot;IsLoggedIn&quot;:true,&quot;Email&quot;:&quot;zachary.walsh@opusivs.com&quot;,&quot;AuthToken&quot;:&quot;a0528b133ef67f6ecfe2245e2dc0cafac00a8b353f4ca5fda9dc4a63161e273b&quot;,&quot;Country&quot;:&quot;United States&quot;}.
        /// </summary>
        internal static string FallBackUserInfo {
            get {
                return ResourceManager.GetString("FallBackUserInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1 | Check for System Updates
        ///2 | Launch FDRS
        ///3 | Login To FDRS
        ///4 | Set VCI - Read VIN
        ///5 | Start Health Check
        ///6 | Clear Codes
        ///7 | Save Report
        ///8 | Prepare Report For Upload
        ///9 | Scan Completed.
        /// </summary>
        internal static string FDRSFallBackTaskList {
            get {
                return ResourceManager.GetString("FDRSFallBackTaskList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1 | Check for System Updates
        ///2 | Launch FJDS
        ///3 | Start Diagnostic Session
        ///4 | Gather Vehicle Data
        ///5 | Run Self Test
        ///6 | Clear Codes
        ///7 | Save Vehicle Report
        ///8 | Prepare Report For Upload
        ///9 | Scan Completed.
        /// </summary>
        internal static string FJDSFallBackTaskList {
            get {
                return ResourceManager.GetString("FJDSFallBackTaskList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1 | Check for RPA Updates
        ///2 | Launch GDS2
        ///3 | Install Diagnostic Packages 
        ///4 | Set Vehicle Interface Module
        ///5 | Reset Vehicle Configuration
        ///6 | Configure Options
        ///7 | Run Vehicle Scan
        ///8 | Print Report To PDF
        ///9 | Prepare Report For Upload
        ///10 | Scan Completed.
        /// </summary>
        internal static string GDS2FallBackTaskList {
            get {
                return ResourceManager.GetString("GDS2FallBackTaskList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {&quot;VIN&quot;:&quot;JTHBK1GG4F2196600&quot;,&quot;Year&quot;:&quot;2015&quot;,&quot;OEM&quot;:&quot;&quot;,&quot;Make&quot;:&quot;Lexus&quot;,&quot;Model&quot;:&quot;ES 350&quot;,&quot;Engine&quot;:&quot;3.5L V6 268hp 248ft. lbs.&quot;,&quot;Error&quot;:null,&quot;Voltage&quot;:&quot;16.70&quot;,&quot;Trim&quot;:null,&quot;AdasModules&quot;:[&quot;*Adaptive Cruise Control&quot;,&quot;*Collision Braking&quot;,&quot;*Blind Spot Detection&quot;,&quot;*Lane Departure Warning&quot;,&quot;*Rear View&quot;,&quot;*Park Assist&quot;,&quot;*Rear Cross Traffic&quot;,&quot;*Night Vision&quot;],&quot;IsAdas&quot;:true,&quot;IsNotAdas&quot;:false,&quot;Odometer&quot;:&quot;&quot;}.
        /// </summary>
        internal static string ITEM_2015_LEXUS_ES350 {
            get {
                return ResourceManager.GetString("ITEM_2015_LEXUS_ES350", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {&quot;VIN&quot;:&quot;3FA6P0H75HR199958&quot;,&quot;Year&quot;:&quot;2017&quot;,&quot;OEM&quot;:&quot;&quot;,&quot;Make&quot;:&quot;Ford&quot;,&quot;Model&quot;:&quot;Fusion&quot;,&quot;Engine&quot;:&quot;2.5L I4 175hp 175ft. lbs.&quot;,&quot;Error&quot;:null,&quot;Voltage&quot;:&quot;16.40&quot;,&quot;Trim&quot;:&quot;SE&quot;,&quot;AdasModules&quot;:[&quot;*Adaptive Cruise Control&quot;,&quot;*Collision Braking&quot;,&quot;*Collision Warning&quot;,&quot;*Lane Departure Warning&quot;,&quot;*Lane Keep Assist&quot;,&quot;*Rear View&quot;,&quot;*Park Assist&quot;,&quot;*Active Park Assist&quot;],&quot;IsAdas&quot;:true,&quot;IsNotAdas&quot;:false,&quot;Odometer&quot;:&quot;&quot;}.
        /// </summary>
        internal static string ITEM_2017_FORD_FUSION {
            get {
                return ResourceManager.GetString("ITEM_2017_FORD_FUSION", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {&quot;VIN&quot;:&quot;5TDJZRFH5HS394292&quot;,&quot;Year&quot;:&quot;2017&quot;,&quot;OEM&quot;:&quot;&quot;,&quot;Make&quot;:&quot;Toyota&quot;,&quot;Model&quot;:&quot;Highlander&quot;,&quot;Engine&quot;:&quot;3.5L V6 295hp 263ft. lbs.&quot;,&quot;Error&quot;:&quot;null&quot;,&quot;Voltage&quot;:&quot;16.70&quot;,&quot;Trim&quot;:&quot;null&quot;,&quot;AdasModules&quot;:[&quot;*360 Camera View&quot;,&quot;*Adaptive Cruise Control&quot;,&quot;*Collision Braking&quot;,&quot;*Blind Spot Detection&quot;,&quot;*Lane Keep Assist&quot;,&quot;*Rear View&quot;,&quot;*Park Assist&quot;,&quot;*Rear Cross Traffic&quot;,&quot;*Night Vision&quot;],&quot;IsAdas&quot;:&quot;true&quot;,&quot;IsNotAdas&quot;:&quot;false&quot;,&quot;Odometer&quot;:&quot;&quot;}.
        /// </summary>
        internal static string ITEM_2017_TOYOTA_HIGHLANDER {
            get {
                return ResourceManager.GetString("ITEM_2017_TOYOTA_HIGHLANDER", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {&quot;VIN&quot;:&quot;3MYDLBYV1HY158483&quot;,&quot;Year&quot;:&quot;2017&quot;,&quot;OEM&quot;:&quot;&quot;,&quot;Make&quot;:&quot;Toyota&quot;,&quot;Model&quot;:&quot;Yaris iA&quot;,&quot;Engine&quot;:&quot;1.5L I4 106hp 103ft. lbs.&quot;,&quot;Error&quot;:null,&quot;Voltage&quot;:&quot;17.00&quot;,&quot;Trim&quot;:&quot;Base&quot;,&quot;AdasModules&quot;:[&quot;*360 Camera View&quot;,&quot;*Adaptive Cruise Control&quot;,&quot;*Collision Braking&quot;,&quot;*Collision Warning&quot;,&quot;*Lane Departure Warning&quot;,&quot;*Lane Keep Assist&quot;,&quot;*Park Assist&quot;],&quot;IsAdas&quot;:true,&quot;IsNotAdas&quot;:false,&quot;Odometer&quot;:&quot;&quot;}.
        /// </summary>
        internal static string ITEM_2017_TOYOTA_YARIS_IA {
            get {
                return ResourceManager.GetString("ITEM_2017_TOYOTA_YARIS-IA", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {&quot;VIN&quot;:&quot;W04GV8SX6J1065675&quot;,&quot;Year&quot;:&quot;2018&quot;,&quot;OEM&quot;:&quot;&quot;,&quot;Make&quot;:&quot;Buick&quot;,&quot;Model&quot;:&quot;Regal TourX&quot;,&quot;Engine&quot;:&quot;2.0L Turbo I4 250hp 295ft. lbs.&quot;,&quot;Error&quot;:&quot;null&quot;,&quot;Voltage&quot;:&quot;16.90&quot;,&quot;Trim&quot;:&quot;Essence&quot;,&quot;AdasModules&quot;:[&quot;*360 Camera View&quot;,&quot;*Adaptive Cruise Control&quot;,&quot;*Collision Braking&quot;,&quot;*Collision Warning&quot;,&quot;*Lane Departure Warning&quot;,&quot;*Lane Keep Assist&quot;,&quot;*Park Assist&quot;],&quot;IsAdas&quot;:&quot;true&quot;,&quot;IsNotAdas&quot;:&quot;false&quot;,&quot;Odometer&quot;:&quot;&quot;}.
        /// </summary>
        internal static string ITEM_2018_BUICK_REGAL_TOUR {
            get {
                return ResourceManager.GetString("ITEM_2018_BUICK_REGAL-TOUR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {&quot;VIN&quot;:&quot;1GKS2CKJ4JR399121&quot;,&quot;Year&quot;:&quot;2018&quot;,&quot;OEM&quot;:&quot;&quot;,&quot;Make&quot;:&quot;GMC&quot;,&quot;Model&quot;:&quot;Yukon&quot;,&quot;Engine&quot;:&quot;EcoTec3 6.2L V8 420hp 460ft. lbs.&quot;,&quot;Error&quot;:null,&quot;Voltage&quot;:&quot;16.60&quot;,&quot;Trim&quot;:&quot;Denali&quot;,&quot;AdasModules&quot;:[&quot;*360 Camera View&quot;,&quot;*Adaptive Cruise Control&quot;,&quot;*Collision Braking&quot;,&quot;*Collision Warning&quot;,&quot;*Lane Departure Warning&quot;,&quot;*Lane Keep Assist&quot;,&quot;*Park Assist&quot;],&quot;IsAdas&quot;:true,&quot;IsNotAdas&quot;:false,&quot;Odometer&quot;:&quot;&quot;}.
        /// </summary>
        internal static string ITEM_2018_GMC_YUKON {
            get {
                return ResourceManager.GetString("ITEM_2018_GMC_YUKON", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {&quot;VIN&quot;:&quot;JN1EV7AR4JM440524&quot;,&quot;Year&quot;:&quot;2018&quot;,&quot;OEM&quot;:&quot;&quot;,&quot;Make&quot;:&quot;INFINITI&quot;,&quot;Model&quot;:&quot;Q50&quot;,&quot;Engine&quot;:&quot;3.0L Twin Turbo V6 300hp 295ft. lbs.&quot;,&quot;Error&quot;:null,&quot;Voltage&quot;:&quot;10.10&quot;,&quot;Trim&quot;:null,&quot;AdasModules&quot;:[&quot;*Adaptive Cruise Control&quot;,&quot;*Collision Braking&quot;,&quot;*Collision Warning&quot;,&quot;*Blind Spot Detection&quot;,&quot;*Lane Departure Warning&quot;,&quot;*Lane Keep Assist&quot;,&quot;*Night Vision&quot;],&quot;IsAdas&quot;:true,&quot;IsNotAdas&quot;:false,&quot;Odometer&quot;:&quot;&quot;}.
        /// </summary>
        internal static string ITEM_2018_INFINITI_Q50 {
            get {
                return ResourceManager.GetString("ITEM_2018_INFINITI_Q50", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {&quot;VIN&quot;:&quot;1FTEW1E59KKF06433&quot;,&quot;Year&quot;:&quot;2019&quot;,&quot;OEM&quot;:&quot;&quot;,&quot;Make&quot;:&quot;Ford&quot;,&quot;Model&quot;:&quot;F-150&quot;,&quot;Engine&quot;:&quot;5.0L Flex Fuel V8 395hp 400ft. lbs.&quot;,&quot;Error&quot;:null,&quot;Voltage&quot;:&quot;16.80&quot;,&quot;Trim&quot;:null,&quot;AdasModules&quot;:[&quot;*360 Camera View&quot;,&quot;*Adaptive Cruise Control&quot;,&quot;*Collision Braking&quot;,&quot;*Collision Warning&quot;,&quot;*Lane Departure Warning&quot;,&quot;*Lane Keep Assist&quot;,&quot;*Park Assist&quot;],&quot;IsAdas&quot;:true,&quot;IsNotAdas&quot;:false,&quot;Odometer&quot;:&quot;&quot;}.
        /// </summary>
        internal static string ITEM_2019_FORD_F150 {
            get {
                return ResourceManager.GetString("ITEM_2019_FORD_F150", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {&quot;VIN&quot;:&quot;1FTER4FH4LLA06922&quot;,&quot;Year&quot;:&quot;2020&quot;,&quot;OEM&quot;:&quot;&quot;,&quot;Make&quot;:&quot;Ford&quot;,&quot;Model&quot;:&quot;Ranger&quot;,&quot;Engine&quot;:&quot;EcoBoost 2.3L Turbo I4 270hp 310ft. lbs.&quot;,&quot;Error&quot;:null,&quot;Voltage&quot;:&quot;12.70&quot;,&quot;Trim&quot;:&quot;Lariat&quot;,&quot;AdasModules&quot;:[&quot;*360 Camera View&quot;,&quot;*Adaptive Cruise Control&quot;,&quot;*Collision Braking&quot;,&quot;*Collision Warning&quot;,&quot;*Lane Departure Warning&quot;,&quot;*Lane Keep Assist&quot;,&quot;*Park Assist&quot;],&quot;IsAdas&quot;:true,&quot;IsNotAdas&quot;:false,&quot;Odometer&quot;:&quot;&quot;}.
        /// </summary>
        internal static string ITEM_2020_FORD_RANGER {
            get {
                return ResourceManager.GetString("ITEM_2020_FORD_RANGER", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {&quot;VIN&quot;:&quot;2T2HZMDA6LC217495&quot;,&quot;Year&quot;:&quot;2020&quot;,&quot;OEM&quot;:&quot;&quot;,&quot;Make&quot;:&quot;Lexus&quot;,&quot;Model&quot;:&quot;RX 350&quot;,&quot;Engine&quot;:&quot;3.5L V6 295hp 267ft. lbs.&quot;,&quot;Error&quot;:null,&quot;Voltage&quot;:&quot;13.60&quot;,&quot;Trim&quot;:&quot;Base&quot;,&quot;AdasModules&quot;:[&quot;*360 Camera View&quot;,&quot;*Adaptive Cruise Control&quot;,&quot;*Collision Braking&quot;,&quot;*Blind Spot Detection&quot;,&quot;*Lane Keep Assist&quot;,&quot;*Rear View&quot;,&quot;*Park Assist&quot;,&quot;*Rear Cross Traffic&quot;,&quot;*Night Vision&quot;],&quot;IsAdas&quot;:true,&quot;IsNotAdas&quot;:false,&quot;Odometer&quot;:&quot;&quot;}.
        /// </summary>
        internal static string ITEM_2020_LEXUS_RX350 {
            get {
                return ResourceManager.GetString("ITEM_2020_LEXUS_RX350", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1 | Check for RPA Updates
        ///2 | Launch TIS
        ///3 | Check For Updates 
        ///4 | Set Vehicle Interface Module
        ///5 | Connect To Vehicle
        ///6 | License Software
        ///7 | Configure Options
        ///8 | Run Health Check
        ///9 | Print Report To PDF
        ///10 | Copy Output Files
        ///11 | Prepare Report For Upload
        ///12 | Scan Completed.
        /// </summary>
        internal static string TISFallBackTaskList {
            get {
                return ResourceManager.GetString("TISFallBackTaskList", resourceCulture);
            }
        }
    }
}
