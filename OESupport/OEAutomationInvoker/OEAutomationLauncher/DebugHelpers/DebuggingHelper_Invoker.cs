﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using OEAutomationLauncher.LogicalHelpers;

namespace OEAutomationLauncher.AutomationLauncher.DebugHelpers
{
    public class DebuggingHelper_Invoker
    {
        public bool IsOptionDebug = false;

        public string MainDebugFile { get; set; }
        public string APIDebugFileName { get; set; }
        public string OptionsDebugFileName { get; set; }

        // Pulled out two useless log dirs. One doesn't get used anymore and the other is for 
        // Desktop Dev work and there's no reason to make it on devices.
        private static List<string> AllDebugFolders = new List<string>
        {
             @"C:\Program Files (x86)\OESupport\OEDebugging\OptionsDebug",
             @"C:\Program Files (x86)\OESupport\OEDebugging\APICallDebug",       
             @"C:\Program Files (x86)\OESupport\OEDebugging\WinAutomationDebug",
             @"C:\Program Files (x86)\OESupport\OEDebugging\SpoofAPIDebug",      
             @"C:\Program Files (x86)\OESupport\OEDebugging\AutomationInvokerDebug",
             @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle",
             @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\CustomViewConfigs"
        };

        private string OptionsDebug = AllDebugFolders[0];
        private string APICallDebug = AllDebugFolders[1];
        private string WinAutoDebug = AllDebugFolders[2];
        private string APISpoofsDir = AllDebugFolders[3];
        private string InvokerDebug = AllDebugFolders[4];
        private string OEOptionsDir = AllDebugFolders[5];
        private string OEViewerInfo = AllDebugFolders[6];

        public DebuggingHelper_Invoker()
        {
            GenerateLogDirs();                      // Checks for output dirs and makes sure they are setup ok.
            MainDebugFile = GetDebugFileName();     // Gets the name of our new debug file.
            SetupDebugFile();                       // Check to see if weve done an OE Scan today or not.
        }

        /// <summary>
        /// Generates log dirs for all required file locations.
        /// </summary>
        public void GenerateLogDirs(bool CleanOutAll = false)
        {
            foreach (var Dir in AllDebugFolders)
            {
                if (!Directory.Exists(Dir)) { Directory.CreateDirectory(Dir); }

                var FilesList = Directory.GetFiles(Dir);
                if (!Debugger.IsAttached)
                {
                    if (CleanOutAll) { foreach (var file in FilesList) { File.Delete(file); } }
                    else
                    {
                        if (!Dir.Contains("CurrentVehicle")) { continue; }
                        foreach (var file in FilesList) { File.Delete(file); }
                    }
                }
            }
        }

        /// <summary>
        /// Gets name of calling Method and uses it to append into the debug log file lines.
        /// </summary>
        /// <returns></returns>
        private static string NameOfCallingClass(out bool IsApi, out bool IsOptions)
        {
            IsApi = false;
            IsOptions = false;

            string fullName;
            Type declaringType;
            int skipFrames = 2;
            do
            {
                MethodBase method = new StackFrame(skipFrames, false).GetMethod();
                declaringType = method.DeclaringType;
                if (declaringType == null)
                {
                    if (method.Name.ToLower().Contains("options")) { IsOptions = true; }
                    if (method.Name.ToLower().Contains("api")) { IsApi = true; }

                    return method.Name;
                }
                skipFrames++;
                fullName = declaringType.FullName;
            }
            while (declaringType.Module.Name.Equals("mscorlib.dll", StringComparison.OrdinalIgnoreCase));

            if (fullName.ToLower().Contains("options")) { IsOptions = true; }
            if (fullName.ToLower().Contains("api")) { IsApi = true; }

            var fullNameSplit = fullName.Split('.');
            fullName = fullNameSplit[fullNameSplit.Length - 1];

            return fullName;
        }

        /// <summary>
        /// Get the current date and format it for the name of our file.
        /// </summary> 
        private string GetDebugFileName()
        {
            // Get DateTimeNow.
            var strTime = DateTime.Now.ToString("s");
            strTime = strTime.Replace("T", "_");
            strTime = strTime.Replace("-", "");

            // Replace the actual time with nothing since we need to call this file again.
            strTime = strTime.Split('_')[0];

            // Append in the VIN if it exists.
            string VINValue = "";
            if (GlobalObjects.ConfigInfo != null) { VINValue = GlobalObjects.ConfigInfo.VIN; }
            else 
            {
                if (Debugger.IsAttached) { VINValue = "UNKNOWN_DEBUG_MODE"; }
                else { VINValue = "UNKNOWN_VIN"; }
            }

            // Combine the date and prefix and return it.
            string InvokerAPI = "OEAutomation_API_Logging_" + VINValue + "_" + strTime + ".txt";
            string InvokerFile = "OEAutomationInvoker_" + VINValue + "_" + strTime + ".txt";
            string OptionsFile = "OEOptionPicker_" + VINValue + "_" + strTime + ".txt";

            APIDebugFileName = InvokerDebug + "\\" + InvokerAPI;
            MainDebugFile = InvokerDebug + "\\" + InvokerFile; ;
            OptionsDebugFileName = OptionsDebug + "\\" + OptionsFile;

            if (!Debugger.IsAttached)
            {
                if (File.Exists(MainDebugFile)) { File.Delete(MainDebugFile); }
                if (File.Exists(APIDebugFileName)) { File.Delete(APIDebugFileName); }
                if (File.Exists(OptionsDebugFileName)) { File.Delete(OptionsDebugFileName); }
            }

            return MainDebugFile;
        }

        /// <summary>
        /// Setup the file and make a new one if needed.
        /// </summary>
        /// <param name="initNew">Force create a new file or not.</param>
        private void SetupDebugFile(bool initNew = false)
        {
            // Base timestamp message.
            string TimeString = "OE AUTOMATION INVOKER DEBUGGING: " + DateTime.Now.ToString("s") + "\n";
            string APIString = "OE AUTOMATION INVOKER API DEBUGGING: " + DateTime.Now.ToString("s") + "\n";
            string OptionsString = "OE AUTOMATION OPTION PICKER: " + DateTime.Now.ToString("s") + "\n";

            List<string> AllFileList = new List<string>() { MainDebugFile, APIDebugFileName, OptionsDebugFileName };
            List<string> ListOfTimeStrings = new List<string>() { TimeString, APIString, OptionsString };

            for (int Count = 0; Count < AllFileList.Count; Count++)
            {
                string DebugFile = AllFileList[Count];
                string FileHeader = ListOfTimeStrings[Count];

                // Clean out if we have the debugger hooked in.
                if (Debugger.IsAttached && File.Exists(DebugFile)) { File.Delete(DebugFile); }

                // Check for existing file. If not setup a new one with timestamp headding.
                if (!File.Exists(DebugFile)) { File.WriteAllText(DebugFile, FileHeader); }
                else
                {
                    // Setup a new file if we have the flag set.
                    if (initNew) { File.AppendAllText(DebugFile, FileHeader); return; }

                    // If the file exists, see if it has anything in it. 
                    var sizeOfFile = File.ReadAllBytes(DebugFile).Length;
                    if (sizeOfFile > 0)
                    {
                        File.AppendAllText(DebugFile, "\n----------------------------------\n");
                        File.AppendAllText(DebugFile, FileHeader);
                    }
                }
            }           
        }

        /// <summary>
        /// Padds both ends of a stirng passed in.
        /// </summary>
        /// <param name="source">String to pad</param>
        /// <param name="length">Desired length of output string.</param>
        /// <returns>Centered pad string.</returns>
        private string PadBothEndsOfString(string source, int length)
        {
            int spaces = length - source.Length;
            int padLeft = spaces / 2 + source.Length;
            return source.PadLeft(padLeft).PadRight(length);

        }

        /// <summary>
        /// Write Option Parse debug info out to here.
        /// </summary>
        /// <param name="DebugInfo">String of information to log into the debug file.</param>
        /// <param name="InfoType">Specify a type of info to pass into the log. INFO, DEBUG, ERR, LOG etc.</param>
        public void WriteDebugLog(string DebugInfo, string InfoType = "LOG", bool APILog = false, bool OptionsLog = false)
        {
            // Get the current time for the logging info.
            string timeNow = DateTime.Now.ToString("T");
            string CallingName = NameOfCallingClass(out bool FoundAPILog, out bool FoundOptionsLog);
            string writeThis = "[" + timeNow + "] ::: ";

            // Add log info
            if (InfoType == "INIT")  { InfoType = "- INIT -"; }
            if (InfoType == "LOG")   { InfoType = "- INFO -"; }
            if (InfoType == "ERROR") { InfoType = "- FAIL -"; }
            if (InfoType == "FILES") { InfoType = "- FILE -"; }
            if (InfoType == "START") { InfoType = "- INIT -"; }
            if (InfoType == "PASS")  { InfoType = "- PASS -"; }
            if (InfoType == "API")   { InfoType = "- HTTP -"; }

            writeThis += "[" + PadBothEndsOfString(InfoType, 10) + "] ::: [" + CallingName + "] ::: " + DebugInfo + "\n";

            // Write the formatted logging info out.
            if (!APILog) File.AppendAllText(MainDebugFile, writeThis);
            if (APILog || FoundAPILog) File.AppendAllText(APIDebugFileName, writeThis);

            // Check for options config open.
            if ((OptionsLog || IsOptionDebug || FoundOptionsLog) && !APILog) { File.AppendAllText(OptionsDebugFileName, writeThis); }

            // Add into the RTF File.
            if (GlobalObjects.Colorizer != null) { GlobalObjects.Colorizer.HTMLColorizer.WriteToLogFile(writeThis); }            

            // If we have our logger TB open.
            if (Debugger.IsAttached)
            {
                try
                {
                    // If the MainWindow object is null, just add to the backlog of text value.
                    if (GlobalObjects.MainWindowView == null) { GlobalObjects.TempTextToFill.Add(writeThis); }
                    else
                    {
                        // Run this async so the UI doesnt get held up too hard.
                        Task.Run(() =>
                        {
                            GlobalObjects.MainWindowView.Dispatcher.Invoke(() =>
                            {
                                // Add the text value and update the logger to scroll to the end.
                                GlobalObjects.DebugInfoTextBox.Text += writeThis;
                                GlobalObjects.DebugInfoTextBox.ScrollToEnd();
                            });
                        });
                    }
                }
                catch { GlobalObjects.TempTextToFill.Add(writeThis); }
            }
        }

        /// <summary>
        /// Write to the debug log without actually using the API or Options tirggers.
        /// This uses the claling name method to return what we need to pass around.
        /// </summary>
        /// <param name="DebugInfo"></param>
        /// <param name="InfoType"></param>
        public void WriteDebugLog(string DebugInfo, string InfoType)
        {
            NameOfCallingClass(out bool APILog, out bool OptionsLog);
            WriteDebugLog(DebugInfo, InfoType, APILog, OptionsLog);
        }

        /// <summary>
        /// Logs an exceptions information to the debug log.
        /// </summary>
        /// <param name="ExToLog">Exception Item to log into logs.</param>
        public void WriteDebugLog(Exception ExToLog, bool IsAPILog = false, bool IsOptionsLog = false)
        {
            string Message = ExToLog.Message;
            string[] ErrorStackTrace = ExToLog.StackTrace.Split('\n');

            // Checks the name of our claling method and sets API or not.
            if (!IsAPILog && !IsOptionsLog) { NameOfCallingClass(out IsAPILog, out IsOptionsLog); }

            WriteDebugLog(Message, "ERROR", IsAPILog, IsOptionsLog);
            foreach (var StringItem in ErrorStackTrace) { WriteDebugLog(StringItem, "ERROR", IsAPILog, IsOptionsLog); }
        }

        /// <summary>
        /// Writes a seperating bar into the console. 
        /// THIS CAN NOT BE CALLED UNLESS THE DEBUGGER IN THE GLBOAL OBJECT IS CONFIGURED.
        /// IT WILL THROW A NULL REF AND CRASH OUT.
        /// Edit: so try/catch exists. Nvm ignroe me.
        /// </summary>
        public static void SeperateConsole()
        {
            try
            {
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine();
                int WindowWidth = Console.WindowWidth;
                Console.ForegroundColor = ConsoleColor.White;
                for (int Count = 0; Count < WindowWidth; Count++) { Console.Write("="); }
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.WriteLine("\n");

                GlobalObjects.DebugHelper.WriteDebugLog("--------------------------------------------");
            }
            catch { }
        }
    }
}
