﻿using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Rendering;
using OEAutomationLauncher.ObjectClasses;
using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OEAutomationLauncher.LogicalHelpers;

namespace OEAutomationLauncher.DebugHelpers
{
    public class LogColorizer
    {
        // HTML Writer Vars
        public string HTMLOutput = @"C:\Program Files (x86)\OESupport\OEDebugging\AutomationInvokerDebug\RTF_";
        internal string InitLogLine = @"<html><body><style>h1 { font-family: sans-serif;color: #FFFFFF; }body { background-color: #000000; font-family: Consolas; }.fillerText { margin: 1px; color: #737373; }p { margin: 2px; }span { margin: 1px; }</style><h1>LOG_TITLE</h1>";
        internal string LogTemplate = "<p>\n\t<span class=\"fillerText\">[</span><span style=\"background: SEG_BG_0; color: SEG_FG_0;\">TIME</span><span class=\"fillerText\">]</span>\n\t<span class=\"fillerText\">:::</span>\n\t<span class=\"fillerText\">[</span><span style=\"background: SEG_BG_1; color: SEG_FG_1;\">TYPE</span><span class=\"fillerText\">]</span>\n\t<span class=\"fillerText\">:::</span>\n\t<span class=\"fillerText\">[</span><span style=\"background: SEG_BG_2; color: SEG_BG_3;\">CALLING_CLASS</span><span class=\"fillerText\">]</span>\n\t<span class=\"fillerText\">:::</span>\n\t<span class=\"fillerText\">[</span><span style=\"background: SEG_BG_3; color: SEG_FG_3;\">LOG_STRING</span><span class=\"fillerText\">]</span>\n</p>";

        // DocWriter Vars
        internal int StartOffset;

        // Objects for loggers.
        internal LogColorizerEdit EditColorizer;
        internal LogColorizerHTML HTMLColorizer;

        /// <summary>
        /// CTOR for colorizer
        /// </summary>
        public LogColorizer()
        {
            EditColorizer = new LogColorizerEdit(this);
            HTMLColorizer = new LogColorizerHTML(this);
        }

        /// <summary>
        /// Gets a list of tuple objects for regex values.
        /// </summary>
        /// <param name="LogLine">String log line to parse.</param>
        /// <returns></returns>
        public List<Tuple<string, int>> ReturnTuples(string LogLine)
        {
            List<string> RegexPats = new List<string>() { @"(\d+:\d+:\d+ [AP]M)", @"(-|[A-Z]|\s){10}", @"(?<= \[)([A-Za-z_<>+0-9]+)" };
            List<Tuple<string, int>> ValueAndIndex = new List<Tuple<string, int>>();

            for (int Counter = 0; Counter < RegexPats.Count; Counter++)
            {
                var Match = Regex.Match(LogLine, RegexPats[Counter]).Value;
                int MatchIndex = LogLine.IndexOf(Match);

                ValueAndIndex.Add(new Tuple<string, int>(Match, MatchIndex));

                if (Counter == 2)
                {
                    string InfoOnly = LogLine.Substring(MatchIndex + Match.Length + 6);
                    int InfoIndex = LogLine.IndexOf(InfoOnly);

                    ValueAndIndex.Add(new Tuple<string, int>(InfoOnly, InfoIndex));
                }
            }

            return ValueAndIndex;
        }

        /// <summary>
        /// Gets a brushcolor object from the input log line item.
        /// </summary>
        /// <param name="LogLine">Line of text to get brushes from.</param>
        /// <returns>LogColoringBrushes item for all sets of brushes to be used.</returns>
        public LogColoringBrushes ReturnBrushesOfLine(string LogLine, List<Tuple<string, int>> RegexMatchValues)
        {
            string InfoType = RegexMatchValues[1].Item1;
            string CallingType = RegexMatchValues[2].Item1;
            LogColoringBrushes RetBrushes = new LogColoringBrushes(InfoType, CallingType);

            for (int Counter = 0; Counter < RegexMatchValues.Count; Counter++)
            {
                try
                {
                    int Index;
                    string TextToFind = RegexMatchValues[Counter].Item1;
                    int StartIndex = RegexMatchValues[Counter].Item2;

                    while ((Index = LogLine.IndexOf(TextToFind, StartIndex)) >= 0)
                    {
                        // Looping Count Switch
                        switch (Counter)
                        {
                            // Time Info String
                            case 0:
                                var fgBrush = BrushHelpers.ReturnBrushFromHex("#737373");   // Dark Grey
                                var bgBrush = BrushHelpers.ReturnBrushFromHex("#000000");   // Black

                                RetBrushes.TimeBrushes = new Tuple<Brush, Brush>(fgBrush, bgBrush);
                                RetBrushes.TimeStrings = new Tuple<string, string>("#737373", "#000000");

                                break;

                            // Info Type String
                            case 1:
                                switch (TextToFind)
                                {
                                    // INFO - BASIC --- WHITE FG AND DARK GREY BG
                                    case string a when a.Contains("INFO"):
                                    case string b when b.Contains("INIT"):
                                        fgBrush = BrushHelpers.ReturnBrushFromHex("#FFFFFF");   // White
                                        bgBrush = BrushHelpers.ReturnBrushFromHex("#222222");   // Dark Grey

                                        RetBrushes.InfoBrushes = new Tuple<Brush, Brush>(fgBrush, bgBrush);
                                        RetBrushes.InfoStrings = new Tuple<string, string>("#FFFFFF", "#222222");
                                        break;

                                    // FAILURES --- WHITE FG AND RED BH
                                    case string a when a.Contains("FAIL"):
                                    case string b when b.Contains("ERROR"):
                                        fgBrush = BrushHelpers.ReturnBrushFromHex("#FFFFFF");   // White
                                        bgBrush = BrushHelpers.ReturnBrushFromHex("#CC3300");   // Red

                                        // Failures need to set the log line value too.
                                        RetBrushes.InfoBrushes = new Tuple<Brush, Brush>(fgBrush, bgBrush);
                                        RetBrushes.LogStringBrushes = new Tuple<Brush, Brush>(fgBrush, bgBrush);

                                        RetBrushes.InfoStrings = new Tuple<string, string>("#FFFFFF", "#CC3300");
                                        RetBrushes.LogLineStrings = new Tuple<string, string>("#FFFFFF", "#CC3300");
                                        break;

                                    // DEBUG --- WHITE FG AND DARK BLUE BG
                                    case string a when a.Contains("DEBUG"):
                                        fgBrush = BrushHelpers.ReturnBrushFromHex("#FFFFFF");   // White
                                        bgBrush = BrushHelpers.ReturnBrushFromHex("#00AEFF");   // Dark Blue

                                        // Debugs need to set the log line value too.
                                        RetBrushes.InfoBrushes = new Tuple<Brush, Brush>(fgBrush, bgBrush);
                                        RetBrushes.LogStringBrushes = new Tuple<Brush, Brush>(fgBrush, bgBrush);

                                        RetBrushes.InfoStrings = new Tuple<string, string>("#FFFFFF", "#00AEFF");
                                        RetBrushes.LogLineStrings = new Tuple<string, string>("#FFFFFF", "#00AEFF");
                                        break;

                                    // API --- YELLOW FG AND DARK PURPLE BG
                                    case string a when a.Contains("API"):
                                    case string b when b.Contains("JSON"):
                                        fgBrush = BrushHelpers.ReturnBrushFromHex("#FFFB00");   // Yellow
                                        bgBrush = BrushHelpers.ReturnBrushFromHex("#46032F");   // Dark Purple

                                        // Debugs need to set the log line value too.
                                        RetBrushes.InfoBrushes = new Tuple<Brush, Brush>(fgBrush, bgBrush);
                                        RetBrushes.LogStringBrushes = new Tuple<Brush, Brush>
                                        (
                                            // FADED WHITE FG AND DARK GREY BG
                                            BrushHelpers.ReturnBrushFromHex("#838383"),     // FADED WHITE                                                                                        
                                            BrushHelpers.ReturnBrushFromHex("#2E2E2E")      // DARK GREY
                                        );

                                        RetBrushes.InfoStrings = new Tuple<string, string>("#FFFB00", "#46032F");
                                        RetBrushes.LogLineStrings = new Tuple<string, string>("#838383", "#2E2E2E");
                                        break;

                                    // OPTIONS --- WHITE FG AND DARK AQUA BG
                                    case string a when a.Contains("DEBUG"):
                                        fgBrush = BrushHelpers.ReturnBrushFromHex("#FFFFFF");   // White
                                        bgBrush = BrushHelpers.ReturnBrushFromHex("#025039");   // Dark Aqua

                                        // Debugs need to set the log line value too.
                                        RetBrushes.InfoBrushes = new Tuple<Brush, Brush>(fgBrush, bgBrush);
                                        RetBrushes.LogStringBrushes = new Tuple<Brush, Brush>(fgBrush, bgBrush);

                                        RetBrushes.InfoStrings = new Tuple<string, string>("#FFFFFF", "#025039");
                                        RetBrushes.LogLineStrings = new Tuple<string, string>("#FFFFFF", "#025039");
                                        break;

                                    // UNKNOWN --- YELLOW FG AND DARK CYAN BG
                                    default:
                                        fgBrush = BrushHelpers.ReturnBrushFromHex("#6A860C");
                                        bgBrush = BrushHelpers.ReturnBrushFromHex("#013236");

                                        RetBrushes.InfoBrushes = new Tuple<Brush, Brush>(fgBrush, bgBrush);
                                        RetBrushes.InfoStrings = new Tuple<string, string>("#6A860C", "#013236");

                                        fgBrush = BrushHelpers.ReturnBrushFromHex("#86460A");
                                        bgBrush = BrushHelpers.ReturnBrushFromHex("#CADF10");

                                        RetBrushes.LogStringBrushes = new Tuple<Brush, Brush>(fgBrush, bgBrush);
                                        RetBrushes.LogLineStrings = new Tuple<string, string>("#86460A", "#CADF10");

                                        break;
                                }
                                break;

                            // Calling Name String
                            case 2:
                                bgBrush = BrushHelpers.ReturnBrushForName(TextToFind);
                                fgBrush = BrushHelpers.ReturnBrushFromHex("#000000");

                                string bgString = BrushHelpers.ReturnBrushString(TextToFind);
                                if (bgString.StartsWith("#FF")) { bgString = bgString.Replace("#FF", "") + "00"; }
                                string fgString = "#000000";

                                // IF this is not a known calling type.
                                if (bgBrush == Brushes.Black)
                                { 
                                    bgBrush = BrushHelpers.ReturnBrushFromHex("#B60808", true);
                                    fgBrush = BrushHelpers.ReturnBrushFromHex("#C7E614");

                                    bgString = "#B60808";
                                    fgString = "#C7E614";
                                }

                                if (BrushHelpers.IsDark(bgBrush))
                                {
                                    fgBrush = BrushHelpers.ReturnBrushFromHex("#FFFFFF");
                                    fgString = "#FFFFFF";
                                }

                                // Set Caller Name Colors here.
                                RetBrushes.CallingBrushes = new Tuple<Brush, Brush>(fgBrush, bgBrush);
                                RetBrushes.CallingStrings = new Tuple<string, string>
                                (
                                    fgString,
                                    bgString
                                );
                                break;

                            // Log Line String
                            case 3:
                                if (RetBrushes.LogStringBrushes != null) { break; }

                                fgBrush = BrushHelpers.ReturnBrushFromHex("#FFFFFF");
                                bgBrush = BrushHelpers.ReturnBrushFromHex("#000000");

                                RetBrushes.LogStringBrushes = new Tuple<Brush, Brush>(fgBrush, bgBrush);
                                RetBrushes.LogLineStrings = new Tuple<string, string>("#FFFFFF", "#000000");
                                break;

                            // We never get here so no worries really.
                            default:
                                break;

                        }

                        // Bump up the index counter. Forgot to do this before and got stuck at 1 forever.
                        StartIndex += Index + 1;
                    }
                }
                catch { }
            }

            // And Finally return the brush set.
            return RetBrushes;
        }
    }

    /// <summary>
    /// Logger Class for AVEdit boxes.
    /// </summary>
    public class LogColorizerEdit : DocumentColorizingTransformer
    {
        // Base colorizer.
        private LogColorizer ColorBase;
        public LogColorizerEdit(LogColorizer ColorizerBase)
        {
            ColorizerBase.EditColorizer = this;
            ColorBase = ColorizerBase;
        }

        /// <summary>
        /// /// Returns a string of text to parse if we dont use RTF logging.
        /// </summary>
        /// <param name="Line">DocLine to parse text from.</param>
        private string GetTextToParse(DocumentLine Line)
        {
            ColorBase.StartOffset = Line.Offset;
            string LogLine = CurrentContext.Document.GetText(Line);

            return LogLine;
        }

        /// <summary>
        /// Colors the line.
        /// </summary>
        /// <param name="Line">DocLine to color</param>
        protected override void ColorizeLine(DocumentLine Line)
        {
            string LogLine = GetTextToParse(Line);
            if (!LogLine.StartsWith("[")) { return; }

            var RegexResults = ColorBase.ReturnTuples(LogLine);
            var BrushValues = ColorBase.ReturnBrushesOfLine(LogLine, RegexResults);

            for (int Counter = 0; Counter < RegexResults.Count; Counter++)
            {
                try
                {
                    int Index;
                    string TextToFind = RegexResults[Counter].Item1;
                    int StartIndex = RegexResults[Counter].Item2;

                    while ((Index = LogLine.IndexOf(TextToFind, StartIndex)) >= 0)
                    {
                        // Lambda to change colors.
                        ChangeLinePart(ColorBase.StartOffset + Index, ColorBase.StartOffset + Index + TextToFind.Length,
                        (LineElement) =>
                        {
                            switch (Counter)
                            {
                                case 0:
                                    LineElement.TextRunProperties.SetForegroundBrush(BrushValues.TimeBrushes.Item1);
                                    LineElement.TextRunProperties.SetBackgroundBrush(BrushValues.TimeBrushes.Item2);
                                    break;

                                case 1:
                                    LineElement.TextRunProperties.SetForegroundBrush(BrushValues.InfoBrushes.Item1);
                                    LineElement.TextRunProperties.SetBackgroundBrush(BrushValues.InfoBrushes.Item2);
                                    break;

                                case 2:
                                    LineElement.TextRunProperties.SetForegroundBrush(BrushValues.CallingBrushes.Item1);
                                    LineElement.TextRunProperties.SetBackgroundBrush(BrushValues.CallingBrushes.Item2);
                                    break;

                                case 3:
                                    LineElement.TextRunProperties.SetForegroundBrush(BrushValues.LogStringBrushes.Item1);
                                    LineElement.TextRunProperties.SetBackgroundBrush(BrushValues.LogStringBrushes.Item2);
                                    break;
                            }
                        });

                        // Bump up the index counter. Forgot to do this before and got stuck at 1 forever.
                        StartIndex += Index + 1;
                    }
                }
                catch { }
            }
        }
    }

    /// <summary>
    /// Logger class for HTML Lines.
    /// </summary>
    public class LogColorizerHTML
    {
        // Base colorizer.
        private LogColorizer ColorBase;
        public LogColorizerHTML(LogColorizer ColorizerBase)
        {
            ColorizerBase.HTMLColorizer = this;
            ColorBase = ColorizerBase;

            SetupRTFFile();     // Init the log file.
        }

        /// <summary>
        /// Setup the log file for HTML Writing.
        /// </summary>
        private void SetupRTFFile()
        {
            // Get DateTimeNow.
            var strTime = DateTime.Now.ToString("s");
            strTime = strTime.Replace("T", "_");
            strTime = strTime.Replace("-", "");

            // Replace the actual time with nothing since we need to call this file again.
            strTime = strTime.Split('_')[0];

            // Append in the VIN if it exists.
            string VINValue = GlobalObjects.ConfigInfo.VIN;
            string FileBaseName = "OEAutomationInvoker_" + VINValue + "_" + strTime;

            ColorBase.HTMLOutput += FileBaseName + ".html";
            ColorBase.InitLogLine = ColorBase.InitLogLine.Replace("LOG_TITLE", FileBaseName);

            // Write log file now.
            if (File.Exists(ColorBase.HTMLOutput)) { File.Delete(ColorBase.HTMLOutput); }
            File.AppendAllText(ColorBase.HTMLOutput, ColorBase.InitLogLine);
        }

        /// <summary>
        /// Writes the colorized log line to the HTML file.
        /// </summary>
        /// <param name="LogLine"></param>
        public void WriteToLogFile(string LogLine)
        {
            string ColorLine = ColorizeLogLine(LogLine);
            File.AppendAllText(ColorBase.HTMLOutput, ColorLine);
        }

        /// <summary>
        /// Write a string of HTML to our log file.
        /// </summary>
        /// <param name="LogLine">String of TEXT to format into HTML</param>
        public string ColorizeLogLine(string LogLine)
        {
            if (!LogLine.StartsWith("[")) { return LogLine; }

            var RegexResults = ColorBase.ReturnTuples(LogLine);
            var BrushValues = ColorBase.ReturnBrushesOfLine(LogLine, RegexResults);

            string NextLogLine = ColorBase.LogTemplate;
            for (int Counter = 0; Counter < RegexResults.Count; Counter++)
            {
                try
                {
                    int Index;
                    string TextToFind = RegexResults[Counter].Item1;
                    int StartIndex = RegexResults[Counter].Item2;
                    while ((Index = LogLine.IndexOf(TextToFind, StartIndex)) >= 0)
                    {
                        switch (Counter)
                        {
                            case 0:
                                NextLogLine = NextLogLine.Replace("TIME", TextToFind);
                                NextLogLine = NextLogLine.Replace("SEG_BG_0", BrushValues.TimeStrings.Item2);
                                NextLogLine = NextLogLine.Replace("SEG_FG_0", BrushValues.TimeStrings.Item1);

                                break;
                                
                            case 1:
                                NextLogLine = NextLogLine.Replace("TYPE", TextToFind);
                                NextLogLine = NextLogLine.Replace("SEG_BG_1", BrushValues.InfoStrings.Item2);
                                NextLogLine = NextLogLine.Replace("SEG_FG_1", BrushValues.InfoStrings.Item1);

                                break;

                            case 2:
                                NextLogLine = NextLogLine.Replace("CALLING_CLASS", TextToFind);
                                NextLogLine = NextLogLine.Replace("SEG_BG_2", BrushValues.CallingStrings.Item2);
                                NextLogLine = NextLogLine.Replace("SEG_FG_2", BrushValues.CallingStrings.Item1);

                                break;

                            case 3:
                                NextLogLine = NextLogLine.Replace("LOG_STRING", TextToFind);
                                NextLogLine = NextLogLine.Replace("SEG_BG_3", BrushValues.LogLineStrings.Item2);
                                NextLogLine = NextLogLine.Replace("SEG_FG_3", BrushValues.LogLineStrings.Item1);

                                break;
                        }

                        // Bump up the index counter. Forgot to do this before and got stuck at 1 forever.
                        StartIndex += Index + 1;
                    }
                }
                catch { }
            }

            return NextLogLine;
        }
    }
}
