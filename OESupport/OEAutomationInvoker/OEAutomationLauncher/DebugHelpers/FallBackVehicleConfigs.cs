﻿using OEAutomationLauncher.AutomationLauncher.ObjectClasses;
using OEAutomationLauncher.DebugHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.DebugHelpers
{
    public class FallBackConfigItem
    {
        public string NameOfVehicle { get; set; }
        public string JSON { get; set; }
        public string OEType { get; set; }
    }

    /// <summary>
    /// This class is just a bunch of default JSON values pulled from test tools. They allow sims to run.
    /// </summary>
    public class FallBackVehicleConfigs
    {
        public static List<FallBackConfigItem> AllConfigItems = new List<FallBackConfigItem>();
        public static FallBackConfigItem SelectedConfig;

        public FallBackVehicleConfigs()
        {
            TIS_FallBackJSON.AddTISItems();
            GDS2_FallBackJSON.AddGDS2Items();
            FJDS_FallBackJSON.AddFJDSItems();
            FDRS_FallBackJSON.AddFDRSItems();
            CONSULT_FallBackJson.AddCONSULTItems();

            // AllConfigItems = AllConfigItems.OrderBy(x => x.OEType).ToList();
        }

        public class TIS_FallBackJSON
        {
            public static string HIGHLANDER_2017 = FallBackItems.ITEM_2017_TOYOTA_HIGHLANDER;
            public static string YARIS_IA_2017 = FallBackItems.ITEM_2017_TOYOTA_YARIS_IA;
            public static string RX350_2020 = FallBackItems.ITEM_2020_LEXUS_RX350;
            public static string ES350_2015 = FallBackItems.ITEM_2015_LEXUS_ES350;

            public static void AddTISItems()
            {
                AllConfigItems.Add(new FallBackConfigItem { OEType = "TIS", NameOfVehicle = "2017_TOYOTA_HIGHLANDER", JSON = HIGHLANDER_2017 });
                AllConfigItems.Add(new FallBackConfigItem { OEType = "TIS", NameOfVehicle = "2018_TOYOTA_YARIS-IA", JSON = YARIS_IA_2017 });
                AllConfigItems.Add(new FallBackConfigItem { OEType = "TIS", NameOfVehicle = "2020_LEXUS_RX350", JSON = RX350_2020 });
                AllConfigItems.Add(new FallBackConfigItem { OEType = "TIS", NameOfVehicle = "2015_LEXUS_ES350", JSON = ES350_2015 });
            }
        }

        public class GDS2_FallBackJSON
        {
            public static string YUKON_2018 = FallBackItems.ITEM_2018_GMC_YUKON;
            public static string REGAL_TOUR_2018 = FallBackItems.ITEM_2018_BUICK_REGAL_TOUR;

            public static void AddGDS2Items()
            {
                AllConfigItems.Add(new FallBackConfigItem { OEType = "GDS2", NameOfVehicle = "2018_GMC_YUKON", JSON = YUKON_2018 });
                AllConfigItems.Add(new FallBackConfigItem { OEType = "GDS2", NameOfVehicle = "2018_BUIKC_REGAL-TOUR-X", JSON = REGAL_TOUR_2018 });
            }
        }

        public class FJDS_FallBackJSON
        {
            public static string FUSION_2017 = FallBackItems.ITEM_2017_FORD_FUSION;
            public static string F150_2019 = FallBackItems.ITEM_2019_FORD_F150;

            public static void AddFJDSItems()
            {
                AllConfigItems.Add(new FallBackConfigItem { OEType = "FJDS", NameOfVehicle = "2017_FORD_FUSION", JSON = FUSION_2017 });
                AllConfigItems.Add(new FallBackConfigItem { OEType = "FJDS", NameOfVehicle = "2019_FORD_F150", JSON = F150_2019 });
            }
        }

        public class FDRS_FallBackJSON
        {
            public static string RANGER_2020 = FallBackItems.ITEM_2020_FORD_RANGER;

            public static void AddFDRSItems()
            {
                AllConfigItems.Add(new FallBackConfigItem { OEType = "FDRS", NameOfVehicle = "2020_FORD_RANGER", JSON = RANGER_2020 });
            }
        }

        public class CONSULT_FallBackJson
        {
            public static string Q50_2018 = FallBackItems.ITEM_2018_INFINITI_Q50;

            public static void AddCONSULTItems()
            {
                AllConfigItems.Add(new FallBackConfigItem { OEType = "CP3", NameOfVehicle = "2018_INFINITI_Q50", JSON = Q50_2018 });
            }
        }
    }
}
