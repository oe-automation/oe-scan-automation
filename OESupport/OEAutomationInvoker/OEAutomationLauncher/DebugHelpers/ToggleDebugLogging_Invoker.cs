﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEAutomationLauncher.AutomationLauncher.DebugHelpers
{
    public class ToggleDebugLogging_Invoker
    {
        public static List<string> ToggleLogging(bool LoggingOn)
        {
            GlobalObjects.DebugHelper.WriteDebugLog("TOGGLING DEBUG LOGGING: " + LoggingOn.ToString());

            try { SetRegKeyValue("Drew Technologies Inc. - CarDAQ-Plus 3", LoggingOn); }
            catch (Exception setEx)
            { 
                GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SET CDP3 LOGGING STATE!!!");
                GlobalObjects.DebugHelper.WriteDebugLog(setEx);
            }

            try { SetRegKeyValue("Drew Technologies Inc. - CarDAQ-Plus 4", LoggingOn); }
            catch (Exception setEx)
            { 
                GlobalObjects.DebugHelper.WriteDebugLog("COULD NOT SET CDP4 LOGGING STATE!!!");
                GlobalObjects.DebugHelper.WriteDebugLog(setEx);
            }

            var AllDevies = new List<string>();
            RegistryKey PassThruSupportKey = Registry.CurrentUser.OpenSubKey("Software\\PassThruSupport.04.04");
            Console.WriteLine("PASS THRU KEYS");

            // MAke sure we're not null on devices here. 
            if (PassThruSupportKey == null) { return new List<string>() { "FAILED TO FIND DEVICES!!!" }; }

            foreach (var PTDevice in PassThruSupportKey.GetSubKeyNames())
            {
                if (!PTDevice.Contains("Drew Technologies")) { continue; }
                GlobalObjects.DebugHelper.WriteDebugLog("PT DEVICE ENTRY: " + PTDevice);
                AllDevies.Add(PTDevice);
            }

            return AllDevies;
        }

        private static void SetRegKeyValue(string NameOfDevice, bool DebugOn)
        {
            RegistryKey PassThruSupportKey = Registry.CurrentUser.OpenSubKey("Software\\PassThruSupport.04.04", true);
            using (PassThruSupportKey.CreateSubKey(NameOfDevice))
            {
                var NewDeviceKey = PassThruSupportKey.OpenSubKey(NameOfDevice, true);

                if (DebugOn) NewDeviceKey.SetValue("DebugEnable", 1);
                if (!DebugOn) NewDeviceKey.SetValue("DebugEnable", 0);
            };
        }

    }
}
