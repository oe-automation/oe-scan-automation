﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootloaderAPISpoof.APIObjectModels
{
    public class VehicleInfoModel
    {
        public string VIN { get; set; }
        public string Year { get; set; }
        public string OEM { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Engine { get; set; }
        public string Error { get; set; }
        public string Voltage { get; set; }
        public string Trim { get; set; }
        public List<string> AdasModules { get; set; }
        public string IsAdas { get; set; }
        public string IsNotAdas { get; set; }
        public string Odometer { get; set; }

        public VehicleInfoModel(bool USE_DEFAULT = true)
        {
            if (!USE_DEFAULT) { return; }

            this.VIN            = "5TDJZRFH5HS394292";
            this.Year           = "2017";
            this.OEM            = "";
            this.Make           = "Toyota";
            this.Model          = "Highlander";
            this.Engine         = "3.5L V6 295hp 263ft. lbs.";
            this.Error          = "null";
            this.Voltage        = "16.70";
            this.Trim           = "null";
            this.AdasModules    = new List<string>() { "*360 Camera View", "*Adaptive Cruise Control", "*Collision Braking", "*Blind Spot Detection", "*Lane Keep Assist", "*Rear View", "*Park Assist", "*Rear Cross Traffic", "*Night Vision" };
            this.IsAdas         = true.ToString();
            this.IsNotAdas      = false.ToString();
            this.Odometer       = "";
        }
    }
}
