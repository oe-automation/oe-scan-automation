﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootloaderAPISpoof.APIObjectModels
{
    public class VinNumber
    {
        public string VIN_NUMBER { get; set; }

        public VinNumber(string VIN)
        {
            this.VIN_NUMBER = VIN;
        }
    }
}
