﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootloaderAPISpoof.APIObjectModels
{
    public class UserInfoObject
    {
        public string NameFirst { get; set; }
        public string NameLast { get; set; }
        public string NameFull { get; set; }
        public string NameFullNoSpaces { get; set; }
        public string Image { get; set; }
        public string IsNewUser { get; set; }
        public string IsLoaded { get; set; }
        public string IsLoggedIn { get; set; }
        public string Email { get; set; }
        public string AuthToken { get; set; }
        public string Country { get; set; }

        public UserInfoObject(bool DEFAULT_INFO = true)
        {
            if (!DEFAULT_INFO) { return; }

            this.NameFirst = "Zachary";
            this.NameLast = "Walsh";
            this.NameFull = "Zachary Walsh";
            this.NameFullNoSpaces = "ZacharyWalsh";
            this.Image = "images/UserWindow/user-shadow.png";
            this.IsNewUser = false.ToString();
            this.IsLoaded = true.ToString();
            this.Email = "zachary.walsh@opusivs.com";
            this.AuthToken = "a0528b133ef67f6ecfe2245e2dc0cafac00a8b353f4ca5fda9dc4a63161e273b";
            this.Country = "United States";
        }

        public string ReturnStringVersion()
        {
            return "UserInfoObject";
        }
    }
}
