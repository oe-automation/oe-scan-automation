﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootloaderAPISpoof.APIObjectModels
{
    public class SpinnerObject
    {
        public string SPINNER_CONTENT { get; set; }
        public bool IS_OPEN { get; set; }

        public bool RAN_OK { get; set; }
        public string RESPONSE { get; set; }

        public SpinnerObject(string Info)
        {
            this.SPINNER_CONTENT = Info;
            this.IS_OPEN = true;
            this.RESPONSE = "Success";
            this.RAN_OK = true;

            BootloaderAPI.ReturnObjects.AllSpinners.Add(this);
        }

        public static string ReturnSpinnerInfo()
        {
            int SpinnerCount = BootloaderAPI.ReturnObjects.AllSpinners.Count - 1;
            string Value = BootloaderAPI.ReturnObjects.AllSpinners[SpinnerCount].SPINNER_CONTENT;

            return "Success, " + Value + ", INDEX: " + SpinnerCount;
        }
    }
}
