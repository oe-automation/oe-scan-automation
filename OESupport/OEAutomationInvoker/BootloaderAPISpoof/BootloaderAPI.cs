﻿using BootloaderAPISpoof.APIHelpers;
using BootloaderAPISpoof.APIObjectModels;
using System;
using System.Collections.Generic;

namespace BootloaderAPISpoof
{
    public class BootloaderAPI
    {
        public static DebuggingHelper DebugHelper;

        public class ReturnObjects
        {
            public static VinNumber VIN;
            public static VehicleInfoModel VehicleConfig = new VehicleInfoModel();
            public static List<SpinnerObject> AllSpinners = new List<SpinnerObject>();
            public static UserInfoObject UserInfo = new UserInfoObject();
        }

        public static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetWindowSize(85, 25);

            string FilePath = "";
            string[] AllFileLines;

            foreach (var ArgValue in Environment.GetCommandLineArgs())
            {
                if (ArgValue.Length != 17) { continue; }
                ReturnObjects.VIN = new VinNumber(ArgValue.ToUpper());
                break;
            }

            if (ReturnObjects.VIN == null) { ReturnObjects.VIN = new VinNumber("UNKNOWN"); }

            Console.WriteLine("STARTING LOCALHOST BOOTLOADER SPOOF API");

            DebugHelper = new DebuggingHelper();
            DebugHelper.WriteDebugLog("STARTING LOCALHOST BOOTLOADER SPOOF API");

            SeperateConsole();
            APITasker.HttpListener(APIEndpoints.AllEndPoints);
        }

        public static void SeperateConsole()
        {
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine();
            int WindowWidth = Console.WindowWidth;
            Console.ForegroundColor = ConsoleColor.White;
            for (int Count = 0; Count < WindowWidth; Count++) { Console.Write("="); }
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("\n");

            DebugHelper.WriteDebugLog("--------------------------------------------");
        }
    }
}
