﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BootloaderAPISpoof
{
    public class DebuggingHelper
    {
        public string DebugFileName { get; set; }

        // Pulled out two useless log dirs. One doesn't get used anymore and the other is for 
        // Desktop Dev work and there's no reason to make it on devices.
        private static List<string> AllDebugFolders = new List<string>
        {
             @"C:\Program Files (x86)\OESupport\OEDebugging\OptionsDebug",
             // @"C:\Program Files (x86)\OESupport\OEDebugging\APICallDebug",       REMOVED SINCE NO LONGER USED.
             @"C:\Program Files (x86)\OESupport\OEDebugging\WinAutomationDebug",
             // @"C:\Program Files (x86)\OESupport\OEDebugging\SpoofAPIDebug",      REMOVED AND ONLY SETUP WHEN SPOOF CALLED
             @"C:\Program Files (x86)\OESupport\OEDebugging\AutomationInvokerDebug",
             @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle",
             @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle\CustomViewConfigs"
        };

        private string OptionsDebug = AllDebugFolders[0];
        private string WinAutoDebug = AllDebugFolders[1];
        private string InvokerDebug = AllDebugFolders[2];
        private string OEOptionsDir = AllDebugFolders[3];
        private string OEViewerInfo = AllDebugFolders[4];

        public DebuggingHelper()
        {
            GenerateLogDirs();                      // Checks for output dirs and makes sure they are setup ok.
            DebugFileName = GetDebugFileName();     // Gets the name of our new debug file.
            SetupDebugFile();                       // Check to see if weve done an OE Scan today or not.
        }

        /// <summary>
        /// Generates log dirs for all required file locations.
        /// </summary>
        public void GenerateLogDirs(bool CleanOutAll = false)
        {
            foreach (var Dir in AllDebugFolders)
            {
                if (!Directory.Exists(Dir)) { Directory.CreateDirectory(Dir); }

                var FilesList = Directory.GetFiles(Dir);
                if (!Debugger.IsAttached)
                {
                    if (CleanOutAll) { foreach (var file in FilesList) { File.Delete(file); } }
                    else
                    {
                        if (!Dir.Contains("CurrentVehicle")) { continue; }
                        foreach (var file in FilesList) { File.Delete(file); }
                    }
                }
            }
        }

        /// <summary>
        /// Gets name of calling Method and uses it to append into the debug log file lines.
        /// </summary>
        /// <returns></returns>
        private static string NameOfCallingClass()
        {
            string fullName;
            Type declaringType;
            int skipFrames = 2;
            do
            {
                MethodBase method = new StackFrame(skipFrames, false).GetMethod();
                declaringType = method.DeclaringType;
                if (declaringType == null)
                {
                    return method.Name;
                }
                skipFrames++;
                fullName = declaringType.FullName;
            }
            while (declaringType.Module.Name.Equals("mscorlib.dll", StringComparison.OrdinalIgnoreCase));

            var fullNameSplit = fullName.Split('.');
            fullName = fullNameSplit[fullNameSplit.Length - 1];

            return fullName;
        }

        /// <summary>
        /// Get the current date and format it for the name of our file.
        /// </summary> 
        private string GetDebugFileName()
        {
            // Get DateTimeNow.
            var strTime = DateTime.Now.ToString("s");
            strTime = strTime.Replace("T", "_");
            strTime = strTime.Replace("-", "");

            // Replace the actual time with nothing since we need to call this file again.
            strTime = strTime.Split('_')[0];

            // Append in the VIN if it exists.
            string VINValue = BootloaderAPI.ReturnObjects.VIN.VIN_NUMBER;

            // Combine the date and prefix and return it.
            string InvokerAPI = "OEAutomation_API_Logging_" + VINValue + "_" + strTime + ".txt";
            string InvokerFile = "OEAutomationInvoker_" + VINValue + "_" + strTime + ".txt";

            DebugFileName = InvokerDebug + "\\" + InvokerFile; ;

            if (!Debugger.IsAttached)
            {
                if (File.Exists(DebugFileName)) { File.Delete(DebugFileName); }
            }

            return DebugFileName;
        }

        /// <summary>
        /// Setup the file and make a new one if needed.
        /// </summary>
        /// <param name="initNew">Force create a new file or not.</param>
        private void SetupDebugFile(bool initNew = false)
        {
            // Base timestamp message.
            string TimeString = "OE AUTOMATION INVOKER DEBUGGING: " + DateTime.Now.ToString("s") + "\n";
            string APIString = "OE AUTOMATION INVOKER API DEBUGGING: " + DateTime.Now.ToString("s") + "\n";

            // Check Spoof DIR.
            if (!Directory.Exists(InvokerDebug)) { Directory.CreateDirectory(InvokerDebug); }


            // Clean out if we have the debugger hooked in.
            if (Debugger.IsAttached && File.Exists(DebugFileName)) { File.Delete(DebugFileName); }

            // Check for existing file. If not setup a new one with timestamp headding.
            if (!File.Exists(DebugFileName)) { File.WriteAllText(DebugFileName, TimeString); }
            else
            {
                // Setup a new file if we have the flag set.
                if (initNew) { File.AppendAllText(DebugFileName, TimeString); return; }

                // If the file exists, see if it has anything in it. 
                var sizeOfFile = File.ReadAllBytes(DebugFileName).Length;
                if (sizeOfFile > 0)
                {
                    File.AppendAllText(DebugFileName, "\n----------------------------------\n");
                    File.AppendAllText(DebugFileName, TimeString);
                }
            }
        }

        /// <summary>
        /// Write Option Parse debug info out to here.
        /// </summary>
        /// <param name="DebugInfo">String of information to log into the debug file.</param>
        /// <param name="InfoType">Specify a type of info to pass into the log. INFO, DEBUG, ERR, LOG etc.</param>
        public void WriteDebugLog(string DebugInfo, string InfoType = "LOG")
        {
            // Get the current time for the logging info.
            string timeNow = DateTime.Now.ToString("T");
            string CallingName = NameOfCallingClass();
            string writeThis = "[" + timeNow + "] ::: [" + CallingName + "] ::: " + DebugInfo + "\n";

            // Write the formatted logging info out.
            File.AppendAllText(DebugFileName, writeThis);
        }

        /// <summary>
        /// Writes a seperating bar into the console. 
        /// THIS CAN NOT BE CALLED UNLESS THE DEBUGGER IN THE GLBOAL OBJECT IS CONFIGURED.
        /// IT WILL THROW A NULL REF AND CRASH OUT.
        /// Edit: so try/catch exists. Nvm ignroe me.
        /// </summary>
        public void SeperateConsole()
        {
            try
            {
                Console.BackgroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine();
                int WindowWidth = Console.WindowWidth;
                Console.ForegroundColor = ConsoleColor.White;
                for (int Count = 0; Count < WindowWidth; Count++) { Console.Write("="); }
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.WriteLine("\n");

                WriteDebugLog("--------------------------------------------");
            }
            catch { }
        }
    }
}
