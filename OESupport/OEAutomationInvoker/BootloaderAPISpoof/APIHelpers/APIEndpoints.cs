﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootloaderAPISpoof.APIHelpers
{
    public class APIEndpoints
    {
        public static string[] AllEndPoints = new string[]
        {
            //Base Page - Lists all possible API Calls
            "http://localhost:15000/DrewTechDataSvc/",
                
                // API Endpoints here
                "http://localhost:15000/DrewTechDataSvc/PleaseWait/",
                "http://localhost:15000/DrewTechDataSvc/VIN/",
                "http://localhost:15000/DrewTechDataSvc/GetUserInfo/",
                "http://localhost:15000/DrewTechDataSvc/DeviceName/",
                "http://localhost:15000/DrewTechDataSvc/SerialNumber/",
                "http://localhost:15000/DrewTechDataSvc/VehicleInfo/",
        };
    }
}
