﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BootloaderAPISpoof.APIHelpers
{
    public class APITasker
    {
        public static async void HttpListener(string[] prefixes)
        {
            if (prefixes == null || prefixes.Length == 0)
                throw new ArgumentException("Prefixes needed");

            HttpListener APIHost = new HttpListener();
            foreach (string s in prefixes) { APIHost.Prefixes.Add(s); }

            if (!APIHost.IsListening) { APIHost.Start(); }
            while (true)
            {
                //Console.WriteLine("Listening...");
                HttpListenerContext context = APIHost.GetContext();
                HttpListenerRequest request = context.Request;
                HttpListenerResponse response = context.Response;

                if (request.HttpMethod == "OPTIONS")
                {
                    response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");
                    response.AddHeader("Access-Control-Allow-Methods", "GET, POST");
                    response.AddHeader("Access-Control-Max-Age", "1728000");
                }
                response.AppendHeader("Access-Control-Allow-Origin", "*");

                // Construct a response.
                bool RootRequest = request.Url.AbsolutePath.Equals("/") || request.Url.AbsolutePath.Equals("/favicon.ico");
                if (RootRequest)
                {
                    byte[] RootRequestBytes = Encoding.ASCII.GetBytes("ROOT DIR HIT. THERE IS NOTHING HERE YET\nHopefully this will end up being a nice API doc for the server running here.");
                    System.IO.Stream output = response.OutputStream;
                    output.Write(RootRequestBytes, 0, RootRequestBytes.Length);
                    try { output.Close(); }
                    catch { }
                }

                if (!RootRequest)
                {
                    bool Checkup = request.Url.AbsolutePath.Contains("latency");
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    if (!Checkup) Console.WriteLine(request.Url.AbsoluteUri);
                    //Console.BackgroundColor = ConsoleColor.Black;

                    // Make a JSON String object from the item we requested to send out.
                    string JsonItem = JsonConvert.SerializeObject(new ResponseTemplate(request.Url.AbsolutePath).Results); 
                    
                    //Console.BackgroundColor = ConsoleColor.DarkBlue;
                    if (!Checkup)
                    {
                        BootloaderAPI.DebugHelper.WriteDebugLog("JSON: " + JsonItem, "JSON");
                        Console.WriteLine("JSON : " + JsonItem + " \n");
                    }

                    //Console.BackgroundColor = ConsoleColor.Black;

                    byte[] ResponseBytes = Encoding.ASCII.GetBytes(JsonItem);

                    //Get a response stream and write the response to it.
                    System.IO.Stream output = response.OutputStream;
                    output.Write(ResponseBytes, 0, ResponseBytes.Length);
                    output.Close();

                    BootloaderAPI.DebugHelper.SeperateConsole();
                }

                //Close it up when completed.

            }
        }
    }
}
