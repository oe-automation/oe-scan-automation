﻿using BootloaderAPISpoof.APIObjectModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BootloaderAPISpoof.APIHelpers
{
    public class ResponseTemplate
    {
        public string Command { get; set; }
        public string RanOK { get; set; }

        public List<string> Results = new List<string>();

        public ResponseTemplate(string InputCommand)
        {
            string VIN = BootloaderAPI.ReturnObjects.VIN.VIN_NUMBER;
            string[] CommandSplit = InputCommand.Split('/');
            Command = CommandSplit[2];
            try
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                BootloaderAPI.DebugHelper.WriteDebugLog("Command: " + Command, "TYPE");
                Console.WriteLine("Command: " + Command);

                if (Command.Contains("VIN")) { Results.Add(VIN); }                               // VIN Number
                if (Command.Contains("DeviceName")) { Results.Add("CDP00102"); }                 // Device Name
                if (Command.Contains("SerialNumber")) { Results.Add("00102"); }                  // Serial Number
                if (Command.Contains("SessionID")) { Results.Add(Guid.NewGuid().ToString()); }   // Session ID Request
                if (Command.Contains("CreatePost")) { Results.Add("DUMMY_UPLOAD_OK"); }          // Upload Scan
                if (Command.Contains("PleaseWait"))
                {
                    var Spinner = new SpinnerObject(CommandSplit[3]);
                    BootloaderAPI.ReturnObjects.AllSpinners.Add(Spinner);

                    Results.Add(Spinner.RESPONSE);
                    Results.Add(Spinner.SPINNER_CONTENT);
                }                                           // Spinner Setup
                if (Command.Contains("GetUserInfo"))
                {
                    var Info = BootloaderAPI.ReturnObjects.UserInfo;
                    Results.Add(Info.NameFirst);
                    Results.Add(Info.NameLast);
                    Results.Add(Info.NameFull);
                    Results.Add(Info.NameFullNoSpaces);
                    Results.Add(Info.Image);
                    Results.Add(Info.IsNewUser);
                    Results.Add(Info.IsLoaded);
                    Results.Add(Info.IsLoggedIn);
                    Results.Add(Info.Email);
                    Results.Add(Info.AuthToken);
                    Results.Add(Info.Country);
                }                                          // User Info
                if (Command.Contains("VehicleInfo"))
                {
                    var VehicleInfo = BootloaderAPI.ReturnObjects.VehicleConfig;

                    Results.Add(VehicleInfo.VIN);
                    Results.Add(VehicleInfo.Year);
                    Results.Add(VehicleInfo.OEM);
                    Results.Add(VehicleInfo.Make);
                    Results.Add(VehicleInfo.Model);
                    Results.Add(VehicleInfo.Engine);
                    Results.Add(VehicleInfo.Error);
                    Results.Add(VehicleInfo.Voltage);
                    Results.Add(VehicleInfo.Trim);
                    Results.Add(JsonConvert.SerializeObject(VehicleInfo.AdasModules));
                    Results.Add(VehicleInfo.IsAdas);
                    Results.Add(VehicleInfo.IsNotAdas);
                    Results.Add(VehicleInfo.Odometer);
                }                                          // Vehicle Info

                RanOK = "true";
                Console.ForegroundColor = ConsoleColor.White;
            }

            catch (Exception e)
            {
                RanOK = "false";
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.Message);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
