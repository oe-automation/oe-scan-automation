﻿using System.Collections.Generic;

namespace IVSLogConverter_GUI.Objects
{
    public class LogZipFileObject
    {
        public int ParserThread { get; set; }
        public int IndexOfFile { get; set; }


        public string NameOfZip { get; set; }
        public string NoPathName { get; set; }


        public long SizeOfZip { get; set; }
        public long SizeOfExtractedDir { get; set; }

        public long SizeOfZipDownloaded { get; set; }
        public long SizeOfCurrentlyExtracted { get; set; }

        public string SizeOfZipString { get; set; }
        public string SizeOfExtractedString { get; set; }


        public string TimeUploaded { get; set; }


        public double OperationProgress { get; set; }
        public string OperationProgressString { get; set; }
        public string OperationProgressValueOnly { get; set; }


        public bool IsDownloaded { get; set; }
        public bool DownloadFailed { get; set; }


        public bool IsExtracted { get; set; }
        public bool ExtractionFailed { get; set; }
        public string DecompressedPath { get; set; }
        public int ExtractedItems { get; set; }
        public int ExtractedNow { get; set; }

        public List<System.Tuple<string, string>> AllFilesToConvert = new List<System.Tuple<string, string>>();
    }
}
