﻿using Amazon.Runtime.Internal.Util;
using IVSLogConverter_GUI.LogicalHelpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static IVSLogConverter_GUI.GlobalObjects;

namespace IVSLogConverter_GUI.Objects
{
    public class LogZipFilesCollection
    {
        public List<LogZipFileObject> AllDownloadedZipFiles = new List<LogZipFileObject>();

        public List<LogZipFileObject> AllLogFileZips = new List<LogZipFileObject>();
        public List<List<LogZipFileObject>> ZipFileCollectionSets = new List<List<LogZipFileObject>>();

        public List<string> ListOfParsedFiles = new List<string>();
        public List<ZipFileOperator> ListOfFileOperators = new List<ZipFileOperator>();

        public List<LogZipFileObject> ListOfExtracted = new List<LogZipFileObject>();

        public LogZipFilesCollection() { }

        public void SetupDownloaders(AWSFilePuller ZipGrabber)
        {
            MainLogger.WriteToDebugLog("ZIP PARSER SETUP NOW");

            GUIZipObjects = new List<LogZipFileSet>();
            for (int ThreadNumber = 0; ThreadNumber < ZipFileCollectionSets.Count; ThreadNumber++)
            {
                string ThreadName = "ZipFileThread_SET" + ThreadNumber;
                var Debugger = new DebugLogHelper(ThreadName);

                try
                {
                    var NextParser = new ZipFileOperator(
                        ZipFileCollectionSets[ThreadNumber],
                        ZipGrabber,
                        Debugger,
                        ThreadNumber);

                    // Add this to a parser list and to our new list of GUI shit.
                    ListOfFileOperators.Add(NextParser);
                    GUIZipObjects.Add(new LogZipFileSet(ThreadName, ZipFileCollectionSets[ThreadNumber]));

                    // Update ItemsSource.
                    MainWindowView.Dispatcher.Invoke(() =>
                    {
                        MainWindowView.AllParsersListView.ItemsSource = GUIZipObjects;
                        MainWindowView.AllParsersListView.Items.Refresh();
                    });

                    List<string> ListOfLogZips = new List<string>();
                    foreach (var ZipItem in GUIZipObjects[ThreadNumber].SetOfZips) { ListOfLogZips.Add(ZipItem.NoPathName); }

                    string LogItemsString = string.Join(",", ListOfLogZips);
                    MainLogger.WriteToDebugLog("PARSER #" + ThreadNumber + " READY! (" + LogItemsString + ")");
                }

                catch (Exception ParseEx)
                {
                    Debugger.WriteToInstanceAndGlobal("FAILED TO WRITE INFO FOR OUR PARSER AND CAN NOT MAKE A LOG PARSING OBJECT!");
                    Debugger.WriteErrorToLog(ParseEx, true, ThreadNumber.ToString());
                }
            }

            // Setup the zip collections with parser indicies now.
            foreach (var ZipFileOperator in GUIZipObjects)
            {
                bool AllDownloaded = true;
                long SizeOfAllZips = 0;

                foreach (var ZipFile in ZipFileOperator.SetOfZips)
                {
                    int IndexOfSet = GUIZipObjects.IndexOf(ZipFileOperator);
                    ZipFile.ParserThread = IndexOfSet;
                    if (!ZipFile.IsDownloaded) { AllDownloaded = false; }
                    SizeOfAllZips += ZipFile.SizeOfZip;
                }

                if (AllDownloaded) { ZipFileOperator.StatusOfSet = "Waiting To Extract"; }
                ZipFileOperator.SizeLong = SizeOfAllZips;
            }

            MainLogger.WriteToDebugLog("SETTING UP PARALLEL EXECUTION NOW");
        }
        public void StartDownloaders()
        {
            Stopwatch timeThis = new Stopwatch();
            timeThis.Start();

            MainLogger.WriteToDebugLog("THREADS STARTING NOW AND A DIAGNOSTIC STOPWATCH HAS BEEN KICKED OFF!");
            MainLogger.WriteToDebugLog("CHECK THE DIR OF LOGCONVERSIONLOGS IN THE APPDATA LOCAL DIR FOR PER THREAD INFO");
            MainLogger.WriteToDebugLog("--------------------------------------------------------------------------------");

            Parallel.ForEach(ListOfFileOperators, Options, (ParserObject) =>
            {
                ParserObject.Logger.WriteToDebugLog("KICKING OFF THREAD " + ParserObject.ThreadSetNumber);
                ParserObject.PullDownAllZipFiles();
            });

            timeThis.Stop();
            MainLogger.WriteToDebugLog("ALL THREADS DONE! OPERATION TOOK: " + timeThis.Elapsed.ToString("c"));
            MainLogger.WriteToDebugLog("--------------------------------------------------------------------------------");
        }

        public void SetupDecompressors()
        {
            GUIHelpers.ParsersCompleted = 0;
            GUIHelpers.SizeOfZipLong = 0;

            MainLogger.WriteToDebugLog("RESETTING ZIP FILE OBJECTS IN PREPERATION FOR PARSING");
            for (int ThreadIndex = 0; ThreadIndex < GUIZipObjects.Count; ThreadIndex++)
            {
                var CurrentSet = GUIZipObjects[ThreadIndex];
                CurrentSet.NumberOfZips = CurrentSet.SetOfZips.Count;
                CurrentSet.NumberOfZipsLeft = CurrentSet.NumberOfZips;
                CurrentSet.SizeLong = 0;

                for (int FileSetIndex = 0; FileSetIndex < CurrentSet.SetOfZips.Count; FileSetIndex++)
                {
                    // File to operate on.
                    var CurrentFile = CurrentSet.SetOfZips[FileSetIndex];

                    // Setup output dir name.
                    string NameNoExt = CurrentFile.NoPathName.Split('.')[0];
                    CurrentFile.DecompressedPath = UnzippedOutputDir + NameNoExt;

                    // Basic status.
                    CurrentFile.OperationProgressString = "Waiting to Extract";
                    CurrentFile.OperationProgress = 0;
                    CurrentFile.OperationProgressValueOnly = "N/A";
                    CurrentFile.IsExtracted = Directory.Exists(CurrentFile.DecompressedPath);

                    // Remove if force remove old.
                    if (Directory.Exists(CurrentFile.DecompressedPath) && RemoveOldFiles) { Directory.Delete(CurrentFile.DecompressedPath, true); }
                    if (!Directory.Exists(CurrentFile.DecompressedPath)) { Directory.CreateDirectory(CurrentFile.DecompressedPath); }
                }
            }
            MainLogger.WriteToDebugLog("RESETTING ZIP FILE OBJECTS OK! READY TO UNZIP");
        }

        public void StartDecompressors()
        {
            Stopwatch timeThis = new Stopwatch();
            timeThis.Start();

            MainLogger.WriteToDebugLog("THREADS STARTING NOW AND A DIAGNOSTIC STOPWATCH HAS BEEN KICKED OFF!");
            MainLogger.WriteToDebugLog("CHECK THE DIR OF LOGUNZIPPING IN THE APPDATA LOCAL DIR FOR PER THREAD INFO");
            MainLogger.WriteToDebugLog("--------------------------------------------------------------------------------");

            Options.MaxDegreeOfParallelism = MaxParallelThreads * 2;
            Parallel.ForEach(ListOfFileOperators, Options, (ParserObject) =>
            {
                ParserObject.Logger.WriteToDebugLog("KICKING OFF THREAD " + ParserObject.ThreadSetNumber);
                ParserObject.ExtractAllZipFiles();
            });

            timeThis.Stop();
            MainLogger.WriteToDebugLog("ALL THREADS DONE! OPERATION TOOK: " + timeThis.Elapsed.ToString("c"));
            MainLogger.WriteToDebugLog("--------------------------------------------------------------------------------");
        }
    }
}
