﻿using IVSLogConverter_GUI.LogicHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static IVSLogConverter_GUI.GlobalObjects;

namespace IVSLogConverter_GUI.Objects
{
    public class GUIValueHelpers
    {
        private MainWindow MainWindow;
        public GUIValueHelpers(MainWindow main) 
        {
            // Hold this to invoke onto for all GUI changes.
            MainWindow = main;
            MainWindowView = main;

            // Zero out the UI Elements here.
            TotalZipCount = 0;
            SizeOfZipLong = 0;
            AllParsersCount = 0;
        }

        #region Zip File Stats

        // Zip File Count
        private int _totalZipCount = 0;
        public int TotalZipCount
        {
            get { return _totalZipCount; }
            set
            {
                _totalZipCount = value;
                MainWindow.Dispatcher.Invoke(() =>
                {
                    string NewTextValue = value + " Zip Files Found";
                    if (_totalZipCount == 0) { NewTextValue = "No Zip Files Found"; }
                    MainWindow.GUI_ZIP_COUNT_TOTAL.Text = NewTextValue;
                });
            }
        }

        // Zip File Count
        public int ConvertableExtractedFiles = 0;
        private int _totalExtractedFileCount = 0;
        public int TotalExtractedFileCount
        {
            get { return _totalExtractedFileCount; }
            set
            {
                _totalExtractedFileCount = value;
                MainWindow.Dispatcher.Invoke(() =>
                {
                    string NewTextValue = value + " Files Extracted (Convertable: " + ConvertableExtractedFiles + ")";
                    if (_totalExtractedFileCount == 0) { NewTextValue = "No Extracted Files Found"; }
                    MainWindow.GUI_ZIP_COUNT_TOTAL.Text = NewTextValue;
                });
            }
        }


        // Size of All Zips So Far
        private string _sizeOfZipsString = "";
        private long _sizeOfZipLong = 0;
        public long SizeOfZipLong
        {
            get { return _sizeOfZipLong; }
            set
            {
                _sizeOfZipLong = value;
                _sizeOfZipsString  = FileSizeHelpers.SizeSuffix(_sizeOfZipLong, 2);

                MainWindow.Dispatcher.Invoke(() =>
                {
                    string NewTextValue = "Downloaded " + _sizeOfZipsString;
                    if (AllZipFiles != null)
                    {
                      NewTextValue += " (" + AllZipFiles.AllDownloadedZipFiles.Count 
                                   + " of " + _totalZipCount + ")";
                    }

                    if (_sizeOfZipLong == 0) { NewTextValue = "No Zip Files Downloaded"; }
                    MainWindow.GUI_ZIP_SIZE_TOTAL.Text = NewTextValue;
                });
            }
        }

        // Number of parsers to setup.
        private int _appParsersCount = 0;
        public int AllParsersCount
        {
            get { return _appParsersCount; }
            set
            {
                _appParsersCount = value;
                MainWindow.Dispatcher.Invoke(() =>
                {
                    string NewTextValue = value + " Parsers Total";
                    if (value == 0) { NewTextValue = "No Parsers Setup"; }
                    MainWindow.GUI_NUMBER_OF_PARSERS.Text = NewTextValue;
                });
            }
        }

        // Remaining Parsers Value
        private int _completedParsers = 0;
        public int ParsersCompleted
        {
            get { return _completedParsers; }
            set
            {
                _completedParsers = value;
                MainWindow.Dispatcher.Invoke(() =>
                {
                    string NewTextValue = _completedParsers + " of " + _appParsersCount + " File Operators Done";
                    MainWindow.GUI_NUMBER_OF_PARSERS.Text = NewTextValue;
                });
            }
        }

        // Size of All Unzipped Files
        private string _sizeOfExtractedString = "";
        private long _sizeOfExtractedLong = 0;
        public long SizeOfExtractedLong
        {
            get { return _sizeOfExtractedLong; }
            set
            {
                _sizeOfExtractedLong = value;
                _sizeOfExtractedString = FileSizeHelpers.SizeSuffix(_sizeOfExtractedLong, 2);

                MainWindow.Dispatcher.Invoke(() =>
                {
                    string NewTextValue = "Extracted " + _sizeOfExtractedString;
                    if (AllZipFiles != null)
                    {
                        NewTextValue += " (" + AllZipFiles.ListOfExtracted.Count
                                     + " of " + _totalZipCount + ")";
                    }

                    if (_sizeOfExtractedLong == 0) { NewTextValue = "No Zip Files Extracted"; }
                    MainWindow.GUI_ZIP_SIZE_TOTAL.Text = NewTextValue;
                });
            }
        }
        #endregion
    }
}
