﻿using IVSLogConverter_GUI.LogicHelpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IVSLogConverter_GUI.Objects
{
    public class LogZipFileSet
    {
        public string ParserName { get; set; }

        public string TotalSize { get; set; }
        public long SizeLong { get; set; }

        public int NumberOfZips { get; set; }
        public int NumberOfZipsLeft { get; set; }
        public string StatusOfSet { get; set; }

        public int CombedTotalExtracted { get; set; }

        public List<LogZipFileObject> SetOfZips { get; set; }

        public LogZipFileSet(string Thread, List<LogZipFileObject> ListOfFiles)
        {
            // Setup name and count of files.
            ParserName = Thread;
            StatusOfSet = "Waiting To Download";
            TotalSize = "N/A";
            NumberOfZipsLeft = NumberOfZips;

            for (int Index = 0; Index < ListOfFiles.Count; Index++) 
            {                
                if (ListOfFiles[Index] == null) 
                { 
                    NumberOfZips -= 1;
                    NumberOfZipsLeft -= 1;

                    ListOfFiles.RemoveAt(Index);
                }

                ListOfFiles[Index].IndexOfFile = Index;
            }

            SetOfZips = ListOfFiles;
            NumberOfZips = SetOfZips.Count;
            NumberOfZipsLeft = NumberOfZips;
            CombedTotalExtracted = 0;
        }
    }
}
