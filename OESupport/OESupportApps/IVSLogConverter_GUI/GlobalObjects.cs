﻿using IVSLogConverter_GUI.LogicalHelpers;
using IVSLogConverter_GUI.Objects;
using IVSLogConverter_GUI.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace IVSLogConverter_GUI
{
    public static class GlobalObjects
    {
        public static CancellationTokenSource Source = new CancellationTokenSource();
        public static CancellationToken CancelToken = Source.Token;
        public static int TimeToWaitForUpdate = 5000;
        public static Task RefreshGUIInterval = new Task(() =>
        {
            // Check if cancel requested.
            while (!CancelToken.IsCancellationRequested)
            {
                // Update UI
                if (GUIZipObjects != null && GUIZipObjects.Count > 0)
                {
                    // MainLogger.WriteToDebugLog("GUIZIP SET WAS NOT NULL! UPDATING ITEMS IN THE MAIN LIST AND DETAILS NOW");
                    MainWindowView.Dispatcher.Invoke(() =>
                    {
                        MainWindowView.AllParsersListView.Items.Refresh();
                        MainWindowView.CurrentParserInfo.Items.Refresh();
                    });

                    // CHANGE THIS TO CHANGE UPDATE TIME!
                    // MainLogger.WriteToDebugLog("ITEMS UPDATED. WAITING " + TimeToWaitForUpdate + " MILISECONDS AND RECHECKING");
                    if (TimeToWaitForUpdate < 1000) { Thread.Sleep(1000); }
                    else { Thread.Sleep(TimeToWaitForUpdate); }
                }

                // Check again once loop is done.
                if (CancelToken.IsCancellationRequested)
                { 
                    CancelToken.ThrowIfCancellationRequested();
                    break;
                }
            }

        }, Source.Token);

        public static GUIValueHelpers GUIHelpers;
        public static List<LogZipFileSet> GUIZipObjects;
        public static MainWindow MainWindowView;

        public static DebugLogHelper MainLogger;

        public static AWSFilePuller MainAWSPuller;
        public static LogZipFilesCollection AllZipFiles;
        public static int SplitFileCount = 20;
        public static int MaxParallelThreads = 10;
        public static ParallelOptions Options;
        public static bool RemoveOldFiles = false;

        public static string LocalAppDataDir;

        public static string BaseDownloadingDir;
        public static string ZipDownloadDir;
        public static string ThreadDownloaderLogs;

        public static string BaseUnzippingDir;
        public static string UnzippingLogs;
        public static string UnzippedOutputDir;

        public static string BaseConversionDir;
        public static string IndividualOutputDirs;
        public static string CombinedAllFilesOutput;

        public static void SetupGlobalObjects()
        {
            // Dirs used globally for all classes to ref.
            LocalAppDataDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            if (Directory.Exists("D:\\IVSLogConverter_WorkingDir")) { LocalAppDataDir = "D:\\IVSLogConverter_WorkingDir\\"; }

            // Dirs for logging and downloading here.
            BaseDownloadingDir = LocalAppDataDir + "\\IVSLogConverter_Downloading\\";
            ZipDownloadDir = BaseDownloadingDir + "\\LogZipDownloads\\";
            ThreadDownloaderLogs = BaseDownloadingDir + "\\LogDownloadingLogs\\";

            BaseUnzippingDir = LocalAppDataDir + "\\IVSLogConverter_Unzipping";
            UnzippingLogs = BaseUnzippingDir + "\\UnzippingLogs\\";
            UnzippedOutputDir = BaseUnzippingDir + "\\UnzippedOutputs\\";

            BaseConversionDir = LocalAppDataDir + "\\IVSLogConverter_Conversions\\";
            IndividualOutputDirs = BaseConversionDir + "BaseConversions\\";
            CombinedAllFilesOutput = BaseConversionDir + "AllConvertableFiles\\";

            // Make sure Dirs exist for files.
            // if (Debugger.IsAttached) { RemoveOldFiles = true; }
            SetupDirs();

            // Parallel 
            Options = new ParallelOptions() { MaxDegreeOfParallelism = MaxParallelThreads };

            // Setup Settings boxes here.
            Settings.Default.Setting_WorkingDirBase = LocalAppDataDir;
            Settings.Default.Setting_MaxParallelThreads = MaxParallelThreads;
            Settings.Default.Setting_SplitListsFileCount = SplitFileCount;
            Settings.Default.Setting_TimeForGUIRefresh = TimeToWaitForUpdate;
            Settings.Default.Setting_RemoveOldZipFiles = RemoveOldFiles;
            Settings.Default.Save();

            // Main logger object
            MainLogger = new DebugLogHelper("IVS_CONVERTER_MAIN");

            // Puller for AWS files and the Zip file info for global use.
            AllZipFiles = new LogZipFilesCollection();
        }

        private static void SetupDirs()
        {
            if (RemoveOldFiles && Directory.Exists(ZipDownloadDir)) { Directory.Delete(ZipDownloadDir, true); }
            if (Directory.Exists(ThreadDownloaderLogs)) { Directory.Delete(ThreadDownloaderLogs, true); }
            if (Directory.Exists(BaseDownloadingDir)) { foreach (var FileItem in Directory.GetFiles(BaseDownloadingDir)) { File.Delete(FileItem); } }
            if (Directory.Exists(LocalAppDataDir)) { foreach (var FileItem in Directory.GetFiles(LocalAppDataDir)) { File.Delete(FileItem); } }
            if (Directory.Exists(UnzippingLogs)) { foreach (var FileItem in Directory.GetFiles(UnzippingLogs)) { File.Delete(FileItem); } }
            if (Directory.Exists(BaseConversionDir)) { foreach (var FileItem in Directory.GetFiles(UnzippingLogs)) { File.Delete(FileItem); } }
            if (RemoveOldFiles && Directory.Exists(UnzippedOutputDir)) { Parallel.ForEach(Directory.GetDirectories(UnzippedOutputDir), (DirItem) => { Directory.Delete(DirItem, true); }); }
            if (RemoveOldFiles && Directory.Exists(IndividualOutputDirs)) { Parallel.ForEach(Directory.GetDirectories(IndividualOutputDirs), (DirItem) => { Directory.Delete(DirItem, true); }); }
            if (RemoveOldFiles && Directory.Exists(BaseConversionDir)) { Directory.Delete(BaseConversionDir); }

            if (!Directory.Exists(BaseDownloadingDir)) { Directory.CreateDirectory(BaseDownloadingDir); }
            if (!Directory.Exists(ZipDownloadDir)) { Directory.CreateDirectory(ZipDownloadDir); }
            if (!Directory.Exists(ThreadDownloaderLogs)) { Directory.CreateDirectory(ThreadDownloaderLogs); }

            if (!Directory.Exists(BaseUnzippingDir)) { Directory.CreateDirectory(BaseUnzippingDir); }
            if (!Directory.Exists(UnzippingLogs)) { Directory.CreateDirectory(UnzippingLogs); }
            if (!Directory.Exists(UnzippedOutputDir)) { Directory.CreateDirectory(UnzippedOutputDir); }

            if (!Directory.Exists(BaseConversionDir)) { Directory.CreateDirectory(BaseConversionDir); }
            if (!Directory.Exists(IndividualOutputDirs)) { Directory.CreateDirectory(IndividualOutputDirs); }
            if (!Directory.Exists(CombinedAllFilesOutput)) { Directory.CreateDirectory(CombinedAllFilesOutput); }
        }
    }
}
