﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using static IVSLogConverter_GUI.GlobalObjects;

namespace IVSLogConverter_GUI.LogicalHelpers
{
    public class DebugLogHelper
    {
        public string ThreadName = "";

        public string OutputFileName = "";
        public List<string> FilesToLogOn;

        public bool IsAWS_Logger = false;

        public DebugLogHelper(bool IsAWS = false)
        {
            if (IsAWS) 
            {
                IsAWS_Logger = true;
                ThreadName = "AWS_PULLER_LOG"; 
            }

            SetupDebugFile(ThreadName);
        }

        public DebugLogHelper(string ThreadOfParser, List<string> LogZipFiles = null)
        {
            ThreadName = ThreadOfParser;
            SetupDebugFile(ThreadOfParser);

            if (LogZipFiles == null) { return; }

            WriteToDebugLog("PARSING OUT FILES AS LISTED BELOW:");
            foreach (var FileItem in LogZipFiles) { WriteToDebugLog("\\__ " + FileItem); }
            WriteToDebugLog("THIS MAY TAKE A BIT BE PATIENT...\n");
        }

        public void ConvertToUnzipper()
        {
            string TimeOfLaunch = DateTime.Now.ToString().Split(' ')[0].Replace("/", "") + "-" + DateTime.Now.TimeOfDay.ToString().Split('.')[0].Replace(":", "");
            OutputFileName = UnzippingLogs + ThreadName + "_" + TimeOfLaunch + ".txt";
        }

        private void SetupDebugFile(string ThreadName = "")
        {
            // Dont overwrite if it exists.
            if (File.Exists(OutputFileName)) { return; }

            string TimeOfLaunch = DateTime.Now.ToString().Split(' ')[0].Replace("/", "") + "-" + DateTime.Now.TimeOfDay.ToString().Split('.')[0].Replace(":", "");
            OutputFileName = LocalAppDataDir + Path.DirectorySeparatorChar.ToString() + "IVSLogConverterLog_" + TimeOfLaunch + ".txt";

            if (ThreadName != "" && ThreadName != "IVS_CONVERTER_MAIN")
            {
                if (IsAWS_Logger) 
                {
                    OutputFileName = MainLogger.OutputFileName;
                    return; 
                }

                OutputFileName = ThreadDownloaderLogs + Path.DirectorySeparatorChar.ToString() +
                                 ThreadName + "_" + TimeOfLaunch + ".txt";
            }

            WriteToDebugLog("--------------------------------------------------------------------------------------------");
            WriteToDebugLog("IVS LOG CONVERTER RUNNING NOW!");
            if (ThreadName != "") { WriteToDebugLog("DEVICE: " + ThreadName + " IS BEING PARSED OUT WITH THIS LOG FILE!\n"); }
            WriteToDebugLog("THIS PROCESS BEGAN AT: " + DateTime.Now.ToString("T")); 
        }

        private string NameOfCallingClass()
        {
            string fullName;
            Type declaringType;
            int skipFrames = 2;
            do
            {
                MethodBase method = new StackFrame(skipFrames, false).GetMethod();
                declaringType = method.DeclaringType;
                if (declaringType == null)
                {
                    return method.Name;
                }
                skipFrames++;
                fullName = declaringType.FullName;
            }
            while (declaringType.Module.Name.Equals("mscorlib.dll", StringComparison.OrdinalIgnoreCase));

            var fullNameSplit = fullName.Split('.');
            fullName = fullNameSplit[fullNameSplit.Length - 1];

            return fullName;
        }

        public void WriteToDebugLog(string DebugInfo, string ThreadNumber = null)
        {
            string timeNow = DateTime.Now.ToString("T");
            string CallingName = NameOfCallingClass();

            if (string.IsNullOrEmpty(ThreadNumber)) { ThreadNumber = ThreadName; }
            string writeThis = "[" + timeNow + "] ::: [" + CallingName + "] ::: [" + ThreadNumber + "] ::: " + DebugInfo + "\n"; 

            for (int retryCount = 0; retryCount < 5; retryCount++)
            {
                try { File.AppendAllText(OutputFileName, writeThis); break; }
                catch { System.Threading.Thread.Sleep(10); }
            }
        }

        public void WriteToMainDebugFile(string DebugInfo, string ThreadNumber = null)
        {
            if (GlobalObjects.MainLogger == null)
            {
                WriteToDebugLog("THIS WAS A REQUEST TO LOG TO THE GLOBAL LOG FILE BUT IT APPEARS THE LOGGER WAS NULL!");
                WriteToDebugLog("LOGGING TO FILE INSTEAD FOR THE CURRENT INSTANCE!");
                WriteToDebugLog(DebugInfo, ThreadNumber);

                return;
            }

            GlobalObjects.MainLogger.WriteToDebugLog("[EXTERNAL LOGGER CALL] ::: " + DebugInfo, ThreadNumber);
        }

        public void WriteToInstanceAndGlobal(string DebugInfo, string ThreadNumber = null)
        {
            WriteToDebugLog(DebugInfo, ThreadNumber);
            WriteToMainDebugFile(DebugInfo, ThreadNumber);            
        }

        public void WriteErrorToLog(Exception Ex, bool LogToBoth = false, string ThreadNumber = null)
        {
            string[] SplitEx = Ex.StackTrace.Split('\n');

            WriteToDebugLog("ERROR. EXCEPTION THROWN! --> " + Ex.Message);
            if (LogToBoth) { WriteToMainDebugFile("ERROR. EXCEPTION THROWN! --> " + Ex.Message); }

            foreach (var Line in SplitEx)
            {
                WriteToDebugLog(Line, ThreadNumber);
                if (LogToBoth) { WriteToMainDebugFile(Line, ThreadNumber); }
            }
        }
    }
}
