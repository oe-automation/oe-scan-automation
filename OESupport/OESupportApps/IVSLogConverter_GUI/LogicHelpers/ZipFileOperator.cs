﻿using IVSLogConverter_GUI.LogicalHelpers;
using IVSLogConverter_GUI.LogicHelpers;
using IVSLogConverter_GUI.Objects;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static IVSLogConverter_GUI.GlobalObjects;

namespace IVSLogConverter_GUI.LogicalHelpers
{
    public class ZipFileOperator
    {
        public int ThreadSetNumber = 0;

        private AWSFilePuller ZipPuller;
        public DebugLogHelper Logger;
        public List<LogZipFileObject> ZipFiles;

        public ZipFileOperator(List<LogZipFileObject> InputZipFiles, AWSFilePuller ZipDownloader, DebugLogHelper FileLogger, int ThreadNumber)
        {
            ZipFiles = InputZipFiles;
            Logger = FileLogger;
            ZipPuller = ZipDownloader;
            ThreadSetNumber = ThreadNumber;

            Logger.WriteToDebugLog("SETUP A NEW ZIP FILE PARSER!");
            Logger.WriteToDebugLog("A TOTAL OF " + ZipFiles.Count + " ZIP FILES ARE TO BE PARSED OUT");
        }

        public void PullDownAllZipFiles()
        {
            Stopwatch timeThis = new Stopwatch();
            timeThis.Start();

            Parallel.ForEach(GUIZipObjects[ThreadSetNumber].SetOfZips, (ZipFileItem) =>
            {
                int Index = GUIZipObjects[ThreadSetNumber].SetOfZips.IndexOf(ZipFileItem);
                string LogFileItem = GUIZipObjects[ThreadSetNumber].SetOfZips[Index].NameOfZip;
                if (LogFileItem.Contains("\\")) { LogFileItem = GUIZipObjects[ThreadSetNumber].SetOfZips[Index].NoPathName; }

                for (int CountOfTries = 0; CountOfTries <= 3; CountOfTries++)
                {
                    if (CheckFileReal(LogFileItem, ThreadSetNumber, Index)) { break; }
                    try
                    {
                        Logger.WriteToDebugLog("TRYING TO PULL FILE FOR THE " + CountOfTries + " TIME");
                        Logger.WriteToDebugLog("---> FILE: " + LogFileItem);

                        if (ZipPuller.PullDownZipFile(GUIZipObjects[ThreadSetNumber].SetOfZips[Index], Logger))
                        {
                            string NewName = ZipDownloadDir + LogFileItem;
                            GUIZipObjects[ThreadSetNumber].SetOfZips[Index].NameOfZip = NewName;

                            Logger.WriteToDebugLog("PULLED DOWN FILE OK AND RENAMED FILE PATH IN LIST OF ZIPS!");
                            Logger.WriteToDebugLog("FILE SIZE WAS FOUND TO BE " + new FileInfo(NewName).Length);
                        }
                        else { Logger.WriteToDebugLog("DOWNLOADED FILE DOES NOT EXIST??"); }

                        CountOfTries++;
                        if (CountOfTries == 3)
                        {
                            GUIHelpers.TotalZipCount -= 1;
                            throw new Exception("CAN NOT DOWNLOAD FILE AFTER 5 FAILED ATTEMPTS!");
                        }
                    }
                    catch (Exception DlEx)
                    {
                        Logger.WriteToInstanceAndGlobal("COULD NOT DOWNLOAD ZIP FILE ON PULLER THREAD NUMBER " + ThreadSetNumber.ToString());
                        Logger.WriteErrorToLog(DlEx, true, ThreadSetNumber.ToString());
                    }
                }

                GUIZipObjects[ThreadSetNumber].NumberOfZipsLeft -= 1;
                if (GUIZipObjects[ThreadSetNumber].NumberOfZipsLeft <= 0)
                {
                    GUIZipObjects[ThreadSetNumber].StatusOfSet = "Ready To Unzip/Parse";
                    GUIZipObjects[ThreadSetNumber].NumberOfZipsLeft = 0;
                }
                UpdateGUI(ThreadSetNumber);
            });

            // Set Status
            GUIZipObjects[ThreadSetNumber].StatusOfSet = "Ready To Unzip/Parse";
            GUIZipObjects[ThreadSetNumber].NumberOfZipsLeft = 0;

            // Check all files and update UI
            for (int Index = 0; Index < GUIZipObjects[ThreadSetNumber].SetOfZips.Count; Index++)
            {
                var NextFile = GUIZipObjects[ThreadSetNumber].SetOfZips[Index].NoPathName;
                CheckFileReal(NextFile, ThreadSetNumber, Index);
            }

            // Stop timer and write into to UI;
            timeThis.Stop();
            Logger.WriteToMainDebugFile("---------------------------------------------------------");
            Logger.WriteToInstanceAndGlobal("THREAD " + ThreadSetNumber + " DOWNLOADED " + ZipFiles.Count + " ZIP FILES");
            Logger.WriteToInstanceAndGlobal("PULLED DOWN FILES IN A TOTAL OF: " + timeThis.Elapsed.ToString("c"));
            Logger.WriteToInstanceAndGlobal("REMOVING THIS PULLER FROM THE LIST AND PREPARING TO INVOKE A JLOG2SIMER ON IT");
            Logger.WriteToInstanceAndGlobal("PARSERS LEFT: " + AllZipFiles.ListOfFileOperators.Count);
            Logger.WriteToMainDebugFile("---------------------------------------------------------");

            // Set done
            GUIHelpers.ParsersCompleted += 1;
            GUIZipObjects[ThreadSetNumber].StatusOfSet = "Ready To Unzip/Parse";

            // Convert the logger into an unzipping object.
            Logger.ConvertToUnzipper();

            // Update UI
            UpdateGUI(ThreadSetNumber);
        }
        public void ExtractAllZipFiles()
        {
            Stopwatch timeThis = new Stopwatch();
            timeThis.Start();

            Parallel.ForEach(GUIZipObjects[ThreadSetNumber].SetOfZips, (ZipFileItem) =>
            {
                // Index of log and current file.
                int Index = GUIZipObjects[ThreadSetNumber].SetOfZips.IndexOf(ZipFileItem);
                var LogFileItem = GUIZipObjects[ThreadSetNumber].SetOfZips[Index];

                // Perform the parsing here
                var ModifiedObj = new LogZipFileObject();
                var CurrentObjExtracter = new ZipFileExtractor(LogFileItem, Logger);

                // Extract here 
                for (int NumberOfTries = 0; NumberOfTries <= 3; NumberOfTries++)
                {
                    try
                    {
                        GUIZipObjects[ThreadSetNumber].StatusOfSet = LogFileItem.NoPathName;

                        Logger.WriteToDebugLog("TRYING TO DECOMPRESS FILE: " + LogFileItem.NoPathName);
                        Logger.WriteToDebugLog("ATTEMPT #" + NumberOfTries);

                        bool DirAlreadyExists = Directory.Exists(LogFileItem.DecompressedPath);
                        if (CurrentObjExtracter.UnZipFile(ThreadSetNumber, out ModifiedObj))
                        {
                            Logger.WriteToDebugLog("EXTRACTED FILE AND IT'S CONTENTS OK!");
                            Logger.WriteToDebugLog("TOOK A TOTAL OF " + NumberOfTries + " TO DECOMPRESS THIS ZIP FILE CORRECTLY");
                            Logger.WriteToDebugLog("FILE SIZE: " + ModifiedObj.SizeOfExtractedDir);
                            Logger.WriteToDebugLog("FILE HELD A TOTAL OF " + ModifiedObj.ExtractedItems + " FILES IN IT");

                            if (!DirAlreadyExists)
                            {
                                ModifiedObj.IsExtracted = true;
                                ModifiedObj.OperationProgressValueOnly = "100%";
                                ModifiedObj.SizeOfExtractedString = FileSizeHelpers.SizeSuffix(ModifiedObj.SizeOfExtractedDir);
                                ModifiedObj.OperationProgressString = "Done - " + ModifiedObj.SizeOfExtractedString + " - " + ModifiedObj.ExtractedItems + " Files";
                            }

                            AllZipFiles.ListOfExtracted.Add(ModifiedObj);
                            GUIZipObjects[ThreadSetNumber].SetOfZips[Index] = ModifiedObj;

                            // Extract Input Logs
                            GUIHelpers.ConvertableExtractedFiles += ModifiedObj.AllFilesToConvert.Count;
                            if (ModifiedObj.AllFilesToConvert.Count > 0)
                            {
                                Parallel.ForEach(ModifiedObj.AllFilesToConvert, (TupleSet) =>
                                {
                                    var FileConfig = new FileInfo(TupleSet.Item1);
                                    if (FileConfig.Length >= 10000)
                                    {

                                        Logger.WriteToDebugLog("COPYING OUTPUT FILE: " + TupleSet.Item1);
                                        if (File.Exists(TupleSet.Item2)) { File.Delete(TupleSet.Item2); }

                                        File.Copy(TupleSet.Item1, TupleSet.Item2, true);
                                        if (ZipFileConverter.ConvertFile(TupleSet.Item2))
                                        {
                                            Logger.WriteToDebugLog("EXTRACTED FILE AND IT'S CONTENTS OK AND CONVERTED AS WELL!!");
                                            Logger.WriteToDebugLog("TOOK A TOTAL OF " + NumberOfTries + " TO DECOMPRESS THIS ZIP FILE CORRECTLY");
                                        }
                                    }
                                });
                            }

                            break;
                        }
                        else
                        {
                            Logger.WriteToDebugLog("FAILED TO UNZIP FILE BUT RECOVERED FROM AN ERROR. TRYING AGAIN");
                            Logger.WriteToDebugLog("FILE " + LogFileItem.NoPathName + " FAILED TO EXTRACT ON ATTEMPT: " + NumberOfTries);

                            if (NumberOfTries == 3) { throw new Exception("FAILED TO EXTRACT FILE AFTER 10 ATTEMPTS!"); }
                        }
                    }

                    catch (Exception zipEx)
                    {
                        Logger.WriteToDebugLog("FILE " + LogFileItem.NoPathName + " FAILED TO EXTRACT AFTER 10 ATTEMPTS!");
                        Logger.WriteErrorToLog(zipEx);
                    }
                }

                // Update UI
                GUIHelpers.TotalExtractedFileCount += ModifiedObj.ExtractedItems;
                GUIHelpers.SizeOfExtractedLong += ModifiedObj.SizeOfExtractedDir;

                GUIZipObjects[ThreadSetNumber].NumberOfZipsLeft -= 1;
                UpdateGUI(ThreadSetNumber, true);
            });

            // Stop timer and write into to UI;
            timeThis.Stop();
            Logger.WriteToMainDebugFile("---------------------------------------------------------");
            Logger.WriteToInstanceAndGlobal("THREAD " + ThreadSetNumber + " EXTRACTED " + ZipFiles.Count + " ZIP FILES");
            Logger.WriteToInstanceAndGlobal("EXTRACTED ALL FILES IN A TOTAL OF: " + timeThis.Elapsed.ToString("c"));
            Logger.WriteToInstanceAndGlobal("REMOVING THIS EXTRACTOR FROM THE LIST AND PREPARING TO INVOKE A JLOG2SIMER ON IT");
            Logger.WriteToInstanceAndGlobal("PARSERS LEFT: " + AllZipFiles.ListOfFileOperators.Count);
            Logger.WriteToMainDebugFile("---------------------------------------------------------");

            // Set done
            GUIHelpers.ParsersCompleted += 1;
            GUIZipObjects[ThreadSetNumber].StatusOfSet = "Ready to Convert";

            // Update UI
            UpdateGUI(ThreadSetNumber, true);
        }
        
        private void UpdateGUI(int ThreadNumber, bool IsExtraction = false)
        {
            MainWindowView.Dispatcher.Invoke(() =>
            {
                var CurrentParser = GUIZipObjects[ThreadNumber];
                if (ThreadNumber == MainWindowView.SelectedIndex)
                {
                    MainWindowView.ParserSizeString.Text = "Total Size: " + CurrentParser.TotalSize;

                    if (IsExtraction) { MainWindowView.ParserFilesLeftString.Text = CurrentParser.CombedTotalExtracted + " Files Extracted"; }
                    if (!IsExtraction) { MainWindowView.ParserFilesLeftString.Text = CurrentParser.NumberOfZipsLeft + " Files Left"; }
                    
                    MainWindowView.CurrentParserInfo.Items.Refresh();
                }
            });
        }

        private bool CheckFileReal(string LogFileItem, int ThreadNumber, int IndexTracker)
        {
            var ThisFile = GUIZipObjects[ThreadNumber].SetOfZips[IndexTracker];
            if (File.Exists(ZipDownloadDir + LogFileItem))
            {
                if (ThisFile.IsDownloaded) { return true; }
                if (ThisFile.SizeOfZip < 0) 
                {
                    ThisFile.OperationProgressString = "Starting Download";
                    GUIZipObjects[ThreadNumber].SetOfZips[IndexTracker] = ThisFile;
                }

                long Size = new FileInfo(ZipDownloadDir + LogFileItem).Length;
                long TotalSize = ThisFile.SizeOfZip;
                
                ThisFile.SizeOfZipDownloaded = Size;
                ThisFile.SizeOfZipString = FileSizeHelpers.SizeSuffix(Size, 2);
                ThisFile.OperationProgress = ((double)Size / (double)TotalSize) * 100.0;
                ThisFile.OperationProgressValueOnly = ThisFile.OperationProgress.ToString("F0") + "%";
                ThisFile.OperationProgressString = FileSizeHelpers.SizeSuffix(Size, 2) + "/" + FileSizeHelpers.SizeSuffix(TotalSize, 2);

                if (Size == TotalSize) { ThisFile.IsDownloaded = true; }
                
                GUIZipObjects[ThreadNumber].SetOfZips[IndexTracker] = ThisFile;
                return true;
            }

            else
            {
                // ThisFile.OperationProgressString = "Waiting To Download";
                // GUIZipObjects[ThreadNumber].SetOfZips[IndexTracker] = ThisFile;
                return false;
            }
        }
    }
}
