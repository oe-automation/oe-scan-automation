﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Runtime.Internal.Util;
using Ionic.Zip;
using IVSLogConverter_GUI.LogicalHelpers;
using IVSLogConverter_GUI.Objects;

using static IVSLogConverter_GUI.GlobalObjects;

namespace IVSLogConverter_GUI.LogicHelpers
{
    public class ZipFileExtractor
    {
        private LogZipFileObject CurrentFile;
        private DebugLogHelper Logger;
        private int ThreadIndex;

        public ZipFileExtractor(LogZipFileObject CurrentZip, DebugLogHelper CurrentLogger = null)
        {
            CurrentFile = CurrentZip;
            if (CurrentLogger == null) { Logger = MainLogger; }
            else { Logger = CurrentLogger; }
        }

        public bool UnZipFile(int IndexOfThread, out LogZipFileObject ModifiedFileObj)
        {
            Logger.WriteToDebugLog("BEGINING UNZIP FOR FILE: " + CurrentFile.NoPathName);
            if (!Directory.Exists(CurrentFile.DecompressedPath)) { Directory.CreateDirectory(CurrentFile.DecompressedPath); }
            try
            {
                // Used to track sets.
                ThreadIndex = IndexOfThread;

                using (ZipFile ThisZip = ZipFile.Read(CurrentFile.NameOfZip))
                {
                    ThisZip.ExtractProgress += new EventHandler<ExtractProgressEventArgs>(ZipProgressEvent);
                    ThisZip.ExtractAll(CurrentFile.DecompressedPath, ExtractExistingFileAction.OverwriteSilently);
                }

                ModifiedFileObj = CurrentFile;

                // Log in size changes here.
                GUIZipObjects[ThreadIndex].SizeLong = FileSizeHelpers.ReturnSizeOfDir(new DirectoryInfo(CurrentFile.DecompressedPath));
                GUIZipObjects[ThreadIndex].TotalSize = FileSizeHelpers.SizeSuffix(CurrentFile.SizeOfExtractedDir);

                return true;
            }
                
            catch (Exception Zipex)
            {
                Logger.WriteToDebugLog("FAILED TO EXTRACT ZIP FILE!" + CurrentFile.NoPathName);
                Logger.WriteToDebugLog("ERR: " + Zipex.Message);
                Logger.WriteErrorToLog(Zipex);

                if (Directory.Exists(CurrentFile.DecompressedPath) && FileSizeHelpers.ReturnSizeOfDir(new DirectoryInfo(CurrentFile.DecompressedPath)) > 0)
                {
                    ModifiedFileObj = CurrentFile;
                    return true;
                }

                CurrentFile.OperationProgressValueOnly = "N/A";
                CurrentFile.OperationProgressString = "Failed - " + CurrentFile.ExtractedNow + "/" + CurrentFile.ExtractedItems;

                CurrentFile.ExtractionFailed = true;
                CurrentFile.IsExtracted = false;

                ModifiedFileObj = CurrentFile;
                return false;
            }
        }

        private void ZipProgressEvent(object sender, ExtractProgressEventArgs e)
        {
            // Add to list of files
            if (e.CurrentEntry != null && e.CurrentEntry.FileName != "" && e.ExtractLocation != null)
            {
                if (e.CurrentEntry.FileName.Contains("CarDAQ") || e.CurrentEntry.FileName.Contains("GDP"))
                {
                    string NextFile = (e.ExtractLocation + "\\" + e.CurrentEntry.FileName).Replace("/", "\\");

                    string ToolName = e.ExtractLocation.Substring(e.ExtractLocation.LastIndexOf("\\") + 1);
                    string FileOnly = e.CurrentEntry.FileName.Substring(e.CurrentEntry.FileName.LastIndexOf("/") + 1);
                    string OutputForFile = CombinedAllFilesOutput + FileOnly;

                    if (FileOnly.StartsWith("CarDAQ") || FileOnly.StartsWith("GDP"))
                    {
                        var NewTuple = new Tuple<string, string>(NextFile, OutputForFile);
                        if (!CurrentFile.AllFilesToConvert.Contains(NewTuple)) 
                        {
                            if (e.TotalBytesToTransfer >= 10000 || e.EventType == ZipProgressEventType.Extracting_AfterExtractEntry)
                            {
                                // Checks to see if the size of the file is less than 1kb.
                                CurrentFile.AllFilesToConvert.Add(NewTuple);
                            }
                        }

                        if (!Directory.Exists(IndividualOutputDirs + ToolName)) { Directory.CreateDirectory(IndividualOutputDirs + ToolName); }
                        if (!Directory.Exists(IndividualOutputDirs + ToolName + "\\InputLogs")) { Directory.CreateDirectory(IndividualOutputDirs + ToolName + "\\InputLogs"); }
                    }
                }
            }

            // If a file was just done.
            if (e.EventType == ZipProgressEventType.Extracting_AfterExtractEntry)
            {
                if (e.TotalBytesToTransfer == 0)
                {
                    GUIZipObjects[ThreadIndex].CombedTotalExtracted += 1;
                    GUIZipObjects[ThreadIndex].TotalSize = FileSizeHelpers.SizeSuffix(GUIZipObjects[ThreadIndex].SizeLong);

                    long NextSetSize = 0;
                    foreach (var FileItem in GUIZipObjects[ThreadIndex].SetOfZips) { NextSetSize += FileItem.SizeOfExtractedDir; }
                    GUIZipObjects[ThreadIndex].SizeLong = NextSetSize;
                }
            }

            // If we have start arg
            if (e.EventType == ZipProgressEventType.Extracting_BeforeExtractAll)
            {
                CurrentFile.OperationProgressString = "Starting Unzip Now";
                CurrentFile.OperationProgressValueOnly = "0%";
            }

            // If we have an item init.
            if (e.EventType == ZipProgressEventType.Extracting_BeforeExtractEntry)
            {
                CurrentFile.ExtractedNow = e.EntriesExtracted;
                CurrentFile.ExtractedItems = e.EntriesTotal;

                if (Directory.Exists(CurrentFile.DecompressedPath) && !RemoveOldFiles)
                {
                    DirectoryInfo DirStats = new DirectoryInfo(CurrentFile.DecompressedPath);
                    long FilesInDir = FileSizeHelpers.ReturnFileCount(DirStats);
                    long SizeOfDir = FileSizeHelpers.ReturnSizeOfDir(DirStats);

                    if (FilesInDir == e.EntriesTotal && !RemoveOldFiles)
                    {
                        CurrentFile.OperationProgress = 100.0;
                        CurrentFile.IsExtracted = true;
                        CurrentFile.ExtractedItems = e.EntriesTotal;
                        CurrentFile.ExtractedNow = e.EntriesTotal;
                        CurrentFile.SizeOfCurrentlyExtracted = SizeOfDir;
                        CurrentFile.SizeOfExtractedDir = SizeOfDir;

                        string SizeExtracted = FileSizeHelpers.SizeSuffix(SizeOfDir);
                        CurrentFile.OperationProgressValueOnly = "100%";
                        CurrentFile.SizeOfExtractedString = SizeExtracted;
                        CurrentFile.OperationProgressString = "Done - " + SizeExtracted + " - " + CurrentFile.ExtractedItems + " Files";

                        // Cancel unzipping here.
                        e.Cancel = true;
                    }
                }
            }

            // Size of whole zip.
            long TotalSizeLong = e.TotalBytesToTransfer;
            if (TotalSizeLong > 0)
            {
                if (TotalSizeLong > CurrentFile.SizeOfExtractedDir) { CurrentFile.SizeOfExtractedDir = TotalSizeLong; }
                else { TotalSizeLong = CurrentFile.SizeOfExtractedDir; }

                CurrentFile.SizeOfExtractedString = FileSizeHelpers.SizeSuffix(TotalSizeLong);
            }

            // Size of current items
            long TotalSizeExtracted = e.BytesTransferred;
            if (TotalSizeExtracted > 0) { CurrentFile.SizeOfCurrentlyExtracted = TotalSizeExtracted; }

            // Total files to pull
            int TotalEntries = e.EntriesTotal;
            if (TotalEntries > 0) { CurrentFile.ExtractedItems = TotalEntries; }

            // Current files pulled
            int CurrentEntries = e.EntriesExtracted;
            if (CurrentFile.ExtractedNow < CurrentEntries) { CurrentFile.ExtractedNow = CurrentEntries; }

            // Progress value and string.
            double CurrentProg = ((double)CurrentEntries / (double)TotalEntries) * 100.0;
            string CurrentSize = FileSizeHelpers.SizeSuffix(TotalSizeExtracted, 2);
            string TotalSize = FileSizeHelpers.SizeSuffix(TotalSizeLong, 2);

            // If we have a prog value. Dont update on every item just on the entry counts.
            if (CurrentProg > CurrentFile.OperationProgress && e.EntriesTotal != 0)
            {
                CurrentFile.OperationProgress = CurrentProg;
                CurrentFile.OperationProgressValueOnly = CurrentProg.ToString("F0") + "%";
            }

            // If zero size for this item
            bool ZeroSizeString = (CurrentSize == TotalSize) && TotalSizeLong == 0;
            if (ZeroSizeString && (CurrentEntries > 0 && TotalEntries > 0)) 
            {
                if ((int)CurrentProg % 2 == 0)
                {
                    string SizeCurrent = FileSizeHelpers.SizeSuffix(CurrentFile.SizeOfCurrentlyExtracted);
                    string NewString = CurrentEntries + "/" + TotalEntries + " Files (" + SizeCurrent + ")";
                    CurrentFile.OperationProgressString = NewString;
                }
            }

            // Update the UI
            MainWindowView.Dispatcher.Invoke(() =>
            {
                var CurrentParser = GUIZipObjects[ThreadIndex];
                if (ThreadIndex == MainWindowView.SelectedIndex)
                {
                    MainWindowView.ParserSizeString.Text = "Total Size: " + CurrentParser.TotalSize;
                    MainWindowView.ParserFilesLeftString.Text = CurrentParser.CombedTotalExtracted + " Files Extracted";

                    if ((int)CurrentFile.OperationProgress % 2 == 0) { MainWindowView.CurrentParserInfo.Items.Refresh(); }
                }
            });

        }
    }
}
