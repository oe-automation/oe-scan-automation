﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IVSLogConverter_GUI.LogicHelpers
{
    public class ZipFileConverter
    {
        public static bool ConvertFile(string InputFile)
        {
            if (!File.Exists(InputFile)) { return false; }

            Process JLogToSimProc = new Process();
            JLogToSimProc.StartInfo = new ProcessStartInfo
            {
                FileName = @"C:\path\JLogToSim.exe",
                Arguments = InputFile,
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden
            };

            JLogToSimProc.Start();
            JLogToSimProc.WaitForExit();

            // File.Delete(InputFile);

            return true;
        }
    }
}
