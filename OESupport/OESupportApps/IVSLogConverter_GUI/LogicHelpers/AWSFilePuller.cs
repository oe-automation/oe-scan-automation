﻿using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using IVSLogConverter_GUI.LogicHelpers;
using IVSLogConverter_GUI.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static IVSLogConverter_GUI.GlobalObjects;

namespace IVSLogConverter_GUI.LogicalHelpers
{
    public class AWSFilePuller
    {
        // Debug helper.
        private DebugLogHelper Logger;

        // Dir of the IVS Log Collector Log Files
        public string BaseFileLocation = @"Deploy Directories/IVS Log Collector/IVS Logs";

        // S3 Bucket Configuration info.
        public string S3BucketName = "drivepro.logs.upload";
        private string S3AccessKey = "AKIASQ7DDKVHCJMHJEXU";
        private string S3SecretKey = "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";

        // S3 Client
        private AmazonS3Client S3Client;
        private TransferUtility S3Downloader;

        public AWSFilePuller(DebugLogHelper LoggerObject = null)
        {
            if (LoggerObject != null) { Logger = LoggerObject; }
            else { Logger = new DebugLogHelper(true); }

            // Init the client and snag all files from the bucket.
            S3Client = new AmazonS3Client(S3AccessKey, S3SecretKey, RegionEndpoint.EUWest1);
            S3Downloader = new TransferUtility(S3Client);

            // Pull file for Parsed
            PullDownParsedFile();

            // Pull down all Zip Files on bucket as list of entries.
            GetListsOfZips();
        }

        private bool PullDownParsedFile()
        {
            // Yank down the PARSED_LOG_FILES text file and see whats in the list there.
            string TextFileName = BaseFileLocation + "/PARSED_LOG_FILES";
            string DownloadPath = BaseDownloadingDir + "\\FILES_PARSED.txt";

            Logger.WriteToDebugLog("WRITING FILE FOR LIST OF DOWNLOADED LOGS NOW...");
            try { S3Downloader.Download(DownloadPath, S3BucketName, TextFileName); }
            catch { Logger.WriteToDebugLog("FILE COULD NOT BE PULLED DOWN."); }

            // Convert that text file into a list of names now.
            if (File.Exists(DownloadPath)) 
            {
                Logger.WriteToDebugLog("FOUND THE FILE OK! DOWNLOADED AND PREPARING TO SPLIT INTO LISTS OF FILES.");
                AllZipFiles.ListOfParsedFiles = File.ReadAllText(DownloadPath).Split('\n').ToList();
                return true;
            }
            else
            {
                Logger.WriteToDebugLog("COULD NOT DL THE PARSED FILES LIST! THIS IS OK AND IT WILL BE CREATED AS A RESULT.");
                File.WriteAllText(DownloadPath, "");
                return false;
            }

        }
        private void GetListsOfZips()
        {
            // Get all zip files here
            int ParsedFileCount = 0;
            S3DirectoryInfo ZipFilesFound = new S3DirectoryInfo(S3Client, S3BucketName, BaseFileLocation);
            var zipList = ZipFilesFound.GetFileSystemInfos().ToList();

            Logger.WriteToDebugLog("LOOPING LIST OF ZIP FILES AND CHECKING AGAINS THE PARSED OUT FILES.");
            Parallel.ForEach(zipList, (ZipFile) =>
            {
                if (ZipFile.Name.Contains(".zip"))
                {
                    LogZipFileObject NextZip = new LogZipFileObject();
                    if (!ShouldReDownload(ZipFile.Name) && !RemoveOldFiles)
                    {
                        ParsedFileCount += 1;
                        var FileOnDiskSize = new FileInfo(ZipDownloadDir + ZipFile.Name).Length;

                        NextZip = new LogZipFileObject()
                        {
                            NoPathName = ZipFile.Name,
                            NameOfZip = ZipDownloadDir + ZipFile.Name,
                            SizeOfZip = FileOnDiskSize,
                            SizeOfZipString = FileSizeHelpers.SizeSuffix(FileOnDiskSize, 2),
                            OperationProgress = 0.0,
                            IsDownloaded = true,
                            ParserThread = -1,
                            TimeUploaded = ZipFile.LastWriteTime.ToString(),
                            OperationProgressString = "Waiting To Extract",
                            OperationProgressValueOnly = "N/A"
                        };
                    }
                    
                    else if (ShouldReDownload(ZipFile.Name) || FileWasParsed(ZipFile.Name))
                    {
                        ParsedFileCount += 1;
                        NextZip = new LogZipFileObject()
                        {
                            NoPathName = ZipFile.Name,
                            NameOfZip = ZipFile.Name,
                            SizeOfZip = -1,
                            SizeOfZipString = "N/A",
                            OperationProgress = 0.0,
                            IsDownloaded = false,
                            ParserThread = -1,
                            TimeUploaded = ZipFile.LastWriteTime.ToString(),
                            OperationProgressString = "Not Started",
                            OperationProgressValueOnly = "N/A",
                        };
                    }

                    AllZipFiles.AllLogFileZips.Add(NextZip);
                    GUIHelpers.TotalZipCount = AllZipFiles.AllLogFileZips.Count;
                }
            });

            // Write Count of zips found.
            Logger.WriteToDebugLog("FOUND A TOTAL OF " + AllZipFiles.AllLogFileZips.Count + " ZIP FILES ON THE BUCKET");
            Logger.WriteToDebugLog("THERE WERE " + ParsedFileCount + " ZIP FILES THAT HAVE ALREADY BEEN PARSED OUT");

            // Set the split value now.
            // int SplitValue = (int)((AllZipFiles.AllLogFileZips.Count / (AllZipFiles.AllLogFileZips.Count / 25)) / 2);         
            // double SplitDouble = SplitValue / 5;
            // int Multi = (int)SplitDouble;
            // SplitValue = Multi * SplitValue;

            // Seperate the original log list into a set of sublists that can easily be parallel parsed.
            AllZipFiles.ZipFileCollectionSets = AllZipFiles.AllLogFileZips.Select((x, i) => new { Index = i, Value = x })
                   .GroupBy(x => x.Index / SplitFileCount)
                   .Select(x => x.Select(v => v.Value).ToList())
                   .ToList();

            // Write out split list sizes.
            Logger.WriteToDebugLog("SPLIT THE ORIGINAL LIST OF ZIP FILES INTO " + AllZipFiles.ZipFileCollectionSets.Count + " SETS OF " + SplitFileCount + " OR LESS\n");
            GUIHelpers.AllParsersCount = AllZipFiles.ZipFileCollectionSets.Count;
        }


        private bool FileWasParsed(string ZipFileName)
        {
            return AllZipFiles.ListOfParsedFiles.Contains(ZipFileName);
        }
        private bool ShouldReDownload(string FileName)
        {
            if (!File.Exists(ZipDownloadDir + FileName)) { return true; }

            var GetMetaRequest = new GetObjectMetadataRequest() { BucketName = S3BucketName, Key = BaseFileLocation + "/" + FileName };
            var MetaDataValues = S3Client.GetObjectMetadata(GetMetaRequest);
            
            var FileInBucketSize = MetaDataValues.Headers.ContentLength;
            var FileOnDiskSize = new FileInfo(ZipDownloadDir + FileName).Length;

            if (FileInBucketSize > FileOnDiskSize) { return true; }
            return false;   
        }

        // RETURNS TO APPDATA/FILENAME
        public bool PullDownZipFile(LogZipFileObject InputZipFile, DebugLogHelper LoggerObject = null)
        {
            if (LoggerObject == null) { LoggerObject = Logger; }

            string OutputFile = InputZipFile.NameOfZip;
            if (!OutputFile.Contains("\\")) { OutputFile = ZipDownloadDir + InputZipFile.NoPathName; }

            string ZipKeyName = BaseFileLocation + "/" + InputZipFile.NoPathName;
            LoggerObject.WriteToDebugLog("TEMP ZIP FILE KEY: " + ZipKeyName); 
            LoggerObject.WriteToDebugLog("TEMP ZIP FILE NAME: " + OutputFile);

            try
            {
                // THIS NEEDS TO BE TURNED OFF OR SOMETHING AT SOME POINT. 
                // THIS USES A FUUUUUUCCQUE TON OF STORAGE SPACE
                if (File.Exists(OutputFile) && InputZipFile.IsDownloaded)
                {
                    if (!RemoveOldFiles) { return true; }

                    LoggerObject.WriteToDebugLog("REMOVING OLD COPY OF ZIP FILE: " + OutputFile);
                   
                    InputZipFile.IsDownloaded = false;
                    InputZipFile.SizeOfZip = -1;
                    InputZipFile.SizeOfZipString = "N/A";
                    InputZipFile.OperationProgress = 0.0;
                    InputZipFile.OperationProgressString = "Not Started";
                    InputZipFile.OperationProgressValueOnly = "N/A";

                    File.Delete(OutputFile);
                }

                LoggerObject.WriteToDebugLog("PULLING DOWN FILE NOW...");
                using (var dlResp = S3Client.GetObject(S3BucketName, ZipKeyName))
                {
                    dlResp.WriteObjectProgressEvent += Response_WriteObjectProgressEvent;
                    dlResp.WriteResponseStreamToFile(OutputFile);
                }

                AllZipFiles.AllDownloadedZipFiles.Add(InputZipFile);
                return true;
            }

            catch (Exception ex)
            {
                LoggerObject.WriteToDebugLog("FAILED TO DOWNLOAD LOG FILE!");
                LoggerObject.WriteToDebugLog("REASON: " + ex.Message);

                return File.Exists(OutputFile);
            }
        }

        private static void Response_WriteObjectProgressEvent(object sender, WriteObjectProgressArgs e)
        {
            GetObjectResponse FileReq = (GetObjectResponse)sender;
            string[] NameOfFileSplit = FileReq.Key.Split('/');
            string FileName = NameOfFileSplit[NameOfFileSplit.Length - 1];

            for (int Index = 0; Index < GUIZipObjects.Count; Index++)
            {
                var ThisParser = GUIZipObjects[Index];
                for (int FileIndex = 0; FileIndex < ThisParser.SetOfZips.Count; FileIndex++)
                {
                    var ThisFile = ThisParser.SetOfZips[FileIndex];
                    if (!ThisFile.NameOfZip.Contains(FileName)) { continue; }

                    // Setup Path For ZipFile
                    string ZipFilePath = ThisFile.NameOfZip;
                    if (!ThisFile.NameOfZip.Contains("\\")) { ZipFilePath = ZipDownloadDir + ThisFile.NameOfZip; }

                    // Setup basic file info.
                    ThisFile.IndexOfFile = FileIndex;
                    ThisFile.SizeOfZip = e.TotalBytes;
                    ThisFile.SizeOfZipDownloaded = new FileInfo(ZipFilePath).Length;
                    ThisFile.OperationProgress = ((double)ThisFile.SizeOfZipDownloaded / (double)ThisFile.SizeOfZip) * 100.0;
                    ThisFile.OperationProgressValueOnly = ThisFile.OperationProgress.ToString("F0") + "%";

                    // Size and progress info strings.
                    string SizeNow = FileSizeHelpers.SizeSuffix(ThisFile.SizeOfZipDownloaded, 2);
                    string TotalSize = FileSizeHelpers.SizeSuffix(ThisFile.SizeOfZip, 2);
                    ThisFile.SizeOfZipString = FileSizeHelpers.SizeSuffix(e.TotalBytes, 2);
                    ThisFile.OperationProgressString = SizeNow + "/" + TotalSize;

                    // Log in size changes here.
                    long CombinedSizeZipSet = 0;
                    Parallel.ForEach(ThisParser.SetOfZips, (FileItem) => { CombinedSizeZipSet += FileItem.SizeOfZip; });
                    ThisParser.SizeLong = CombinedSizeZipSet;
                    ThisParser.TotalSize = FileSizeHelpers.SizeSuffix(CombinedSizeZipSet);

                    // This might be a mistake lol
                    long CombinedSizeAllSets = 0;
                    Parallel.ForEach(GUIZipObjects, (ZipSet) => { CombinedSizeAllSets += ZipSet.SizeLong; });
                    GUIHelpers.SizeOfZipLong = CombinedSizeAllSets;

                    // Check if done - Update UI
                    if (e.IsCompleted)
                    {
                        ThisFile.IsDownloaded = true;
                        ThisFile.DownloadFailed = false;
                    }

                    // Update parser info/size here.
                    if (!ThisFile.IsDownloaded) { GUIZipObjects[Index].StatusOfSet = FileName; }
                    if (ThisParser.StatusOfSet == "Waiting To Download") { ThisParser.StatusOfSet = FileName; }

                    // Set this item back.
                    GUIZipObjects[Index] = ThisParser;
                    GUIZipObjects[Index].SetOfZips[FileIndex] = ThisFile;

                    // Update the UI.
                    MainWindowView.Dispatcher.Invoke(() =>
                    {
                        var CurrentParser = GUIZipObjects[Index];
                        if (Index == MainWindowView.SelectedIndex)
                        {
                            MainWindowView.ParserSizeString.Text = "Total Size: " + CurrentParser.TotalSize;
                            MainWindowView.ParserFilesLeftString.Text = CurrentParser.NumberOfZipsLeft + " Files Left";
                            if (e.PercentDone % 2 == 0) { MainWindowView.CurrentParserInfo.Items.Refresh(); }
                        }
                    });

                    // This might help a bit.
                    break;
                }

            }
        }
    }
}
