﻿using IVSLogConverter_GUI.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IVSLogConverter_GUI.SettingsView
{
    /// <summary>
    /// Interaction logic for SettingsPageView.xaml
    /// </summary>
    public partial class SettingsPageView : UserControl
    {
        private static List<PropertyInfo> ListOfSettingsProps = new List<PropertyInfo>();
        private static List<string> SettingNames = new List<string>
        {
            "Working Directory",
            "Files Per Parser",
            "GUI Refresh Time",
            "Max Parallel Threads",
            "Remove Old Zips"
        };

        public SettingsPageView()
        {
            InitializeComponent();

            // Gets setting names.
            GetPropsOfSettings();

            // Populates the new setting grid.
            CreateSettingPages();
        }

        private void GetPropsOfSettings()
        {
            var SettingsProps = typeof(Settings).GetProperties();
            foreach (var SettingObj in SettingsProps)
            {
                if (!SettingObj.Name.StartsWith("Setting_")) { continue; }
                ListOfSettingsProps.Add(SettingObj);
            }
        }

        private void CreateSettingPages()
        {
            int HeightOfObject = 35;
            SettingsContainerGrid.RowDefinitions.Clear();
            SettingsContainerGrid.Children.Clear();

            // Loop names of the items and make new row defs/values here.
            for (int Index = 0; Index < SettingNames.Count; Index++)
            {
                // Make a new rowdef object to setup the contents of it.
                RowDefinition NextRow = new RowDefinition();
                NextRow.Height = new GridLength(HeightOfObject, GridUnitType.Pixel);

                // Add new row def to the list here.
                SettingsContainerGrid.RowDefinitions.Add(NextRow);

                // Make the settings item here.
                string SettingInternalName = ListOfSettingsProps[Index].Name;
                var SettingItem = new SettingObjectView(SettingNames[Index], SettingInternalName, Index);
                SettingItem.SetValue(Grid.RowProperty, Index);

                // Add to list and update.
                SettingsContainerGrid.Children.Add(SettingItem);
                SettingsContainerGrid.UpdateLayout();
            }

            // Add ending row.
            RowDefinition FinalRow = new RowDefinition();
            FinalRow.Height = new GridLength(1, GridUnitType.Star);
            SettingsContainerGrid.RowDefinitions.Add(FinalRow);

        }

        private void CloseAndSaveSettings(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }
    }
}
