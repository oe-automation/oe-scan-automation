﻿using IVSLogConverter_GUI.LogicHelpers;
using IVSLogConverter_GUI.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using static IVSLogConverter_GUI.GlobalObjects;

namespace IVSLogConverter_GUI.SettingsView
{
    /// <summary>
    /// Interaction logic for SettingObjectView.xaml
    /// </summary>
    public partial class SettingObjectView : UserControl
    {
        private string SettingName_Internal;
        private int SettingIndex;
        public SettingObjectView(string DisplaySettingName, string SettingToFill, int IndexOfSetting)
        {
            InitializeComponent();

            // Name of setting.
            this.SettingNameTextBlock.Text = DisplaySettingName;
            this.SettingName_Internal = SettingToFill;

            // Index of setting
            SettingIndex = IndexOfSetting;

            // Set TextBox default value.  
            SettingSetBox.Text = Settings.Default[SettingName_Internal].ToString();
        }

        private void SettingSetBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var sendBox = (TextBox)sender;
            string BoxContent = sendBox.Text;

            switch (SettingIndex)
            {
                case 0:
                    BoxContent = BoxContent.Trim();
                    LocalAppDataDir = BoxContent;

                    Settings.Default[SettingName_Internal] = BoxContent;
                    Settings.Default.Save();

                    return;

                case 1:
                case 2:
                case 3:
                    if (int.TryParse(BoxContent.Trim(), out int SetThis)) 
                    {
                        if (SettingIndex == 1) { SplitFileCount = SetThis; }
                        if (SettingIndex == 2) { TimeToWaitForUpdate = SetThis; }
                        if (SettingIndex == 3) 
                        {
                            if (SetThis <= 0) { SetThis = 10; }
                            MaxParallelThreads = SetThis;
                            Options = new ParallelOptions() { MaxDegreeOfParallelism = MaxParallelThreads };
                        }
                        Settings.Default[SettingName_Internal] = SetThis;

                    }
                    else
                    {
                        if (SettingIndex == 1) { SetThis = 20; SplitFileCount = SetThis; }
                        if (SettingIndex == 2) { SetThis = 5000; TimeToWaitForUpdate = SetThis; }
                        if (SettingIndex == 3)
                        {
                            SetThis = 10;
                            MaxParallelThreads = SetThis;
                            Options = new ParallelOptions() { MaxDegreeOfParallelism = MaxParallelThreads };
                        }
                        Settings.Default[SettingName_Internal] = SetThis;
                    }

                    Settings.Default.Save();
                    return;

                case 4:
                    if (bool.TryParse(BoxContent.Trim(), out bool RemoveOldFiles)) { Settings.Default[SettingName_Internal] = RemoveOldFiles; }
                    else { Settings.Default[SettingName_Internal] = false; }
                    
                    Settings.Default.Save();
                    return;
            }
        }
    }
}
