﻿using IVSLogConverter_GUI.LogicalHelpers;
using IVSLogConverter_GUI.LogicHelpers;
using IVSLogConverter_GUI.Objects;
using IVSLogConverter_GUI.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using static IVSLogConverter_GUI.GlobalObjects;

namespace IVSLogConverter_GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            // Init global constants here.
            SetupGlobalObjects();

            // Init MainWindow
            InitializeComponent();

            // Setup the mainwindow GUI Helpers.
            GUIHelpers = new GUIValueHelpers(this);
        }

        private async void PopulateZipLists_Button(object sender, RoutedEventArgs e)
        {
            // Turn Off Buttons.
            GetZipListsButton.Content = "Finding Zips Now...";
            GetZipListsButton.IsEnabled = false;

            await Task.Run(() =>
            {
                // Clearout on rerun.
                GUIHelpers.TotalZipCount = 0;
                GUIHelpers.AllParsersCount = 0;
                GUIHelpers.SizeOfZipLong = 0;

                Dispatcher.Invoke(() => { AllParsersListView.ItemsSource = null; });

                // Call the logic setup now. Makes our objects and such.
                MainAWSPuller = new AWSFilePuller(MainLogger);

                // Setup the actual parsers here.
                AllZipFiles.SetupDownloaders(MainAWSPuller);

                // Startup the task for UI Updating.
                RefreshGUIInterval.Start();
            });

            // Reset Buttons.
            GetZipListsButton.Content = "Setup Parser Sets";
            GetZipListsButton.IsEnabled = true;
            PullDownZipsButton.IsEnabled = true;
        }

        /// <summary>
        /// Pull down all zip files found on the parsers.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void PullDownAllZipFiles_Button(object sender, RoutedEventArgs e)
        {
            // Turn of buttons.
            GetZipListsButton.IsEnabled = false;
            PullDownZipsButton.IsEnabled = false;
            PullDownZipsButton.Content = "Downloading Now...";

            // Clearout on rerun.
            GUIHelpers.SizeOfZipLong = 0;

            // Starts the parser pull down
            await Task.Run(() => AllZipFiles.StartDownloaders());

            // Reset buttons.
            GetZipListsButton.IsEnabled = true;
            PullDownZipsButton.IsEnabled = true;
            ExtractZipFilesButton.IsEnabled = true;
            PullDownZipsButton.Content = "Download All Zips";
        }

        /// <summary>
        /// Unzip files and setup their output Dirs for parsing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ExtractZipFilesAndConvert(object sender, RoutedEventArgs e)
        {
            // Turn off buttons.
            GetZipListsButton.IsEnabled = false;
            PullDownZipsButton.IsEnabled = false;
            ExtractZipFilesButton.IsEnabled = false;
            ExtractZipFilesButton.Content = "Extracting Zips...";

            await Task.Run(() =>
            {
                // Update all items to status of "Waiting to Decompress"
                AllZipFiles.SetupDecompressors();

                // Unzip here
                AllZipFiles.StartDecompressors();
            });

            // Reset Buttons.
            GetZipListsButton.IsEnabled = true;
            PullDownZipsButton.IsEnabled = true;
            ExtractZipFilesButton.IsEnabled = true;
            ExtractZipFilesButton.Content = "Extract/Parse Zips";
        }

        // Updates based on index changed for the main listview.
        public int SelectedIndex = -1;
        private void AllParsersListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedIndex = AllParsersListView.SelectedIndex;
            if (SelectedIndex != -1)
            {
                if (GUIZipObjects.Count < SelectedIndex) { return; }

                var CurrentParser = GUIZipObjects[SelectedIndex];
                var ListToSet = GUIZipObjects[SelectedIndex].SetOfZips;
                CurrentParserInfo.ItemsSource = ListToSet;

                CurrentParserInfo.Visibility = Visibility.Visible;
                ListViewBorder.Visibility = Visibility.Visible;

                ParserNameString.Text = "Parser Thread: " + CurrentParser.ParserName;
                ParserSizeString.Text = "Total Size: " + CurrentParser.TotalSize;
                // ParserFilesLeftString.Text = CurrentParser.NumberOfZipsLeft + " Files Left";

                CurrentParserInfo.Items.Refresh();
                if (CancelToken.IsCancellationRequested || RefreshGUIInterval.IsCompleted)
                {
                    AllParsersListView.Items.Refresh();
                }
            }
        }

        /// <summary>
        /// Show or hide settings pane on this program.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowSettingsToggle(object sender, RoutedEventArgs e)
        {
            if (SettingsViewMain.Visibility == Visibility.Hidden)
            { 
                SettingsViewMain.Visibility = Visibility.Visible;
                return; 
            }

            if (SettingsViewMain.Visibility == Visibility.Visible)
            {
                SettingsViewMain.Visibility = Visibility.Hidden;
                return;
            }
        }
    }
}
