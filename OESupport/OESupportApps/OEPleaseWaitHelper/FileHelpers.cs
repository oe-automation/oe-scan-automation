﻿using System;
using System.IO;

namespace OEProgressInvoker
{
    public class FileHelpers
    {
        // Name of our debug file.
        public string DebugFileName { get; set; }
        private bool UseInvoker { get; set; }

        private string OEOptionsDir = @"C:\Program Files (x86)\OESupport\VehicleOptions\CurrentVehicle";
        private string OptionsDebug = @"C:\Program Files (x86)\OESupport\OEDebugging\OptionsDebug";
        private string APICallDebug = @"C:\Program Files (x86)\OESupport\OEDebugging\APICallDebug";
        private string WinAutoDebug = @"C:\Program Files (x86)\OESupport\OEDebugging\WinAutomationDebug";
        private string MockAPIDebug = @"C:\Program Files (x86)\OESupport\OEDebugging\SpoofAPIDebug";
        private string InvokerDebug = @"C:\Program Files (x86)\OESupport\OEDebugging\AutomationInvokerDebug";

        public FileHelpers(bool UsingInvoker = false, string DebugFile = "")
        {
            // Bool that says if we're using the invoker or not.
            UseInvoker = UseInvoker;

            // Checks for output dirs and makes sure they are setup ok.
            CheckForDirs();

            // Check the name of the debug file passed in and see if the invoker is in use.
            if (UsingInvoker && DebugFile.Length != 0)
            {
                DebugFileName = DebugFile;
                if (!File.Exists(DebugFile))
                {
                    WriteAPILog("COULD NOT FIND INVOKER DEBUG FILE MAKING A NEW ONE.", "----");
                    SetupDebugFile();
                }
            }

            if (DebugFile.Length == 0)
            {
                // Set the debug File name since one wasnt passed in.
                DebugFileName = GetDebugFileName();

                // Check to see if weve done an OE Scan today or not.
                SetupDebugFile();
            }  
        }

        // Checks to see if debug locations/options locations exist.
        private void CheckForDirs()
        {
            if (!Directory.Exists(APICallDebug)) { Directory.CreateDirectory(APICallDebug); }
            if (!Directory.Exists(OEOptionsDir)) { Directory.CreateDirectory(OEOptionsDir); }
            if (!Directory.Exists(OptionsDebug)) { Directory.CreateDirectory(OptionsDebug); }
            if (!Directory.Exists(WinAutoDebug)) { Directory.CreateDirectory(WinAutoDebug); }
            if (!Directory.Exists(MockAPIDebug)) { Directory.CreateDirectory(MockAPIDebug); }
            if (!Directory.Exists(InvokerDebug)) { Directory.CreateDirectory(InvokerDebug); }
        }

        // Get the current date and format it for the name of our file.
        private string GetDebugFileName()
        {
            // Get DateTimeNow.
            var strTime = DateTime.Now.ToString("s");
            strTime = strTime.Replace("T", "_");
            strTime = strTime.Replace("-", "");

            // Replace the actual time with nothing since we need to call this file again.
            strTime = strTime.Split('_')[0];

            // Combine the date and prefix and return it.
            if (!UseInvoker) { strTime = "PleaseWaitDebug_" + strTime + ".txt"; }
            if (UseInvoker) { strTime = "OEInvokerDebug_" + strTime + ".txt"; }

            var fullName = APICallDebug + "\\" + strTime;

            return fullName;
        }

        // Setup the file and make a new one if needed.
        private void SetupDebugFile(bool initNew = false)
        {
            // Base timestamp message.
            string TimeString = "API DEBUGGING: " + DateTime.Now.ToString("s") + "\n";

            // Check for existing file. If not setup a new one with timestamp headding.
            if (!File.Exists(DebugFileName)) { File.WriteAllText(DebugFileName, TimeString); }
            else
            {
                // Setup a new file if we have the flag set.
                if (initNew) { File.AppendAllText(DebugFileName, TimeString); return; }

                // If the file exists, see if it has anything in it. 
                var sizeOfFile = File.ReadAllBytes(DebugFileName).Length;
                if (sizeOfFile > 0)
                {
                    File.AppendAllText(DebugFileName, "\n----------------------------------\n");
                    File.AppendAllText(DebugFileName, TimeString);

                    if (UseInvoker) { File.AppendAllText(DebugFileName, "USING OE INVOKER METHODS!!"); }
                }
            }
        }

        // Write API Call debug info out to here.
        public void WriteAPILog(string CallString, string RespString)
        {            
            // Get the current time for the logging info.
            string timeNow = DateTime.Now.ToString("T");
            string writeThis = "[" + timeNow + "] ::: [" + CallString + "]" + " ::: [" + RespString + "]\n";

            // Write the formatted logging info out.
            File.AppendAllText(DebugFileName, writeThis); 
        }
    }
}
