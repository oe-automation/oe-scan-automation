﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEProgressInvoker
{
    public class WaitHelperMain
    {
        public static void Main(string[] args)
        {
            if (args[0] != "MULTI_TEST") { RunAPIHelper(args); }
            if (args[0] == "MULTI_TEST")
            {
                if (Debugger.IsAttached)
                {
                    Console.WriteLine("Press Enter To Send API Call...");
                    Console.ReadLine();
                    Console.Clear();
                }

                var ListOfAllArgs = new List<string>()
                {                   
                    "INVOKER, -b Debug, -c ToggleDebugLogging, -i 0",
                    "INVOKER, -b Debug, -c ReturnDebugFileName",
                    "INVOKER, -b Timers, -c StartTimer, -i 0",
                    "INVOKER, -b UpdateStatus, -c IncreaseProgressCounter, -i 0, -t 00_20-21",
                    "INVOKER, -b UpdateStatus, -c IncreaseProgressCounter, -i 1, -t 01_32-13",
                    "INVOKER, -b ProgressBar, -c StartProgressBar, -i 2, -t 15",
                    "INVOKER, -b ProgressBar, -c TickProgressBar, -i 2, -t 1",
                    "INVOKER, -b ProgressBar, -c TickProgressBar, -i 2, -t 5",
                    "INVOKER, -b ProgressBar, -c TickProgressBar, -i 2, -t 10",
                    "INVOKER, -b ProgressBar, -c LoopingProgressBar, -i 2, -t 1_3",
                    "INVOKER, -b ProgressBar, -c LoopingProgressBar, -i 2, -t 2_3",
                    "INVOKER, -b ProgressBar, -c TickProgressBar, -i 2, -t 15",
                    "INVOKER, -b UpdateStatus, -c IncreaseProgressCounter, -i 2, -t 00_46-53",
                    "INVOKER, -b ProgressBar, -c StartProgressBar, -i 3, -t 15",
                    "INVOKER, -b ProgressBar, -c TickProgressBar, -i 3, -t 1",
                    "INVOKER, -b ProgressBar, -c TickProgressBar, -i 3, -t 5",
                    "INVOKER, -b ProgressBar, -c HideProgressBar, -i 3",
                    "INVOKER, -b ProgressBar, -c StartProgressBar, -i 3, -t 15",
                    "INVOKER, -b ProgressBar, -c TickProgressBar, -i 3, -t 10",
                    "INVOKER, -b Timers, -c StopTimer, -i 0",
                };

                foreach (var stringSet in ListOfAllArgs)
                {
                    string[] ArgSet = stringSet.Split(',').ToArray();
                    int ArgSetLen = ArgSet.Length;

                    int counter = 0;
                    string[] FinalArgs = new string[ArgSetLen];
                    foreach (var stringValue in ArgSet) { FinalArgs[counter] = stringValue + ","; counter++; }
                    
                    RunAPIHelper(FinalArgs);
                    System.Threading.Thread.Sleep(1000);
                }
            }

            if (Debugger.IsAttached) Console.ReadLine();
        }

        public static void RunAPIHelper(string[] args)
        {
            var allArgsClean = ReturnArgsCleaned(args, out bool InvokerInUse, out bool StopAll);

            // Create Arg values here.
            string ExCommand = "";
            string IndexOfTask = "";
            string ExtraParam = "";

            string FinalCommand = "";

            if (InvokerInUse)
            {
                string CommandType = allArgsClean[1].Replace("-b", "").Trim();
                if (allArgsClean.Count > 1) ExCommand = allArgsClean[2].Replace("-c", "").Trim();
                if (allArgsClean.Count > 2) IndexOfTask = allArgsClean[3].Replace("-i", "").Trim();
                if (allArgsClean.Count > 3) ExtraParam = allArgsClean[4].Replace("-t", "").Trim();

                FinalCommand = CommandType + "/" + ExCommand + "/";
                if (IndexOfTask != "") { FinalCommand += IndexOfTask + "/"; }
                if (ExtraParam != "") { FinalCommand += ExtraParam + "/"; }
            }

            if (!InvokerInUse)
            {
                if (!StopAll) { ExCommand = allArgsClean[0].Replace("_", " "); }
                if (StopAll) { ExCommand = "OFF"; }

                FinalCommand = ExCommand;
            }

            APIInvokerSetup APIHelper = new APIInvokerSetup(InvokerInUse, StopAll);
            APIHelper.SendAPIRequest(InvokerInUse, FinalCommand);
        }

        public static List<string> ReturnArgsCleaned(string[] Args, out bool UsingInvoker, out bool StopAllSpinners)
        {
            string AllArgsString = "";
            foreach (string arg in Args) { AllArgsString += arg + " "; }

            List<string> ArgsCleaned = AllArgsString.Split(',').ToList();

            if (ArgsCleaned.Contains("INVOKER")) { UsingInvoker = true; }
            else { UsingInvoker = false; }

            if (ArgsCleaned.Contains("STOPALL")) { StopAllSpinners = true; }
            else { StopAllSpinners = false; }

            return ArgsCleaned;
        }
    }
}
