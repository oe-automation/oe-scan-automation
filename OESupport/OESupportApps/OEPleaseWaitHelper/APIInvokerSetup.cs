﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OEProgressInvoker
{
    public class APIInvokerSetup
    {
        private string PleaseWaitBaseURL = @"http://localhost:15000/DrewTechDataSvc/PleaseWait/";
        private string OEInvokerBaseURL = @"http://localhost:57570/OEAutomationTasker/";

        private FileHelpers APICallDebugger;         // Debug File name/location helper for Please Wait

        private bool UsingInvoker;
        private bool StopAll;

        /// <summary>
        /// Init an instance of the API Helper for Please Wait.
        /// </summary>
        /// <param name="stopAll">Stops all PleaseWaitSpinners.</param>
        /// <param name="UsingInvoker">Sets up if we use the invoker methods or not.</param>
        public APIInvokerSetup(bool Invoker, bool stopAll = false)
        {
            // Invoker Bool Toggle
            UsingInvoker = Invoker; 

            // Check if debug file name is empty or not for the invoker.
            if (Invoker)
            {
                string LinkName = OEInvokerBaseURL + "/Debug/ReturnDebugFileName";
                string TempFileName = new WebClient().DownloadString(LinkName);

                string DebugFileName = TempFileName.Substring(2);
                DebugFileName = DebugFileName.Substring(0, DebugFileName.Length - 2);
                DebugFileName = DebugFileName.Replace("__", "\\");
                Console.WriteLine(DebugFileName);

                // Debug file helper.
                APICallDebugger = new FileHelpers(true, DebugFileName);
            }

            // If we're going for pleasewait helpers.
            if (!Invoker)
            {
                // Debug file helper.
                APICallDebugger = new FileHelpers(false);

                // Set Stop All and send off if we're not stopping all.
                StopAll = stopAll;
            }
        }

        public string SendAPIRequest(bool UsingInvoker, string URLorMessage)
        {
            string ResultString = "";

            if (UsingInvoker)
            {
                string FinalURL = OEInvokerBaseURL + URLorMessage;

                Console.WriteLine("Debug File: " + APICallDebugger.DebugFileName);
                Console.WriteLine("Sending: " + FinalURL);

                ResultString = WebCallInvoker(FinalURL);
            }

            if (!UsingInvoker)
            {
                string FinalURL = PleaseWaitBaseURL + URLorMessage;

                Console.WriteLine("Debug File: " + APICallDebugger.DebugFileName);
                Console.WriteLine("Sending: " + FinalURL);

                ResultString = PleaseWaitAPIHelper(URLorMessage); 
            }

            return ResultString;
        }

        // PleaseWait API Calling methods
        private string PleaseWaitAPIHelper(string PleaseWaitMessage)
        {
            // Store our API Call here
            string APICallURL = PleaseWaitBaseURL + PleaseWaitMessage;

            // Basic debug info on console.
            Console.WriteLine("Debug File: " + APICallDebugger.DebugFileName);
            Console.WriteLine("Sending: " + APICallURL);

            if (StopAll) { for (int Counter = 0; Counter < 8; Counter++) { WebCallInvoker(APICallURL); } }

            return WebCallInvoker(APICallURL);
        }

        /// <summary>
        /// Web Request Heavy lifting method.
        /// </summary>
        /// <param name="APICallURL">String value of the api call used to make the request.</param>
        /// <returns></returns>
        private string WebCallInvoker(string APICallURL)
        { 
            // Webrequest object that opens a connection to the base API URL.
            HttpWebRequest getPostHelper = (HttpWebRequest)WebRequest.Create(APICallURL);
            getPostHelper.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            string APIResponseValue = "";
            try
            {
                // Get the response of the sent command. This all we need to do here really. 
                using (HttpWebResponse apiResponse = (HttpWebResponse)getPostHelper.GetResponse())
                using (Stream respStream = apiResponse.GetResponseStream())
                using (StreamReader respRead = new StreamReader(respStream)) { APIResponseValue = respRead.ReadToEnd(); }

                if (!StopAll) APICallDebugger.WriteAPILog(APICallURL, APIResponseValue);
            }

            catch (Exception e)
            {
                APIResponseValue = "CALL FAILED: " + e.Message;
                APICallDebugger.WriteAPILog(APICallURL, APIResponseValue);
            }

            Console.WriteLine("Response:" + APIResponseValue);
            return APIResponseValue;
        }
    }
}
