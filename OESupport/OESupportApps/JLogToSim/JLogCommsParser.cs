﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLogToSim
{
    public class JLogCommsParser
    {
        private JLogFileObject FileObject { get; set; }

        public JLogCommsParser(JLogFileObject InputObject)
        {
            FileObject = InputObject;
        }

        /// <summary>
        /// Pulls out the address pairs for the SDF Headder from the input file object.
        /// </summary>
        /// <param name="ModifyInputFile"></param>
        /// <returns></returns>
        public List<Tuple<string, string>> GetAddressPairs(bool ModifyInputFile = false, JLogFileObject InputFile = null)
        {
            // If we dont have a file set in the class or want to overrise it for some reason.
            // This is after the modify input since the input file would need to be specified if they pass a custom input.
            if (InputFile != null) { FileObject = InputFile; }

            // Return object and temp storage of file lines.
            var OutputAddrs = new List<Tuple<string, string>>();
            List<string> AllLines = FileObject.ContentsModifiedList;

            // Loop all the lines here.
            for (int LineCounter = 0; LineCounter < AllLines.Count; LineCounter++)
            {
                // Try catch since this can be a bit buggy here.
                try
                {
                    // Check for invalid lenght 
                    if (AllLines[LineCounter].Length < 3) { continue; }

                    // Get the current line address.
                    string CurrentAddr = AllLines[LineCounter].Substring(0, 3);
                    while (LineCounter < AllLines.Count)
                    {
                        // Pull the next line address value. If we have the same address move on. if not
                        // set that new address to what we wanna match. Increase the line counter each time.

                        LineCounter++;

                        // Make sure we're inside the right index.
                        if (LineCounter >= AllLines.Count) { break; }

                        // Check length of the next string object.
                        if (AllLines[LineCounter].Length < 3) { continue; }

                        // Make the next address line.
                        string NextLineAddr = AllLines[LineCounter].Substring(0, 3);

                        // If equal move on. If not add to the list.
                        if (NextLineAddr == CurrentAddr) { continue; }
                        else
                        {
                            // To avoid getting log lines that are conflicting (no resp in particular) convert these addresses into
                            // Hex/int values and then compare. The start address must ALWAYS be less than the response address. 
                            int StartHexConverted = int.Parse(CurrentAddr, System.Globalization.NumberStyles.HexNumber);
                            int EndHexConverted = int.Parse(NextLineAddr, System.Globalization.NumberStyles.HexNumber);

                            // See if we have a potentially bad second address.
                            // var InvalidSecondAddrs = new List<string> { "7DF", "7E0" };
                            // if (InvalidSecondAddrs.Contains(NextLineAddr)) { break; }

                            // Check for valid pairing.
                            if (StartHexConverted >= EndHexConverted) { break; }

                            // If were good, add a new string tuple to the output address list.
                            var NextPair = new Tuple<string, string>(CurrentAddr, NextLineAddr);
                            OutputAddrs.Add(NextPair);

                            // This breaks so we move onto the next line sets.
                            break;
                        }
                    }
                }

                catch (Exception loopEx)
                {
                    // Print out info about the fialure and the line it happened on if we're inside the index of line objects.
                    if (LineCounter >= AllLines.Count) { Console.WriteLine("COUNTER WAS OUT OF INDEX BOUNDS!!!"); }
                    else { Console.WriteLine("LINE: " + AllLines[LineCounter]); }

                    Console.WriteLine("EX IN LOOP FOR ADDRS: " + loopEx.Message);
                    Console.WriteLine("STACK: " + loopEx.StackTrace);

                    throw new Exception("ERROR IN ADDR LOOPING!! -> " + loopEx.StackTrace);
                }
            }

            // Return list of tuples here. Clean out the dupes first.
            OutputAddrs = OutputAddrs.Distinct().ToList();

            // Set file object values too if we want.
            if (ModifyInputFile)
            {
                FileObject.AddressPairs = OutputAddrs;
                FileObject.WriteAddressPairs();
            }

            // Inform of count found in this file.
            Console.WriteLine("TOTAL COMMS ADDRESS PAIRS: " + OutputAddrs.Count);
            return OutputAddrs;
        }

        /// <summary>
        /// Gets the list of all comms in the input file based on the previously generated address list.
        /// When some pairs of comms arent matched, they just get ignored and the CAN ID is left in place.
        /// </summary>
        /// <param name="ModifyInput"></param>
        /// <param name="InputFile"></param>
        /// <returns></returns>
        public List<List<string>> GetCommsForAddressPair(bool ModifyInput = false, JLogFileObject InputFile = null)
        {
            // Return object. 
            var AllPairedComms = new List<List<string>>();

            // Pass in an optional file object here.
            if (InputFile != null) { FileObject = InputFile; }

            // Loop all the comm addrs and find the matches in the file.
            var AllLines = FileObject.ContentsModifiedList;

            // Loops the address objects in the specified file object.
            foreach (var AddrPair in FileObject.AddressPairs)
            {
                // Temp assignment for easier access.
                string Addr1 = AddrPair.Item1;
                string Addr2 = AddrPair.Item2;

                // Holds the items for the current pairing. 
                List<string> NextAddrPairs = new List<string>();

                for (int LineCounter = 0; LineCounter < AllLines.Count; LineCounter++)
                {
                    string CurrentLine = AllLines[LineCounter];
                    if (CurrentLine.StartsWith(Addr1))
                    {
                        // We found a match here for the first address. Look at the response and loop while the value
                        // of the next line starts with the resp address. 

                        // List of new items to hold.
                        List<string> CurrentAddressComms = new List<string>();

                        // Index track for the loop.
                        int NextLineCounter = LineCounter + 1;
                        while (LineCounter < AllLines.Count)
                        {
                            // Different Counter now.
                            if (NextLineCounter >= AllLines.Count) { break; }

                            // New counter in use.
                            string NextLine = AllLines[NextLineCounter];

                            // Check if we have a good string.
                            if (NextLine.StartsWith(Addr2)) { CurrentAddressComms.Add(NextLine.Trim()); }
                            else 
                            {
                                // If we dont, then we need to format the existing strings accordingly.
                                if (CurrentAddressComms.Count == 0) { break; }

                                // More than one line we need to loop thru and append to the ConcatThis string.
                                // Once added together we close it off with a semicolon and add to the list.
                                foreach (var StringObj in CurrentAddressComms)
                                {
                                    NextAddrPairs.Add(CurrentLine + " : " + StringObj + ";");
                                }

                                break;
                            }

                            NextLineCounter++;
                        }

                        // Add the list of new comms to the final list if its not empty.
                        // Also need to make sure we dont dupe up on comms somehow.
                        if (NextAddrPairs.Count > 0) { AllPairedComms.Add(NextAddrPairs.Distinct().ToList()); }
                    }
                }
            }

            // New clean list object.
            var CleanedLists = new List<List<string>>();

            // Combine the lists into uniform lists so we dont write out 600+ sets of comms for the same address. 
            foreach (var AddrPair in FileObject.AddressPairs)
            {
                string FirstAddr = AddrPair.Item1;

                // Temp new list with single comm items in it.
                List<string> NewListSet = new List<string>();
                foreach (var ListSet in AllPairedComms)
                {
                    // If the list isnt valid based on comm items ignore it.
                    if (!ListSet[0].StartsWith(FirstAddr)) { continue; }
                    foreach (var LineObj in ListSet) { NewListSet.Add(LineObj); }
                }

                // If somehow we now have a zero item list just move on.
                // Otherwise add to the list of new lists. Oh and take out dupes.
                if (NewListSet.Count != 0) { CleanedLists.Add(NewListSet.Distinct().ToList()); }
            }

            // Return the new comms pairs and modify the comms object if needed.
            if (ModifyInput)
            {
                FileObject.AllCommsPaired = new List<List<string>>();
                FileObject.AllCommsPaired = CleanedLists;
            }

            // Inform user of what we did and return the cleaed sets of lists.
            Console.WriteLine("CONDENSED INTO: " + CleanedLists.Count + " PAIRED OFF LINE SETS");
            return CleanedLists;
        }
    }
}
