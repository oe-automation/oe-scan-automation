﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JLogToSim
{
    class Program
    {
        public static void Main(string[] args)
        {
            // Debug mode setup file so it's a 2017 F150.
            if (Debugger.IsAttached)
            {
                args = new string[1];
                args[0] = @"A:\Data Dump\SDF Files\SimTech SDFs\2017_F150_1FTEW1CP3JFE49559\2017_F150_CombinedRawLog.txt"; 
            }

            // Check to make sure a file was passed into here.
            string ParseThisFile = "";
            foreach (var argString in args)
            {
                Console.WriteLine("ARG: " + argString);

                string TempFileName = argString;
                if (!TempFileName.Contains(".txt")) { TempFileName += ".txt"; }
                if (File.Exists(TempFileName))
                {                
                    ParseThisFile = TempFileName;

                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;

                    break;
                }
            }

            if (ParseThisFile == "") 
            {
                string TryThis = string.Join("", args);
                if (!File.Exists(TryThis)) { throw new Exception("FAILED TO FIND A FILE TO PARSE OUT!"); }

                ParseThisFile = TryThis;
            }
            
            // Create a new file reader and pull out the file info here. 
            JLogFileParser LogFileParser = new JLogFileParser(ParseThisFile);
            LogFileParser.RunCommsRegexs();

            // At this point, all comms lines we want to keep are converted into a 7DF 
            // format. They are not paired yet. To do pairing, we loop the items in the list
            // of comms lines we generated above. Read the first three chars, store them.
            // If the chars of the next three lines are not the same, begin reading. 
            // So when we see 7E0, read the next line. See what those three bytes are. 
            // Since normally its 7E8, we keep reading lines while we see 7E8 is the first three.
            // Once 7E8 is not the first three, check the index of the loop used to find those lines.
            // The index of that loop when started is the first line number and the index when stopped
            // is the last line number. 

            // Call the looper now.

            // Make parser object here. 
            JLogCommsParser Parser = new JLogCommsParser(LogFileParser.SelectedFile);

            // This gets all address pairs in our comms and saves them.
            // Since using true as the bool value we also set the input file object (from before)
            // to hold these new values.
            Parser.GetAddressPairs(true);
            Parser.GetCommsForAddressPair(true);

            // Write the comms lines out now.
            LogFileParser.SelectedFile.WriteCommsLines();

            // Inform we done gucci
            Console.WriteLine("\nDONE! FILE WAS MADE OK!");

            // Debug hold and wait for close.
            if (Debugger.IsAttached) { Console.ReadLine(); }            
        }
    }
}
