﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace JLogToSim
{
    /// <summary>
    /// Does all the parsing operations and other useful things. 
    /// Used to run Regex setup and paring comms. 
    /// </summary>
    public class JLogFileParser
    {
        public JLogFileObject SelectedFile { get; set; }   // File object that will be passed around for the remainder of operations.

        // Regex strings used for comms pulling.
        // NEED TO MAKE SURE I REMOVE DOUBLE SPACES FROM THE INPUT LIST FIRST!!!
        // Read in contents. Remove "  " and "[" and "]".
        private string CommsRegex = @"\\__\s+([0-9a-fA-F]{2}\s+){5,}|\s{3}(([0-9a-fA-F]{2}\s+))+|(\[([0-9a-fA-F]{2})\]){8,}";
        private string RemoveSplit = @"\n\s{3}";                // Replace with " "
        private string CondenseByte = @"\\__ 00 00 0(\d) ";     // Replace with "$1"

        /// <summary>
        /// Constructor for file parser. Used to init the internal FileObject.
        /// </summary>
        /// <param name="InputFilePath">Path of file to load into this program.</param>
        public JLogFileParser(string InputFilePath)
        {
            // Make sure the file exists.
            if (!File.Exists(InputFilePath)) { throw new Exception("ERROR INPUT FILE DOES NOT EXIST!"); }

            // Setup our new file object. Then make sure we have contents to read in. (Size and Content)
            SelectedFile = new JLogFileObject(InputFilePath);
            if (SelectedFile.FileLength == 0) { throw new Exception("ERROR. INPUT FILE SIZE IS ZERO. THERE ARE NO COMMS TO BE PARSED"); }
        }

        /// <summary>
        /// Runs Regex Operations on the comms for the three main regexs.
        /// </summary>
        public void RunCommsRegexs()
        {
            // Runs the three regex sets.
            RunInitMatching();      // Grabs the initial comms lines out of the log.
            RunLineCondensing();    // Removes all the Newline splits between log lines.
            RunAddressSnips();      // Snips down the first bytes into a "7DF" format.

            Console.WriteLine("TOTAL COMMS LINES: " + SelectedFile.ContentsModifiedList.Count);
        }

        /// <summary>
        /// Used to pull out the initial lines of comms in a log file.
        /// </summary>
        private void RunInitMatching()
        {
            var AllMatches = Regex.Matches(SelectedFile.FileContentsString, CommsRegex);
            if (AllMatches.Count == 0) { throw new Exception("COULD NOT EXTRACT ANY COMMS FROM THIS FILE!"); }

            // Convert the matches into a new string and list item for the selectedFile object.
            SelectedFile.ContentsModifiedList = new List<string>();

            foreach (Match matchItem in AllMatches)
            {
                SelectedFile.ContentsModifiedList.Add(matchItem.Value.ToUpper());
                SelectedFile.ContentsModifiedString += matchItem.Value.ToUpper();
            }
        }

        /// <summary>
        /// Used to pull out the splits that divide comms up into multiple lines.
        /// </summary>
        private void RunLineCondensing()
        {
            // Replace newline breaks.
            var RemovedSplits = Regex.Replace(SelectedFile.ContentsModifiedString, RemoveSplit, " ");

            // Populate the contents list with new values.
            SelectedFile.ContentsModifiedList = new List<string>();
            foreach (var LinePart in RemovedSplits.Split('\n'))
            {
                string NextPart = LinePart.Replace("\r", String.Empty).Replace("  ", " ").Trim();
                SelectedFile.ContentsModifiedList.Add(NextPart.Replace("  ", " ") + "\n");
            }

            // Populate the contents string with new values.
            SelectedFile.ContentsModifiedString = "";
            foreach (var LineItem in SelectedFile.ContentsModifiedList) { SelectedFile.ContentsModifiedString += LineItem; }
        }

        /// <summary>
        /// Snips down the leading 00 00 07 XX into a "7DF" format.
        /// </summary>
        private void RunAddressSnips()
        {
            // Now run the replacing function.
            var ReplacedElevenBits = Regex.Replace(SelectedFile.ContentsModifiedString, CondenseByte, "$1");

            // Set the new string and list vals again.
            SelectedFile.ContentsModifiedList = new List<string>();
            SelectedFile.ContentsModifiedString = "";

            foreach (string LinePart in ReplacedElevenBits.Split('\n'))
            {
                // Ignore anything that didnt make the Regex matching.
                if (LinePart.Contains("\\__")) { continue; }

                SelectedFile.ContentsModifiedList.Add(LinePart);
                SelectedFile.ContentsModifiedString += LinePart.Trim() + "\n";
            }
        }
    }
}
